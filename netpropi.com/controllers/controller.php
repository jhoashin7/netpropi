<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require $conArr['admindestino'].'controllers/phpmailer/src/Exception.php';
require $conArr['admindestino'].'controllers/phpmailer/src/PHPMailer.php';
require $conArr['admindestino'].'controllers/phpmailer/src/SMTP.php';

require_once $conArr['admindestino'].'controllers/mandrill-api-php/src/Mandrill.php';

require_once($conArr['admindestino'].'controllers/stripe-php-master/init.php');

require_once($conArr['admindestino'].'controllers/dompdf/autoload.inc.php');

use Dompdf\Dompdf;

use Dompdf\Options;

$resp = false;
$data = array( 'msg' => 'No se enviaron correctamente tus datos. Intenta de nuevo.' );

$ip = getUserIP();
$goresp = (isset($_POST['g-recaptcha-response']) && ($_POST['accion'] == 1 || $_POST['accion'] == 3 || $_POST['accion'] == 5))?$_POST['g-recaptcha-response']:false;
$getvars = sanitize( $_POST );
unset( $_POST );
$accion = $getvars[ 'accion' ];
if($accion >= 6 && $accion != 9 ){
    $id_veris = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'idveruser' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veris[0] == $conArr['enc_string'] && is_numeric($id_veris[1])){
        $accion = $accion;        
    }else{
       $accion = 0; 
    }
}

if ( $accion == 1 ) {
    if(true){
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$conArr['google_sec']."&response=" . $goresp . "&remoteip=" . $_SERVER['REMOTE_ADDR']));       
        if( false){            
            $resp = false;
        }else{
            $contrasena = strip_tags( $mysqli->real_escape_string( $getvars[ 'contrasena' ] ) );
            $contrasena = simple_crypt( $conArr['enc_string'].'***'.utf8_decode($contrasena), 'e', $conArr['enc_string'] );            
            $email = strip_tags( $mysqli->real_escape_string( $getvars[ 'email' ] ) );	
            $consulta = "SELECT * FROM admins WHERE email = '" . $email . "' AND contrasena = '" . $contrasena . "' LIMIT 1";
            if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
                if ( $rows > 0 ) {
                    $user = mysqli_fetch_object( $result );
                    $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$user->id, 'e', $conArr['enc_string'] ), 'rol' => $user->rol, 'nombre' => utf8_encode( $user->nombre ) );
                    $resp = true;
                } else {
                    $data = array( 'msg' => 'Tus datos no concuerdan, verifica la información.' );
                }
                $result->close();
            } 
        }
    }
}
if ( $accion == 2 ) {
    $id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
        $us = $id_veri[1];        	
        $consulta = "SELECT * FROM admins WHERE id = " . $us;
        if ( $result = $mysqli->query( $consulta ) ) {
            $rows = $result->num_rows;
            if ( $rows > 0 ) {
                $user = mysqli_fetch_object( $result );
                $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$user->id, 'e', $conArr['enc_string'] ), 'rol' => $user->rol, 'nombre' => utf8_encode( $user->nombre ), 'mail' => $user->email, 'pass' => utf8_encode( $user->contrasena ), 'user' => $user->usuario );
                $resp = true;
            } else {
                $data = array( 'msg' => 'Tus datos no concuerdan, verifica la información.' );
            }
            $result->close();
        }
    }
}
if ( $accion == 3 ) {
    if($goresp){
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$conArr['google_sec']."&response=" . $goresp . "&remoteip=" . $_SERVER['REMOTE_ADDR']));       
        if($captcha->success == false){            
            $resp = false;
        }else{
            $mail = strip_tags( $mysqli->real_escape_string( $getvars[ 'mail' ] ) );            
            $consulta = "SELECT nombre, usuario, contrasena, email FROM admins WHERE email = '" . $mail . "' LIMIT 1";
            if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
                if ( $rows > 0 ) {
                    $user = mysqli_fetch_object( $result );
                    $contrasena = explode('***', simple_crypt( $user->contrasena, 'd', $conArr['enc_string'] ));
                    $extra_data = array('nombre' => utf8_encode( $user->nombre ), 'usuario' => $user->usuario, 'pass' => utf8_encode( $contrasena[1] ), 'urlsitio' => $conArr['base_url_sitio'], 'urladmin' => $conArr['base_url'] );
					$consulta2 = "SELECT subject, texto, plano FROM mails WHERE id = 1";
					if ( $result2 = $mysqli->query( $consulta2 ) ) {
						$mailin = mysqli_fetch_object( $result2 );
						$result2->close();
					}					
					email( $user->email, utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );					
                    $data = array( 'msg' => '<strong>'.utf8_encode( $user->nombre ).'</strong>, te hemos enviado un correo a la dirección <strong>'.$mail.'</strong>, con tus datos de acceso.');
                    $resp = true;
                } else {
                    $data = array( 'msg' => 'No encontramos esta dirección de E-mail (<strong>'.$mail.'</strong>) en nuestros registros.' );
                }
                $result->close();
            }            
        }
    }	
}
if ( $accion == 4 ) {
	date_default_timezone_set('America/Bogota'); 
	$id_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_user' ] ) );
	$id_propiedad = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_propiedad' ] ) );
	if($id_user != ''){
		$id_veris = explode('***', simple_crypt( $id_user, 'd', $conArr['enc_string'] ));
		if($id_veris[0] == $conArr['enc_string'] && is_numeric($id_veris[1])){
			$id_user = $id_veris[1];        
		}else{
			$id_user = 0;			
		}		 
	}else{
		$id_user = 0;		
	}
	$fecha = date("Y-m-d H:i:s", strtotime('-2 minutes'));
	$consulta = "SELECT id, creado FROM ses_user WHERE id_user = " . $id_user . " AND id_propiedad = ".$id_propiedad." AND fecha >= '".$fecha."' AND ip = '".$ip."' LIMIT 1";
	if ( $result = $mysqli->query( $consulta ) ) {
        $rows = $result->num_rows;
        if ( $rows > 0 ) {
            $datos = mysqli_fetch_object( $result );
            $tiempo = strtotime("now") - strtotime($datos->creado);
            $consulta2 = "UPDATE ses_user SET tiempo = ".$tiempo.", fecha = '".date("Y-m-d H:i:s")."' WHERE id = " . $datos->id;
            if ( $result2 = $mysqli->query( $consulta2 ) ) {
                $data = array( 'fecha' => $datos->creado );
                $resp = true;
            }
        }else{
            $consulta2 = "INSERT INTO ses_user (id_user, id_propiedad, ip, tiempo, fecha, creado) VALUES (".$id_user.", ".$id_propiedad.", '".$ip."', 0, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
            if ( $result2 = $mysqli->query( $consulta2 ) ) {
                $data = array( 'fecha' => date("Y-m-d H:i:s") );
                $resp = true;
            }
        }
        $result->close();
    }
}
if ( $accion == 5 ) {
    if($goresp){
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$conArr['google_sec']."&response=" . $goresp . "&remoteip=" . $_SERVER['REMOTE_ADDR']));       
        if($captcha->success == false){                       
            $resp = false;
        }else{
			$editor = $id_veri[1];
			$getvars[ 'editor' ] = $editor;
			$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );			
			$nombre = strip_tags( $mysqli->real_escape_string( $getvars[ 'nombre' ] ) );
            $apellido = strip_tags( $mysqli->real_escape_string( $getvars[ 'apellido' ] ) );
			$email = strip_tags( $mysqli->real_escape_string( $getvars[ 'email' ] ) );
			$celular = ($getvars[ 'celular' ] != '')?strip_tags( $mysqli->real_escape_string('('.$getvars[ 'phoneCode' ].') '. $getvars[ 'celular' ] ) ):0;
			$pais = strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) );
			$tipo = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo' ] ) ); 
			$codigo = strip_tags( $mysqli->real_escape_string( $getvars[ 'codigo' ] ) );
			$usuario = strip_tags( $mysqli->real_escape_string( $getvars[ 'usuario' ] ) ); 
			$contrasena = strip_tags( $mysqli->real_escape_string( $getvars[ 'contrasena' ] ) );
            $contrasenaConfirm = strip_tags( $mysqli->real_escape_string( $getvars[ 'contrasenaConfirm' ] ) );
            // $checkTerminos = strip_tags( $mysqli->real_escape_string( $getvars[ 'checkTerminos' ] ) );
            // $checkPoliticas = strip_tags( $mysqli->real_escape_string( $getvars[ 'checkPoliticas' ] ) );
			if($id != 0){
				$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ), 'd', $conArr['enc_string'] ));
    			if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
					$consulta = "SELECT id FROM admins WHERE email = '" . $email . "' AND id != " . $id_veri[1] . " LIMIT 1";
					if ( $result = $mysqli->query( $consulta ) ) {
						$rows = $result->num_rows;
						$result->close();
						if($rows > 0){
							$data = array( 'id' => $id, 'msg' => 'El correo electrónico <strong>' . $email . '</strong> ya está siendo usado, debes usar otra dirección única de E-mail.' );
							$resp = false;
						}else{
							$consulta2 = "SELECT id, relacion FROM admins WHERE id = " . $id_veri[1];
							if ( $result2 = $mysqli->query( $consulta2 ) ) {
								$user = mysqli_fetch_object( $result2 );
								$result2->close();
								$consulta2 = "UPDATE usuarios SET nombre = '".mb_convert_encoding($nombre,'HTML-ENTITIES','utf-8')."', apellido = '".mb_convert_encoding($apellido,'HTML-ENTITIES','utf-8')."', email = '".$email."', pais = ".$pais.", celular = ".$celular.", tipo = ".$tipo.", codigo = '".$codigo."', editor = ".$id_veri[1]." WHERE id = " . $user->relacion;
								if ( $result2 = $mysqli->query( $consulta2 ) ) {
									$contrasenas = simple_crypt( $conArr['enc_string'].'***'.$contrasena, 'e', $conArr['enc_string'] );
									$consulta2 = "UPDATE admins SET usuario = '".$usuario."', contrasena = '".$contrasenas."' WHERE id = " . $id_veri[1];
									if ( $result2 = $mysqli->query( $consulta2 ) ) {
										$data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$id_veri[1], 'e', $conArr['enc_string'] ), 'msg' => 'Los datos han sido actualizados.' );
										$resp = true;
									}
								}								
							}
						}
					}
				}
			}else{
				$consulta = "SELECT id FROM admins WHERE email = '" . $email . "' LIMIT 1";
				if ( $result = $mysqli->query( $consulta ) ) {
					$rows = $result->num_rows;
					$result->close();
					if($rows > 0){
						$data = array( 'id' => $id, 'msg' => 'El correo electrónico <strong>' . $email . '</strong> ya está siendo usado, debes usar otra dirección única de E-mail.' );
						$resp = false;
					}else{		
                        if($contrasena === $contrasenaConfirm){
                            $consulta2 = "INSERT INTO usuarios (nombre, apellido, email, pais, celular, tipo, creado) VALUES('".mb_convert_encoding($nombre,'HTML-ENTITIES','utf-8')."', '".mb_convert_encoding($apellido,'HTML-ENTITIES','utf-8')."', '".$email."', ".$pais.", '".$celular."', ".$tipo.", NOW())";
                            if ( $result2 = $mysqli->query( $consulta2 ) ) {
                                $iden = $mysqli->insert_id;							
                                $contrasenas = simple_crypt( $conArr['enc_string'].'***'.$contrasena, 'e', $conArr['enc_string'] );
                                $consulta2 = "UPDATE admins SET usuario = '".$email."', contrasena = '".$contrasenas."' WHERE rol = 5 AND relacion = " . $iden;
                                if ( $result2 = $mysqli->query( $consulta2 ) ) {
                                    $consulta2 = "SELECT id FROM admins WHERE relacion = " . $iden . " AND rol = 5 LIMIT 1";
                                    if ( $result2 = $mysqli->query( $consulta2 ) ) {
                                        $user = mysqli_fetch_object( $result2 );
                                        $result2->close();
                                        $iden2 = $user->id;
                                        $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$iden2, 'e', $conArr['enc_string'] ), 'msg' => '¡Hola, te damos la bienvenida a NetPropi!' );
                                        $resp = true;							
                                        $extra_data = array('nombre' => $nombre, 'urlsitio' => $conArr['base_url_sitio'], 'urladmin' => $conArr['base_url'] );
                                        $consulta2 = "SELECT subject, texto, plano FROM mails WHERE id = 2";
                                        if ( $result2 = $mysqli->query( $consulta2 ) ) {
                                            $mailin = mysqli_fetch_object( $result2 );
                                            $result2->close();
                                        }					
                                        email( $email, utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );									
                                    }							
                                }						
                            }
                        }else{
                            $data = array( 'msg' => 'Las contraseñas no coinciden.' );
                            $resp = false; 
                        }
					}
				}
			}
        }
    }
}
if ( $accion == 6 ) {
    $id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'editor' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
        $editor = $id_veri[1];
        $getvars[ 'editor' ] = $editor;
        $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
        $db = strip_tags( $mysqli->real_escape_string( $getvars[ 'db' ] ) );
        $columns = array();
        $puede = true;
        $consulta = "SHOW COLUMNS FROM ".$db;
        if ( $result = $mysqli->query( $consulta ) ) {
            while($row = $result->fetch_assoc()){
                array_push($columns, $row['Field'].','.$row['Type']);			
            }
            $result->close();
        }
        if($db == 'admins'){
            
            $contrasena = simple_crypt( $conArr['enc_string'].'***'.utf8_decode(strip_tags( $mysqli->real_escape_string( $getvars[ 'contrasena' ] ) )), 'e', $conArr['enc_string'] );
            
            $consulta = ($id != 0)?"SELECT id FROM admins WHERE usuario = '" . $getvars['email'] . "' AND contrasena = '" . $contrasena . "' AND id != ".$id." LIMIT 1":"SELECT id FROM admins WHERE usuario = '" . $getvars['usuario'] . "' AND contrasena = '" . $contrasena . "' LIMIT 1";
            if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
                if ( $rows > 0 ) {
                    $puede = false;
                    $data = array( 'msg' => 'Los datos de acceso (usuario y/o contraseña), ya están siendo utilizados por otro usuario.' );
                    $resp = false;
                }
                $result->close();
            }
        }	
        if($puede){
            if($id != 0){
                $setval = array();
                foreach ($columns as &$valor) {
                    $valor = explode(',', $valor);
                    if($valor[0] != "id"){
                        if(array_key_exists($valor[0], $getvars)){
                            $inval = $valor[0]." = "; 
                            $invalor = (is_array($getvars[$valor[0]]))?implode(',', $getvars[$valor[0]]):strip_tags( $mysqli->real_escape_string( str_replace("'", "´", $getvars[$valor[0]]) )); 
                            $invalor = (preg_match('!!u', $invalor))?mb_convert_encoding($invalor,'HTML-ENTITIES','utf-8'):$invalor;
                            if(substr( $invalor, 0, 17 ) === '**esSummertexto**'){
                                $invalor = str_replace(array('**esSummertexto**', '*{*', '*}*'), array('','<','>'), $invalor);
                            }
                            if($db == 'admins' && $valor[0] == 'contrasena'){
                                $invalor = simple_crypt( $conArr['enc_string'].'***'.$invalor, 'e', $conArr['enc_string'] );
                            }
                            
                            $inval .= (strpos($valor[1], 'int'))?$invalor:"'".$invalor."'";
                            array_push($setval, $inval);
                        }
                    }			
                }
                unset($valor);
                $consulta = "UPDATE ".$db." SET ".implode(',', $setval)." WHERE id = " . $id;
                if ( $result = $mysqli->query( $consulta ) ) {
                    $data = array( 'id' => $id, 'msg' => 'Los datos han sido actualizados.' );
                    $resp = true;
                }else{
                    $data = array( 'msg' => $mysqli->error );
                }
            }else{
                $setval = array();
                $setcol = array();
                $creado = false;
                foreach ($columns as &$valor) {
                    $valor = explode(',', $valor);
                    if($valor[0] != "id"){
                        if(array_key_exists($valor[0], $getvars)){
                            array_push($setcol, $valor[0]);									
                            $invalor = (is_array($getvars[$valor[0]]))?implode(',', $getvars[$valor[0]]):strip_tags( $mysqli->real_escape_string( str_replace("'", "´", $getvars[$valor[0]]) )); 
                            $invalor = (preg_match('!!u', $invalor))?mb_convert_encoding($invalor,'HTML-ENTITIES','utf-8'):$invalor;
                            if(substr( $invalor, 0, 17 ) === '**esSummertexto**'){
                                $invalor = str_replace(array('**esSummertexto**', '*{*', '*}*'), array('','<','>'), $invalor);
                            }
                            if($db == 'admins' && $valor[0] == 'contrasena'){
                                $invalor = simple_crypt( $conArr['enc_string'].'***'.$invalor, 'e', $conArr['enc_string'] );
                            }
                            $invalor = (strpos($valor[1], 'int'))?$invalor:"'".$invalor."'";
                            array_push($setval, $invalor);
                        }
                    }
                    if($valor[0] == "creado"){
                        $creado = true;
                    }
                }
                unset($valor);
                $creadofi = ($creado)?array(",creado", ",NOW()"):array("","");
                $consulta = "INSERT INTO ".$db." (".implode(',', $setcol).$creadofi[0].") VALUES (".implode(',', $setval).$creadofi[1].")";
                if ( $result = $mysqli->query( $consulta ) ) {
                    $iden = $mysqli->insert_id;
                    $data = array( 'id' => $iden, 'msg' => 'Los datos han sido enviados.' );
                    $resp = true;
                }else{
                    $data = array( 'msg' => $mysqli->error );
                }
            }
        }
    }
}
if ( $accion == 7 ) {    
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
    $db = strip_tags( $mysqli->real_escape_string( $getvars[ 'db' ] ) );
    $consulta = "SELECT * FROM ".$db." WHERE id = " . $id;
    if ( $result = $mysqli->query( $consulta ) ) {		
        $rows = $result->num_rows;
        if ( $rows > 0 ) {
            $datos = mysqli_fetch_object( $result );
            if($db == 'admins'){
                $contrasena = explode('***', simple_crypt( $datos->contrasena, 'd', $conArr['enc_string'] ));
                $datos->contrasena = $contrasena[1];
            }
            utf8_encode_deep($datos);
            $data = $datos;
            $resp = true;
        } else {
            $data = array( 'msg' => 'No encontramos estos datos en nuestros registros.' );
        }
        $result->close();
    }    	
}
if ( $accion == 8 ) { 
	date_default_timezone_set('America/Bogota');
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
    $id_user = $id_veris[1];
	$puede = false;
	$consulta = "SELECT id FROM plan WHERE id_user = ".$id_user;
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$puede = ($rows == 0)?true:false;
		$result->close();
	}
	if($puede){
		$consulta = "SELECT * FROM planes WHERE id = ".$id;	
		if ( $result = $mysqli->query( $consulta ) ) {
			$nplan = mysqli_fetch_object( $result );    
			$result->close();
			$consulta = "INSERT INTO pagos (id_pago, id_pasarela, tipo_pago, referencia, nombre_pago, tipo, valor, id_user, estado, editor, creado) VALUES (".$nplan->id.", 'NA', 0, '".$nplan->prefijo."', '".$nplan->nombre."', 1, '".$nplan->valor."', ".$id_user.", 1, ".$id_user.", NOW())";
			if ( $result = $mysqli->query( $consulta ) ) {
				$iden = $mysqli->insert_id;
				$dias = $nplan->tiempo;
				$fin = date("Y-m-d", strtotime("+".$dias." days"));
				$consulta = "INSERT INTO plan (id_user, id_plan, id_pago, propiedades, dias, fin, renovadas, estado, editor, creado) VALUES (".$id_user.", ".$nplan->id.", ".$iden.", ".$nplan->maximo.", ".$dias.", '".$fin."', '', 1, ".$id_user.", NOW())";
				if ( $result = $mysqli->query( $consulta ) ) {
					$iden = $mysqli->insert_id;
					$valores = array();
					for($i = 0; $i < $nplan->maximo; $i++){
						array_push($valores, "(".$iden.", '".$fin."', ".$id_user.", ".$id_user.", NOW())");
					}
					$consulta = "INSERT INTO propiedades (plan_id, final, id_user, editor, creado) VALUES ". implode(',', $valores);
					if ( $result = $mysqli->query( $consulta ) ) {							
						$consulta = "INSERT INTO balance (nombre, valor, moneda, tipo, id_propiedad, editor, fecha, creado) VALUES ('".$nplan->nombre."', '".$nplan->valor."', 1, 2, 0, ".$id_user.", '".date('Y-m-d')."', NOW())";
						if ( $result = $mysqli->query( $consulta ) ) {
							$data = array( 'id' => $iden, 'msg' => 'Tu plan <strong>'.utf8_encode($nplan->nombre).'</strong>, con <strong>'.$nplan->maximo.'</strong> propiedad por <strong>'.$dias.'</strong> días, ha sido activado exitosamente.' );
							$resp = true;
						}
					}
				}
			}
		}
	}else{
		$data = array( 'msg' => 'Lo sentimos, ya tienes o haz tenido planes activados con nosotros.', 'tiene' => true );
		$resp = false;
	}
}
if ( $accion == 9 ) {
	$pais = strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) );
	$habeas = '';
	$consulta = "SELECT id, documento FROM documentos WHERE tipo = 0 AND FIND_IN_SET(".$pais.", paises) ORDER BY id DESC LIMIT 1";
	if ( $result = $mysqli->query( $consulta ) ) {
		$docu = mysqli_fetch_object( $result );
		$habeas = $docu->documento;
		$result->close();
	}
	$termino = '';
	$consulta = "SELECT id, documento FROM documentos WHERE tipo = 1 AND FIND_IN_SET(".$pais.", paises) ORDER BY id DESC LIMIT 1";
	if ( $result = $mysqli->query( $consulta ) ) {
		$docu = mysqli_fetch_object( $result );
		$termino = $docu->documento;
		$result->close();
	}	
	$data = array( 'habeas' => $habeas, 'terminos' => $termino );
	$resp = true;
}
if ( $accion == 10 ) { 
	date_default_timezone_set('America/Bogota');
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
    $id_user = $id_veris[1];
	$puede = false;
	$consulta = "SELECT id FROM fav_prop WHERE id_user = ".$id_user." AND id_propiedad = ".$id;
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$puede = ($rows == 0)?true:false;
		$result->close();
	}
	if($puede){
		$consulta = "INSERT INTO fav_prop (id_user, id_propiedad, fecha,creado) VALUES (".$id_user.", ".$id.", '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
		if ( $result = $mysqli->query( $consulta ) ) {
			$data = array( 'id' => $iden, 'msg' => 'La propiedad ha sido agregada a tus favoritos.' );
			$resp = true;
		}		
	}else{
		$data = array( 'msg' => 'Lo sentimos, ya tienes esta propiedad agregada a tus favoritos.', 'tiene' => true );
		$resp = false;
	}
}
if ( $accion == 11 ) { 
	date_default_timezone_set('America/Bogota');
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
    $id_responsable = $id_veris[1];
	$id_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_user' ] ) );
	$tipo = 3;
	$texto = strip_tags( $mysqli->real_escape_string( $getvars[ 'texto' ] ) );
	$comentario = strip_tags( $mysqli->real_escape_string( $getvars[ 'comentario' ] ) );
	$consulta = "INSERT INTO novedades (id_user, id_responsable, tipo, id_propiedad, texto, comentario, editor, creado) VALUES (".$id_user.", ".$id_responsable.", ".$tipo.", ".$id.", '".mb_convert_encoding($texto,'HTML-ENTITIES','utf-8')."',  '".mb_convert_encoding($comentario,'HTML-ENTITIES','utf-8')."', ".$id_responsable.", NOW())";
	$consulta2 = "INSERT INTO chat (emisor_id, receptor_id, mensaje, fecha) VALUES (".$id_responsable.", ".$id_user.", '".mb_convert_encoding($comentario,'HTML-ENTITIES','utf-8')."', NOW())";
    if ( $result = $mysqli->query( $consulta ) && $result2 = $mysqli->query( $consulta2 ) ) {
        $data = array( 'id' => $iden, 'msg' => 'El responsable de la propiedad sera notificado de tu interes.' );
        $resp = true;
    }	
}
//chat
if ( $accion == 12 ) { 
    $id_user = $id_veris[1];
    $consulta = "SELECT id, emisor_id, receptor_id, mensaje, fecha FROM chat WHERE receptor_id = $id_user AND leido = 0 ";
	if ( $result = $mysqli->query( $consulta ) ) {
            $data = array( 'pendientes' => $result->num_rows , 'msg' => 'Tienes mensajes pendientes.' );
            $resp = true;
            $result->close();
	}
}

if ( $accion == 13 ) { 
    $id_user = $id_veris[1];
    $consulta = "SELECT if(rec.id != $id_user, rec.id, emi.id) user_id, if(rec.id != $id_user, rec.nombre, emi.nombre) nombre,  SUM(if(c.leido = 0, if( c.receptor_id =  $id_user, 1,0),0)) noLeidos
                FROM admins emi 
                INNER JOIN chat c on c.emisor_id = emi.id
                INNER JOIN admins rec on c.receptor_id = rec.id
                WHERE emisor_id = $id_user OR receptor_id = $id_user
                GROUP BY user_id";
	if ( $result = $mysqli->query( $consulta ) ) {
            $data = mysqli_fetch_all($result);
            $resp = true;
            $result->close();
	}
}

if ( $accion == 14 ) { 
    $id_user = $id_veris[1];
    $id_user_chat = $getvars[ 'userChat' ];
    $consulta = "SELECT emisor_id, receptor_id, mensaje, fecha
                FROM chat
                WHERE emisor_id = $id_user AND receptor_id = $id_user_chat OR emisor_id = $id_user_chat AND receptor_id = $id_user";
    
    $consulta2 = "UPDATE chat SET leido = 1 WHERE receptor_id = $id_user AND emisor_id = $id_user_chat";
    $mysqli->query( $consulta2 );
	if ( $result = $mysqli->query( $consulta ) ) {
            $data = array('login' => $id_user, 'chat' => mysqli_fetch_all($result));
            $resp = true;
            $result->close();
	}
}

if ( $accion == 15 ) { 
    $id_user = $id_veris[1];
    $id_user_chat = $getvars[ 'userChat' ];
    $message = $getvars[ 'message' ];
    $consulta = "INSERT INTO chat (emisor_id, receptor_id, mensaje, fecha, leido) VALUES (".$id_user.", ".$id_user_chat.", '".mb_convert_encoding($message,'HTML-ENTITIES','utf-8')."', NOW(), 0)";
	if ( $result = $mysqli->query( $consulta ) ) {
            $data = true;
            $resp = true;
	}
}


if ( $accion == 'paises' ) { 
    $pais_id = $getvars[ 'pais_id' ];
    $consulta = "SELECT phonecode FROM countries WHERE id = $pais_id";
	if ( $result = $mysqli->query( $consulta ) ) {
        $row = mysqli_fetch_object($result);
        $data = $row->phonecode ;
        $resp = true;
	}
}
$respuesta = array( 'resp' => $resp, 'data' => $data );
$mysqli->close();
echo json_encode( $respuesta );

function getUserIP() {
	if ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $_SERVER ) && !empty( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) ) {
		if ( strpos( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ], ',' ) > 0 ) {
			$addr = explode( ",", $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] );
			return trim( $addr[ 0 ] );
		} else {
			return $_SERVER[ 'HTTP_X_FORWARDED_FOR' ];
		}
	} else {
		return $_SERVER[ 'REMOTE_ADDR' ];
	}
}

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function cleanTexto($string) {
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

   return strtolower(preg_replace('/[^A-Za-z0-9\_]/', '', $string)); // Removes special chars.
}

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}

function convertMethod($text){
    //Problem is that utf8_decode convert HTML chars for &bdquo; and other to ? or &nbsp; to \xA0. And you can not replace, that they are in some char bytes and you broke cyrillic (or other alphabet) chars.
    $problem_enc=array(
        'euro',
        'sbquo',
        'bdquo',
        'hellip',
        'dagger',
        'Dagger',
        'permil',
        'lsaquo',
        'lsquo',
        'rsquo',
        'ldquo',
        'rdquo',
        'bull',
        'ndash',
        'mdash',
        'trade',
        'rsquo',
        'brvbar',
        'copy',
        'laquo',
        'reg',
        'plusmn',
        'micro',
        'para',
        'middot',
        'raquo',
        'nbsp'
    );
    $text=mb_convert_encoding($text,'HTML-ENTITIES','UTF-8');
    $text=preg_replace('#(?<!\&ETH;)\&('.implode('|',$problem_enc).');#s','--amp{$1}',$text);
    $text=mb_convert_encoding($text,'UTF-8','HTML-ENTITIES');
    $text=utf8_decode($text);
    $text=mb_convert_encoding($text,'HTML-ENTITIES','UTF-8');
    $text=preg_replace('#\-\-amp\{([^\}]+)\}#su','&$1;',$text);
    $text=mb_convert_encoding($text,'UTF-8','HTML-ENTITIES');
    return $text;
}

function utf8_encode_deep(&$input) {
	if (is_string($input)) {
		$input = mb_convert_encoding($input,'UTF-8','HTML-ENTITIES');
	} else if (is_array($input)) {
		foreach ($input as &$value) {
			utf8_encode_deep($value);
		}
		
		unset($value);
	} else if (is_object($input)) {
		$vars = array_keys(get_object_vars($input));
		
		foreach ($vars as $var) {
			utf8_encode_deep($input->$var);
		}
	}
}

function deleteAll($str) {
    //It it's a file.
    if (is_file($str)) {
        //Attempt to delete it.
        return unlink($str);
    }
    //If it's a directory.
    elseif (is_dir($str)) {
        //Get a list of the files in this directory.
        $scan = glob(rtrim($str,'/').'/*');
        //Loop through the list of files.
        foreach($scan as $index=>$path) {
            //Call our recursive function.
            deleteAll($path);
        }
        //Remove the directory itself.
        return @rmdir($str);
    }
}

function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}

function email( $para, $titulo, $mensaje, $mensaje_txt, $extra_data, $mysqli, $copia = false ) {
	$to = $para;
	$subject = $titulo;
    
    $correo = false;
    $consulta = "SELECT tipo, nombre, email, servidor, puerto, contrasena FROM correo ORDER BY id ASC LIMIT 1";
    if ( $result = $mysqli->query( $consulta ) ) {
        $correo = mysqli_fetch_object( $result );
        $result->close();
    }
    if($correo){
        if($correo->tipo == 1){
            try {
                $mail = new PHPMailer(true);
                $mail->SMTPDebug = 0;
                $mail->isSMTP();
                $mail->Host = $correo->servidor;
                $mail->SMTPAuth = true;
                $mail->Username = $correo->email;
                $mail->Password = $correo->contrasena;
                $mail->SMTPSecure = 'tls';
                $mail->Port = $correo->puerto;

                $mail->setFrom($correo->email, utf8_encode( $correo->nombre ));
                $mail->addAddress($to, $extra_data['nombre']);
                $mail->addReplyTo($correo->email, utf8_encode( $correo->nombre ));
                if($copia){
                    $mail->addBCC($copia);
                }            


                foreach ( $extra_data as $key => $val ) {
                    $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
                }

                foreach ( $extra_data as $key => $val ) {
                    $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
                }

                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = utf8_decode($mensaje);
                $mail->AltBody = utf8_decode($mensaje_txt);

                $mail->send();
                return true;
            } catch (Exception $e) {
                return false;
            }
        }
        if($correo->tipo == 2){
            $to = $para;

            $boundary = uniqid( 'np' );

            $subject = $titulo;

            $headers = "From: ".utf8_encode( $correo->nombre )." <" . strip_tags( $correo->email ) . ">\r\n";
            if ( $copia ) {
                $headers .= "CC: " . strip_tags( $copia ) . "\r\n";
            }
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "To: " . $to . "\r\n";
            $headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";

            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Return-Path: ".utf8_encode( $correo->nombre )." <" . strip_tags( $correo->email ) . ">\r\n";
            $headers .= "Organization: ".utf8_encode( $correo->nombre )."\r\n";
            //$headers .= "MIME-Version: 1.0\r\n";
            //$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

            //here is the content body
            $message = "";
            $message .= "\r\n\r\n--" . $boundary . "\r\n";
            $message .= "Content-type: text/plain;charset=UTF-8\r\n\r\n";
            //Plain text body
            foreach ( $extra_data as $key => $val ) {
                $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
            }

            $message .= $mensaje_txt;
            $message .= "\r\n\r\n--" . $boundary . "\r\n";

            $message .= "Content-type: text/html;charset=UTF-8\r\n\r\n";

            foreach ( $extra_data as $key => $val ) {
                $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
            }

            $message .= $mensaje;
            $message .= "\r\n\r\n--" . $boundary . "--";
            return mail( $to, $subject, $message, $headers, '-f '.$correo->email.' -F "'.utf8_encode( $correo->nombre ).'"' );
        }
        if($correo->tipo == 3){
            try {
                $mandrill = new Mandrill($correo->servidor);
                foreach ( $extra_data as $key => $val ) {
                    $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
                }

                foreach ( $extra_data as $key => $val ) {
                    $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
                }
                $message = array(
                    'html' => $mensaje,
                    'text' => $mensaje_txt,
                    'subject' => $subject,
                    'from_email' => $correo->email,
                    'from_name' => utf8_encode( $correo->nombre ),
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $extra_data['nombre'],
                            'type' => 'to'
                        )
                    ),
                    'headers' => array('Reply-To' => $correo->email),
                    'important' => false,
                    'track_opens' => null,
                    'track_clicks' => null,
                    'auto_text' => null,
                    'auto_html' => null,
                    'inline_css' => null,
                    'url_strip_qs' => null,
                    'preserve_recipients' => null,
                    'view_content_link' => null,
                    'bcc_address' => ($copia)?$copia:'',
                    'tracking_domain' => null,
                    'signing_domain' => null,
                    'return_path_domain' => null,
                    'merge' => true,
                    'merge_language' => 'mailchimp',                
                    'tags' => array()
                );
                $async = false;
                $result = $mandrill->messages->send($message, $async);
                return true;
            }catch(Mandrill_Error $e){
                return false;
            }            
            
        }
    }else{
        return false;        
    }
}
?>