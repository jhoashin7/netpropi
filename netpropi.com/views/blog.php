<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">		
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<?php
						if($pais){
							$consulta = "SELECT id, nombre, imagen, texto_corto, creado FROM blog WHERE FIND_IN_SET(".$pais.", paises) ORDER BY creado DESC";
							if ( $result = $mysqli->query( $consulta ) ) {
								while($row = $result->fetch_assoc()){
						?>
						<div class="col-6 col-md-4 col-xl-3 text-start pb-3">
							<a href="#" class="ratio ratio-1x1 overflow-hidden d-block" onClick="loader('articulo', {'iden': <?php echo $row['id'] ?>}); return false">
								<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-image: url(blog/<?php echo $row['imagen'] ?>); background-repeat: no-repeat; background-position: center center; background-size: cover"></div>
							</a>
							<div class="py-2">
								<h4 class="m-0 fw-bold text-danger"><?php echo utf8_encode($row['nombre']) ?></h4>
							</div>
							<small class="text-muted"><i class="fas icon-calendar fa-fw"></i> <?php echo date('Y-m-d', strtotime($row['creado'])) ?></small>
							<div class="py-2">
								<?php echo utf8_encode($row['texto_corto']) ?>
							</div>
							<div class="text-end">
								<button class="btn btn-danger btn-sm text-white rounded-pill fw-bold lh-1 text-center py-2" onClick="loader('articulo', {'iden': <?php echo $row['id'] ?>}); return false"><i class="fas icon-check-circle fa-fw fa-lg"></i> Leer más</button>
							</div>
						</div>
						<?php                      
								}
								$result->close();
							}
						}
						?>						
					</div>
				</div>												
			</div>
		</div>		
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/blog');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>