<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
$iden = (isset($getvars[ 'iden' ]) && $getvars[ 'iden' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'iden' ] ) ):false;
$consulta = "SELECT prop.*, LPAD(prop.id,6,0) AS codigo, cam.moneda AS moneda, cama.moneda AS monedaad, cit.name AS ciudad FROM propiedades AS prop LEFT JOIN cambio AS cam ON (cam.id = prop.moneda_valor) LEFT JOIN cambio AS cama ON (cama.id = prop.moneda_admon) LEFT JOIN cities AS cit ON (cit.id = prop.id_city) WHERE prop.id = ".$iden." AND prop.estado = 1";
$propiedad = false;
if ( $result = $mysqli->query( $consulta ) ) {
    $propiedad = mysqli_fetch_object( $result );
	$ubicacion = explode(',', $propiedad->ubicacion);
    $result->close();
}
$fav = true;
if($id){
	$id_veri = explode('***', simple_crypt( $id, 'd', $conArr['enc_string'] ));
	$puede = false;
	$consulta = "SELECT id FROM fav_prop WHERE id_user = ".$id_veri[1]." AND id_propiedad = ".$iden;
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$fav = ($rows == 0)?true:false;
		$result->close();
	}
}
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Netpropi</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">
		<?php
        if($propiedad){
        ?>
		<div class="bg-white">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-start">
						<div class="col-12 col-sm-6">
							<div id="carrusel_fotos" class="carousel slide border borde-light" data-bs-ride="carousel">
								<div class="carousel-inner">
									<?php
									$fotos = explode(',', $propiedad->foto);
									$count = 0;
									foreach ($fotos as $valor) {
									?>
									<div class="carousel-item <?php echo ($count == 0)?'active':'' ?>">
										<div class="ratio ratio-1x1 overflow-hidden d-block">
											<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-image: url(propiedades/<?php echo $valor ?>); background-repeat: no-repeat; background-position: center center; background-size: contain"></div>
										</div>										
									</div>
									<?php
										$count++;
									}
									?>									
								</div>
								<button class="carousel-control-prev" type="button" data-bs-target="#carrusel_fotos" data-bs-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="visually-hidden">Previous</span>
								</button>
								<button class="carousel-control-next" type="button" data-bs-target="#carrusel_fotos" data-bs-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="visually-hidden">Next</span>
								</button>								
							</div>
						</div>
						<div class="col-12 col-sm-6 py-3">
							<h3 class="mb-3 fw-bold text-danger"><span class="text-responsive"><?php echo utf8_encode($propiedad->nombre) ?></span></h3>
							<p class="mb-2"><span class="text-responsive text-muted fw-bold"><?php echo ($propiedad->tipo == 1)?'Local Comercial':'Vivienda' ?></span></p>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold"><?php echo ($propiedad->fin == 1)?'En Venta':'En Alquiler' ?>:</span> <span class="text-danger fw-bold">$ <?php echo number_format($propiedad->valor, 2, ',', '.') ?> (<?php echo $propiedad->moneda ?>)</span></span></p>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold">Administración:</span> <span class="text-dark fw-bold">$ <?php echo number_format($propiedad->administracion, 2, ',', '.') ?> (<?php echo $propiedad->monedaad ?>)</span></span></p>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold">Área:</span> <span class="text-dark fw-bold"><?php echo number_format($propiedad->area, 0, ',', '.') ?> m²</span></span></p>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold">Dirección:</span> <span class="text-dark fw-bold"><?php echo utf8_encode($propiedad->direccion) ?> | <?php echo utf8_encode($propiedad->ciudad) ?></span></span></p>
							<?php if($propiedad->estrato != 0){ ?>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold">Estrato:</span> <span class="text-dark fw-bold"><?php echo $propiedad->estrato ?></span></span></p>
							<?php } ?>
							<hr>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold">Características: </span></span></p>
							<ul>
							<?php							
                            $consulta = "SELECT id, nombre FROM caracteristicas WHERE FIND_IN_SET(id, '".$propiedad->caracteristicas."') ORDER BY nombre ASC";					
                            if ( $result = $mysqli->query( $consulta ) ) {
                                while($row = $result->fetch_assoc()){
							?>
								<li><?php echo utf8_encode($row['nombre']) ?></li>							
							<?php                      
                                }
                                $result->close();
                            }							
							?>
							</ul>
							<hr>
							<p class="mb-2"><span class="text-responsive"><span class="text-muted fw-bold">Facilidades: </span></span></p>
							<ul>
							<?php							
                            $consulta = "SELECT id, nombre FROM facilidades WHERE FIND_IN_SET(id, '".$propiedad->facilidades."') ORDER BY nombre ASC";					
                            if ( $result = $mysqli->query( $consulta ) ) {
                                while($row = $result->fetch_assoc()){
							?>
								<li><?php echo utf8_encode($row['nombre']) ?></li>							
							<?php                      
                                }
                                $result->close();
                            }							
							?>
							</ul>
							<hr>
							<div class="d-block pb-2">
								<?php echo utf8_encode($propiedad->descripcion) ?>
							</div>
							<div class="d-block text-end">
								<div class="btn-group" role="group" aria-label="Acciones">
									<button type="button" class="btn btn-danger text-white fw-bold <?php echo($fav)?'':'disabled' ?>" onClick="<?php if($fav){ ?>openlogIn('setFavorito', [<?php echo $propiedad->id ?>]); <?php } ?>return false"><i class="fas fa-star fa-fw fa-lg"></i> Favorita</button>
									<button type="button" class="btn btn-outline-danger text-dark fw-bold" onClick="openlogIn('meInteresa', [<?php echo $propiedad->id ?>, <?php echo $propiedad->id_user ?>]);return false">Me interesa <i class="fas fa-comment fa-fw fa-lg"></i></button>
								</div>
							</div>
						</div>
					</div>					
				</div>					
			</div>
		</div>
		<div class="bg-white">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 text-start px-0">
					<div id="map_cont" class="ratio ratio-21x9 overflow-hidden d-block">
						<div id="mapa" class="w-100 h-100"></div>
					</div>
				</div>
			</div>
		</div>
		<?php	
        }
        ?>
		
		 <div class="modal fade" id="mod-meInteresa" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">
							<span class="text-responsive d-flex justify-content-start align-items-center">
								<span class="fa-stack text-danger align-top">
									<i class="fas fa-circle fa-stack-2x"></i>
									<i class="far fa-comment fa-stack-1x text-white"></i>
								</span>                                
								<span>Hola, me interesa tu propiedad</span>
							</span>
							<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
								<i class="fas fa-times-circle fa-lg fa-fw"></i>						
							</button>
						</h5>                    
					</div>
					<div class="modal-body text-center">					
						<form id="form-meInteresa">
							<input type="hidden" class="id" name="id" value=0 />
							<input type="hidden" class="id_user" name="id_user" value="" />							
							<div class="form-group pb-3">
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas icon-info fa-fw"></i></span>
									<input type="text" name="texto" class="form-control texto" placeholder="Título" aria-label="Título" required>
								</div>
							</div>													
							<div class="form-group pb-3">                            
								<textarea name="comentario" class="comentario form-control" rows="4" placeholder="Comentario" required></textarea>
							</div>
						</form>					
					</div>
					<div class="modal-footer">
						<div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
							<button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-danger text-white" onClick="ValInteres(); return false"><span class="text-responsive">enviar <i class="fas fa-check-circle fa-fw"></i></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<script>			
            $(function() {
				document.title ="NetPropi - Propiedad: <?php echo $propiedad->nombre ?> ";
				<?php
				if($propiedad){
				?>
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/propiedad/<?php echo  str_replace(" ", "_", $propiedad->nombre) ?>/<?php echo $propiedad->codigo ?>');
					initMapa();
					
					var positiont = new google.maps.LatLng(parseFloat(<?php echo $ubicacion[0] ?>),parseFloat(<?php echo $ubicacion[1] ?>));
                    marcador = new google.maps.Marker({
                        clickable: true,
                        map: map,
                        title: '<?php echo utf8_encode($propiedad->nombre) ?>',
                        zIndex: 100,
						icon: 'images/marcador.png',
                        position: positiont,
						draggable: true
                    });
                    map.setCenter(positiont);
					
				}
				<?php	
				}
				?>
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });
			function setFavorito(iden){
				if(login){		
					loaderShow();
					var datos = { 
						'id': iden,						
						'accion': 10,						
						'idveruser': login
					};
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {							               
							$('#loader').one("hide", function() { 								
								showError(error);
							});
							loaderHide();
						},
						success:  function (response) {					
							if(response.resp){                    
								showGood(response.data.msg);
								reloader();                    						
							}else{								                   
								$('#loader').one("hide", function() { 									
									showError(response.data.msg);
								});
								loaderHide();
							}					
						}
					});
				}
			}
			function meInteresa(prop, usid){
				$('#mod-meInteresa').one('show.bs.modal', function () {
                    $('#form-meInteresa .id').val(prop);
					$('#form-meInteresa .id_user').val(usid);
                });
				openData('meInteresa');
				
			}
			function ValInteres(){
				if($('#form-meInteresa').parsley().validate()){		
					loaderShow();
					var datos = $('#form-meInteresa').serializeArray();
					datos.push({ name: "accion", value: 11 }); 
					datos.push({ name: "editor", value: login });
            		datos.push({ name: "idveruser", value: login });
					$.ajax({
						url: dirCont,
						data: $.param(datos),
						method:  'POST',
						dataType: 'json',
						error: function () {							               
							$('#loader').one("hide", function() { 								
								showError(error);
							});
							loaderHide();
						},
						success:  function (response) {					
							if(response.resp){                    
								$('#mod-meInteresa').one('hidden.bs.modal', function () {
									showGood(response.data.msg);
								});
								$('#loader').one("hide", function() { 
									$('#mod-meInteresa').modal('hide');
								});
								loaderHide();                    						
							}else{								                  
								$('#loader').one("hide", function() {
									showError(response.data.msg);
								});
								loaderHide();
							}					
						}
					});
				}
			}
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>