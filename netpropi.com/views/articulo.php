<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
$iden = (isset($getvars[ 'iden' ]) && $getvars[ 'iden' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'iden' ] ) ):false;
$consulta = "SELECT id, nombre, url, imagen, texto, creado FROM blog WHERE id = ".$iden;
$blog = false;
if ( $result = $mysqli->query( $consulta ) ) {
    $blog = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');
	</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">		
		<div class="bg-white pb-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<?php
				if($blog){
				?>
				<div class="col-12 text-start px-0 pb-4">
					<div class="ratio ratio-21x9 overflow-hidden d-block">
						<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-image: url(blog/<?php echo $blog->imagen ?>); background-repeat: no-repeat; background-position: center center; background-size: cover"></div>
					</div>
				</div>
				<div class="col-12 col-xl-10 pb-3 text-start">
					<h2 class="m-0 fw-bold text-danger"><?php echo utf8_encode($blog->nombre) ?></h2>
					<small class="text-muted"><i class="fas icon-calendar fa-fw"></i> <?php echo date('Y-m-d', strtotime($blog->creado)) ?></small>
				</div>
				<div class="col-12 col-xl-10 text-start">
					<?php echo utf8_encode($blog->texto) ?>
				</div>
				<?php	
				}
				?>							
			</div>
		</div>		
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/<?php echo $blog->url ?>');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>