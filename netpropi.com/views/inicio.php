<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">
		<div class="ratio ratio-21x9">
			<div class="overflow-hidden">
				<iframe class="vw-100 top-50 start-50 translate-middle position-absolute" src="https://www.youtube.com/embed/Yj2iELI6OeI?controls=0&autoplay=1&mute=1&playlist=_ri7JMP9KdE&loop=1" style="height: 150vh"></iframe>
				<div class="position-absolute start-0 top-0 w-100 h-100" style="background-color: rgba(0,0,0,0.5)">
					<div class="d-flex w-100 justify-content-evenly align-items-center position-absolute start-0 bottom-0 p-3" style="background-color: rgba(0,0,0,0.5)">
						<div class="px-2">
							<button class="btn btn-danger text-white rounded-pill fw-bold" onClick="loader('planes'); return false"><i class="fas icon-check-circle fa-fw fa-lg"></i> Pruebalo</button>
						</div>
						<div class="px-2">
							<button class="btn btn-outline-danger text-white rounded-pill fw-bold" onClick="loader('propiedades'); return false"><i class="fas icon-search fa-fw fa-lg"></i> Descubre propiedades</button>
						</div><div class="px-2">
							<a href="https://www.youtube.com/watch?v=Yj2iELI6OeI" class="btn btn-outline-danger text-decoration-none text-white rounded-pill fw-bold" target="blank"><i class="fas fa-play-circle fa-fw fa-lg"></i> Ver video</a>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="bg-white pt-4 pb-2">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-md-6 col-xl-5 text-center pb-2">
					<img src="images/inicio1.png" class="img-fluid"/>
				</div>
				<div class="col-12 col-md-6 col-xl-5 text-start pb-2">
					<h4 class="fw-bold text-danger"><i class="fas icon-info fa-fw fa-2x"></i> ¿cómo funciona?</h4>
					<p><strong class="text-danger">NetPropi</strong> es una plataforma creada para gestionar tus propiedades, encontrar tu hogar ideal y simplificar tu vida.</p>
					<p>Con más de <strong>100.000</strong> visitas al mes en busca de inquilinos, compradores inmuebles de segunda mano, propiedades nuevas y viviendas en alquiler; <strong class="text-danger">NetPropi</strong> tiene algo bueno para todos.</p>
					<p>Creemos en que cada persona merece un gran hogar, por eso somos un marketplace digital que ofrece las ventajas de inmobiliarias tradicionales con una menor inversión de tiempo, dinero y precios más atractivos para cada visitante.</p>
				</div>
			</div>
		</div>
		<div class="bg-danger text-white py-3">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-10 text-start py-3">
					<h4 class="fw-bold mb-0"><i class="fas fa-user-tie fa-fw fa-2x"></i> Suscriptores</h4>
				</div>
				<div class="col-12 col-md-6 col-xl-5 text-start pb-2">					
					<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
						<div class="carousel-indicators">
							<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
							<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
						</div>
						<div class="carousel-inner">
							<div class="carousel-item pb-4 px-5 active">
								<h6 class="fw-bold"><i class="fas fa-house-user fa-fw fa-2x"></i> PROPIETARIOS:</h6>
								<p><strong>Te damos soluciones, no complicaciones.</strong></p>
								<p>Rápida, clara, justa, segura y efectiva, así es la forma en la que gestionas tus inmuebles con Netpropi.<br>¿Para que tanto papeleo y estrés? Dejamos el trabajo duro a nosotros y asómbrate con los resultados.</p>
								<p><strong>Tú tienes el poder.</strong><br>Con nosotros tienes la capacidad de analizar el desempeño de tu propiedad entre más de 100.000 visitas mensuales y la ventaja de poder escoger quién quieres que viva en ella.</p>
								<p><strong>Acá lo tienes todo.</strong><br>Reduce el tiempo de gestión de tus propiedades encontrando todo lo que necesites. Desde un equipo legal hasta profesionales en reparaciones. Relájate y siente como te quitamos ese peso de encima.</p>
							</div>
							<div class="carousel-item pb-4 px-5">
								<h6 class="fw-bold"><i class="fas fa-users fa-fw fa-2x"></i> ARRENDATARIOS Y/O COMPRADORES:</h6>
								<p>Las personas o empresas que hayan activado la búsqueda de una propiedad podrán encontrar en nuestra plataforma propiedades según precio, destinación, ubicación y características más a fin a su búsqueda.</p>
								<p>Una vez encuentren la propiedad de interés, nos dejará sus datos para que nuestra plataforma sirva de puente y poder finalizar el acuerdo y respectivamente el contrato de arrendamiento.</p>
								<p>Este arrendatario también tendrá la posibilidad de acceder al portafolio de empresas que presten servicio de reparación.</p>
								<p><strong>NetPropi</strong> será el aliado para lograr la venta de una propiedad garantizando llegar al público objetivo y lo más importante sin un tercero que intervenga en la finalización de una venta de un inmueble.</p>
								<ul>
									<li>El propietario podrá incluir en nuestra plataforma imágenes del inmueble, características y valor.</li>
									<li>Tendrá acceso a contratos y orientación de un abogado especialista para finalizar la compra.</li>
								</ul>
							</div>
						</div>
						<button class="carousel-control-prev" style="width: auto" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="visually-hidden">Previous</span>
						</button>
						<button class="carousel-control-next" style="width: auto" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="visually-hidden">Next</span>
						</button>
					</div>
				</div>
				<div class="col-12 col-md-6 col-xl-5 text-center pb-2">
					<img src="images/inicio2.png" class="img-fluid"/>
				</div>
			</div>
		</div>
		<div class="bg-white pt-4 pb-2">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-md-6 col-xl-5 text-center pb-2">
					<img src="images/net_animacion.png" class="img-fluid"/>
				</div>
				<div class="col-12 col-md-6 col-xl-5 text-start pb-2">
					<h4 class="fw-bold text-danger"><i class="fas fa-hands-helping fa-fw fa-2x"></i> ¿Qué hacemos?</h4>
					<ol>
                        <li><strong>Acercar.</strong></li>
                        <li><strong>Conectar.</strong></li>
						<li><strong>Acelerar</strong> procesos entre arrendador y arrendatario.</li>
						<li>Hacer que puedas <strong>gestionar</strong> tus propiedades con un par de clicks.</li>
						<li><strong>Construir</strong> data inteligente para la toma de decisiones, a través de centralización de información de inquilinos, agentes inmobiliarios, abogados, contratistas, constructores y propietarios.</li>
                    </ol>
				</div>
			</div>
		</div>
		<div class="bg-danger text-white py-3">
			<div class="row w-100 mx-0 justify-content-center align-items-center">				
				<div class="col-12 col-md-6 col-xl-5 text-start pb-2">
					<h4 class="fw-bold"><i class="fas fa-clipboard-check fa-fw fa-2x"></i> Caracterisiticas</h4>
					<ol>
                        <li><strong>Ahorro</strong> económico.</li>
                        <li><strong>Control</strong> sobre el Inmueble.</li>
						<li><strong>Sin desplazamiento.</strong></li>
						<li>Plataforma <strong>Intuitiva.</strong></li>
						<li><strong>Ahorro</strong> en tiempo.</li>
						<li><strong>Ahorro</strong> en costos operativos.</li>
						<li><strong>Altos estándares</strong> para la seguridad en la información.</li>
						<li>Conectividad <strong>24/7</strong></li>
						<li><strong>Bajos costos</strong> en suscripción.</li>
						<li>Data en <strong>tiempo real.</strong></li>
						<li><strong>Administración</strong> a un clic.</li>
						<li>Excelente <strong>servicio al cliente.</strong></li>
                    </ol>
				</div>
				<div class="col-12 col-md-6 col-xl-5 text-center pb-2">
					<img src="images/inicio3.png" class="img-fluid"/>
				</div>
			</div>
		</div>
		<div class="bg-white pt-4 pb-2">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-md-6 col-xl-5 text-center pb-2">
					<i class="fas fa-comments fa-fw fa-10x text-danger"></i>
				</div>
				<div class="col-12 col-md-6 col-xl-5 text-start pb-2">
					<div id="carouselExampleIndicators2" class="carousel carousel-dark slide" data-bs-ride="carousel">
						<div class="carousel-indicators">
							<button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
							<button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="1" aria-label="Slide 2"></button>
							<button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="2" aria-label="Slide 3"></button>
							<button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="3" aria-label="Slide 4"></button>
							<button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="4" aria-label="Slide 5"></button>
						</div>
						<div class="carousel-inner pb-3">
							<div class="carousel-item pb-4 px-5 text-center active">
								<div class="d-inline-block rounded-circle border border-danger border-2 overflow-hidden">
									<img src="images/cara2.jpg" class="img-fluid" />
								</div>
								<div class="d-flex w-100 justify-content-between align-items-start pt-2">
									<div><i class="fas fa-quote-left fa-fw fa-2x text-danger"></i></div>
									<div class="flex-fill align-self-center">
										<p>"Llegué a más personas de las que imaginé". Con <strong class="text-danger">NetPropi</strong> pude escoger quién iba a vivir en mi propiedad. Pude escoger al mejor candidato para mí, entre un montón de perfiles.</p>
									</div>
									<div class="align-self-end"><i class="fas fa-quote-right fa-fw fa-2x text-danger"></i></div>
								</div>								
							</div>
							<div class="carousel-item pb-4 px-5 text-center">
								<div class="d-inline-block rounded-circle border border-danger border-2 overflow-hidden">
									<img src="images/cara3.jpg" class="img-fluid" />
								</div>
								<div class="d-flex w-100 justify-content-between align-items-start pt-2">
									<div><i class="fas fa-quote-left fa-fw fa-2x text-danger"></i></div>
									<div class="flex-fill align-self-center">
										<p>¡Es muy fácil de usar!. Son muy responsables y ofrecen precios llamativos. La plataforma es sencilla y el servicio está muy bien.</p>
									</div>
									<div class="align-self-end"><i class="fas fa-quote-right fa-fw fa-2x text-danger"></i></div>
								</div>
							</div>
							<div class="carousel-item pb-4 px-5 text-center">
								<div class="d-inline-block rounded-circle border border-danger border-2 overflow-hidden">
									<img src="images/cara1.jpg" class="img-fluid" />
								</div>
								<div class="d-flex w-100 justify-content-between align-items-start pt-2">
									<div><i class="fas fa-quote-left fa-fw fa-2x text-danger"></i></div>
									<div class="flex-fill align-self-center">
										<p>"Lo pensé, me decidí y lo logré". <strong class="text-danger">NetPropi</strong> ha sido una agradable sorpresa en mi vida, porque pude vender mi casa y no tuve que invertir tiempo en salidas ni trámites innecesarios.</p>
									</div>
									<div class="align-self-end"><i class="fas fa-quote-right fa-fw fa-2x text-danger"></i></div>
								</div>
							</div>
							<div class="carousel-item pb-4 px-5 text-center">
								<div class="d-inline-block rounded-circle border border-danger border-2 overflow-hidden">
									<img src="images/cara2.jpg" class="img-fluid" />
								</div>
								<div class="d-flex w-100 justify-content-between align-items-start pt-2">
									<div><i class="fas fa-quote-left fa-fw fa-2x text-danger"></i></div>
									<div class="flex-fill align-self-center">
										<p>¡Encontré mi lugar a tiempo!. Mi anterior contrato fue cancelado y necesitaba encontrar un lugar que me gustara en el menor tiempo posible. En <strong class="text-danger">NetPropi</strong> me simplificaron las cosas y el precio fue mejor de lo que esperaba.</p>
									</div>
									<div class="align-self-end"><i class="fas fa-quote-right fa-fw fa-2x text-danger"></i></div>
								</div>
							</div>
							<div class="carousel-item pb-4 px-5 text-center">
								<div class="d-inline-block rounded-circle border border-danger border-2 overflow-hidden">
									<img src="images/cara1.jpg" class="img-fluid" />
								</div>
								<div class="d-flex w-100 justify-content-between align-items-start pt-2">
									<div><i class="fas fa-quote-left fa-fw fa-2x text-danger"></i></div>
									<div class="flex-fill align-self-center">
										<p><strong class="text-danger">NetPropi</strong> es una red creada para gestionar tus propiedades, encontrar tu hogar ideal y simplificar tu vida.<br>Con más de 100.000 visitas al mes en busca de inquilinos, compradores inmuebles de segunda mano, propiedades nuevas y viviendas en alquiler; <strong class="text-danger">Netpropi</strong> tiene algo bueno para todos. Creemos en que cada persona merece un gran hogar, por eso somos un marketplace digital que ofrece las ventajas de inmobiliarias tradicionales con una menor inversión de tiempo, dinero y precios más atractivos para cada visitante.</p>
									</div>
									<div class="align-self-end"><i class="fas fa-quote-right fa-fw fa-2x text-danger"></i></div>
								</div>
							</div>
						</div>
						<button class="carousel-control-prev" style="width: auto" type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="visually-hidden">Previous</span>
						</button>
						<button class="carousel-control-next" style="width: auto" type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="visually-hidden">Next</span>
						</button>
					</div>
				</div>
			</div>
		</div>
		
		
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/inicio');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>