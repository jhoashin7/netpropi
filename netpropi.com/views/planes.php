<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
$puede = true;
if($id){
	$id_veri = explode('***', simple_crypt( $id, 'd', $conArr['enc_string'] ));
	$puede = false;
	$consulta = "SELECT id FROM plan WHERE id_user = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$puede = ($rows == 0)?true:false;
		$result->close();
	}
}
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">		
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-12 text-start">
							<h4 class="m-0 fw-bold text-danger"><span class="text-responsive"><i class="fas icon-home-outline fa-fw fa-lg"></i> Nuestros planes</span></h4>
						</div>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-stretch aliados_cont">
						<?php
						if($pais){
							$consulta = "SELECT id, nombre, minimo, maximo, valor, tiempo FROM planes WHERE tipo = 0 AND id = 1";
							if ( $result = $mysqli->query( $consulta ) ) {
								while($row = $result->fetch_assoc()){
						?>
						<div class="col-12 col-sm-6 col-md-4 col-xl-3 text-start pb-3">
							<div class="d-block w-100 h-100 rounded-3 bg-danger position-relative overflow-hidden">
								<div class="py-4 px-3 text-center gray-100 border">
									<h4 class="m-0 fw-bold text-danger lh-1"><span class="text-responsive"><?php echo utf8_encode($row['nombre']) ?></span></h4>
								</div>
								<div class="py-4 px-3 text-center bg-danger">
									<h3 class="m-0 fw-bold text-white lh-1"><span class="text-responsive">GRATIS</span></h3>
									<small class="text-white">válido solo 1 vez</small>
								</div>
								<div class="py-4 px-3 text-start gray-200">
									<h5 class="mb-2 fw-bold text-muted lh-1"><span class="text-responsive">Características</span></h5>
									<ul>
										<li><small><strong><?php echo $row['minimo'] ?></strong> propiedad.</small></li>
										<li><small>Duración <strong><?php echo $row['tiempo'] ?></strong> días.</small></li>
										<li><small>Acceso <strong>completo</strong> al <strong>panel de control de NetPropi</strong> para seguimiento y edición de tus propiedades durante el tiempo total de vigencia de tu plan.</small></li>
									</ul>
								</div>
								<div class="py-4 px-3 text-center border border-danger bg-danger">
									<button class="btn btn-light text-danger rounded-pill fw-bold text-decoration-none<?php if(!$puede){ ?> disabled<?php } ?>" onClick="<?php if($puede){ ?>openlogIn('setPlanGrat', [<?php echo $row['id'] ?>]); <?php } ?>return false"><i class="fas icon-check-circle fa-fw fa-lg"></i> Contratar</button>
								</div>
							</div>														
						</div>
						<?php                      
								}
								$result->close();
							}
							
							$consulta = "SELECT p.id, nombre, minimo, maximo, valor, tiempo, c.currency FROM planes p INNER JOIN countries c ON c.id = p.pais_id WHERE pais_id = $pais AND tipo = 0 AND valor > 0 ORDER BY valor DESC";
							if ( $result = $mysqli->query( $consulta ) ) {
								while($row = $result->fetch_assoc()){
						?>
						<div class="col-12 col-sm-6 col-md-4 col-xl-3 text-start pb-3">
							<div class="d-block w-100 h-100 rounded-3 bg-danger position-relative overflow-hidden">
								<div class="py-4 px-3 text-center gray-100 border">
									<h4 class="m-0 fw-bold text-danger lh-1"><span class="text-responsive"><?php echo utf8_encode($row['nombre']) ?></span></h4>
								</div>
								<div class="py-4 px-3 text-center bg-danger">
									<h3 class="m-0 fw-bold text-white lh-1"><span class="text-responsive">$<?php echo $row['valor'].'  '.$row['currency'] ?></span></h3>
									<small class="text-white">valor * propiedad</small>
								</div>
								<div class="py-4 px-3 text-start gray-200">
									<h5 class="mb-2 fw-bold text-muted lh-1"><span class="text-responsive">Características</span></h5>
									<ul>
										<li><small><?php if($row['minimo'] == $row['maximo']){ ?>Mínimo <strong><?php echo $row['minimo'] ?></strong> propiedades.<?php }else{ ?>Desde <strong><?php echo $row['minimo'] ?></strong> hasta <strong><?php echo $row['maximo'] ?></strong> propiedades.<?php } ?></small></li>
										<li><small>Duración mínima <strong><?php echo $row['tiempo'] ?></strong> días.</small></li>
										<li><small>Acceso <strong>completo</strong> al <strong>panel de control de NetPropi</strong> para seguimiento y edición de tus propiedades durante el tiempo total de vigencia de tu plan.</small></li>
									</ul>
								</div>
								<div class="py-4 px-3 text-center border border-danger bg-danger">
									<button class="btn btn-light text-danger rounded-pill fw-bold text-decoration-none" onClick="openlogIn('showModPlan', [<?php echo $row['id'] ?>]); return false"><i class="fas icon-check-circle fa-fw fa-lg"></i> Contratar</button>
								</div>
							</div>														
						</div>
						<?php                      
								}
								$result->close();
							}
						}
						?>
						<div class="col-12 col-sm-6 col-md-4 col-xl-3 text-start pb-3">
							<div class="d-block w-100 h-100 rounded-3 bg-danger position-relative overflow-hidden">
								<div class="py-4 px-3 text-center gray-100 border">
									<h4 class="m-0 fw-bold text-danger lh-1"><span class="text-responsive">Plan a Medida</span></h4>
								</div>
								<div class="py-4 px-3 text-center bg-danger">
									<h3 class="m-0 fw-bold text-white lh-1"><span class="text-responsive">Contactar</span></h3>
									<small class="text-white">asesor comercial</small>
								</div>
								<div class="py-4 px-3 text-start gray-200">
									<h5 class="mb-2 fw-bold text-muted lh-1"><span class="text-responsive">Características</span></h5>
									<ul>
										<li><small>Tu defines la cantidad de propiedades.</small></li>
										<li><small>Tu defines el tiempo de duración.</small></li>
										<li><small>Acceso <strong>completo</strong> al <strong>panel de control de NetPropi</strong> para seguimiento y edición de tus propiedades durante el tiempo total de vigencia de tu plan.</small></li>
									</ul>
								</div>
								<div class="py-4 px-3 text-center border border-danger bg-danger">
									<button class="btn btn-light text-danger rounded-pill fw-bold text-decoration-none" onClick="openlogIn('openData', ['contactoMod'], true); return false"><i class="fas icon-mail fa-fw fa-lg"></i> Contáctanos</button>
								</div>
							</div>														
						</div>
					</div>
				</div>						
			</div>
		</div>
		
		<div class="modal fade" id="mod-showModplan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">
							<span class="text-responsive d-flex justify-content-start align-items-center">
								<span class="fa-stack text-danger align-top">
									<i class="fas fa-circle fa-stack-2x"></i>
									<i class="fas icon-home-outline fa-stack-1x text-white"></i>
								</span>                                
								<span class="lh-1">¿Interesado en un nuevo plan?</span>
							</span>
							<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
								<i class="fas fa-times-circle fa-lg fa-fw"></i>						
							</button>
						</h5>                    
					</div>
					<div class="modal-body text-start">						
						<div class="row w-100 mx-0 justify-content-center align-items-center">
							<div class="col-10 py-2">
								<p class="mb-3 text-muted"><span class="text-responsive">Si estás interesado en adquirir un nuevo plan, puedes hacerlo desde tu panel control accediendo con tu usuario y contraseña.</span></p>
								<div class="d-grid gap-3">
									<a href="<?php echo $conArr['base_url'] ?>" class="btn btn-danger text-white rounded-pill fw-bold text-decoration-none" target="_blank"><i class="fas icon-sliders fa-fw fa-lg"></i> Panel de control</a>
								</div>
							</div>
						</div> 
					</div>                
				</div>
			</div>
		</div>
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/planes');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });
			function showModPlan(iden){
				loaderHide();
				$('#mod-showModplan').modal('show');
			}
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>