<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">
		
        <div class="container">
    <div class="col-12 col-lg-8 col-xxl-7 mx-auto mb-5 mt-5">
      <!-- <span class="text-muted">Lorem ipsum</span> -->
        <h5 class="fw-bold mt-5 mb-5 text-center">
            TERMINOS Y CONDICIONES
            <!-- <br><br>ZAPATA HOLDINGS S. A. S.
            <br><br>WWW.NETPROPI.COM -->
        </h6>
        <p class="mt-5 ">Fecha de publicaci&oacute;n: 30/03/2021</p>
		<p>&Uacute;ltima actualizaci&oacute;n: --/--/----</p>
		<p>1) SOBRE EL SITIO WEB WWW.NETPROPI.COM y la App NETPROPI</p>
		<p>El portal web o plataforma WWW.NETPROPI.COM &laquo;NetPropi&raquo; y su App es propiedad de ZAPATA HOLDINGS S. A. S., sociedad legalmente constituida, identificada con NIT 901.478.337-7, con domicilio en Medell&iacute;n / Colombia.</p>
		<p><br></p>
		<p>2) CONDICIONES GENERALES DE USO</p>
		<p><br></p>
		<p>ACEPTACI&Oacute;N DE LAS CONDICIONES DE USO. Los siguientes son los t&eacute;rminos de uso de la p&aacute;gina web www.netpropi.com y la App (en adelante la plataforma, portal web, sitio web, p&aacute;gina web), as&iacute; como las condiciones generales de prestaci&oacute;n de servicios por parte de NETPROPI. Es su obligaci&oacute;n leer cuidadosamente la siguiente informaci&oacute;n antes de usar cualquiera de las herramientas disponibles en el sitio web y/o solicitar la prestaci&oacute;n de cualquier servicio ofrecido.</p>
		<p><br></p>
		<p>NETPROPI asume de buena fe que los visitantes del portal web y/o usuarios de los servicios prestados cumplen con su deber de leer detenidamente cada uno de los t&eacute;rminos y condiciones. Al navegar, explorar, acceder, revisar y/o usar cualquier herramienta y/o aplicativo dispuesto en el portal web y/o solicitar la prestaci&oacute;n de cualesquiera de los servicios ofrecidos; el visitante y/o usuario reconoce y acepta cada uno de los t&eacute;rminos y condiciones, oblig&aacute;ndose as&iacute; al cumplimiento de los mismos. Igualmente, el acceso y uso a determinadas funcionalidades, plazas y/o herramientas del portal, puede quedar condicionado a que se logre la identificaci&oacute;n completa del usuario.</p>
		<p><br></p>
		<p>NETPROPI podr&aacute; modificar, actualizar o eliminar unilateralmente los t&eacute;rminos y condiciones en cualquier tiempo, y publicar&aacute; en www.netpropi.com y la App los ajustes realizados para facilitar el acceso al p&uacute;blico. El visitante/usuario tiene la obligaci&oacute;n de revisar estos t&eacute;rminos y condiciones cada vez que haga uso de las herramientas de la plataforma, con el fin de verificar las condiciones de modo, tiempo y lugar bajo las cuales se entender&aacute; la actuaci&oacute;n de NETPROPI, sus colaboradores, dependientes y/o Aliados.</p>
		<p><br></p>
		<p>Los t&eacute;rminos &laquo;usuario&raquo;, &laquo;visitante&raquo; y/o semejantes, se refiere a todas las personas naturales o jur&iacute;dicas o entidades de cualquier naturaleza que visiten www.netpropi.com y/o soliciten o contraten la prestaci&oacute;n de servicios por parte de NETPROPI.</p>
		<p><br></p>
		<p>3) PROPIEDAD INTELECTUAL</p>
		<p><br></p>
		<p>Los logotipos, im&aacute;genes, esquemas, gr&aacute;ficas, iconos, textos, c&oacute;digos, software, direcciones de p&aacute;gina web, o cualquier otro contenido susceptible de apropiaci&oacute;n, que se encuentre en el sitio web y la App son de propiedad de ZAPATA HOLDINGS S. A. S., en todo o en parte, de acuerdo con los permisos y convenios de licenciamiento que tenga con otros titulares de los derechos de propiedad intelectual de tales.</p>
		<p><br></p>
		<p>NETPROPI autoriza al visitante a consultar, verificar, revisar, visualizar, compartir el material sometido a las normas nacionales, comunitarias y/o internacionales relacionadas con la protecci&oacute;n de la propiedad intelectual, siempre que tales acciones no tengan como objetivo la posibilidad de obtener beneficios econ&oacute;micos por ello. Cualquier otro uso no se&ntilde;alado en estos t&eacute;rminos constituye una infracci&oacute;n a los derechos de propiedad intelectual de ZAPATA HOLDINGS / NETPROPI y los dem&aacute;s titulares.</p>
		<p><br></p>
		<p>Cualquier uso no autorizado en estos t&eacute;rminos configura una infracci&oacute;n a las normas nacionales, supranacionales o internacionales de propiedad intelectual, o cualquier otra normatividad que se le relacione. El usuario asume las responsabilidades civiles, administrativas, penales o de cualquier naturaleza por el uso indebido o desautorizado de los contenidos protegidos por las normas de propiedad intelectual. Adem&aacute;s deber&aacute; reparar los perjuicios causados tanto a ZAPATA HOLDINGS / NETPROPI</p>
		<p>como a los titulares de los derechos de propiedad intelectual de los contenidos, software o cualquier otro derecho involucrado en el portal web, por los actos cometidos directamente por el usuario o por interpuesta persona, o utilizando cualquier otro mecanismo como instrumento para infringir tales derechos.</p>
		<p><br></p>
		<p>Los usuarios ceden, transfieren y/o licencian a t&iacute;tulo gratuito a ZAPATA HOLDINGS / NETPROPI y sus subordinados o dependientes, los derechos de uso, reproducci&oacute;n, modificaci&oacute;n, adaptaci&oacute;n, cualquier forma de transformaci&oacute;n y comunicaci&oacute;n al p&uacute;blico de las im&aacute;genes, dise&ntilde;os o cualquier otra obra que incorporen directamente o por conducto de NETPROPI al portal web, con el prop&oacute;sito de permitir la visualizaci&oacute;n de las mismas y el eventual contacto. Teniendo en cuenta que www.netpropi.com se encuentra disponible en internet, la licencia otorgada no tendr&aacute; restricciones en raz&oacute;n al territorio o al tiempo.</p>
		<p><br></p>
		<p>4) USO DE LA PLATAFORMA</p>
		<p><br></p>
		<p>NETPROPI ofrece a los usuarios una plataforma para la administraci&oacute;n de bienes inmuebles y propiedades. Desde el portal web y App el usuario encontrar&aacute; una serie de plazas y funcionalidades desde las distintas secciones habilitadas en la misma: (i) opciones, (ii) perfiles, (iii) propuesta, (iv) registro de actividad, (v) mis chats, (v) mis contratos.</p>
		<p><br></p>
		<p>(i) Opciones. El usuario podr&aacute; ordenar la propuesta a trav&eacute;s de las caracter&iacute;sticas concretas que seleccione: por localizaci&oacute;n, por metros cuadrados o por precio.</p>
		<p><br></p>
		<p>(ii) Perfiles (Arrendador - Arrendatario). El usuario podr&aacute; visualizar aqu&iacute;: sus datos personales (como nombre, apellidos, correo electr&oacute;nico, n&uacute;mero de contacto, ciudad, pa&iacute;s, &aacute;rea y fotograf&iacute;a del directorio); el n&uacute;mero de propuestas realizadas; as&iacute; como las valoraciones que tenga como arrendatario o como arrendador.</p>
		<p><br></p>
		<p>(iii) Propuesta. El usuario encontrar&aacute; todos los bienes inmuebles que est&eacute;n disponibles en ese momento. En una primera vista podr&aacute; conocer los datos b&aacute;sicos sobre los bienes inmuebles; si hace clic sobre la imagen del inmueble acceder&aacute; a la descripci&oacute;n del bien donde podr&aacute; consultar todos los detalles: descripci&oacute;n detallada de la propiedad, precio de arrendamiento, ubicaci&oacute;n en mapa.</p>
		<p><br></p>
		<p>(iv) Registro de actividad. La plataforma guardar&aacute; las propuestas que usted realice, con la finalidad de conocer sus preferencias.</p>
		<p><br></p>
		<p>(v) Mis chats. En esta plaza usted encontrara los chats que ha tenido con otros usuarios y/o aliados, que podr&aacute;n comunicarse de forma interna a trav&eacute;s de la herramienta de chat integrada en la App.</p>
		<p><br></p>
		<p>(vi) Mis contratos. En esta plaza usted encontrara los contratos que haya suscrito con anterioridad y los que se encuentren a&uacute;n vigentes.</p>
		<p><br></p>
		<p>(vii) Ayuda. En esta plaza el usuario podr&aacute; resolver las dudas, y tener acceso a un servicio de soporte t&eacute;cnico las 24 horas del d&iacute;a. Los usuarios podr&aacute;n acceder a las Preguntas Frecuentes y las PQRS sobre el portal web y la App de NETPROPI, de acuerdo con los T&eacute;rminos y Condiciones, la Pol&iacute;tica de Tratamiento de la Informaci&oacute;n y Protecci&oacute;n de Datos Personales, el Aviso de Privacidad y la Pol&iacute;tica de Navegaci&oacute;n - &laquo;Cookies&raquo;, de acuerdo con los medios que se destinan para tal efecto.</p>
		<p><br></p>
		<p>(viii) Notificaciones. En este apartado se recoger&aacute;n las notificaciones relacionadas con la actividad del usuario. Accediendo a los ajustes de las notificaciones, el usuario tendr&aacute; la posibilidad de personalizar las notificaciones por correo electr&oacute;nico, push o avisos.</p>
		<p><br></p>
		<p>IMPORTANTE. El usuario reconoce y acepta que el correcto funcionamiento de las distintas herramientas y funcionalidades que ofrece NETPROPI a trav&eacute;s del portal web y la App, dependen &uacute;nica y exclusivamente del diligenciamiento correcto de la informaci&oacute;n y la aceptaci&oacute;n de las condiciones de prestaci&oacute;n de los diferentes servicios y funcionalidades.</p>
		<p><br></p>
		<p>5) PROTECCI&Oacute;N DE DATOS PERSONALES</p>
		<p><br></p>
		<p>Los datos personales del usuario se tratar&aacute;n conforme se indica en la Pol&iacute;tica de Tratamiento de la Informaci&oacute;n y Protecci&oacute;n de Datos Personales del portal web www.netpropi.com y de la App, as&iacute; como en los diferentes textos de tratamiento de datos que se puedan facilitar al usuario en el apartado que corresponda. ZAPATA HOLDINGS / NETPROPI recolectar&aacute;, usar&aacute;, almacenar&aacute;, circular&aacute;, transmitir&aacute;, transferir&aacute; y, en general, realizar&aacute; el tratamiento de sus datos personales, en su calidad de responsable del tratamiento.</p>
		<p><br></p>
		<p>El tratamiento de sus datos personales se realizar&aacute;, en general, para (i) mejorar nuestro servicio; (ii) verificar la legalidad de la informaci&oacute;n; (iii) en lo posible realizar controles para evitar fraudes; (iv) tener una adecuada atenci&oacute;n al cliente; (v) publicidad, marketing y estudios de opini&oacute;n; (vi) ofrecer productos, servicios, descuentos, promociones, eventos, sorteos, entre otros; (vii) identificaci&oacute;n y autenticaci&oacute;n de su identidad; (viii) gesti&oacute;n de aplicaciones y redes sociales; (ix) enriquecimiento de nuestros datos; (x) evaluar h&aacute;bitos de navegaci&oacute;n; (xi) perfilamiento de nuestros clientes y visitantes; (xii) an&aacute;lisis de rendimiento; y (xiii) en general, desarrollar la actividad comercial de ZAPATA HOLDINGS / NETPROPI.</p>
		<p><br></p>
		<p>6) CONDICIONES PARTICULARES DE USO</p>
		<p>El usuario se obliga a hacer un buen uso del sitio web www.netpropi.com y de la App de NETPROPI, entendi&eacute;ndose por buen uso el que sea conforme con la legislaci&oacute;n vigente, buena fe y orden p&uacute;blico. Igualmente, se compromete a no usar el portal web ni la App con fines fraudulentos, asimismo a no llevar a cabo conducta alguna que pudiera da&ntilde;ar la imagen, los intereses y los derechos de ZAPATA HOLDINGS / NETPROPI o de terceros. El usuario se compromete a no realizar acto alguno con el objeto de da&ntilde;ar, inutilizar o sobrecargar el portal web o la App, o que impidiera, de cualquier forma, la normal utilizaci&oacute;n y funcionamiento del mismo.</p>
		<p><br></p>
		<p>El usuario conoce y acepta que, en el caso de que incumpla el contenido o las obligaciones que se derivan de las condiciones de uso del sitio web y/o de la App, o de cualesquiera otros t&eacute;rminos o condiciones generales y particulares, ZAPATA HOLDINGS / NETPROPI se reserva el derecho a limitar, suspender, expulsar o terminar su acceso a la plataforma (sitio web o App) sin reembolso alguno, adoptando cualquier medida t&eacute;cnica que sea necesaria con ese fin.</p>
		<p><br></p>
		<p>Igualmente, ZAPATA HOLDINGS / NETPROPI se reserva la posibilidad de ejercer tales medidas en el supuesto de que sospeche razonablemente que el usuario est&aacute; vulnerando cualquiera de los t&eacute;rminos y condiciones; y se reserva el derecho a actualizar, modificar o eliminar la informaci&oacute;n contenida en la plataforma, sitio web y/o App, incluyendo los contenidos y/o servicios que en &eacute;l se integran, pudiendo incluso limitar o no permitir el acceso a dicha informaci&oacute;n, en cualquier momento y sin previo aviso.</p>
		<p><br></p>
		<p>Especialmente, NETPROPI se reserva el derecho a eliminar, limitar o impedir el acceso a la plataforma portal web o App cuando surjan dificultades t&eacute;cnicas por hechos o circunstancias ajenos a NETPROPI que, a su criterio, disminuyan o anulen los niveles de seguridad o est&aacute;ndares adoptados para el adecuado funcionamiento de la misma. Para ese efecto, NETPROPI se reserva la facultad de, en todo momento, decidir sobre la continuidad de los servicios.</p>
		<p><br></p>
		<p>Condiciones particulares de las plazas del sitio web y/o la App. El usuario respecto de las siguientes plazas que ofrece NETPROPI, deber&aacute; tener en cuenta respecto de las mismas que:</p>
		<p><br></p>
		<p>Arrendatario. Si participa en una propuesta realizada por otro usuario es consciente de que el servicio de sitio web y/o App &uacute;nicamente consiste en una plataforma o herramienta de intermediaci&oacute;n o administraci&oacute;n de bienes y propiedades, donde dicha propuesta es responsabilidad exclusiva del usuario y no de ZAPATA HOLDINGS / NETPROPI. Contar&aacute; con la posibilidad de valorar tanto al interesado como a la propuesta, de modo que se compromete a hacerlo siempre de forma respetuosa, sincera y honesta.</p>
		<p><br></p>
		<p>Usuario que realiza la publicaci&oacute;n. Si la propuesta ha sido validada por el administrador de la plataforma e incluida, ser&aacute; el &uacute;nico y exclusivo responsable de la propuesta que se publique, as&iacute; como de la gesti&oacute;n y coordinaci&oacute;n que haga, exonerando y manteniendo indemnes a ZAPATA HOLDINGS / NETPROPI y a otros usuarios de cualquier responsabilidad que pudiera derivarse de la misma. En caso de utilizar contenido audiovisual sujeto a derechos de propiedad intelectual, garantiza disponer de los permisos y derechos necesarios para hacer uso del mismo dentro del servicio, teniendo especial consideraci&oacute;n de los derechos de los ni&ntilde;os, ni&ntilde;as y adolescentes. Respetar&aacute; las capacidades, disponibilidad, competencia y circunstancias del resto de los usuarios que participen en la misma. Gestionar&aacute; y coordinar&aacute; de la mejor manera posible dentro del marco de la plataforma, de modo que toda la informaci&oacute;n quede adecuadamente registrada y todo se realice dentro de la misma salvo que resulte imprescindible para el buen fin de la propuesta otro tipo de contacto. En ning&uacute;n caso recabar&aacute; los datos personales de los usuarios participantes que se apunten libremente ni los comunicar&aacute; a tercero alguno, teniendo la obligaci&oacute;n constitucional y legal de respetar la confidencialidad de la informaci&oacute;n y datos personales. Ser&aacute; responsabilidad del usuario seguir las instrucciones de participaci&oacute;n, completar los formularios y contactar con terceros, si as&iacute; fuese necesario. Igual que los usuarios que participen en su propuesta podr&aacute;n valorar, podr&aacute; valorar y puntuar a los usuarios participantes, pero se compromete a hacerlo siempre de forma respetuosa, sincera y honesta.</p>
		<p><br></p>
		<p>Chat. El chat ser&aacute; administrado por ZAPATA HOLDINGS / NETPROPI y el uso del mismo estar&aacute; regulado as&iacute;: la herramienta de chat se vincular&aacute; a cada una de las propuestas. El usuario se compromete a no utilizar esta herramienta de forma il&iacute;cita, ni publicar contenido inapropiado u ofensivo, ni la utilizar&aacute; para fines distintos ni ileg&iacute;timos, con respeto de los derechos de propiedad intelectual del sitio web y de la App de NETPROPI, incluyendo la estructura, selecci&oacute;n y disposici&oacute;n de los contenidos del mismo, el derecho sobre las bases de datos subyacentes, el dise&ntilde;o gr&aacute;fico e interfaz de usuario, los programas de ordenador subyacentes (incluidos los c&oacute;digos fuente y objeto), as&iacute; como los distintos elementos que integran el sitio web www.netpropi.com y la App, las marcas y nombres comerciales, que corresponden a ZAPATA HOLDINGS / NETPROPI y/o a sus licenciantes.</p>
		<p><br></p>
		<p>En ning&uacute;n caso se entender&aacute; que el acceso y uso del sitio web y la App de NETPROPI implica la renuncia, transmisi&oacute;n, licencia o cesi&oacute;n total o parcial de dichos derechos. Para tal efecto, mediante las presentes condiciones de uso, salvo en aquellos supuestos en los que est&eacute; legalmente permitido o medie previa autorizaci&oacute;n de ZAPATA HOLDINGS / NETPROPI, queda expresamente prohibido al usuario la reproducci&oacute;n, transformaci&oacute;n, distribuci&oacute;n, comunicaci&oacute;n p&uacute;blica, puesta a disposici&oacute;n, extracci&oacute;n y/o reutilizaci&oacute;n del portal web y de la App, sus contenidos y/o los signos distintivos de ZAPATA HOLDINGS / NETPROPI o de cualesquiera otros terceros incorporados en la plataforma.</p>
		<p><br></p>
		<p>ZAPATA HOLDINGS se reserva la posibilidad de ejercer las acciones legales que correspondan contra los usuarios que violen o infrinjan los derechos de propiedad intelectual. Queda igualmente prohibido modificar, copiar, reutilizar, explotar, reproducir, comunicar p&uacute;blicamente, hacer segundas o posteriores publicaciones, cargar archivos, enviar por correo, transmitir, usar, tratar o distribuir de cualquier forma la totalidad o parte de los contenidos incluidos si no se cuenta con la autorizaci&oacute;n expresa y por escrito de ZAPATA HOLDINGS / NETPROPI o en su caso, del titular de los derechos.</p>
		<p><br></p>
		<p>Asimismo los usuarios se comprometen a que el contenido utilizado o incluido por ellos mismos en el sitio web y la App, es de su propiedad o que cuentan con la autorizaci&oacute;n correspondiente de su titular, sin vulnerar ning&uacute;n derecho de terceros. El usuario suministrador de contenidos (textos, fotograf&iacute;as, contenido audiovisual, etc.) garantiza los derechos necesarios para su emplazamiento en el sitio web www.netpropi.com y la App, para su reproducci&oacute;n, uso, y comunicaci&oacute;n p&uacute;blica con el fin de que los dem&aacute;s usuarios puedan igualmente acceder a los mismos.</p>
		<p><br></p>
		<p>7) ENLACES (&laquo;LINKS&raquo;)</p>
		<p><br></p>
		<p>Sitios enlazados durante la navegaci&oacute;n y a fin de encontrar informaci&oacute;n adicional. El usuario podr&aacute; encontrar o incluir en determinadas partes diversos dispositivos t&eacute;cnicos de enlaces que permitan al usuario acceder a otras webs (los &laquo;Sitios Enlazados&raquo;). En estos casos, ZAPATA HOLDINGS / NETPROPI act&uacute;a como un prestador de servicios de intermediaci&oacute;n. De acuerdo con lo previsto</p>
		<p>NETPROPI no ser&aacute; responsable de los servicios y contenidos facilitados a trav&eacute;s de los Sitios Enlazados. El usuario dar&aacute; aviso inmediato a NETPROPI de la ilicitud de los Sitios Enlazados para proceder desactivando y/o eliminando el enlace con la debida diligencia.</p>
		<p><br></p>
		<p>En ning&uacute;n caso la existencia de Sitios Enlazados significan recomendaci&oacute;n, promoci&oacute;n, identificaci&oacute;n o conformidad de NETPROPI con las manifestaciones, contenidos o servicios proporcionados a trav&eacute;s de los Sitios Enlazados. En consecuencia, NETPROPI no se hace responsable del contenido de los Sitios Enlazados, ni de sus condiciones de uso y pol&iacute;ticas de privacidad, siendo el usuario el &uacute;nico responsable de comprobarlos y aceptarlos cada vez que acceda y use los mismos.</p>
		<p><br></p>
		<p>Comunicaci&oacute;n de actividades de car&aacute;cter il&iacute;cito e inadecuado. En el caso de que el usuario tuviera conocimiento de que cualquier clase de informaci&oacute;n o contenido en el sitio web www.netpropi.com y la App o facilitados a trav&eacute;s del mismo es il&iacute;cito, lesivo de derechos de terceros, nocivo, denigrante, violento, inadecuados, contrario a lo establecido en las presentes T&eacute;rminos y Condiciones, de cualquier otro modo, contrario a la moral, usos y buenas costumbres, podr&aacute; ponerse en contacto con NETPROPI indicando los siguientes datos: datos personales del comunicante: nombre, direcci&oacute;n, n&uacute;mero de contacto y correo electr&oacute;nico; descripci&oacute;n de los hechos que revelan el car&aacute;cter il&iacute;cito o inadecuado del contenido o informaci&oacute;n as&iacute; como la direcci&oacute;n concreta en la que se encuentra disponible.</p>
		<p><br></p>
		<p>En el supuesto de violaci&oacute;n de derechos de terceros, tales como propiedad intelectual, habr&aacute; de facilitarse los datos del titular del derecho infringido cuando sea persona distinta del comunicante. Asimismo, deber&aacute; aportar el t&iacute;tulo que acredite la titularidad de los derechos vulnerados y, en su caso, la representaci&oacute;n para actuar por cuenta del titular cuando sea persona distinta del comunicante.</p>
		<p><br></p>
		<p>La recepci&oacute;n por parte de ZAPATA HOLDINGS / NETPROPI de la comunicaci&oacute;n prevista en esta cl&aacute;usula no supondr&aacute;, el conocimiento efectivo de las actividades y/o contenidos indicados por el comunicante cuando ello no resulte notorio o evidente. En todo caso, ZAPATA HOLDINGS / NETPROPI se reserva el derecho de suspender o retirar los contenidos que, aun no siendo il&iacute;citos, resulten contrarios a las normas establecidas en estos T&eacute;rminos y condiciones, sopesando en cada caso los bienes jur&iacute;dicos en conflicto.</p>
		<p><br></p>
		<p>8) RESPONSABILIDAD DE ZAPATA HOLDINGS</p>
		<p><br></p>
		<p>El usuario debe saber que las comunicaciones a trav&eacute;s de redes abiertas est&aacute;n expuestas a una pluralidad de amenazas que hace que no sean seguras. Es responsabilidad del usuario tomar las medidas necesarias y adecuadas para controlar razonablemente estas amenazas y, entre ellas, el tener sistemas actualizados de detecci&oacute;n de software malicioso, tales como virus, troyanos, etc., as&iacute; como tener actualizados los parches de seguridad de los correspondientes navegadores.</p>
		<p><br></p>
		<p>Con el alcance m&aacute;ximo permitido por la legislaci&oacute;n aplicable, ZAPATA HOLDINGS / NETPROPI no se responsabiliza de los da&ntilde;os y perjuicios causados al usuario como consecuencia de riesgos inherentes al medio empleado, ni de los ocasionados por las vulnerabilidades en sus sistemas y herramientas.</p>
		<p><br></p>
		<p>ZAPATA HOLDINGS / NETPROPI no garantiza tampoco la total seguridad de sus sistemas y aunque tiene adoptadas medidas de seguridad adecuadas, no puede descartarse totalmente la existencia de vulnerabilidades y, en consecuencia, el usuario debe ser precavido en la interactuaci&oacute;n con el sitio web www.netpropi.com y la App.</p>
		<p><br></p>
		<p>ZAPATA HOLDINGS / NETPROPI no ser&aacute; responsable de:</p>
		<p><br></p>
		<p>a) Los da&ntilde;os y perjuicios de cualquier tipo causados en los equipos inform&aacute;ticos del usuario por virus, gusanos, troyanos o cualquier otro elemento da&ntilde;ino.</p>
		<p>b) Los da&ntilde;os y perjuicios de cualquier tipo causados en fallos o desconexiones en las redes de telecomunicaciones que produzcan la suspensi&oacute;n, cancelaci&oacute;n o interrupci&oacute;n del servicio el sitio web y la App durante la prestaci&oacute;n del mismo. El usuario reconoce que el acceso al sitio web y la App requiere de servicios suministrados por terceros ajenos al control de NETPROPI (operadores de</p>
		<p>redes de telecomunicaciones, proveedores de acceso, etc.) cuya fiabilidad, calidad, continuidad y funcionamiento no corresponden a NETPROPI, ni forma parte de su responsabilidad garantizar la disponibilidad del servicio.</p>
		<p>c) De que la informaci&oacute;n contenida en el sitio web y la App no responda a las expectativas del usuario. NETPROPI no responde de la veracidad, exactitud, suficiencia, integridad o actualizaci&oacute;n de las informaciones que no sean de elaboraci&oacute;n propia y de las que se indique otra fuente.</p>
		<p>d) Tampoco se hace responsable de las opiniones o comentarios que puedan aparecer en el sitio web y la App ya que, o bien podr&iacute;an ser realizadas por los usuarios a t&iacute;tulo personal, o bien provenir de las fuentes que se indican.</p>
		<p>e) De la informaci&oacute;n de terceros en los casos en los que NETPROPI act&uacute;e como prestador de servicios de intermediaci&oacute;n en el sentido dado por la norma legal.</p>
		<p>f) NETPROPI tampoco ser&aacute; responsable de los da&ntilde;os y perjuicios, directos o indirectos, que pudieran sufrir los usuarios por un uso inadecuado del sitio web y la App o de su contenido, tampoco compromiso alguno de comunicar cambios ni modificar el contenido del mismo.</p>
		<p><br></p>
		<p>9) USO DE COOKIES</p>
		<p><br></p>
		<p>NETPROPI utiliza el dispositivo virtual denominado &laquo;Cookies&raquo;. En la Pol&iacute;tica de Navegaci&oacute;n - &laquo;Cookies&raquo; est&aacute; disponible para el usuario la informaci&oacute;n detallada sobre c&oacute;mo utiliza NETPROPI las Cookies en sitio web www.netpropi.com y la App.</p>
		<p><br></p>
		<p>10) LEGISLACI&Oacute;N APLICABLE</p>
		<p><br></p>
		<p>Las leyes aplicables a los servicios que presta NETPROPI a trav&eacute;s del sitio web y la App, ser&aacute;n las de Colombia. Por tanto, en toda cuesti&oacute;n que corresponda al sitio web www.netpropi.com y la App o a cualesquiera conflictos en torno al mismo entre los usuarios y NETPROPI, ser&aacute; de aplicaci&oacute;n la legislaci&oacute;n colombiana. NETPROPI no asume responsabilidad por los contenidos que no se encuentren ajustados a las leyes de terceros pa&iacute;ses o a tratados o instrumentos internacionales no vinculantes para el Estado colombiano.</p>
		<p><br></p>
		<p>12) ACTIVIDAD REALIZADA POR NETPROPI</p>
		<p><br></p>
		<p>Portal web para la administraci&oacute;n de bienes y propiedades. En atenci&oacute;n a que la plataforma web y la App, es una herramienta y un espacio electr&oacute;nico para la administraci&oacute;n de bienes y propiedades de los usuarios o terceras personas ajenas a NETPROPI, con el fin que los usuarios/visitantes a la misma puedan contactar a los propietarios, corredores o cualquier otra forma de intermediarios para procurar la realizaci&oacute;n de negocios (arrendamiento, compraventa, etc.) sin tener inter&eacute;s en todo o parte, y sin estar vinculado a ninguna de las partes de la relaci&oacute;n jur&iacute;dica que entre estos pueda surgir, de acuerdo con la Ley 1480 de 2011 y su normatividad complementaria, el servicio prestado por NETPROPI trata de un portal de administraci&oacute;n y contacto, teniendo &uacute;nicamente como deberes a su cargo:</p>
		<p><br></p>
		<p>a) Exigir a todos los usuarios informaci&oacute;n que permita su identificaci&oacute;n, para lo cual se dise&ntilde;&oacute; un registro en el que consta, como m&iacute;nimo: nombre o raz&oacute;n social, documento de identificaci&oacute;n, correo electr&oacute;nico y direcci&oacute;n f&iacute;sica de notificaciones, y n&uacute;mero de contacto.</p>
		<p>b) Permitir la consulta de la informaci&oacute;n a quien acredite haber intervenido en la celebraci&oacute;n de negocios con los usuarios y/o aliados, con el prop&oacute;sito de presentar una queja o reclamo.</p>
		<p><br></p>
		<p>13) PROHIBICIONES DEL PORTAL WEB</p>
		<p><br></p>
		<p>El sitio web y la App solo podr&aacute; ser utilizado para prop&oacute;sitos que se adecuen a los T&eacute;rminos y Condiciones. NETPROPI proh&iacute;be el uso de su plataforma para:</p>
		<p>a) Incluir en la plataforma cualquier informaci&oacute;n falsa, err&oacute;nea, inexacta o que de cualquier otra manera no corresponda con la realidad, induciendo en error a los usuarios, visitantes y/o aliados sobre el producto a ofrecer o vender a trav&eacute;s de este medio.</p>
		<p>b) Utilizar el sitio web y la App para difundir u ofrecer cualquier franquicia, esquema de pir&aacute;mide, membres&iacute;a a clubes o grupos, representaci&oacute;n, mandato o corretaje en venta de bienes,</p>
		<p>oportunidades de negocio que requiera pagos anticipados o pagos peri&oacute;dicos en cualquier tiempo o solicitando la vinculaci&oacute;n de terceras personas a esquemas piramidales de negocio o la captaci&oacute;n masiva de dineros del p&uacute;blico sin la autorizaci&oacute;n de la entidad competente.</p>
		<p>c) Utilizar el portal web para realizar actividades o la comisi&oacute;n de conductas relacionadas con los delitos de lavado de activos o financiaci&oacute;n del terrorismo, o aquellos conexos con estos.</p>
		<p>d) Eliminar o modificar de cualquier forma o comercializar cualquier material publicado en el portal web por cualquiera otra persona o entidad sin la debida autorizaci&oacute;n.</p>
		<p>e) Usar cualquier elemento, dise&ntilde;o, software o herramienta para interferir con el correcto funcionamiento de la p&aacute;gina web y la App o la modificaci&oacute;n de la misma.</p>
		<p>f) Irrumpir, corromper, trascribir, compilar o desarticular cualquier funcionalidad, herramienta software o sistema que sea utilizado en y por el sitio web y la App.</p>
		<p>g) Incluir en el sitio web o App im&aacute;genes, informaci&oacute;n o contenido cualesquiera que sea su naturaleza que pueda ser considerada como pornogr&aacute;fica, y que incluyan material sexual expl&iacute;cito o que sea pornograf&iacute;a infantil en los t&eacute;rminos del Decreto 1524 de 2002, o cualquier otra disposici&oacute;n legal o reglamentaria que regule la materia.</p>
		<p><br></p>
		<p>14) REGISTRO DEL USUARIO Y CLAVE DE ACCESO (PASSWORD)</p>
		<p><br></p>
		<p>Toda persona natural o jur&iacute;dica que desee utilizar los servicios de NETPROPI para administrar sus bienes y propiedades deber&aacute; registrarse en la p&aacute;gina web www.netpropi.com llenando los campos del formulario con la informaci&oacute;n establecida en los T&eacute;rminos y Condiciones.</p>
		<p><br></p>
		<p>El nombre de usuario y su clave de acceso o password es de uso personal e intransferible. Todo usuario/visitante ser&aacute; responsable de forma directa, exclusiva y excluyente por la confidencialidad de su nombre de usuario y contrase&ntilde;a y su destinaci&oacute;n.</p>
		<p><br></p>
		<p>El usuario notificar&aacute; a NETPROPI de forma inmediata cuando conozca que su nombre de usuario y/o clave de acceso han sido alterados o terceros se encuentran haciendo uso de ella sin su consentimiento.</p>
		<p><br></p>
		<p>NETPROPI presume de buena fe que la informaci&oacute;n diligenciada por el usuario en su registro al sitio web y la App corresponde a su propia identidad o a la de un tercero con la debida representaci&oacute;n. La verificaci&oacute;n previa y autom&aacute;tica que realiza NETPROPI se ce&ntilde;ir&aacute; estrictamente al suministro de la totalidad de la informaci&oacute;n para que los dem&aacute;s usuarios y/o visitantes tengan razonable confiabilidad sobre las operaciones que pretendan realizar.</p>
		<p><br></p>
		<p>Ser&aacute; deber de los usuarios, de acuerdo a las reglas de la l&oacute;gica y/o de la experiencia, valorar razonablemente las propuestas a fin de formarse un consentimiento serio sobre la operaci&oacute;n que pretende realizar.</p>
		<p><br></p>
		<p>Si un usuario tiene una contrase&ntilde;a que le permita el acceso a un &aacute;rea no p&uacute;blica de esta plataforma, no podr&aacute; revelarla o compartirla con terceros ni usarla para prop&oacute;sitos no autorizados.</p>
		<p><br></p>
		<p>15) DERECHOS DEL CONSUMIDOR</p>
		<p><br></p>
		<p>Garant&iacute;a. Cuando los servicios ofrecidos no sean prestados o se ejecuten evidenciando fallas o deficiencias, el usuario deber&aacute; informar a ZAPATA HOLDINGS / NETPROPI el defecto presentado y la fecha.</p>
		<p><br></p>
		<p>El usuario podr&aacute; elegir, bien que NETPROPI preste nuevamente el mismo servicio de acuerdo con las condiciones ofrecidas y sin ning&uacute;n tipo de cargo adicional, o que proceda con la devoluci&oacute;n del dinero por el mismo medio en el que se realiz&oacute; el pago.</p>
		<p><br></p>
		<p>En caso que el usuario elija la devoluci&oacute;n del dinero, NETPROPI solicitar&aacute; los documentos requeridos por las entidades financieras y/o empresas de procesamiento de pagos electr&oacute;nicos, con el objetivo de adelantar la correspondiente gesti&oacute;n. NETPROPI dispondr&aacute; de quince (15) d&iacute;as h&aacute;biles despu&eacute;s de recibida la solicitud para realizar la devoluci&oacute;n y/o reversi&oacute;n del dinero pagado.</p>
		<p><br></p>
		<p>No obstante, NETPROPI y el usuario podr&aacute;n hacer uso del arreglo directo o cualquiera de los mecanismos alternativos de resoluci&oacute;n de conflictos, a fin de continuar la prestaci&oacute;n del servicio reparando la falta o falla.</p>
		<p>Retracto. Todo usuario que solicite los servicios de NETPROPI a trav&eacute;s de la p&aacute;gina web www.netpropi.com y la App, tendr&aacute; cinco (5) d&iacute;as h&aacute;biles contados a partir la aceptaci&oacute;n de los T&eacute;rminos y Condiciones para retractarse de la misma, siempre que el servicio que pretenda deshacer no haya empezado a ejecutarse. Verificado los requisitos para el ejercicio del derecho de retracto, NETPROPI dispondr&aacute; de quince (15) d&iacute;as h&aacute;biles para el reintegro del dinero que el usuario haya pagado.</p>
		<p><br></p>
		<p>16) COMPLEMENTO</p>
		<p><br></p>
		<p>NETPROPI no garantiza que las publicaciones, textos, im&aacute;genes o contenidos sean accesibles y/o visibles en pa&iacute;ses distintos a Colombia, o que tal material se encuentre de acuerdo a leyes distintas.</p>
		<p><br></p>
		<p>De presentarse una decisi&oacute;n administrativa y/o judicial que declare la nulidad, invalidez o ineficacia de alguna de las disposiciones de estos T&eacute;rminos y Condiciones, no se afectar&aacute; la subsistencia y la producci&oacute;n de efectos jur&iacute;dicos de las dem&aacute;s declaraciones aqu&iacute; contenidas.</p>
		<p><br></p>
		<p>NETPROPI no tiene la obligaci&oacute;n legal o contractual para inspeccionar las publicaciones, ofertas o bienes publicados, con excepci&oacute;n de lo establecido en estos T&eacute;rminos y Condiciones. Asimismo, NETPROPI no tiene participaci&oacute;n o injerencia alguna en los actos o negocios que se celebren entre los usuarios o usuarios y visitantes de este portal web. La aceptaci&oacute;n y celebraci&oacute;n de actos o negocios jur&iacute;dicos por parte de usuarios o visitantes solamente obliga entre s&iacute; de acuerdo con las declaraciones de voluntad que estos realicen.</p>
		<p><br></p>
		<p>13) PRUEBA GRATIS</p>
		<p><br></p>
		<p>NETPROPI proporcionar&aacute; una prueba de uso totalmente gratuita que se limitar&aacute; al registro de una (1) propiedad, por el t&eacute;rmino de diez (10) d&iacute;as calendario. Los d&iacute;as de prueba gratis iniciaran a partir de la confirmaci&oacute;n de la creaci&oacute;n de la cuenta y finalizando el t&eacute;rmino, NETPROPI comunicar&aacute; al usuario por el medio electr&oacute;nico que haya suministrado, el d&iacute;a de finalizaci&oacute;n de la prueba de uso gratuita y los precios de los paquetes, planes y/o membres&iacute;a para continuar con el uso de la plataforma.</p>
		<p><br></p>
		<p>El usuario RECONOCE Y ACEPTA las declaraciones de ZAPATA HOLDINGS / NETPROPI anteriores y adicionalmente, GARANTIZA el cumplimiento de la normativa que le sea de aplicaci&oacute;n en su condici&oacute;n de usuario/visitante del sitio web www.netpropi.com y la App.</p>
    </div>
		
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/terminos');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>