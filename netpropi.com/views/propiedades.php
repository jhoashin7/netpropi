<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
?>
<!doctype html>
<html>
	<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
	<style>
		#mapa{
			min-height:700px;
			min-width:300px;
		}
	</style>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">
		<?php if($pais){ ?>
		<div class="bg-danger py-2 ">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-6 col-md-8 col-xl-6 text-start">
					<div class="input-group">
						<select class="form-select filtro_tipo" aria-label="Tipo">
                            <option value="">Tipo</option>
							<option value="1">Comercial</option>
							<option value="2">Vivienda</option>                            
                        </select>
						<select class="form-select filtro_fin" aria-label="Fin">
                            <option value="">Fin</option>
							<option value="1">Venta</option>
							<option value="2">Alquiler</option>                            
                        </select>
					</div>
				</div>
				<div class="col-6 col-md-4 col-xl-4 text-end">
					<div class="btn-group" role="group" aria-label="Busqueda">
						<!--<button type="button" class="btn btn-light text-danger fw-bold"><i class="fas icon-sliders fa-fw fa-lg"></i> Avanzada</button>-->
						<button type="button" class="btn btn-outline-danger border border-white text-white fw-bold" onClick="buscarPropb(); return false">Buscar <i class="fas icon-search fa-fw fa-lg"></i></button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?php
					$prop_array = array();
					$consulta = "SELECT prop.id, prop.nombre, prop.tipo, prop.fin, prop.valor, cam.moneda, prop.ubicacion, prop.habitaciones, prop.banos, prop.area, prop.caracteristicas, prop.facilidades, prop.foto FROM propiedades AS prop LEFT JOIN cambio AS cam ON (cam.id = prop.moneda_valor) WHERE prop.estado = 1 AND prop.id_pais = ".$pais." ORDER BY id ASC";
					if ( $result = $mysqli->query( $consulta ) ) {
						while($row = $result->fetch_assoc()){
							$foto = explode(',', $row['foto'])[0];
							$row['nombre'] =  html_entity_decode($row['nombre'], ENT_QUOTES, "UTF-8");
							$row['foto'] = $foto;
							$row['valor2'] = number_format($row['valor'], 2, ',', '.');
							$row['area2'] = number_format($row['area'], 0, ',', '.');
							array_push($prop_array, $row);
					?>
					<div class="card mb-3 col-md-12"  onClick="loader('propiedad', {'iden': <?php echo $row['id'] ?>}); return false" style="cursor: pointer;" >
						<div class="row g-0">
							<div class="col-md-5">
								<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-image: url(propiedades/<?php echo $foto ?>); background-repeat: no-repeat; background-position: center center; background-size: cover"></div>
							</div>
							<div class="col-md-7">
								<div class="card-body">
									<h5 class="card-title"><?php echo $row['nombre'] ?></h5>
									<p class="card-text">Tipo: <?php echo ($row['tipo'] == 1)?'Local Comercial':'Vivienda' ?></p>
									<p class="card-text"><?php echo ($row['fin'] == 1)?'En Venta':'En Alquiler' ?>: <span class="text-danger fw-bold">$ <?php echo number_format($row['valor'], 2, ',', '.') ?> (<?php echo $row['moneda'] ?>)</p>
									<p class="card-text">Área: <?php echo number_format($row['area'], 0, ',', '.') ?> m²</p>
								</div>
							</div>
						</div>
					</div>
				<?php                      
					}
					$result->close();
				}						
				?>
			</div>
			<div class="col-sm-6 position-fixed end-0" style="min-width:300px;min-height:700px"> 
				<div class="bg-white" style="min-width:300px;min-height:700px">
					<div class="row w-100 mx-0 justify-content-center align-items-center" style="min-width:300px;min-height:700px">
						<div class="col-12 text-start px-0" style="min-height:min-width:300px;700px">
							<div id="map_cont" class="ratio ratio-21x9 overflow-hidden d-block" style="min-width:300px;min-height:700px">
								<div id="mapa" class="w-100" style="min-width:300px;min-height:700px" ></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		
		<div class="modal fade" id="mod-showPropMod" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">
							<span class="text-responsive d-flex justify-content-start align-items-center">
								<span class="fa-stack text-danger align-top">
									<i class="fas fa-circle fa-stack-2x"></i>
									<i class="fas icon-home-outline fa-stack-1x text-white"></i>
								</span>                                
								<span class="lh-1 titulo"></span>
							</span>
							<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
								<i class="fas fa-times-circle fa-lg fa-fw"></i>						
							</button>
						</h5>                    
					</div>
					<div class="modal-body text-start">						
						<div class="row w-100 mx-0 justify-content-center align-items-center">
							<div class="col-10 py-2">
								<div class="ratio ratio-1x1 bg-white overflow-hidden d-block overflow-hidden text-decoration-none">
									<div class="d-flex w-100 h-100 justify-content-center align-items-center foto" style="background-image: url(''); background-repeat: no-repeat; background-position: center center; background-size: cover"></div>
								</div>
								<div class="py-2 text-start">									
									<small class="d-block"><span class="text-muted fw-bold tipo"></span></small>
									<small class="d-block"><span class="text-muted fw-bold fin"></span> <span class="text-danger fw-bold valor"></span></small>	
									<small class="d-block"><span class="text-muted fw-bold area"></span></small>
								</div>
								<div class="d-grid gap-3">
									<button class="btn btn-danger text-white rounded-pill fw-bold text-decoration-none btn_prop" onClick="return false">Ver detalle</button>
								</div>
							</div>
						</div> 
					</div>                
				</div>
			</div>
		</div>
		<script>
			var prop_arr = <?php echo json_encode($prop_array) ?>;
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/propiedades');
					initMapa();
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});
						bounds = new google.maps.LatLngBounds();
						var cuenta = 0;
						$.each( prop_arr, function( key, value ) {
							var posimaker = value.ubicacion.split(',');
							var finalLatLng = new google.maps.LatLng(parseFloat(posimaker[0]),parseFloat(posimaker[1]));
                            var finalLatLng2 = finalLatLng;                            
							marcadores[cuenta] = new google.maps.Marker({
								map: map,
								title: value.nombre,
								zIndex: 100,
								icon: 'images/marcador.png',
								position: finalLatLng
							});							
							var posia = cuenta;
							var idenp = value.id;
							marcadores[posia].addListener('click', function() {
								openmProp(idenp);
                            });
							bounds.extend(marcadores[cuenta].position);
							cuenta++;
							
						});
						if(cuenta > 0){
							map.fitBounds(bounds);
						}
					});
					loaderHide();
				});				
            });
			function openmProp(iden){
				$.each( prop_arr, function( key, value ) {
					if(value.id == iden){
						$('#mod-showPropMod .modal-title .titulo').html(value.nombre);
						$('#mod-showPropMod .modal-body .foto').css('background-image', 'url(propiedades/'+value.foto+')');
						if(value.tipo == 1){
							$('#mod-showPropMod .modal-body .tipo').html('Local Comercial');
						}else{
							$('#mod-showPropMod .modal-body .tipo').html('Vivienda');
						}
						if(value.fin == 1){
							$('#mod-showPropMod .modal-body .tipo').html('En Venta:');
						}else{
							$('#mod-showPropMod .modal-body .tipo').html('En Alquiler:');
						}
						$('#mod-showPropMod .modal-body .valor').html('$ '+value.valor2+' ('+value.moneda+')');
						$('#mod-showPropMod .modal-body .area').html('Área: '+value.area2+' m²');
						$('#mod-showPropMod .modal-body .btn_prop').attr('onClick', "closeActModal('loader', ['propiedad', {'iden': "+iden+"}]); return false");
						return false;
					}
				});
				$('#mod-showPropMod').modal('show');				
			}
			function buscarPropb(){
				loaderShow();
				$('.propiedades_cont .prop_cont').hide();
				var array_nuevo = [];
				$.each( prop_arr, function( key, value ) {
					var ison = true;
					if($('.filtro_tipo').val() != ''){
						ison = ($('.filtro_tipo').val() == value.tipo)?true:false;						
					}
					if($('.filtro_fin').val() != '' && ison){
						ison = ($('.filtro_fin').val() == value.fin)?true:false;
					}
					if(ison){
						array_nuevo.push(value.id);
					}
				});
				$.each( array_nuevo, function( key, value ) {
					$('.propiedades_cont .prop_'+value).show();
				});
				if(marcadores.length > 0){			
                    for (var i = 0; i < marcadores.length; i++) {
                        try{					
                            marcadores[i].setMap(null);
                        }catch(e){						
                        }					
                    }
                    marcadores = [];					
                }
				bounds = new google.maps.LatLngBounds();
                var cuenta = 0;
                $.each( prop_arr, function( key, value ) {
                    if($.inArray(value.id, array_nuevo) >= 0){
                        var posimaker = value.ubicacion.split(',');
                        var finalLatLng = new google.maps.LatLng(parseFloat(posimaker[0]),parseFloat(posimaker[1]));
                        var finalLatLng2 = finalLatLng;                            
                        marcadores[cuenta] = new google.maps.Marker({
                            map: map,
                            title: value.nombre,
                            zIndex: 100,
                            icon: 'images/marcador.png',
                            position: finalLatLng
                        });							
                        var posia = cuenta;
                        var idenp = value.id;
                        marcadores[posia].addListener('click', function() {
                            openmProp(idenp);
                        });
                        bounds.extend(marcadores[cuenta].position);
                        cuenta++;
                    }
                });
                if(cuenta > 0){
                    map.fitBounds(bounds);
                }
				loaderHide();
				
			}			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>