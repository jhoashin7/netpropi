<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">		
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start">
					<div id="acordion_preguntas" class="accordion">
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-1">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-1" aria-expanded="false" aria-controls="pregunta-1">
									<strong>¿Cómo me suscribo?</strong>
								</button>
							</h2>
							<div id="pregunta-1" class="accordion-collapse collapse" aria-labelledby="headpregunta-1" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>Si eres propietario ve al botón de Iniciar sesión y crea tu cuenta si aún no la tienes, llenas los campos con tus datos e información de tu propiedad. Recuerda que mientras más información y detalles tenga tu propiedad tendrás mejores resultados.</p>
									<p>si estás buscando propiedad, ve al buscador, filtra según tu interés. ¡que comience la búsqueda! al seleccionar la propiedad de tu interés podrás diligenciar un formulario y tus datos irán inmediatamente al propietario y así podrás finalizar la negociación directamente.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-2">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-2" aria-expanded="false" aria-controls="pregunta-2">
									<strong>¿Cuánto dura la suscripción?</strong>
								</button>
							</h2>
							<div id="pregunta-2" class="accordion-collapse collapse" aria-labelledby="headpregunta-2" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>Tienes el beneficio o plus de recibir 10 días gratis para publicar tu propiedad  y  luego comienzas a realizar pagos mensuales, el valor dependerá de la cantidad de Inmuebles que deseen suscribir.</p>
									<p>NO hay contrato de permanencia puedes ser parte de NetPropi el tiempo que quieras.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-3">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-3" aria-expanded="false" aria-controls="pregunta-3">
									<strong>¿Hay pagos a terceros?</strong>
								</button>
							</h2>
							<div id="pregunta-3" class="accordion-collapse collapse" aria-labelledby="headpregunta-3" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>En NetPropi no realizas pagos a terceros por publicar tu propiedad, con la suscripción puedes acceder a  todos los beneficios del sitio web.</p>
									<p>Haciendo claridad que el apoyo jurídico y el servicio de Aliados son Contratos personales fuera de la plataforma.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-4">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-4" aria-expanded="false" aria-controls="pregunta-4">
									<strong>Como suscriptor, ¿a qué tengo derecho?</strong>
								</button>
							</h2>
							<div id="pregunta-4" class="accordion-collapse collapse" aria-labelledby="headpregunta-4" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>Por el pago de tu suscripción tienes derecho a:</p>
									<ul>
										<li>Publicar tu propiedad por un mes en NetPropi y editar la información las veces que creas necesario.</li>
										<li>Acceder a un dashboard donde podrás encontrar toda la información asociada a tu propiedad (visitas, interesados, pagos próximos a vencer, métricas).</li>
										<li>Puedes escoger con cuál de las personas interesadas en tu propiedad finalizas la negociación.</li>
										<li>A través de la plataforma puedes tener contratos civiles para finalizar tu negociación con total validez jurídica.</li>
										<li>A través de la plataforma puedes encontrar tu mejor aliado para reformas, arreglos y  mantenimiento en tu propiedad, con negociación directa. Hemos construido una base de datos de profesionales a un clic.</li>
										<li>A través de la plataforma puedes llegar a una comunidad amplia y calificada que está en búsqueda de tu propiedad.</li>
									</ul>									
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-5">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-5" aria-expanded="false" aria-controls="pregunta-5">
									<strong>¿Qué son los asociados?</strong>
								</button>
							</h2>
							<div id="pregunta-5" class="accordion-collapse collapse" aria-labelledby="headpregunta-5" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>En el sitio web encontrarás el botón de Asociados, en el cual habrá un sin fin de servicios para tu hogar, tales como: Cerrajería, Jardinería, Construcción, electricidad entre otros.</p>
									<p>Empresas y Personal Calificado que tu puedes escoger según su perfil y calificación por los demás usuarios del sitio web, este atenderá en el menor tiempo posible tu necesidad y el contrato y cobros se realizan de manera personal.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-6">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-6" aria-expanded="false" aria-controls="pregunta-6">
									<strong>¿Cómo pago?</strong>
								</button>
							</h2>
							<div id="pregunta-6" class="accordion-collapse collapse" aria-labelledby="headpregunta-6" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>Puedes realizar  tus pagos a través de stripe, una plataforma 100% segura con cobertura en todos los países del mundo, podrás hacer pagos con tarjetas crédito y débito.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-7">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-7" aria-expanded="false" aria-controls="pregunta-7">
									<strong>¿Quién puede ver mi propiedad?</strong>
								</button>
							</h2>
							<div id="pregunta-7" class="accordion-collapse collapse" aria-labelledby="headpregunta-7" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>Una vez crees el perfil de tu propiedad y le des clic en el botón publicar, tu propiedad estará pública durante el tiempo de tu suscripción para todas las personas que entren a nuestra plataforma.</p>
									<p>En NetPropi activaremos todos nuestros canales para aumentar las visitas a las propiedades, aquí le llegaremos a un público altamente calificado para que tu propiedad se venda o se arriende en el menor tiempo posible.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-8">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-8" aria-expanded="false" aria-controls="pregunta-8">
									<strong>¿Quién respalda mis pagos como propietario?</strong>
								</button>
							</h2>
							<div id="pregunta-8" class="accordion-collapse collapse" aria-labelledby="headpregunta-8" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>En Netpropi tenemos una pasarela de pagos que cuenta con certificación que avala el cumplimiento de los estándares más exigentes de seguridad y tecnología en cada transacción.</p>
									<p>Cuenta con:</p>
									<ul>
										<li>Más de 135 divisas y métodos de pago aceptados.</li>
										<li>Más de 35 países en los que tenemos acuerdos con entidades adquirentes locales, lo que incrementa las tasas de aceptación.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headpregunta-9">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#pregunta-9" aria-expanded="false" aria-controls="pregunta-9">
									<strong>¿Tengo apoyo jurídico?</strong>
								</button>
							</h2>
							<div id="pregunta-9" class="accordion-collapse collapse" aria-labelledby="headpregunta-9" data-bs-parent="#acordion_preguntas">
								<div class="accordion-body">
									<p>En el Sitio Web de NetPropi encontrarás un botón de Apoyo Jurídico, en el cual se tiene contacto con los Abogados de la empresa, profesionales capacitados en cada área del derecho que te  asesorar y brindara todas las herramientas y soluciones tanto para contratos como posibles litigios que se puedan llegar a presentar.</p>
								</div>
							</div>
						</div>						
					</div>
				</div>				
			</div>
		</div>		
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/preguntas_frecuentes');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>