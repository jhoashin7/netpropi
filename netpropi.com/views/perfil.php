<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
$iden = (isset($getvars[ 'iden' ]) && $getvars[ 'iden' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'iden' ] ) ):false;
$puede = false;
if($id){
	$id_veri = explode('***', simple_crypt( $id, 'd', $conArr['enc_string'] ));
	if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
		$consulta = "SELECT id, rol, relacion FROM admins WHERE id = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
			$usrm = mysqli_fetch_object( $result );
			$result->close();
		}
		$puede = true;    
    }else{
		$puede = false;
    }	
}
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">
		<?php
        if($puede){
        ?>
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-12 text-end">
							<div class="btn-group" role="group" aria-label="Busqueda">
								<button type="button" class="btn btn-danger border text-white fw-bold" onClick="openData('EdtAcc', <?php echo $id_veri[1] ?>); return false"><i class="fas fa-fingerprint fa-fw fa-lg"></i> Datos de acceso</button>
								<?php
								if($usrm->rol == 5){
								?>
								<button type="button" class="btn btn-outline-danger fw-bold" onClick="openData('EdtUsr', <?php echo $usrm->relacion ?>); return false"><i class="fas icon-user fa-fw fa-lg"></i> Datos personales</button>
								<?php	
								}
								?>								
								<button type="button" class="btn btn-danger border text-white fw-bold" onClick="logOut(); return false">Salir <i class="fas icon-x-circle fa-fw fa-lg"></i></button>
							</div>
						</div>						
					</div>
				</div>																
			</div>
		</div>
		<div class="gray-300 py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-12 text-start">
							<h4 class="m-0 fw-bold text-danger"><span class="text-responsive"><i class="fas fa-star fa-fw"></i> Mis Propiedades Favoritas</span></h4>
						</div>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-start pb-3 px-0">					
					<div class="row w-100 mx-0 justify-content-center align-items-stretch propiedades_cont">
						<?php
						$prop_array = array();
                        $consulta = "SELECT prop.id, prop.nombre, prop.tipo, prop.fin, prop.valor, cam.moneda, prop.ubicacion, prop.habitaciones, prop.banos, prop.area, prop.caracteristicas, prop.facilidades, prop.foto FROM fav_prop AS fav LEFT JOIN propiedades AS prop ON (fav.id_propiedad = prop.id) LEFT JOIN cambio AS cam ON (cam.id = prop.moneda_valor) WHERE prop.estado = 1 AND fav.id_user = ".$id_veri[1]." ORDER BY id ASC";
                        if ( $result = $mysqli->query( $consulta ) ) {
                            while($row = $result->fetch_assoc()){
								$foto = explode(',', $row['foto'])[0];
								$row['nombre'] =  html_entity_decode($row['nombre'], ENT_QUOTES, "UTF-8");
								$row['foto'] = $foto;
								$row['valor2'] = number_format($row['valor'], 2, ',', '.');
								$row['area2'] = number_format($row['area'], 0, ',', '.');
								array_push($prop_array, $row);
						?>
						<div class="col-4 col-md-3 col-xl-3 text-start pb-3 prop_cont prop_<?php echo $row['id'] ?>">
							<div class="d-block w-100 h-100 rounded-3 gray-100 border border-light p-3">
								<a href="#" class="ratio ratio-1x1 bg-white overflow-hidden d-block overflow-hidden text-decoration-none" onClick="loader('propiedad', {'iden': <?php echo $row['id'] ?>}); return false">
									<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-image: url(propiedades/<?php echo $foto ?>); background-repeat: no-repeat; background-position: center center; background-size: cover"></div>
								</a>
								<div class="py-2 text-start">
									<p class="mb-2 fw-bold text-dark lh-1"><span class="text-responsive"><?php echo $row['nombre'] ?></span></p>
									<small class="d-block"><span class="text-muted fw-bold"><?php echo ($row['tipo'] == 1)?'Local Comercial':'Vivienda' ?>.</span></small>
									<small class="d-block"><span class="text-muted fw-bold"><?php echo ($row['fin'] == 1)?'En Venta':'En Alquiler' ?>:</span> <span class="text-danger fw-bold">$ <?php echo number_format($row['valor'], 2, ',', '.') ?> (<?php echo $row['moneda'] ?>)</span></small>	
									<small class="d-block"><span class="text-muted fw-bold">Área: <?php echo number_format($row['area'], 0, ',', '.') ?> m²</span></small>
								</div>
								<div class="d-grid gap-3">
									<button class="btn btn-danger text-white rounded-pill fw-bold" onClick="loader('propiedad', {'iden': <?php echo $row['id'] ?>}); return false">Ver detalle</button>
								</div>
							</div>														
						</div>
						<?php                      
                            }
                            $result->close();
                        }						
						?>
					</div>
				</div>
			</div>
		</div>		
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-12 text-start">
							<h4 class="m-0 fw-bold text-danger"><span class="text-responsive"><i class="fas icon-file-text fa-fw"></i> Mis Contratos Completados</span></h4>
						</div>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-stretch aliados_cont">
						<?php						
                        $consulta = "SELECT con.id, con.archivo, con.fecha, cons.nombre FROM contrato AS con LEFT JOIN contratos AS cons ON (cons.id = con.id_contrato) WHERE con.estado = 2 AND id_user = ".$id_veri[1]." ORDER BY fecha ASC";
                        if ( $result = $mysqli->query( $consulta ) ) {
                            while($row = $result->fetch_assoc()){
						?>
						<div class="col-4 col-md-3 col-xl-3 text-start pb-3 us_cont">
							<a href="contratos/<?php echo $row['archivo'] ?>" class="d-block w-100 h-100 rounded-3 gray-100 border border-light p-3 text-decoration-none" target="_blank">
								<div class="ratio ratio-1x1 bg-white overflow-hidden d-block overflow-hidden rounded-circle">
									<div class="d-flex w-100 h-100 justify-content-center align-items-center"><span class="text-responsive"><i class="fas icon-file-text fa-6x text-muted"></i></span></div>
								</div>
								<div class="py-2 text-center">
									<p class="m-0 fw-bold text-danger lh-1"><span class="text-responsive"><?php echo utf8_encode($row['nombre']) ?></span></p>
									<small class="text-muted"><?php echo $row['fecha'] ?></small>
								</div>
							</a>														
						</div>
						<?php                      
                            }
                            $result->close();
                        }
						?>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-center pb-3">
					<p class="mb-3 text-muted"><span class="text-responsive">Si estás interesado en adquirir uno de nuestros modelos de contrato, puedes hacerlo desde tu panel control accediendo con tu usuario y contraseña.</span></p>
					<a href="<?php echo $conArr['base_url'] ?>" class="btn btn-danger text-white rounded-pill fw-bold text-decoration-none" target="_blank"><i class="fas icon-sliders fa-fw fa-lg"></i> Panel de control</a>
				</div>												
			</div>
		</div>
		<div class="bg-white">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				
							
											
			</div>
		</div>
		<?php	
        }
        ?>
		<div class="modal fade" id="mod-EdtAcc" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">
							<span class="text-responsive d-flex justify-content-start align-items-center">
								<span class="fa-stack text-danger align-top">
									<i class="fas fa-circle fa-stack-2x"></i>
									<i class="fas fa-fingerprint fa-stack-1x text-white"></i>
								</span>                                
								<span>Datos de Acceso</span>
							</span>
							<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
								<i class="fas fa-times-circle fa-lg fa-fw"></i>						
							</button>
						</h5>                    
					</div>
					<div class="modal-body text-start">					
						<form id="form-EdtAcc">
							<input type="hidden" class="id" name="id" value="0"/>
							<input type="hidden" class="db noclear" name="db" value="admins" />
							<div class="form-group pb-3">
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas icon-user fa-fw"></i></span>
									<input type="text" name="usuario" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" data-parsley-type="alphanum" required>
								</div>
							</div>
							<div class="form-group pb-3">
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas fa-fingerprint fa-fw"></i></span>
									<input type="password" autocomplete="new-password" name="contrasena" class="form-control" placeholder="Contraseña" aria-label="Contraseña" data-parsley-minlength="6" data-parsley-type="alphanum" data-parsley-pattern="^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$" data-parsley-pattern-message="La contraseña debe contener dígitos y letras" required>
								</div>
							</div>
						</form>					
					</div>
					<div class="modal-footer">
						<div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
							<button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-danger text-white" onClick="Valform('form-EdtAcc', reloader, [], true); return false"><span class="text-responsive">guardar <i class="fas iicon-check-circle fa-fw"></i></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="mod-EdtUsr" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">
							<span class="text-responsive d-flex justify-content-start align-items-center">
								<span class="fa-stack text-danger align-top">
									<i class="fas fa-circle fa-stack-2x"></i>
									<i class="far icon-user fa-stack-1x text-white"></i>
								</span>                                
								<span>Datos Personales</span>
							</span>
							<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
								<i class="fas fa-times-circle fa-lg fa-fw"></i>						
							</button>
						</h5>                    
					</div>
					<div class="modal-body text-start">					
						<form id="form-EdtUsr">
							<input type="hidden" class="id" name="id" value="0"/>
							<input type="hidden" class="db noclear" name="db" value="usuarios" />					
							<div class="form-group pb-3">
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas icon-user fa-fw"></i></span>
									<input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
								</div>
							</div>
							<div class="form-group pb-3 cont_reg req">
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas icon-mail fa-fw"></i></span>
									<input type="email" name="email" class="form-control email" placeholder="E-mail" aria-label="E-mail" data-parsley-type="email" required>
								</div>
							</div>
							<div class="form-group pb-3 cont_reg">
								<label><small class="text-muted d-block lh-1">Si estás interesado en recibir alertas completa este campo con el indicativo de tu país.</small></label>
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas fa-mobile-alt fa-fw"></i></span>
									<input type="text" name="celular" class="form-control celular" placeholder="Teléfono móvil" aria-label="Teléfono móvil" data-parsley-type="digits">
								</div>
							</div>
							<div class="form-group pb-3 cont_reg req">                            
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas icon-globe-outline fa-fw"></i></span>
									<select name="pais" class="form-select pais" aria-label="País" required>
										<option value="">País</option>
										<?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
											}
											$result->close();
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group pb-3 cont_reg req">                            
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas fa-user-tie fa-fw"></i></span>
									<select name="tipo" class="form-select tipo" aria-label="Tipo de usuario" required>
										<option value="">Tipo de usuario</option>
										<option value="0">Particular</option>
										<option value="1">Agente inmobiliario</option>
									</select>
								</div>
							</div>
							<div class="form-group pb-3 cont_reg">
								<label><small class="text-muted d-block lh-1">Ingresa tu código inmobiliario si posees uno.</small></label>
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas fa-mobile-alt fa-fw"></i></span>
									<input type="text" name="codigo" class="form-control codigo" placeholder="Código inmobiliario" aria-label="Código inmobiliario">
								</div>
							</div>							
						</form>					
					</div>
					<div class="modal-footer">
						<div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
							<button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-danger text-white" onClick="Valform('form-EdtUsr', reloader, [], true); return false"><span class="text-responsive">guardar <i class="fas iicon-check-circle fa-fw"></i></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>		
		
		<script>			
            $(function() {
				<?php
				if($puede){
				?>
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/perfil');
				}				
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});
				<?php	
				}else{
				?>
				loader('inicio');
				<?php	
				}
				?>
            });			
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>