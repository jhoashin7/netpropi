<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$getvars = sanitize( $_POST );
unset( $_POST ); 
$id = (isset($getvars[ 'id' ]) && $getvars[ 'id' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ):false;
$pais = (isset($getvars[ 'pais' ]) && $getvars[ 'pais' ] != 'false')?strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) ):false;
?>
<!doctype html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P9CLB9H');</script>
	<!-- End Google Tag Manager -->
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9CLB9H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="contenido" class="w-100 position-relative">		
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-6 col-sm-8 text-start">
							<h4 class="m-0 fw-bold text-danger"><span class="text-responsive"><i class="fas fa-toolbox fa-fw"></i> Aliados</span></h4>
						</div>
						<div class="col-6 col-sm-4 text-start">
							<div class="form-group">                            
								<div class="input-group">									
									<select class="form-select especialidades_srv" aria-label="Especialidades" onChange="showAliados()">
										<option value="">Especialidades</option>
										<?php
										$consulta = "SELECT id, nombre FROM especialidad_srv ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
									</select>
									<span class="input-group-text bg-danger text-white"><i class="fas icon-search fa-fw"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-stretch aliados_cont">
						<?php
						if($pais){
							$consulta = "SELECT id, nombre, logo, especialidad FROM aliados WHERE tipo = 6 AND pais = ".$pais." ORDER BY nombre ASC";
							if ( $result = $mysqli->query( $consulta ) ) {
								while($row = $result->fetch_assoc()){
						?>
						<div class="col-4 col-md-3 col-xl-3 text-start pb-3 us_cont" data-espe="<?php echo $row['especialidad'] ?>">
							<div class="d-block w-100 h-100 rounded-3 gray-100 border border-light p-3">
								<div class="ratio ratio-1x1 bg-white overflow-hidden d-block overflow-hidden rounded-circle">
									<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="<?php if($row['logo'] != ''){ ?>background-image: url(aliados/<?php echo $row['logo'] ?>); background-repeat: no-repeat; background-position: center center; background-size: cover<?php } ?>"><?php if($row['logo'] == ''){ ?><span class="text-responsive"><i class="fas icon-user fa-6x text-muted"></i></span><?php } ?></div>
								</div>
								<div class="py-2 text-center">
									<p class="m-0 fw-bold text-danger lh-1"><span class="text-responsive"><?php echo utf8_encode($row['nombre']) ?></span></p>
								</div>
							</div>														
						</div>
						<?php                      
								}
								$result->close();
							}
						}
						?>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-center pb-3">
					<p class="mb-3 text-muted"><span class="text-responsive">Si estás interesado en solicitar un servicio para tus propiedades, puedes hacerlo desde tu panel control accediendo con tu usuario y contraseña.</span></p>
					<a href="<?php echo $conArr['base_url'] ?>" class="btn btn-danger text-white rounded-pill fw-bold text-decoration-none" target="_blank"><i class="fas icon-sliders fa-fw fa-lg"></i> Panel de control</a>
				</div>
												
			</div>
		</div>
		<div class="bg-danger py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-6 col-sm-8 text-start">
							<h4 class="m-0 fw-bold text-white"><span class="text-responsive"><i class="fas fa-user-tie fa-fw"></i> Asistencia legal</span></h4>
						</div>
						<div class="col-6 col-sm-4 text-start">
							<div class="form-group">                            
								<div class="input-group">									
									<select class="form-select especialidades_abg" aria-label="Especialidades" onChange="showAbogados()">
										<option value="">Especialidades</option>
										<?php
										$consulta = "SELECT id, nombre FROM especialidad_abg ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
									</select>
									<span class="input-group-text bg-white text-danger"><i class="fas icon-search fa-fw"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-stretch abogados_cont">
						<?php
						if($pais){
							$consulta = "SELECT id, nombre, logo, especialidad FROM aliados WHERE tipo = 7 AND pais = ".$pais." ORDER BY nombre ASC";
							if ( $result = $mysqli->query( $consulta ) ) {
								while($row = $result->fetch_assoc()){
						?>
						<div class="col-4 col-md-3 col-xl-3 text-start pb-3 us_cont" data-espe="<?php echo $row['especialidad'] ?>">
							<div class="d-block w-100 h-100 rounded-3 border border-light p-3" style="background-color: rgba(255,255,255,0.1)">
								<div class="ratio ratio-1x1 bg-white overflow-hidden d-block overflow-hidden rounded-circle">
									<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="<?php if($row['logo'] != ''){ ?>background-image: url(aliados/<?php echo $row['logo'] ?>); background-repeat: no-repeat; background-position: center center; background-size: cover<?php } ?>"><?php if($row['logo'] == ''){ ?><span class="text-responsive"><i class="fas icon-user fa-6x text-muted"></i></span><?php } ?></div>
								</div>
								<div class="py-2 text-center">
									<p class="m-0 fw-bold text-white lh-1"><span class="text-responsive"><?php echo utf8_encode($row['nombre']) ?></span></p>
								</div>
							</div>														
						</div>
						<?php                      
								}
								$result->close();
							}
						}
						?>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-center pb-3">
					<p class="mb-3 text-white"><span class="text-responsive">Si estás interesado en solicitar un servicio de asistencia legal, puedes hacerlo desde tu panel control accediendo con tu usuario y contraseña.</span></p>
					<a href="<?php echo $conArr['base_url'] ?>" class="btn btn-light text-danger rounded-pill fw-bold text-decoration-none" target="_blank"><i class="fas icon-sliders fa-fw fa-lg"></i> Panel de control</a>
				</div>
												
			</div>
		</div>
		<div class="bg-white py-4">
			<div class="row w-100 mx-0 justify-content-center align-items-center">
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-12 text-start">
							<h4 class="m-0 fw-bold text-danger"><span class="text-responsive"><i class="fas icon-file-text fa-fw"></i> Contratos</span></h4>
						</div>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-start pb-3 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-stretch aliados_cont">
						<?php
						if($pais){
							$consulta = "SELECT id, nombre FROM contratos WHERE texto != '' AND FIND_IN_SET(".$pais.", paises) ORDER BY nombre ASC";
							if ( $result = $mysqli->query( $consulta ) ) {
								while($row = $result->fetch_assoc()){
						?>
						<div class="col-4 col-md-3 col-xl-3 text-start pb-3 us_cont">
							<a href="#" class="d-block w-100 h-100 rounded-3 gray-100 border border-light p-3 text-decoration-none" onClick="openlogIn('showContrato', [<?php echo $row['id'] ?>]); return false">
								<div class="ratio ratio-1x1 bg-white overflow-hidden d-block overflow-hidden rounded-circle">
									<div class="d-flex w-100 h-100 justify-content-center align-items-center"><span class="text-responsive"><i class="fas icon-file-text fa-6x text-muted"></i></span></div>
								</div>
								<div class="py-2 text-center">
									<p class="m-0 fw-bold text-danger lh-1"><span class="text-responsive"><?php echo utf8_encode($row['nombre']) ?></span></p>
									<small class="text-muted">previsualizar</small>
								</div>
							</a>														
						</div>
						<?php                      
								}
								$result->close();
							}
						}
						?>						
					</div>
				</div>
				<div class="col-12 col-xl-10 text-center pb-3">
					<p class="mb-3 text-muted"><span class="text-responsive">Si estás interesado en adquirir uno de nuestros modelos de contrato, puedes hacerlo desde tu panel control accediendo con tu usuario y contraseña.</span></p>
					<a href="<?php echo $conArr['base_url'] ?>" class="btn btn-danger text-white rounded-pill fw-bold text-decoration-none" target="_blank"><i class="fas icon-sliders fa-fw fa-lg"></i> Panel de control</a>
				</div>												
			</div>
		</div>
		<script>			
            $(function() {	
				if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					history.replaceState(null, null, '<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/servicios');
				}
				$('#contenido').imagesLoaded({ background: true }).done(function(instance){
					respClass();										
					$('#loader').one("hide", function() { 
						$('#contenido .animados').each(function(index){
							$(this).removeClass('pen_animated').addClass($(this).data('anima'));
						});						
					});
					loaderHide();
				});				
            });	
			function showAliados(){
				if($('.especialidades_srv').val() != ''){
					$('.aliados_cont .us_cont').hide();
					$('.aliados_cont .us_cont').each(function(index){												
						datos = $(this).attr('data-espe').split(',');						
						if($.inArray($('.especialidades_srv').val(), datos) >= 0){
							$(this).show();
						}
					});					
				}else{
					$('.aliados_cont .us_cont').show();
				}
			}
			function showAbogados(){
				if($('.especialidades_abg').val() != ''){
					$('.abogados_cont .us_cont').hide();
					$('.abogados_cont .us_cont').each(function(index){												
						datos = $(this).attr('data-espe').split(',');						
						if($.inArray($('.especialidades_abg').val(), datos) >= 0){
							$(this).show();
						}
					});					
				}else{
					$('.abogados_cont .us_cont').show();
				}
			}
			function showContrato(iden){
				if(login){
					var random = Math.floor((Math.random() * 999) + 100);
					window.open('<?php echo $conArr['base_url'] ?>/test_contrato.php?veruser='+login+'&idcont='+iden+'&ver='+random);
					loaderHide();
				}				
			}
        </script>
	</div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>