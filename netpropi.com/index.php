<?php
header('X-Frame-Options: DENY');
include '../admin.netpropi.com/controllers/db_connect.php';
if(isset($_GET['url'])){
    $getvars = sanitize( $_REQUEST );
    unset( $_REQUEST ); 
}
$requesto  = (isset($getvars[ 'url' ]))?strip_tags( $mysqli->real_escape_string( rtrim($getvars[ 'url' ], DIRECTORY_SEPARATOR) ) ):false;
$params = (isset($getvars[ 'url' ]))?explode ("/", $requesto):false;
$destino = false;
$iden = false;
$pais = false;
$busqueda = false;
if($params){
	if(count($params) > 1){
		if(strlen($params[0]) == 3){
			$consulta = "SELECT id FROM countries WHERE iso3 = '".strtoupper($params[0])."'  ORDER BY id ASC LIMIT 1";
			if ( $result = $mysqli->query( $consulta ) ) {
				$rows = $result->num_rows;
				if($rows > 0){
					$datos = mysqli_fetch_object( $result ); 
					$pais = $datos->id;
				}				
				$result->close();
			}
			$busqueda = ($pais)?$params[1]:$params[0];
		}else{
			$busqueda = $params[0];
		}
		
	}
    if($params[1] == 'propiedad'){
		if(strlen($params[0]) == 3){
			$consulta = "SELECT id FROM countries WHERE iso3 = '".strtoupper($params[0])."'  ORDER BY id ASC LIMIT 1";
			if ( $result = $mysqli->query( $consulta ) ) {
				$rows = $result->num_rows;
				if($rows > 0){
					$datos = mysqli_fetch_object( $result ); 
					$pais = $datos->id;
				}				
				$result->close();
			}
			$busqueda = ($pais)?$params[3]:$params[0];
		}else{
			$busqueda = $params[0];
		}
		
	}
    
	if(count($params) == 1){
		if(strlen($params[0]) == 3){
			$consulta = "SELECT id FROM countries WHERE iso3 = '".strtoupper($params[0])."' ORDER BY id ASC LIMIT 1";
			if ( $result = $mysqli->query( $consulta ) ) {
				$rows = $result->num_rows;
				if($rows > 0){
					$datos = mysqli_fetch_object( $result ); 
					$pais = $datos->id;
				}
				$result->close();
			}
			$busqueda = ($pais)?false:$params[0];
		}else{
			$busqueda = $params[0];
		}
	}
}
if($busqueda){
	if($busqueda == 'inicio'){
		$destino = 'inicio';
	}
	if($busqueda == 'propiedades'){
		$destino = 'propiedades';
	}
	if($busqueda == 'planes'){
		$destino = 'planes';
	}
	if($busqueda == 'preguntas_frecuentes'){
		$destino = 'preguntas_frecuentes';
	}
	if($busqueda == 'blog'){
		$destino = 'blog';
	}
	if($busqueda == 'servicios'){
		$destino = 'servicios';
	}
	if($busqueda == 'terminos'){
		$destino = 'terminos';
	}
	if($busqueda == 'politicas_privacidad'){
		$destino = 'politicas_privacidad';
	}
	if(!$destino){
		$consulta = "SELECT id FROM blog WHERE url = '".$busqueda."' ORDER BY id DESC LIMIT 1";
        if ( $result = $mysqli->query( $consulta ) ) {
			$rows = $result->num_rows;
            if($rows > 0){
                $datos = mysqli_fetch_object( $result ); 
                $destino = 'articulo';
            	$iden = $datos->id;
            }
            $result->close();
        }
		if(!$destino){
			$consulta = "SELECT id FROM propiedades WHERE id = ".$busqueda." AND estado = 1";
			if ( $result = $mysqli->query( $consulta ) ) {
				$rows = $result->num_rows;
				if($rows > 0){
					$datos = mysqli_fetch_object( $result ); 
					$destino = 'propiedad';
					$iden = $datos->id;
				}				
				$result->close();
			}
		}
	}
}
$rand = mt_rand();
$google = false;
$consulta = "SELECT codigo FROM analytics ORDER BY id DESC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $google = mysqli_fetch_object( $result );
    $result->close();
}
$mapas = false;
$consulta = "SELECT codigo FROM mapas ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $mapas = mysqli_fetch_object( $result );    
    $result->close();
}
$hubspot = false;
$consulta = "SELECT codigo FROM hubspot ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $hubspot = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html lang="es">
<head>
	<base href="<?php echo $conArr['base_url_sitio'] ?>/">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="NetPropi">
    <meta name="theme-color" content="#f71140"/>
    <meta name = "viewport" content = "width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">	
	<link rel="shortcut icon" href="images/icons/icono.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="58x58" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="icon" sizes="192x192" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
	<link rel="icon" sizes="128x128" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
	<link rel="icon" sizes="32x32" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
	<link rel="icon" sizes="16x16" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
    <link href="images/icons/splashscreens/iphone5_splash.png?v=<?php echo $rand ?>" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphone6_splash.png?v=<?php echo $rand ?>" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphoneplus_splash.png?v=<?php echo $rand ?>" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphonex_splash.png?v=<?php echo $rand ?>" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphonexr_splash.png?v=<?php echo $rand ?>" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphonexsmax_splash.png?v=<?php echo $rand ?>" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipad_splash.png?v=<?php echo $rand ?>" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipadpro1_splash.png?v=<?php echo $rand ?>" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipadpro3_splash.png?v=<?php echo $rand ?>" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipadpro2_splash.png?v=<?php echo $rand ?>" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <title>NetPropi</title>
    <link rel="manifest" href="manifest.json">    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet" type="text/css">
    <link href="css/icomoon.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.css" integrity="sha256-RXPAyxHVyMLxb0TYCM2OW5R4GWkcDe02jdYgyZp41OU=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/plugins/confirmDate/confirmDate.css" integrity="sha256-WcAI0bCUFw5J3lXNkoQRlLI4WoUQhAhP5uMnmFZo2fE=" crossorigin="anonymous">   
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/css/fileinput.min.css" integrity="sha256-szDlNFIcJxlNdyPGmATPgF6uKIF1H2NBkuJWYcEmveY=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/themes/explorer-fas/theme.min.css" integrity="sha256-+tjjsfGUJBjof9SD+0L4ljplK5Cp4QvhOX4QyTNYtgI=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2@2.0.8/src/spectrum.css" integrity="sha256-iOD8YCPdWyUFZM7U7VwvzX41SLL5BrGBRG3Ituwbkyg=" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css" integrity="sha512-eMxdaSf5XW3ZW1wZCrWItO2jZ7A9FhuZfjVdztr7ZsKNOmt6TUMTQgfpNoVRyfPE5S9BC0A4suXzsGSrAOWcoQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/main.css?v=<?php echo $rand ?>" type="text/css">
</head>
<?php
    if($google && strlen($google->codigo) > 7){
?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo $google->codigo ?>', 'auto');
ga('send', 'pageview');
</script>    
<?php    
    }
?>
<?php
    if($hubspot){
?>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/<?php echo $hubspot->codigo ?>.js"></script>
<!-- End of HubSpot Embed Code -->  
<?php    
    }
?>	
<body>
    <div id="loader" class="position-fixed w-100 h-100 flex-column justify-content-center align-items-center">
		<div class="position-relative">
			<div class="spinner-border text-white-50" role="status" style="width: 110px; height: 110px">
			  <span class="visually-hidden">Loading...</span>
			</div>
			<div class="position-absolute d-flex justify-content-center align-items-center w-100 h-100" style="left: 0px; top: 0px">
				<img src="images/icono.png" class="img-fluid" />
			</div>
		</div>
    </div>
	<div id="menu" class="position-fixed w-100 h-100 bg-danger justify-content-center align-items-center" style="display: none">
		<div class="position-absolute w-100 d-block px-3 py-2 top-0 start-0">
			<div class="d-flex w-100 d-flex justify-content-between align-items-center py-2 fitCont">
				<div class="text-start">
                    <a href="#" class="d-block" title="Inicio" onClick="showMenu(); loader('inicio'); return false">
                        <img src="images/logonpb.png" alt="Inicio" class="img-fluid" style="max-width: 150px"/>
                    </a>            
                </div>
				<div class="flex-fill text-end ps-2">
					<a href="#" class="text-decoration-none link-light d-inline-block" onClick="showMenu(); return false"><span class="text-responsive"><i class="fas icon-times fa-2x fa-fw"></i></span></a>
				</div>
			</div>
		</div>
		<div class="w-100 text-center">
			<a href="#" class="text-decoration-none link-light d-block px-3 py-1 my-2 fw-bold" onClick="showMenu(); loader('inicio'); return false"><h2 class="m-0"><span class="text-responsive">¿Cómo funciona?</span></h2></a>
            <a href="#" class="text-decoration-none link-light d-block px-3 py-1 my-2 fw-bold" onClick="showMenu(); loader('propiedades'); return false"><h2 class="m-0"><span class="text-responsive">Propiedades</span></h2></a>
            <a href="#" class="text-decoration-none link-light d-block px-3 py-1 my-2 fw-bold" onClick="showMenu(); loader('planes'); return false"><h2 class="m-0"><span class="text-responsive">Planes</span></h2></a>
            <a href="#" class="text-decoration-none link-light d-block px-3 py-1 my-2 fw-bold" onClick="showMenu(); loader('servicios'); return false"><h2 class="m-0"><span class="text-responsive">Servicios</span></h2></a>
            <a href="#" class="text-decoration-none link-light d-block px-3 py-1 my-2 fw-bold" onClick="showMenu(); loader('preguntas_frecuentes'); return false"><h2 class="m-0"><span class="text-responsive">FAQ</span></h2></a>
            <a href="#" class="text-decoration-none link-light d-block px-3 py-1 my-2 fw-bold" onClick="showMenu(); loader('blog'); return false"><h2 class="m-0"><span class="text-responsive">Blog</span></h2></a>
		</div>
    </div>
    <header id="barra_nav" class="fixed-top w-100 gray-100" style="overflow-x: hidden;">
		<div class="row w-100 mx-0 justify-content-center align-items-center py-2">
			<div class="col-12 col-xl-10">
				<div class="d-flex w-100 d-flex justify-content-between align-items-center py-2 fitCont">
					<div class="text-start">
						<a href="#" class="d-block" title="Inicio" onClick="loader('inicio'); return false">
							<img id="logo" src="images/logonp.png" alt="Inicio" class="img-fluid" style="max-width: 150px"/>
						</a>            
					</div>
					<div class="d-none d-lg-inline-flex flex-fill justify-content-center align-items-center px-2">						
						<a href="#" class="text-decoration-none link-dark d-block px-3 bt-menu bt-inicio" onClick="loader('inicio'); return false"><span class="text-responsive">¿Cómo funciona?</span></a>
						<a href="#" class="text-decoration-none link-dark d-block px-3 bt-menu bt-propiedades" onClick="loader('propiedades'); return false"><span class="text-responsive">Propiedades</span></a>
						<a href="#" class="text-decoration-none link-dark d-block px-3 bt-menu bt-planes" onClick="loader('planes'); return false"><span class="text-responsive">Planes</span></a>
						<a href="#" class="text-decoration-none link-dark d-block px-3 bt-menu bt-servicios" onClick="loader('servicios'); return false"><span class="text-responsive">Servicios</span></a>
						<a href="#" class="text-decoration-none link-dark d-block px-3 bt-menu bt-preguntas_frecuentes" onClick="loader('preguntas_frecuentes'); return false"><span class="text-responsive">FAQ</span></a>
						<a href="#" class="text-decoration-none link-dark d-block px-3 bt-menu bt-blog" onClick="loader('blog'); return false"><span class="text-responsive">Blog</span></a>						
					</div>
					<div class="ps-2 d-inline-flex justify-content-end align-items-center">
						<a href="#mod-pais" class="text-decoration-none link-dark d-inline-block me-3" data-bs-toggle="modal" onClick="return false"><span class="text-responsive"><i class="fas icon-globe-outline fa-2x fa-fw"></i></span></a>
						<button class="btn btn-outline-dark text-nowrap log-out" onClick="setReg(1); return false"><span class="text-responsive">Iniciar sesión</span></button>
						<!-- <button class="btn btn-outline-dark text-nowrap log-out" onClick="openlogIn(); return false"><span class="text-responsive">Iniciar sesión</span></button> -->
                        <button class="btn btn-outline-dark text-nowrap log-out" onClick="setReg(2); return false"><span class="text-responsive">Regístrate</span></button>
						<button class="btn btn-danger text-white text-nowrap log-in" onClick="loader('perfil');return false"><span class="text-responsive"><i class="fas icon-user fa-fw"></i> Mi perfil</span></button>
                        <div id="iconChat"><button class="btn btn btn-outline-dark log-in"><span class="text-responsive"><i onClick="openChat();return false" class="fas  fa-comment fa-fw"></i></span></button></div>
						<a href="#" class="text-decoration-none link-dark d-inline-block d-lg-none ms-3" onClick="showMenu(); return false"><span class="text-responsive"><i class="fas fa-ellipsis-v fa-lg fa-fw"></i></span></a>
					</div>
				</div>
			</div>
		</div>		 
    </header>    
    <main id="main_c" class="d-flex w-100 flex-column justify-content-center align-items-center fitCont" style="min-height: 100vh">
		<div id="m_cont" class="w-100 position-relative mb-auto" style="z-index: 20">
			<div></div>
		</div>		
        <div id="pie_pagina" class="d-flex justify-content-center align-items-center w-100 position-relative bg-danger text-white py-2" style="z-index: 30">
			<div class="row w-100 mx-0 justify-content-center align-items-center pt-2">
				<div class="col-12 col-xl-10 px-0">
					<div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-12 col-sm-6 col-md-4 text-center text-md-start pb-2 order-0">
							<img id="logo" src="images/logonpb.png" alt="NetPropi" class="img-fluid" style="max-width: 150px"/>
						</div>
						<div class="col-12 col-md-4 text-center pb-2 order-2 order-md-1">
							<div class="d-flex w-100 justify-content-center align-items-center pb-2">
								<!-- <a href="#" class="text-decoration-none link-light d-block" onClick="openDocu(0); return false"> -->
								<a href="#" class="text-decoration-none link-light d-block" onClick="loader('politicas_privacidad'); return false">
									<p class="m-0"><span class="text-responsive lh-1">Tratamiento de datos personales</span></p>
								</a>
								<div class="mx-2 align-self-stretch border border-start border-white" style="width: 1px"></div>
								<!-- <a href="#" class="text-decoration-none link-light d-block" onClick="openDocu(1); return false"> -->
								<a href="#" class="text-decoration-none link-light d-block" onClick="loader('terminos'); return false">
									<p class="m-0"><span class="text-responsive lh-1">Términos y condiciones</span></p>
								</a>
							</div>
							<small><span class="text-responsive lh-1">Copyright © 2021 <strong>NetPropi</strong><br>Todos los derechos reservados Colombia</span></small>
						</div>
						<div class="col-12 col-sm-6 col-md-4 text-center text-md-end pb-2 order-1 order-md-2">
							<div class="d-flex w-100 justify-content-center justify-content-md-end align-items-center">
								<a href="https://www.facebook.com/NetPropi-101035582339909/" class="text-decoration-none link-danger d-block px-1" target="blank">
									<span class="text-responsive">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x text-white"></i>
											<i class="fab fa-facebook-f fa-stack-1x"></i>
										</span>
									</span>
								</a>
								<a href="https://www.instagram.com/netpropi/" class="text-decoration-none link-danger d-block px-1" target="blank">
									<span class="text-responsive">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x text-white"></i>
											<i class="fab fa-instagram fa-stack-1x"></i>
										</span>
									</span>
								</a>
								<a href="https://www.tiktok.com/@netpropi" class="text-decoration-none link-danger d-block px-1" target="blank">
									<span class="text-responsive">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x text-white"></i>
											<i class="fab fa-tiktok fa-stack-1x"></i>
										</span>
									</span>
								</a>								
								<a href="https://www.youtube.com/channel/UCm-BAfbvLVCDMpafRKUxHXw" class="text-decoration-none link-danger d-block px-1" target="blank">
									<span class="text-responsive">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x text-white"></i>
											<i class="fab fa-youtube fa-stack-1x"></i>
										</span>
									</span>
								</a>
								<a href="#" class="text-decoration-none link-danger d-block px-1" onClick="openlogIn('openData', ['contactoMod'], true); return false">
									<span class="text-responsive">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x text-white"></i>
											<i class="fas fa-envelope-open fa-stack-1x"></i>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>		
	</main>
    
    <div id="modals_cont"></div>
    
    <div class="offcanvas offcanvas-top bg-danger overflow-hidden" tabindex="-1" id="off-error" aria-labelledby="Error">
        <div class="offcanvas-header justify-content-between">
            <h4 class="position-absolute m-0" style="top: -1.5rem; right: -3rem"><i class="fas icon-alert-circle fa-fw fa-10x" style="color: rgba(255,255,255,0.2)"></i></h4>
            <h5 class="offcanvas-title text-uppercase fw-bolder text-white position-relative m-0"><span class="text-responsive">Error</span></h5>
            <button type="button" class="btn btn-close btn-link text-decoration-none text-white position-relative" data-bs-dismiss="offcanvas" aria-label="Close"><h2 class="m-0"><span class="text-responsive"><i class="fas fa-times-circle"></i></span></h2></button>
        </div>
        <div class="offcanvas-body">
            <div class="position-relative text-white"><span class="error_msg text-responsive"></span></div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-pais" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-danger align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas icon-globe-outline fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Selecciona un País</span>
                        </span>                            
                    </h5>                    
                </div>
                <div class="modal-body text-center">
                    <?php
					$paises = array();
                    $consulta = "SELECT id, name, iso3, latitude, longitude FROM countries WHERE activo = 1 ORDER BY name ASC";
                    if ( $result = $mysqli->query( $consulta ) ) {
                        while($row = $result->fetch_assoc()){
							array_push($paises, '{"id": '.$row['id'].', "iso": "'.strtolower($row['iso3']).'", "coordenada": ['.$row['latitude'].', '.$row['longitude'].'] }');
                    ?>
					<a href="#" data-bs-dismiss="modal" class="text-decoration-none link-dark d-block fw-bold" onClick="setPais(<?php echo $row['id'] ?>); return false;"><?php echo utf8_encode($row['name'])?></a>
                    <hr>
                    <?php                                    
                        }
                        $result->close();
                    }
                    ?> 
                </div>                
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-setLogin" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-danger align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-fingerprint fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Binvenido a NetPropi</span>
                        </span>
						<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
							<i class="fas fa-times-circle fa-lg fa-fw"></i>						
						</button>
                    </h5>                    
                </div>
                <div class="modal-body text-center">
					<form id="form-setLogin">						
						<input type="hidden" class="accion" name="accion" value=""/>
						<input type="hidden" class="parametros" name="parametros" value=""/>
					</form>
                    <div class="row w-100 mx-0 justify-content-center align-items-center">
						<div class="col-10 py-2">
							<div class="d-grid gap-3">
								<button class="btn btn-danger text-white rounded-pill fw-bold" onClick="setReg(1); return false"><i class="fas icon-user-check fa-fw fa-lg"></i> Ya estás registrado</button>
								<button class="btn btn-outline-danger rounded-pill fw-bold" onClick="setReg(2); return false"><i class="fas icon-user-plus fa-fw fa-lg"></i> Regístrate</button>
								<button class="btn btn-outline-warning rounded-pill fw-bold" onClick="RecordarD(); return false"><i class="far fa-lightbulb fa-fw fa-lg"></i> Recordar datos</button>								
							</div>
						</div>
					</div> 
                </div>                
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-recordarD" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-warning align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="far fa-lightbulb fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Recordar Datos</span>
                        </span>
						<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
							<i class="fas fa-times-circle fa-lg fa-fw"></i>						
						</button>
                    </h5>                    
                </div>
                <div class="modal-body text-center">
					<p><span class="text-responsive text-muted">Te enviaremos un E-mail con tu información de acceso.</span></p>
					<form id="form-recordarD">
                        <div class="form-group pb-3">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-envelope fa-fw"></i></span>
                                <input type="email" name="mail" class="form-control mail" placeholder="E-mail" aria-label="E-mail" data-parsley-type="email" required>
                            </div>
                        </div>
                        <div class="d-block pb-4">
                            <div id="recGc" class="recap d-flex justify-content-center align-items-center"></div>
                            <div class="recerror d-block text-center"></div>
                        </div>
                    </form>					
                </div>
				<div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        <button type="button" class="btn btn-warning text-white" onClick="Valrecdata(); return false"><span class="text-responsive">recuperar <i class="fas fa-check-circle fa-fw"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-loginRec" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-danger align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="far icon-user fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Ingresar</span>
                        </span>
						<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
							<i class="fas fa-times-circle fa-lg fa-fw"></i>						
						</button>
                    </h5>                    
                </div>
                <div class="modal-body text-start">					
					<form id="form-loginRec">
						<input type="hidden" class="accion" name="accion" value=""/>						
						<div class="form-group pb-3 cont_reg req">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-user fa-fw"></i></span>
                                <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                            </div>
                        </div>
						<div class="form-group pb-3 cont_reg req">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-user fa-fw"></i></span>
                                <input type="text" name="apellido" class="form-control nombre" placeholder="Apellido" aria-label="Apellido" required>
                            </div>
                        </div>
						<div class="form-group pb-3">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-mail fa-fw"></i></span>
                                <input type="email" name="email" class="form-control email" placeholder="E-mail" aria-label="E-mail" data-parsley-type="email" required>
                            </div>
                        </div>
						<div class="form-group pb-3 cont_reg req">                            
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-globe-outline fa-fw"></i></span>
                                <select name="pais" class="form-select pais" aria-label="País" required onchange="codePhone('form-loginRec', this.value)">
                                    <option value="">País</option>
                                    <?php
                                    $consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
                                    if ( $result = $mysqli->query( $consulta ) ) {
                                        while($row = $result->fetch_assoc()){
                                            echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
                                        }
                                        $result->close();
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group pb-3 cont_reg">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-mobile-alt fa-fw"></i></span>
                                <span class="input-group-text phoneCode">+</i></span>
                                <input type="text" name="phoneCode" class="inputPhoneCode" value='0' hidden>
                                <input type="text" name="celular" class="form-control celular" placeholder="Teléfono móvil" aria-label="Teléfono móvil" data-parsley-type="digits">
                            </div>
                        </div>
						<div class="form-group pb-3 cont_reg req">                            
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-user-tie fa-fw"></i></span>
                                <select name="tipo" class="form-select tipo" aria-label="Tipo de usuario" required>
                                    <option value="">Tipo de usuario</option>
									<option value="0">Particular</option>
                                    <option value="1">Agente inmobiliario</option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group pb-3">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-user fa-fw"></i></span>
                                <input type="text" name="usuario" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" data-parsley-type="alphanum" required>
                            </div>
                        </div> -->
                        <div class="form-group pb-3">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-fingerprint fa-fw"></i></span>
                                <input type="password" name="contrasena" class="form-control contrasena" placeholder="Contraseña" aria-label="Contraseña" data-parsley-minlength="6" data-parsley-type="alphanum" data-parsley-pattern="^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$" data-parsley-pattern-message="La contraseña debe contener dígitos y letras" required>
                            </div>
                        </div>
                        <div class="form-group pb-3 cont_reg req">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-fingerprint fa-fw"></i></span>
                                <input type="password" name="contrasenaConfirm" class="form-control contrasena" placeholder="Confirmar contraseña" aria-label="Contraseña" data-parsley-minlength="6" data-parsley-type="alphanum" data-parsley-pattern="^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$" data-parsley-pattern-message="La contraseña debe contener dígitos y letras" required>
                            </div>
                        </div>
                        <div class="form-group pb-3">
                            <div class="form-check form-switch swt-md d-flex justify-content-start align-items-center">
                                <input class="form-check-input mt-0 chk-danger" type="checkbox" id="recordar">
                                <label class="form-check-label text-wrap ps-2" for="recordar"><span class="text-responsive">Recordarme en este dispositivo</span></label>
                            </div>
                        </div>
                        <div class="pb-3 cont_reg mb'3 cont_reg req">
                            <div class="form-check">
                                <input name="checkTerminos" class="form-check-input" type="checkbox" value="car" id="flexCheckDefault" required>
                                <label class="form-check-label" for="flexCheckDefault">
                                    He leído y acepto los <strong onClick="abrirTerminos('terminos'); return false" style="cursor:pointer">términos y condiciones</strong> del sistema
                                </label>
                            </div>
                            <div class="form-check">
                                <input name="checkPoliticas" class="form-check-input" type="checkbox" value="das" id="flexCheckDefault" required>
                                <label class="form-check-label" for="flexCheckDefault">
                                    He leído y acepto las <strong onClick="abrirTerminos('politicas_privacidad'); return false" style="cursor:pointer">políticas de tratamiento de datos</strong> del sistema
                                </label>
                            </div>
                            <!-- <small class="text-muted d-block lh-1">Al enviar tu informacón aceptas los <strong onClick="abrirTerminos('terminos'); return false" style="cursor:pointer">términos y condiciones</strong> y <strong onClick="abrirTerminos('politicas_privacidad'); return false" style="cursor:pointer">la política de tratamiento de datos</strong></small> -->
                        </div>
                        <div class="d-block pb-4">
                            <div id="recG" class="recap d-flex justify-content-center align-items-center"></div>
                            <div class="recerror d-block text-center"></div>
                        </div>                        
                        <span class=" rounded-pill fw-bold d-flex flex-row-reverse text-info" onClick="RecordarD(); return false" style="cursor: pointer"><i class="far fa-lightbulb fa-fw fa-lg"></i> Recordar datos</span>	
                    </form>					
                </div>
				<div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        <button type="button" class="btn btn-danger text-white" onClick="Vallog(); return false"><span class="text-responsive">ingresar <i class="fas icon-unlock fa-fw"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="modal fade" id="mod-chat" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-danger align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="far icon-user fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Mensajes</span>
                        </span>
						<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
							<i class="fas fa-times-circle fa-lg fa-fw"></i>						
						</button>
                    </h5>                    
                </div>
                <div class="modal-body text-start">		
                    <div class="container">
                        <div class="row" style="min-height:400px; max-height:400px;" >			
                            <div class="col col-md-4" id="usersChat"></div>
                            <div class="col col-md-8">
                                <div class="col col-md-12" id="messageByUser">
                                </div>
                                <div class="input-group mb-3" style='display:none; bottom:0' id='field-message'>
                                    <input type="text" class="form-control" placeholder="Escriba un mensaje" aria-label="Recipient's username" aria-describedby="button-addon2" id='sendMessage'>
                                    <button class="btn btn-outline-secondary"  onclick="sendMessage($('#id-user-to-send').val())" type="button" id="button-addon2">Button</button>
                                </div>
                            </div>
                    <!-- <div style="width:30%; min-height:400px; max-height:400px; float:left; position:fixed" id="usersChat"></div>
                    <div style="width:70%; min-height:400px; max-height:400px; float:right; bottom:10px" id="messageByUser"></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-contactoMod" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-danger align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="far icon-mail fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Contáctanos</span>
                        </span>
						<button type="button" class="btn btn-link text-decoration-none text-danger p-0 position-absolute top-0 end-0" data-bs-dismiss="modal" aria-label="Close">
							<i class="fas fa-times-circle fa-lg fa-fw"></i>						
						</button>
                    </h5>                    
                </div>
                <div class="modal-body text-center">					
					<form id="form-contactoMod">
						<input type="hidden" class="id" name="id" value=0 />                            
						<input type="hidden" class="db noclear" name="db" value="contacto" />
						<div class="form-group pb-3 cont_reg req">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-user fa-fw"></i></span>
                                <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                            </div>
                        </div>
                        <div class="form-group pb-3">
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-mail fa-fw"></i></span>
                                <input type="email" name="email" class="form-control mail" placeholder="E-mail" aria-label="E-mail" data-parsley-type="email" required>
                            </div>
                        </div>
						<div class="form-group pb-3">							
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-smartphone fa-fw"></i></span>
                                <input type="text" name="telefono" class="form-control telefono" placeholder="Teléfono" aria-label="Teléfono" data-parsley-type="digits">
                            </div>
                        </div>
						<div class="form-group pb-3">                            
                            <div class="input-group">
                                <span class="input-group-text gray-700 text-white"><i class="fas icon-globe-outline fa-fw"></i></span>
                                <select name="pais" class="form-select pais" aria-label="País" required>
                                    <option value="">País</option>
                                    <?php
                                    $consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
                                    if ( $result = $mysqli->query( $consulta ) ) {
                                        while($row = $result->fetch_assoc()){
                                            echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
                                        }
                                        $result->close();
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>						
						<div class="form-group pb-3">                            
                            <textarea name="motivo" class="motivo form-control" rows="4" placeholder="Comentario" required></textarea>
                        </div>
                    </form>					
                </div>
				<div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        <button type="button" class="btn btn-danger text-white" onClick="Valform('form-contactoMod', reloader, [], true, false); return false"><span class="text-responsive">enviar <i class="fas fa-check-circle fa-fw"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="modal fade" id="error_mod" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-danger overflow-hidden">
				<div class="modal-header border-0 py-3">
					<i class="fas icon-alert-circle fa-fw fa-9x position-absolute" style="color: rgba(255,255,255,0.2); top: -0.5rem; left: -3rem"></i>
                    <a href="#" class="text-decoration-none position-absolute text-white" data-bs-dismiss="modal" onClick="return false" style="right: 0.5rem; top: 0.5rem">
                        <i class="fas fa-times-circle"></i>
                    </a>					
				</div>
				<div class="modal-body pl-5">					
					<div class="position-relative text-white"><span class="error_msg text-responsive"></span></div>
				</div>				
			</div>
		</div>
	</div>
    
    <div class="modal fade" id="good_mod" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-success overflow-hidden">
				<div class="modal-header border-0 py-3">
					<i class="fas icon-check-circle fa-fw fa-9x position-absolute" style="color: rgba(255,255,255,0.2); top: -0.5rem; left: -3rem"></i>
                    <a href="#" class="text-decoration-none position-absolute text-white" data-bs-dismiss="modal" onClick="return false" style="right: 0.5rem; top: 0.5rem">
                        <i class="fas fa-times-circle"></i>
                    </a>					
				</div>
				<div class="modal-body pl-5">					
					<div class="position-relative text-white"><span class="good_msg text-responsive"></span></div>
				</div>				
			</div>
		</div>
	</div>
    
    <div class="modal fade" id="procc_mod" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-warning overflow-hidden">
				<div class="modal-header border-0 py-3">
					<i class="fas fa-circle-notch fa-spin fa-fw fa-9x position-absolute" style="color: rgba(255,255,255,0.2); top: -0.5rem; left: -3rem"></i>
				</div>
				<div class="modal-body pl-5">					
					<div class="position-relative text-white"><span class="procc_msg text-responsive"></span></div>
				</div>				
			</div>
		</div>
	</div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-ui-touch-punch@0.2.3/jquery.ui.touch-punch.min.js" integrity="sha256-AAhU14J4Gv8bFupUUcHaPQfvrdNauRHMt+S4UVcaJb0=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-fullscreen-plugin@1.1.5/jquery.fullscreen.js" integrity="sha256-rkIE81gwpMsNyKpuGlgG8T9fp/MvHa6ucB655PuOBik=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/parsley.min.js" integrity="sha256-pEdn/pJ2tyT37axbEIPkyUUfuG1yXR0+YV+h+jphem4=" crossorigin="anonymous"></script>
    <script src="js/extra/comparison.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/dateiso.js" integrity="sha256-udsaGJFsLl5CBWh1W9XZhVJoRlh40ib7xvib9Du68Ps=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/luhn.js" integrity="sha256-mwT7OcVKnfgVEkJR4i5+TOqDkV/4r4ybb8KOUDFoclI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/notequalto.js" integrity="sha256-OlUSGfTtCU1bsBAyrxUei5kCg82cb81ykk1v8+e/jkE=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/words.js" integrity="sha256-2utXQtk3tGxBIzkmNPJi8vJURbO1btmdY0I+nlek1aQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/i18n/es.js" integrity="sha256-dcJkxln8t7jxoFFA4jP3/ru8rFOlKpt478JM/wsMsgU=" crossorigin="anonymous"></script>
    <script src="js/i18n/es.extra.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous"></script>    
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.js" integrity="sha256-AkQap91tDcS4YyQaZY2VV34UhSCxu2bDEIgXXXuf5Hg=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/l10n/es.js" integrity="sha256-G5b/9Xk32jhqv0GG6ZcNalPQ+lh/ANEGLHYV6BLksIw=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/plugins/confirmDate/confirmDate.js" integrity="sha256-PdR2doyoDlR7G8IzjCJaBJEOqhyEOedRlokldFdxPuY=" crossorigin="anonymous"></script>    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/js/plugins/piexif.min.js" integrity="sha256-WYoKe0uREimiMKk7Z5ocKDhOubCqP3qHxmC4gXcMutk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/js/fileinput.min.js" integrity="sha256-RBRWMVrgmfIx8hdOUye+1cQuUkb/lmea4qlkqnnEsWM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/themes/explorer-fas/theme.min.js" integrity="sha256-N+j9ZK7htAf9TM7qTUMEnOlyefABPdmDMRiz65N/zi8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/themes/fas/theme.min.js" integrity="sha256-QBBS6qp56s19FjROArAI5cC/C9zKP7Wyznmc+owuiGk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/js/locales/es.js" integrity="sha256-7rIYI1hXx7Cu0o94OMKMrb87RH/1zWX93tm2BnBO7FU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.js" integrity="sha256-icjghcPaibMf1jv4gQIGi5MeWNHem2SispcorCiCfSg=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.2.2/circle-progress.min.js" integrity="sha512-6kvhZ/39gRVLmoM/6JxbbJVTYzL/gnbDVsHACLx/31IREU4l3sI7yeO0d4gw8xU5Mpmm/17LMaDHOCf+TvuC2Q==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2@2.0.8/dist/spectrum.min.js" integrity="sha256-mkd7mvxhNxvMs5bpRKBH37g0T9gOxiQKhiS3ql4LEHA=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.2/html2canvas.min.js" integrity="sha512-tVYBzEItJit9HXaWTPo8vveXlkK62LbA+wez9IgzjTmFNLMBO1BEYladBw2wnM3YURZSMUyhayPCoLtjGh84NQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js" integrity="sha512-S5PZ9GxJZO16tT9r3WJp/Safn31eu8uWrzglMahDT4dsmgqWonRY9grk3j+3tfuPr9WJNsfooOR7Gi7HL5W2jw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/min/tiny-slider.js" integrity="sha512-D/zaRVk05q6ERt1JgWB49kL6tyerY7a94egaVv6ObiGcw3OCEv0tvoPDEsVqL28HyAZhDd483ix8gkWQGDgEKw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="js/main.js?v=<?php echo mt_rand() ?>"></script>	
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=es-419" async defer></script>
	<?php
	if($mapas){
	?>
	<script async src="https://maps.googleapis.com/maps/api/js?key=<?php echo $mapas->codigo ?>&callback=initMap"></script>
	<?php	
	}
	?>
	<script>
		<?php
        if($google && strlen($google->codigo) > 7){
        ?>
        googlea = true;
        <?php
        }
        ?>
		<?php
        if($hubspot){
        ?>
        hubspot = true;
        <?php
        }
        ?>        
		<?php
        if($destino){
        ?>		
        position = '<?php echo $destino ?>';
		<?php
        if($iden){
        ?>
        extras = {"iden": <?php echo $iden ?>};
		<?php    
        }
        ?>		  
        <?php    
        }
        ?>
		paises = [<?php echo implode(',',$paises) ?>];
		<?php
        if($pais){
        ?>
        pais = <?php echo $pais ?>;
        <?php
        }
        ?>
        
        function abrirTerminos(route){
            if(pais){
					var prefijo = 'nan';
					$.each(paises, function(index, value){
						if(value.id == pais){
							prefijo = value.iso;
							return false;
						}
					});
					var win = window.open('<?php echo $conArr['base_url_sitio'] ?>/'+prefijo+'/'+route, '_blank');
				}
            // Cambiar el foco al nuevo tab (punto opcional)
            win.focus(); 
        }
        function codePhone(form, id) {
            $.ajax({
                    url: dirCont,
                    data: { 'pais_id': id, 'accion' : 'paises'},
                    method:  'POST',
                    dataType: 'json',
                    error: function () {								
                        $('#loader').one("hide", function() { 
                            showError(error);
                        });
                        loaderHide();
                    },
                    success:  function (response) {						
                        console.log(response);			
                        $('#'+form+' .phoneCode' ).html(response.data);
                        $('#'+form+' .inputPhoneCode' ).val(response.data);
                    }
                });	
        }
		$(function() {				  
			$('#loader').one("hide", function() { 
                widGo = grecaptcha.render('recG', {
					'sitekey' : '<?php echo $conArr['google_public'] ?>',
					'callback': function(response){
						setWid('form-loginRec', true);
					},
					'expired-callback': function(response){
						setWid('form-loginRec', true);
					},
					'error-callback': function(response){
						setWid('form-loginRec', true);
					}                    
				});
				
				widGo2 = grecaptcha.render('recGc', {
					'sitekey' : '<?php echo $conArr['google_public'] ?>',
					'callback': function(response){
						setWid('form-recordarD', true);
					},
					'expired-callback': function(response){
						setWid('form-recordarD', true);
					},
					'error-callback': function(response){
						setWid('form-recordarD', true);
					}                    
				});				
            });        
        });		  
    </script>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
?>