var position = 'inicio';
var nombre_seccion = 'inicio';
var extras = {};
var historia = [];
var historiapos = 0;
var paises = [];
var pais = false;
var activo_bt = false;
var id_propiedad = 0;
var dirCont = 'controllers/controller.php';
var login = false;
var userwithChat;
var sesionT = false;
var error = 'Se presento un problema de conexión, inténtalo de nuevo más tarde.';
var widGo = false;
var widGo2 = false;
var googlea = false;
var hubspot = false;
var documentos = false;
var onmap = false;
var map = false;
var pos = false;
var infoWindow;
var marcadores = [];
var marcador = false;
var geocoder = false;
var bounds;
var estilos_map = [
    {
      "featureType": "administrative.country",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#bb3aaa"
        },
        {
          "weight": 8
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ];
var inputDefaults = {
    theme: 'fas',
    language: 'es',
    layoutTemplates: {
        main2: '{preview}\n<div class="kv-upload-progress kv-hidden"></div>\n<div class="clearfix"></div>\n' + '<div class="btn-group btn-group-sm d-flex w-100" role="group">\n{upload}\n</div>\n',
        modal: '<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg{rtl}" role="document">\n' + 
        '   <div class="modal-content">\n' +
        '       <div class="modal-header flex-row">\n' + 
        '           <h5 class="modal-title">\n' +
        '               <span class="text-responsive d-flex justify-content-start align-items-center">\n' +
        '                   <span class="fa-stack text-warning align-top">\n' +
        '                           <i class="fas fa-circle fa-stack-2x"></i>\n' +
        '                           <i class="fas fa-search-plus fa-stack-1x text-white"></i>\n' +
        '                   </span>\n' +                                
        '                   <span class="lh-1">{heading}<small class="text-muted d-block kv-zoom-title" style="max-width: unset;"></small></span>\n' +
        '               </span>\n' +
        '           </h5>\n' +
        '           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>\n' +
        '       </div>\n' +
        '       <div class="modal-body">\n' +
        '           <div class="floating-buttons"></div>\n' +
        '           <div class="kv-zoom-body file-zoom-content {zoomFrameClass}"></div>\n' + '{prev} {next}\n' +
        '       </div>\n' +
        '   </div>\n' +
        '</div>'
    },
    browseClass: 'btn btn-warning text-white w-100 border-preselect',
    removeClass: 'btn btn-danger w-100',
    cancelClass: 'btn btn-danger w-100',
    uploadClass: 'btn btn-warning text-white w-100',
    previewZoomButtonClasses: {
        prev: 'd-none',
        next: 'd-none',
        toggleheader: 'd-none',
        fullscreen: 'd-none',
        borderless: 'd-none',
        close: 'btn btn-kv btn-default btn-outline-secondary'
    },
    previewZoomButtonIcons: {
        close: '<i class="fas fa-times fa-fw"></i>'
    },
	mimeTypeAliases: {
		'audio': 'audio/mpeg',
		'video': 'video/mp4'
	},
    zoomIndicator : '<i class="fas fa-search-plus fa-fw"></i>',
    previewFileIconSettings: {
        'gltf': '<i class="fas fa-cube text-muted"></i>'
    },		
    fileActionSettings: {
        showDownload: false,
        showUpload: false,
        showRemove: false,
        zoomIcon: '<i class="fas fa-search-plus fa-fw"></i>'
    },
    buttonLabelClass: 'd-none d-sm-inline',
    browseIcon: '<i class="fas fa-folder-open fa-fw"></i>&nbsp;',
    removeIcon: '<i class="fas fa-trash fa-fw"></i>&nbsp;',
    uploadIcon: '<i class="fas fa-cloud-upload-alt fa-fw"></i>&nbsp;',
    showCaption: false,
    autoReplace: true,
    uploadAsync: true,
    overwriteInitial: true,
    showUploadedThumbs: true,
    browseOnZoneClick: true,
    showAjaxErrorDetails: false,
    allowedPreviewTypes: ['image', 'video', 'audio', 'pdf'],
    disabledPreviewTypes: ['text', 'object', 'office', 'html', 'other'],
    uploadUrl: 'controllers/archivo.php'
};


if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js?v='+Math.floor(Math.random() * 100));
}

$(document).ready(function() {	
    function handlerChat() {
        if(login){
            var datos = {
                'idveruser': (login)?login:'',
                'accion':12
            };
            $.ajax({
                url: dirCont,
                data: datos,
                method:  'POST',
                dataType: 'json',
                error: function () {
                    $('#loader').one("hide", function() { 
                        showError(error);
                    });
                    loaderHide();
                },
                success:  function (response) {
                    console.log(response.data.mensaje );
                    if(response.data.pendientes > 0){
                        $("div #iconChat").html(
                            "<button class='btn btn btn-outline-danger' ><span class='text-responsive'><i onClick='openChat();return false' class='fas  fa-comment fa-fw'>"+response.data.pendientes+"</i></span></button>"
                        );
                    }else{
                        $("div #iconChat").html(
                            "<button class='btn btn btn-outline-dark' ><span class='text-responsive'><i onClick='openChat();return false' class='fas  fa-comment fa-fw'></i></span></button>"
                        );
                    }
                    if(userwithChat){
                        chatMessage(userwithChat)
                    }
                }
            });
        }
    }
    
    setInterval(handlerChat, 10000);
});


$(document).ready(function() {	
    function handlerChat() {
        if(login){
            var datos = {
                'idveruser': (login)?login:'',
                'accion':12
            };
            $.ajax({
                url: dirCont,
                data: datos,
                method:  'POST',
                dataType: 'json',
                error: function () {
                    $('#loader').one("hide", function() { 
                        showError(error);
                    });
                    loaderHide();
                },
                success:  function (response) {
                    console.log(response.data.mensaje );
                    if(response.data.pendientes > 0){
                        $("div #iconChat").html(
                            "<button class='btn btn btn-outline-danger' ><span class='text-responsive'><i onClick='openChat();return false' class='fas  fa-comment fa-fw'>"+response.data.pendientes+"</i></span></button>"
                        );
                    }else{
                        $("div #iconChat").html(
                            "<button class='btn btn btn-outline-dark' ><span class='text-responsive'><i onClick='openChat();return false' class='fas  fa-comment fa-fw'></i></span></button>"
                        );
                    }
                    if(userwithChat){
                        chatMessage(userwithChat)
                    }
                }
            });
        }
    }
    
    setInterval(handlerChat, 10000);
});

var onloadCallback = function() {
  init();
};

var initMap = function(){
	onmap = true;
	geocoder = new google.maps.Geocoder();
};

var setMap = function(){
	onmap = true;
	geocoder = new google.maps.Geocoder();	
};

function showLocationOnnMap(position){
    var latitud = position.coords.latitude;
    var longitud= position.coords.longitude;

    console.log(latitud, longitud);
    const myLatLng = {lat:latitud, lng:longitud};
    map.setCenter(myLatLng);
    map.setZoom(12);
    
}

function errorHandler(){
    if (err.code ==1) {
        alert("error: acceso denegado");
    }
}
function getLocation(){
    if (navigator.geolocation) {
        geoLoc = navigator.geolocation;
        watchID = geoLoc.watchPosition(showLocationOnnMap,errorHandler)
    } else{
        alert('sin geolocaliacion')
    }
}
function initMapa() {
    // var zooms = (1920 / $(window).width()) * 0.3;
	// var finzoom = Math.round(10 - zooms);
    if(map){		
		if(marcadores.length > 0){			
            for (var i = 0; i < marcadores.length; i++) {
                try{					
                    marcadores[i].setMap(null);
                }catch(e){						
                }					
            }
            marcadores = [];			
        }
		if(marcador){
			marcador.setMap(null);
			marcador = false;
		}        
    } 	
    if(map){
        $('#map_cont').html(map.getDiv());
        map.setCenter(pos);
        map.setOptions({
            gestureHandling: 'greedy'
        });
        map.setZoom(finzoom);
        google.maps.event.trigger(map, 'resize');					
    }else{	
		map = new google.maps.Map(document.getElementById('mapa'), {
            center: pos,
            zoom: 4,
            maxZoom: 19,
			mapTypeControl: false,
			fullscreenControl: false,
			gestureHandling: 'greedy',
			styles: estilos_map
        }); 
        getLocation();       				
    }

    google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
        $(window).trigger('resize');					
    });
	google.maps.event.trigger(map, 'resize');
	google.maps.event.addListener(map, 'bounds_changed', function(){        
		google.maps.event.trigger(map, 'resize');		
    });
}

(function($) {
	$.each(['show', 'hide'], function(i, ev) {		
		var el = $.fn[ev];
		$.fn[ev] = function() {
			this.triggerHandler(ev);			
			return el.apply(this, arguments);
		};
	});
})(jQuery);

$(function() { 
    loaderShow(true);
    window.Parsley.setLocale('es');
    window.Parsley.options.excluded = 'input[type=button], input[type=submit], input[type=reset]';
    window.Parsley.options.errorClass = 'is-invalid';
    window.Parsley.options.successClass = 'is-valid';
    window.Parsley.options.errorsContainer = function classHandler(Field) {return Field.$element.closest('.form-group');};
    window.Parsley.options.errorsWrapper = '<ul class="parsley-errors-list text-end text-danger mb-0 list-unstyled" style="font-size: .75em"></ul>';
    window.Parsley.options.errorTemplate = '<li class="pr-1 lh-1 text-responsive"></li>';
    window.Parsley.addValidator('typeColor', {
        requirementType: 'string',
        validateString: function(value){
            return tinycolor(value).isValid();
        },
        messages: {
            en: 'The value is not a valid color.',
            es: 'No es un valor de color válido.'
        }
    });
    moment.locale('es');
	$(window).on('load', function(){
		window.loaded = true;
	});
    
    window.addEventListener("popstate", function(e) {
        var estado = historiapos - 1;
        historiapos = estado;
        historia.pop();
        if(estado >= 0){            
            history.pushState(historia.length, null, null); 
        }        
        if(estado !== null && estado !== 'undefined' && historiapos >= 0){            
            var accpos = historia[estado];
            if($('.modal.show').length > 0){
                $('.modal.show').each(function( index ) {
                    if($('.modal.show').length <= 1){
                        $(this).one('hidden.bs.modal', function(){
                            loader(accpos.position, accpos.extras);
                        });                        
                    }
                    $(this).modal('hide');
                              
                });                
            }else{
                loader(accpos.position, accpos.extras);                
            }            
        }else{
            window.history.back();
        }        
    });
    
    let deferredPrompt;    
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      // Update UI to notify the user they can add to home screen     
    });       
	$(document).on('hidden.bs.modal', '.modal', function () {
        $("video").each(function() {
            $(this).get(0).pause();
        });
        $("audio").each(function() {
            $(this).get(0).pause();
        });
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});
	$(window).resize(respClass);	
	SetSesion();
	respClass();	
});

function tooltipsOn(){
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        var place = ($(tooltipTriggerEl).data('fallpla'))?$(tooltipTriggerEl).data('fallpla').split(','):['top', 'right', 'bottom', 'left'];
        return new bootstrap.Tooltip(tooltipTriggerEl, { 
            boundary: 'window',
            fallbackPlacements: place,
            popperConfig: function(defaultBsPopperConfig){
                var newPopperConfig = {strategy: 'fixed'};
                return newPopperConfig;
            }
        });
    }); 
}

function init(){	
	/*if(window.loaded){
		reInit();
	}else{
		$(window).on('load', reInit);
	}*/
    reInit();    
}

function reInit() {	
	if (typeof (Storage) !== "undefined") {
		if (localStorage.npf2021_iden && localStorage.npf2021_reco) {
			logIn(localStorage.npf2021_iden, true, true);
		} else {			
			reloader();
		}		
		if(!pais){
			if (localStorage.npf2021_pais) {
				$.each(paises, function(index, value){
                    if(value.id == localStorage.npf2021_pais){
						pais = localStorage.npf2021_pais;
						geoLocation = {
                            lat: value.coordenada[0],
                            lng: value.coordenada[1]
                        };
						pos = geoLocation;						
                        return false;
                    }
                });
				if(!pais){
					$('#mod-pais').modal('show');
				}
			}else{
				$('#mod-pais').modal('show');
			}			
		}else{
			$.each(paises, function(index, value){
                if(value.id == pais){                    
                    geoLocation = {
                        lat: value.coordenada[0],
                        lng: value.coordenada[1]
                    };
                    pos = geoLocation;						
                    return false;
                }
            });
		}
	} else {
		if(!pais){
			$('#mod-pais').modal('show');
		}else{
			$.each(paises, function(index, value){
                if(value.id == pais){                    
                    geoLocation = {
                        lat: value.coordenada[0],
                        lng: value.coordenada[1]
                    };
                    pos = geoLocation;						
                    return false;
                }
            });
		}
		reloader();
	}
}

function setPais(iden){
	pais = iden;
	$.each(paises, function(index, value){
        if(value.id == pais){ 
			geoLocation = {
                lat: value.coordenada[0],
                lng: value.coordenada[1]
            };
            pos = geoLocation;            
            return false;
        }
    });	 
	if (typeof (Storage) !== "undefined") {
		localStorage.setItem("npf2021_pais", iden);
	}	
	reloader();
}

function showMenu(){
	if($('#menu').is(':hidden')){		
		$('#menu').show();		
	}else{		
		$('#menu').hide();		
	}
	respClass();
}

function reloader(){		
	if($('#loader').is(':hidden')){
		loaderShow();
	}
	if($('#menu_cont').is(':visible')){
		showMenu();
	}
	var datos = {
		'id': login,
		'pais': pais
	};
	if(pais && !documentos){
		getDocumentos();
	}
	var positionar = position;
    console.log('Hola',positionar, datos, extras);
	$.extend(datos, extras);
	$.ajax({
		url: 'views/' + positionar + '.php',
		method: 'POST',
		data: datos,
		error: function () {
			$('#loader').one("hide", function() {				
				showOffError(error);
                position = historia[historia.length - 1]['position'];
                extras = historia[historia.length - 1]['extras'];
			});			
			loaderHide();
		},
		success: function (datas, status, jqXHR) {            
			$('#modals_cont').html('');
			setHistoria(position, extras);		
			$('#m_cont').html($(datas).find('#contenido').addBack('#contenido'));
			$('#m_cont .modal').each(function (index, element) {
                $(element).clone().appendTo('#modals_cont');
				$(element).remove();
			});
			$(window).scrollTop(0);
			$('.bt-menu').removeClass('active');
			if(!activo_bt){
				$('.bt-'+positionar).addClass('active');
			}else{
				$('.bt-'+activo_bt).addClass('active');
			}			
			if(googlea){
				ga('send', 'event', 'navegacion', positionar, positionar);
            }
			id_propiedad = (positionar == 'propiedad')?datos.iden:0;			
		}
	});
}

function loader(destino, datos, activo) {    
	position = destino;
	extras = datos;
	activo_bt = (activo)?activo:false;
	reloader();
}

function RecordarD(){
	// $('#mod-setLogin').one('hidden.bs.modal', function () {
    //     openData('recordarD');
    // });
	$('#mod-loginRec').one('hidden.bs.modal', function () {
        openData('recordarD');
    });
	$('#mod-loginRec').modal('hide');
}

function Valrecdata(){	
	if($('#form-recordarD').parsley().validate()){		
        loaderShow();
        var datos = $('#form-recordarD').serializeArray();
        datos.push({ name: "accion", value: 3 });        
        $.ajax({
            url: dirCont,
            data: $.param(datos),
            method:  'POST',
            dataType: 'json',
            error: function () {
                grecaptcha.reset(widGo2);                
                $('#loader').one("hide", function() { 
                    setWid('form-recordarD', true);
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {					
                if(response.resp){                    
                    $('#mod-recordarD').one('hidden.bs.modal', function () {
                        showGood(response.data.msg);
                    });
                    $('#loader').one("hide", function() { 
                        $('#mod-recordarD').modal('hide');
                    });
                    loaderHide();                    						
                }else{
                    grecaptcha.reset(widGo2);                    
                    $('#loader').one("hide", function() { 
                        setWid('form-recordarD', true);
                        showError(response.data.msg);
                    });
                    loaderHide();
                }					
            }
        });
	}
}

function setHistoria(destino, datos){
    var posihis = {'position':destino, 'extras': datos};
    if(typeof historia[historiapos] === 'undefined' || historia[historiapos]['position'] != destino || historia[historiapos]['extras'] != datos){
        historia.push(posihis);
        history.pushState((historia.length - 1), null, null);        
        historiapos = historia.length - 1;        
    }else{
        history.replaceState(historiapos, null, null);
    }
}

function setReg(tipo, accion, parametros){
	var actio = (tipo == 1)?1:5;
	if(tipo == 1){
		$('#mod-loginRec .cont_reg').hide();		
		$('#mod-loginRec .cont_reg.req input').removeAttr('required');
		$('#mod-loginRec .cont_reg.req select').removeAttr('required');
	}else{
		$('#mod-loginRec .cont_reg').show();
		$('#mod-loginRec .cont_reg.req input').prop('required',true);
		$('#mod-loginRec .cont_reg.req select').prop('required',true);
	}
	// $('#mod-setLogin').one('hidden.bs.modal', function () {
	// 	$('#mod-loginRec').one('shown.bs.modal', function () {
	// 		$('#form-loginRec .accion').val(actio);			
	// 	});
    // });
    // console.log(actio);
    openData('loginRec');
	$('#mod-setLogin').modal('hide');
    $('#form-loginRec .accion').val(actio);	
}

function logIn(iden, record, init){
	var datos = {
		'accion': 2,
		'id': iden
	};
	$.ajax({
		url: dirCont,
		data: datos,
		method:  'POST',
		dataType: 'json',
		error: function () {
            $('#loader').one("hide", function() { 
                showError(error);
            });
            loaderHide();
		},
		success:  function (response) {
			if(response.resp){
				login = response.data.id;
				rol = response.data.rol;
				if(record){
					LocalSt(login, true);
				}else{
					LocalStrem();
				}                
				$('.log-out').hide();
				$('.log-in').show();
				if(init){
					reloader();
				}
			}else{				
				$('#loader').one("hide", function() { 
					showError(response.data.msg);
				});
                loaderHide();
			}					
		}
	});
}

function Vallog(){
    console.log($('#form-loginRec').parsley());
    if($('#form-loginRec').parsley().validate()){
        console.log('Hola', $('#form-loginRec').serializeArray());
		loaderShow();
		var datos = $('#form-loginRec').serializeArray();
        console.log(datos,dirCont);
		datos.push({ name: "accion", value: $('#form-loginRec .accion').val() });
		$.ajax({
            url: dirCont,
            data: $.param(datos),
            method:  'POST',
            dataType: 'json',
            error: function () {
                grecaptcha.reset(widGo2);				
                $('#loader').one("hide", function() { 
                    setWid('form-loginRec', true);					
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {					
                if(response.resp){
					login = response.data.id;
					var record = ($('#recordar').is(':checked')) ? true : false;                    
                    logIn(response.data.id, record);
					$('.nombre_us').html(response.data.nombre);
					if($('#mod-setLogin .accion').val() != ''){						
						$('#mod-loginRec').one('hidden.bs.modal', function(){
							var accion = $('#mod-setLogin .accion').val();
							if($('#mod-setLogin .parametros').val() != ''){
								var parametros = JSON.parse($('#mod-setLogin .parametros').val());
								window[accion](...parametros);							
							}else{
								window[accion];
							}
						});
						$('#mod-loginRec').modal('hide');
					}else{
						$('#mod-loginRec').modal('hide');					
						reloader();
					}
                    
                }else{
                    grecaptcha.reset(widGo2);
                    $('#loader').one("hide", function() { 
                        setWid('form-loginRec', true);
                        showError(response.data.msg);
                    });
                    loaderHide();
                }					
            }
        });
	}
}

function logOut(){	
	login = false;
	rol = false;
	$('.log-out').show();
	$('.log-in').hide();
	LocalStrem();
	reloader();
}

function loaderShow(inicio){
    if($('#loader').is(":hidden")){
        $('#loader').fadeIn(400, function(){
			$('#loader').show();
		});		
    }			
}

function loaderHide(){
	$('#loader').fadeOut(400, function(){
		$('#loader').hide();
	});	
}

function SetSesion(){
	if(id_propiedad > 0){
		var datos = {
			'accion': 4,
			'id_user': (login)?login:'',
			'id_propiedad': id_propiedad
		};
		$.ajax({
			url: dirCont,
			data: datos,
			method:  'POST',
			dataType: 'json',
			error: function () {
				setTimeout(SetSesion, 60000);
			},
			success:  function (response) {
				setTimeout(SetSesion, 60000);
				if(response.resp){
					sesionT = response.data.fecha;										
				}				
			}
		});
	}else{
		setTimeout(SetSesion, 60000);
	}		
}

function openlogIn(accion, parametros){

	if(login){
		if(accion){			
            if(parametros){                
                window[accion](...parametros);							
            }else{
                window[accion];
            }
		}		
	}else{
		if(accion){
			$('#mod-setLogin').one('shown.bs.modal', function(){
				$('#mod-setLogin .accion').val(accion);
				if(parametros){
					$('#mod-setLogin .parametros').val(JSON.stringify(parametros));
				}
			});
		}
		openData('setLogin');
	}	
}

function openReg(){
	$('#form-registro input').val('');
    $('#form-registro select').val('');
    $('#form-registro select.set_codep').data('valori', '').trigger('change');
    $('#form-registro input:checkbox').prop('checked', false);
    $('#form-registro').parsley().reset();
    grecaptcha.reset(widGo2);
    setWid('form-registro');
	if($('#form-login .cond').val() != ''){
		$('#form-registro .cond').val(1);
		$('#form-login .cond').val('');
		$('#mod-registro').one('hidden.bs.modal', function(){
			if($('#form-registro .cond').val() != ''){
				if($('#form-registro .iden').val() != ''){
					reloader();
				}else{
					if(historia.length > 0){
						window.history.back();
					}else{
						loader('inicio');
					}
				}				
			}
		});
	}
	$('#mod-login').one('hidden.bs.modal', function(){
		$('#mod-registro').modal('show');		
	});
	$('#mod-login').modal('hide');
}

function inputvars_Base(ele, extras){
    var varCus = ele.data('funcionvar');
    var cont = ele.data('imgsho');
    var fdestino = ele.data('imgcont');
    var indom = ele.parents('.file_up').first().find('input[type="file"]');
    var forigen = indom.attr('name');
    var inputvars = (extras)?$.extend(true, {}, window[varCus], extras):window[varCus];    
    
    var inputvars_especial = {
        maxFileSize: 4000,
        maxFileCount: 1,
        elErrorContainer: '#'+ele.parents('form').first().attr('id')+' .error_upload_file',
        initialPreviewShowDelete: false,        
        showCancel: false,
        showUpload: true,
        showRemove: false,
        showBrowse: false,
        uploadExtraData: function(){
            return {
                'carpeta': fdestino,
                'user': login,
                'origen': forigen
            }
        }
    };      
    indom.fileinput('destroy').fileinput($.extend(true, {}, inputDefaults, inputvars_especial, inputvars))
    .on('filecleared', function(event) {        
        ele.val('');
    })
    .on('filepreajax', function(event, previewId, index) {
        loaderShow();
    })
    .on('fileloaded', function(event, file, previewId, index, reader) {       
        ele.val('');
    })
    .on('fileuploaderror', function(event, data, msg) {
        if(data.formdata === null){
            showError(msg);
        }else{
            loaderHide();
            showError('Error en la subida de archivos, inténtalo de nuevo.');						
        }
        $('#error_mod').one('shown.bs.modal', function () {
            indom.fileinput('clear');
        });					
    })
    .on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        ele.parent('.file_up').first().find('.file-input.theme-fas').addClass('file-input-ajax-new');        
        ele.val(response.extras);
        loaderHide();					
    });
    ele.parent('.file_up').first().find('.file-input.theme-fas').addClass('file-input-ajax-new');
}

function SetCodep(elem, filtro){
    var selected = (elem.data('valori'))?elem.data('valori'):"";
    var destino = $(elem.data('destino'));
    var objeto = elem.data('relacion');
    var datos =  $.extend({}, objeto, {'valor': elem.val()});
    if(filtro){
		$.extend(datos, {'filtro': filtro});
	}
    if(elem.val() == ''){
        destino.html('<option value="">Ciudad</option>');
        destino.val('').trigger('change');
    }else{
        $.ajax({
			url: 'controllers/codep.php',
			data: datos,
			method:  'POST',
			dataType: 'json',
			error: function () {
				destino.html('<option value="">Ciudad</option>');
				destino.val('').trigger('change');
			},
			success:  function (response) {
				destino.html(response.datos);
				if(selected != ""){
					destino.val(selected).trigger('change');
				}else{
					destino.val('').trigger('change');
				}
			}
		});
    }
}

$(document).ready(function(){     
    $("#sendMessage").keypress(function(e) {
      if(e.which == 13) {
        sendMessage($('#id-user-to-send').val());
      }
    });
});

function sendMessage(userId){
    if(login){
        var datos = {
            'idveruser': (login)?login:'',
            'accion':15,
            'userChat':userId,
            'message': $('#sendMessage').val()
        };

        $.ajax({
            url: dirCont,
            data: datos,
            method:  'POST',
            dataType: 'json',
            error: function () {
                $('#loader').one("hide", function() { 
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {  
               $('#sendMessage').val('');
               chatMessage(userId);
            }
        });
    }
    let message = $('#sendMessage').val();
    console.log(message);
}
function chatMessage(userId) {
    userwithChat = userId;
    if(login){
        var datos = {
            'idveruser': (login)?login:'',
            'accion':14,
            'userChat':userId
        };
        $.ajax({
            url: dirCont,
            data: datos,
            method:  'POST',
            dataType: 'json',
            error: function () {
                $('#loader').one("hide", function() { 
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {
                let message = '';
                $( "div #messageByUser").html("");
                console.log(response);
                response.data.chat.forEach(chat => {
                        if(chat[0] == response.data.login){
                            message += '<div class="col col-md-12" id="messageByUser" style="display:flex; justify-content: flex-end; align-items: flex-end">'+
                                            '<div class="col col-md-8 align-self-end bg-danger text-white rounded p-3 text-end d-flex flex-row-reverse text-break" >'+
                                                chat[2]+
                                            '</div>'+
                                        '</div>'+
                                        '<p class="small text-muted text-end">'+chat[3]+'</p>'
                        }else{
                            message += '<div class="col col-md-12" >'+
                                            '<div class="col col-md-8 align-self-end bg-secondary text-white rounded p-3 text-end d-flex text-break">'+
                                                chat[2]+
                                            '</div>'+
                                        '</div>'+
                                        '<p class="small text-muted">'+chat[3]+'</p>'
                        }
                    }
                )
                message += '<input id="id-user-to-send" value="'+userId+'" hidden>';
                $( "div #messageByUser").html(message);
                $( "#field-message").css('display','flex');
                $('#sendMessage').focus();
            }
        });
    }
}

function chatUsers() {
    if(login){
        var datos = {
            'idveruser': (login)?login:'',
            'accion':13
        };
        $.ajax({
            url: dirCont,
            data: datos,
            method:  'POST',
            dataType: 'json',
            error: function () {
                $('#loader').one("hide", function() { 
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {
                console.log(response.data);
                $( "div #usersChat").html("");
                response.data.forEach(user => {
                    $( "div #usersChat").append(
                        '<div class="chat col col-md-12 row" style="width:100%; height:50px; border-bottom: 1px solid #D6D6D6; padding:1px; cursor:pointer" onClick="chatMessage('+user[0]+')">'+
                                '<div class="col col-md-11">'+
                                    user[1]+
                                '</div>'+
                                ((user[2] == '0' )?'': '<div class="col col-md-1 rounded-circle bg-danger text-white text-center" style="height:25px;">'+user[2]+'</div>')+
                        ' </div>'
                    );
                  }
                )
            }
        });
    }
}
function openChat(){
    chatUsers()
    $('#form-chat'+' .id').val(0);
		$('#mod-chat').modal('show');
		if($('#loader').is(':visible')){
			loaderHide();
		}
}

function openData(elem, iden){
	$('#form-'+elem+' input:not(.noclear)').val('');
	$('#form-'+elem+' textarea:not(.noclear)').val('');
	$('#form-'+elem+' textarea:not(.noclear)').html('');
	if($('#form-'+elem+' textarea.summer:not(.noclear)').length > 0){
		$('#mod-'+elem).one('shown.bs.modal', function() {
			$('#form-'+elem+' textarea.summer:not(.noclear)').each(function( index ) {			
				$(this).summernote({
					placeholder: $(this).attr('placeholder'),
					dialogsInBody: true,
					tabsize: 2,
					height: 200,
					toolbar: [
						['style', ['style', 'bold', 'italic', 'underline']],												
						['para', ['ul', 'ol', 'paragraph']],
						['table', ['table']],

						['view', ['codeview']]
					],
					lang: 'es-ES',
					codemirror: {
						theme: 'monokai'
					}
				});
				$(this).summernote('code', '');
			});			
			$('#mod-'+elem).one('hidden.bs.modal', function() {
				$('#form-'+elem+' textarea.summer:not(.noclear)').each(function( index ) {			
					$(this).summernote('destroy');
				});
			});
		});
	}	
	$('#form-'+elem+' input:checkbox:not(.noclear)').prop('checked', false);
	$('#form-'+elem+' select:not(.noclear)').val('');
	$('#form-'+elem+' select.set_codep:not(.noclear)').data('valori', '').trigger('change');
	if($('#form-'+elem+' .file-input.theme-fas').length > 0){
		$('#form-'+elem+' .file-input.theme-fas input[type="file"]').fileinput('clear');
        $('#form-'+elem+' input[data-funcion]').each(function() {
            var este = this;
            if($(este).data('funcion') == 'inputvars_Base'){
                window[$(este).data('funcion')].apply(null,[                    
                    $(este)
                ]);
            }else{                
                window[$(este).data('funcion')].apply(null); 
            }            
        });        
	}	
	if($('#form-'+elem+' input.date:not(.noclear)').length > 0){
        $('.flatpickr-calendar input').addClass('form-control');
        $('.flatpickr-calendar select').addClass('form-select d-inline-block');
		$('#form-'+elem+' input.date:not(.noclear)').each(function( index ) {
            var este = this;
            try{
                window[$(este).data('vardate')].clear();                
            } catch (err){}
            $(este).removeClass('invisible');
		});        
	}
	if($('#form-'+elem+' .input-group.colorPick').length > 0){
		$('#form-'+elem+' .input-group.colorPick input').val('');
		$('#form-'+elem+' .input-group.colorPick input').each(function( index ) {
			$(this).spectrum('set');		  
		});       						
    }	
	
	$('#form-'+elem).parsley().reset();
	if(iden){		
		loaderShow();
		$.ajax({
			url: dirCont,
			data: {
				id: iden,
				db: $('#form-'+elem+' .db').val(),                
				accion: 7,
                'idveruser': login
			},
			type: 'post',
			dataType: 'json',
			error: function () {
				loaderHide();
				$('#loader').one("hide", function() { 
					showError(error);
				});
			},
			success: function (response) {
				if (response.resp) {					
					$.each( response.data, function( key, value ) {
						if($('#form-'+elem+' .'+key).prop('multiple')){
							if(value != ''){
								$('#form-'+elem+' .'+key+' option').prop("selected", false);
								$.each(value.split(","), function(i,e){
									$('#form-'+elem+' .'+key+' option[value="'+e+'"]').prop("selected", true);								
								});
							}
							$('#form-'+elem+' .'+key).bootstrapDualListbox('refresh');
						}else{
							$('#form-'+elem+' .'+key).val(value);
							if($('#form-'+elem+' .'+key).hasClass('summer')){
								$('#mod-'+elem).one('shown.bs.modal', function() {
									$('#form-'+elem+' .'+key).summernote('code', value);
								});
							}
                                                       
							$('#form-'+elem+' .set_codep.'+key).data('valori', response.data[$('#form-'+elem+' .set_codep.'+key).data('destino')]);
							$('#form-'+elem+' .set_codep.'+key).trigger('change');
							if($('#form-'+elem+' .'+key).data('imgcont')){
								if(value != ''){
                                    if($('#form-'+elem+' .'+key).data('funcion')){
                                        if($('#form-'+elem+' .'+key).data('funcion') == 'inputvars_Base'){
                                            if($('#form-'+elem+' .'+key).data('prevurl')){
                                                window[$('#form-'+elem+' .'+key).data('funcion')].apply(null,[                                                
                                                    $('#form-'+elem+' .'+key),                                                
                                                    {                                                
                                                        initialPreview:(value.startsWith("http"))?[
                                                            value
                                                        ]:[
                                                            $('#form-'+elem+' .'+key).data('prevurl')+'/'+value
                                                        ],
                                                        initialPreviewAsData: true
                                                    }
                                                ]);                                                
                                            }else{
                                                window[$('#form-'+elem+' .'+key).data('funcion')].apply(null,[                                                
                                                    $('#form-'+elem+' .'+key),                                                
                                                    {                                                
                                                        initialPreview:(value.startsWith("http"))?[
                                                            value
                                                        ]:[
                                                            $('#form-'+elem+' .'+key).data('imgcont')+'/'+value
                                                        ],
                                                        initialPreviewAsData: true
                                                    }
                                                ]);                                                
                                            }
                                        }else{
                                            window[$('#form-'+elem+' .'+key).data('funcion')].apply(null, [
                                                {                                                
                                                    initialPreview:(value.startsWith("http"))?[
                                                        value
                                                    ]:[
                                                        $('#form-'+elem+' .'+key).data('imgcont')+'/'+value
                                                    ],
                                                    initialPreviewAsData: true
                                                }
                                            ]); 
                                        }                                                                                                                      
                                    }
								}		
							}
							
							if($('#form-'+elem+' .input-group.colorPick .'+key).length > 0){
                                $('#form-'+elem+' .'+key).spectrum('set', value);															      						
							}
							
							if($('#form-'+elem+' .'+key+'.date').length > 0){
								window[$('#form-'+elem+' .'+key).data('vardate')].setDate(value);
							}
							if($('#form-'+elem+' .'+key).hasClass('icono-selector')){								
								icono_select.setIcon(value);
							}
						}												
					});					
					loaderHide();
					$('#loader').one("hide", function() { 
						$('#mod-'+elem).modal('show');
					});
				} else {
					loaderHide();
					$('#loader').one("hide", function() { 
						showError(response.data.msg);
					});
				}
			}
		});
	}else{
		$('#form-'+elem+' .id').val(0);
		$('#mod-'+elem).modal('show');
		if($('#loader').is(':visible')){
			loaderHide();
		}
	}
}

function consData(bd, iden, datos, callback){
    $.ajax({
        url: dirCont,
        data: {
            'id': iden,
            'db': bd,                
            'accion': 7,
            'idveruser': login
        },
        method:  'POST',
        dataType: 'json',
        error: function () {						
            callback(false);
        },
        success:  function (response) {
            if (response.resp) {
                var respuesta = {};
                $.each( datos, function( index, value ) {
                    respuesta[value] = response.data[value];								
                });
                callback(respuesta);
            }else{
                callback(false);
            }						
        }
    });
}

function Valform(formulario, accion, params, loaderhide, modalin, acciont){
	if($('#'+formulario).parsley().validate()){
		loaderShow();
		if($('#'+formulario+' .summer').length > 0){
			$('#'+formulario+' .summer').each(function( index ) {
				$(this).val('**esSummertexto**'+$(this).summernote('code').replace(/</g, '*{*').replace(/>/g, '*}*'));
			});
		}
        if($('#'+formulario+' .db').length > 0){
            $('#'+formulario).find(':input').prop('disabled', false);
            var datos = $('#'+formulario).serializeArray();	
            datos.push({ name: "accion", value: (acciont)?acciont:6 });
            datos.push({ name: "editor", value: login });
            datos.push({ name: "idveruser", value: login });        
            $.ajax({
                url: dirCont,
                data: $.param(datos),
                method:  'POST',
                dataType: 'json',
                error: function () {					
                    $('#loader').one("hide", function() { 
                        showError(error);
                    });
                    loaderHide();
                },
                success:  function (response) {					
                    if(response.resp){
                        if($('.modal.show').length > 0){
                            var modid = (modalin)?$('#'+modalin):$('.modal.show');
                            modid.one('hidden.bs.modal', function(){
                                if(loaderhide){
                                    if(accion){
                                        var para = [response.data.id, response.data.msg];
                                        var npara = $.merge(params, para);
                                        accion.apply(null, npara);
                                    }								
                                    loaderHide();
                                    $('#loader').one("hide", function() { 
                                        showGood(response.data.msg);
                                    });
                                }else{
                                    if(accion){
                                        var para = [response.data.id, response.data.msg];
                                        var npara = $.merge(params, para);
                                        accion.apply(null, npara);
                                    }                                
                                }
                            });
                            modid.modal('hide');
                        }else{
                            if(loaderhide){
                                if(accion){
                                    var para = [response.data.id, response.data.msg];
                                    var npara = $.merge(params, para);
                                    accion.apply(null, npara);
                                }							
                                $('#loader').one("hide", function() { 
                                    showGood(response.data.msg);
                                });
                                loaderHide();
                            }else{
                                if(accion){
                                    var para = [response.data.id, response.data.msg];
                                    var npara = $.merge(params, para);
                                    accion.apply(null, npara);
                                }							
                            }
                        }					
                    }else{					
                        $('#loader').one("hide", function() { 
                            showError(response.data.msg);
                        });
                        loaderHide();
                    }					
                }
            });            
        }else{
           if($('.modal.show').length > 0){
               var modid = (modalin)?$('#'+modalin):$('.modal.show');
               modid.one('hidden.bs.modal', function(){
                   if(loaderhide){
                       if(accion){
                           var para = [];
                           var npara = $.merge(params, para);
                           accion.apply(null, npara);
                       }
                       loaderHide(); 
                   }else{
                       if(accion){
                           var para = [];
                           var npara = $.merge(params, para);
                           accion.apply(null, npara);
                       }
                   }
               });
               modid.modal('hide');
            }else{
                if(loaderhide){
                    if(accion){
                        var para = [];
                        var npara = $.merge(params, para);
                        accion.apply(null, npara);
                    }
                    loaderHide();
                }else{
                    if(accion){
                        var para = [];
                        var npara = $.merge(params, para);
                        accion.apply(null, npara);
                    }							
                }
            } 
        }        
	}
}

function setPlanGrat(iden){
	if(login){
		var datos = {
			'accion': 8,
			'id': iden,
			'idveruser': login
		};
		$.ajax({
            url: dirCont,
            data: $.param(datos),
            method:  'POST',
            dataType: 'json',
            error: function () {					
                $('#loader').one("hide", function() { 
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {					
                if(response.resp){
					showGood(response.data.msg);
					reloader();
				}else{
					$('#loader').one("hide", function() { 
						showError(response.data.msg);
					});
					if(response.data.tiene){
						reloader();
					}else{
						loaderHide();
					}					
				}
			}
		});
	}
}

function getDocumentos(){
	var datos = {
        'accion': 9,
		'pais': pais
    };
    $.ajax({
        url: dirCont,
        data: $.param(datos),
        method:  'POST',
        dataType: 'json',
        error: function () {					
            documentos = false;
        },
        success:  function (response) {					
            if(response.resp){
				documentos = [];
				documentos[0] = response.data.habeas;
				documentos[1] = response.data.terminos;                
            }else{
                documentos = false;					
            }
        }
    });
}

function openDocu(pos){
	if(pos == 1){
		if(documentos && documentos[1] != '' && documentos[1] != null){
			window.open('documentos/'+documentos[1]);
		}		
	}else{
		if(documentos && documentos[0] != '' && documentos[0] != null){
			window.open('documentos/'+documentos[0]);
		}		
	}
}

function closeActModal(accion, params, ismodal){
	if($('.modal.show').length > 0){
		var modid = $('.modal.show');		
		modid.one('hidden.bs.modal', function(){
			if(ismodal){
				$('#'+accion).modal('show');
			}else{
				window[accion](...params);								
			}
		});
        modid.modal('hide');
	}else{
		if(ismodal){
			$('#'+accion).modal('show');
		}else{
			window[accion](...params);				
		}
	}
}

function openVid(video){
    if(video.indexOf('.mp4') >= 0){
        $('#mod-video .video_p').html('<video class="embed-responsive-item" preload="metadata" autoplay controls><source src="'+video+'#t=0.5" type="video/mp4" /></video>');
    }else{
       $('#mod-video .video_p').html('<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'+video+'?autoplay=1&origin='+window.location.origin+'&rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'); 
    }    
    $('#mod-video').one('hidden.bs.modal', function () {
        $('#mod-video .video_p').html('');
    });
    $('#mod-video').modal('show');
}

function setWid(form, vali){
    $('#'+form+' .g-recaptcha-response').prop('required', true);
    $('#'+form+' .g-recaptcha-response').attr('data-parsley-errors-container', '#'+form+' .recerror');
    if(vali){
        $('#'+form+' .g-recaptcha-response').parsley().reset();
        $('#'+form+' .g-recaptcha-response').parsley().validate();
    }
}

function showError(msg) {
	$('#error_mod .error_msg').html(msg);
	$('#error_mod').modal('show');
}

function showOffError(msg) {
	$('#off-error .error_msg').html(msg);
	$('#off-error').offcanvas('show');
}

function showGood(msg) {
	$('#good_mod .good_msg').html(msg);
	$('#good_mod').modal('show');
}

function LocalSt(iden, record) {
	if (typeof (Storage) !== "undefined") {
		localStorage.setItem("npf2021_iden", iden);
		localStorage.setItem("npf2021_reco", true);
	}
}

function LocalStrem() {
	if (typeof (Storage) !== "undefined") {
		localStorage.removeItem("npf2021_iden");
		localStorage.removeItem("npf2021_reco");
	}
}

function isTouchDevice(){
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

function respClass() {
	var inside = $(window).innerHeight() - ($('#pie_pagina').outerHeight());
	var inside2 = inside - $('#barra_nav').outerHeight();
	$('#m_cont').css('padding-top', $('#barra_nav').outerHeight()+'px');	
	if($('.minfull').length > 0){
		$('.minfull').css('min-height', inside2+'px');
	}	
    if($('.flatpickr-calendar').length > 0){
        $('.flatpickr-calendar input').addClass('form-control');
        $('.flatpickr-calendar select').addClass('form-select d-inline-block');        
    }
     
}

function stopPropagation(evt) {
    if (evt.stopPropagation !== undefined) {
        evt.stopPropagation();
 
    } else {
        evt.cancelBubble = true;
    }
}

function remove_accents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');

    return strAccentsOut.replace(/[^a-z0-9\s]/gi, '').replace(/[-\s]/g, '_').toLowerCase();
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}