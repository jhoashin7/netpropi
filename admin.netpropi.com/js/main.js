var position = 'log';
var extras = {};
var historia = [];
var historiapos = 0;
var activo_bt = false;
var dirCont = 'controllers/controller.php';
var login = false;
var rol = false;
var sesionT = false;
var error = 'Se presento un problema de conexión, inténtalo de nuevo más tarde.';
var listo = false;
var geoLocation = false;
var googlea = false;
var hubspot = false;
var weatherIcons = {};
var curhora = 0.0;
var curmin = 0.0;
var widGo = false;
var widGo2 = false;
var inputDefaults = {};
var menuCont = {};
var desc = 'npa';
var onmap = false;
var map = false;
var pos = false;
var infoWindow;
var marcadores = [];
var marcador = false;
var geocoder = false;
var bounds;
var heatmap = false;
var markerCluster = null;
var estilos_map = [
  {
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#f5f5f5"
	  }
	]
  },
  {
	"elementType": "labels.icon",
	"stylers": [
	  {
		"visibility": "off"
	  }
	]
  },
  {
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#616161"
	  }
	]
  },
  {
	"elementType": "labels.text.stroke",
	"stylers": [
	  {
		"color": "#f5f5f5"
	  }
	]
  },
  {
	"featureType": "administrative.land_parcel",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#bdbdbd"
	  }
	]
  },
  {
	"featureType": "poi",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#eeeeee"
	  }
	]
  },
  {
	"featureType": "poi",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#757575"
	  }
	]
  },
  {
	"featureType": "poi.park",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#e5e5e5"
	  }
	]
  },
  {
	"featureType": "poi.park",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#9e9e9e"
	  }
	]
  },
  {
	"featureType": "road",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#ffffff"
	  }
	]
  },
  {
	"featureType": "road.arterial",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#757575"
	  }
	]
  },
  {
	"featureType": "road.highway",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#dadada"
	  }
	]
  },
  {
	"featureType": "road.highway",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#616161"
	  }
	]
  },
  {
	"featureType": "road.local",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#9e9e9e"
	  }
	]
  },
  {
	"featureType": "transit.line",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#e5e5e5"
	  }
	]
  },
  {
	"featureType": "transit.station",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#eeeeee"
	  }
	]
  },
  {
	"featureType": "water",
	"elementType": "geometry",
	"stylers": [
	  {
		"color": "#c9c9c9"
	  }
	]
  },
  {
	"featureType": "water",
	"elementType": "labels.text.fill",
	"stylers": [
	  {
		"color": "#9e9e9e"
	  }
	]
  }
];
var inputDefaults = {
    theme: 'fas',
    language: 'es',
    layoutTemplates: {
        main2: '{preview}\n<div class="kv-upload-progress kv-hidden"></div>\n<div class="clearfix"></div>\n' + '<div class="btn-group btn-group-sm d-flex w-100" role="group">\n{upload}\n</div>\n',
        modal: '<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg{rtl}" role="document">\n' + 
        '   <div class="modal-content">\n' +
        '       <div class="modal-header flex-row">\n' + 
        '           <h5 class="modal-title">\n' +
        '               <span class="text-responsive d-flex justify-content-start align-items-center">\n' +
        '                   <span class="fa-stack text-warning align-top">\n' +
        '                           <i class="fas fa-circle fa-stack-2x"></i>\n' +
        '                           <i class="fas fa-search-plus fa-stack-1x text-white"></i>\n' +
        '                   </span>\n' +                                
        '                   <span class="lh-1">{heading}<small class="text-muted d-block kv-zoom-title" style="max-width: unset;"></small></span>\n' +
        '               </span>\n' +
        '           </h5>\n' +
        '           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>\n' +
        '       </div>\n' +
        '       <div class="modal-body">\n' +
        '           <div class="floating-buttons"></div>\n' +
        '           <div class="kv-zoom-body file-zoom-content {zoomFrameClass}"></div>\n' + '{prev} {next}\n' +
        '       </div>\n' +
        '   </div>\n' +
        '</div>'
    },
    browseClass: 'btn btn-warning text-white w-100 border-preselect',
    removeClass: 'btn btn-danger w-100',
    cancelClass: 'btn btn-danger w-100',
    uploadClass: 'btn btn-warning text-white w-100',
    previewZoomButtonClasses: {
        prev: 'd-none',
        next: 'd-none',
        toggleheader: 'd-none',
        fullscreen: 'd-none',
        borderless: 'd-none',
        close: 'btn btn-kv btn-default btn-outline-secondary'
    },
    previewZoomButtonIcons: {
        close: '<i class="fas fa-times fa-fw"></i>'
    },
	mimeTypeAliases: {
		'audio': 'audio/mpeg',
		'video': 'video/mp4'
	},
    zoomIndicator : '<i class="fas fa-search-plus fa-fw"></i>',
    previewFileIconSettings: {
        'gltf': '<i class="fas fa-cube text-muted"></i>'
    },		
    fileActionSettings: {
        showDownload: false,
        showUpload: false,
        showRemove: false,
        zoomIcon: '<i class="fas fa-search-plus fa-fw"></i>'
    },
    buttonLabelClass: 'd-none d-sm-inline',
    browseIcon: '<i class="fas fa-folder-open fa-fw"></i>&nbsp;',
    removeIcon: '<i class="fas fa-trash fa-fw"></i>&nbsp;',
    uploadIcon: '<i class="fas fa-cloud-upload-alt fa-fw"></i>&nbsp;',
    showCaption: false,
    autoReplace: true,
    uploadAsync: true,
    overwriteInitial: true,
    showUploadedThumbs: true,
    browseOnZoneClick: true,
    showAjaxErrorDetails: false,
    allowedPreviewTypes: ['image', 'video', 'audio', 'pdf'],
    disabledPreviewTypes: ['text', 'object', 'office', 'html', 'other'],
    uploadUrl: 'controllers/archivo.php'
};


if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js?v='+Math.floor(Math.random() * 100));
}

var onloadCallback = function() {
  init();
};

var initMap = function(){
	onmap = true;
	geocoder = new google.maps.Geocoder();
};

var setMap = function(){
	onmap = true;
	geocoder = new google.maps.Geocoder();	
};

function initMapa() {
    //geocoder = (geocoder)?geocoder:new google.maps.Geocoder();
    if(map){		
		if(marcadores.length > 0){			
            for (var i = 0; i < marcadores.length; i++) {
                try{					
                    marcadores[i].setMap(null);
                }catch(e){						
                }					
            }
            marcadores = [];
			markerCluster.clearMarkers();
			markerCluster = null;
        }		
		if(heatmap){
			heatmap.setMap(null);
			heatmap = false;
		}
		if(marcador){
			marcador.setMap(null);
			marcador = false;
		}
        
    }				

       	
    if(map){
        $('#map_cont').html(map.getDiv());
        map.setCenter(pos);
        map.setOptions({
            gestureHandling: 'cooperative'
        });
        map.setZoom(12);
        google.maps.event.trigger(map, 'resize');					
    }else{					
        map = new google.maps.Map(document.getElementById('mapa'), {
            center: pos,
            zoom: 12,					
            styles: estilos_map
        });					
    }

    google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
        $(window).trigger('resize');					
    });
}

(function($) {
	$.each(['show', 'hide'], function(i, ev) {		
		var el = $.fn[ev];
		$.fn[ev] = function() {
			this.triggerHandler(ev);			
			return el.apply(this, arguments);
		};
	});
})(jQuery);

$(function() { 
    loaderShow(true);
    window.Parsley.setLocale('es');
    window.Parsley.options.excluded = 'input[type=button], input[type=submit], input[type=reset]';
    window.Parsley.options.errorClass = 'is-invalid';
    window.Parsley.options.successClass = 'is-valid';
    window.Parsley.options.errorsContainer = function classHandler(Field) {return Field.$element.closest('.form-group');};
    window.Parsley.options.errorsWrapper = '<ul class="parsley-errors-list text-right text-danger mb-0 list-unstyled small"></ul>';
    window.Parsley.options.errorTemplate = '<li class="pr-1 text-responsive"></li>';
    window.Parsley.addValidator('typeColor', {
        requirementType: 'string',
        validateString: function(value){
            return tinycolor(value).isValid();
        },
        messages: {
            en: 'The value is not a valid color.',
            es: 'No es un valor de color válido.'
        }
    });
    moment.locale('es');    		
    Chart.defaults.font.size = 10;
    Chart.defaults.font.lineHeight = 1;
    Chart.defaults.plugins.legend.display = false;
    $.fn.printThis.defaults.base = window.location.origin+window.location.pathname;
	$(window).on('load', function(){
		window.loaded = true;
	});
    
    window.addEventListener("popstate", function(e) {
        var estado = historiapos - 1;
        historiapos = estado;
        historia.pop();
        if(estado >= 0){            
            history.pushState(historia.length, null, null); 
        }        
        if(estado !== null && estado !== 'undefined' && historiapos >= 0){            
            var accpos = historia[estado];
            if($('.modal.show').length > 0){
                $('.modal.show').each(function( index ) {
                    if($('.modal.show').length <= 1){
                        $(this).one('hidden.bs.modal', function(){
                            loader(accpos.position, accpos.extras);
                        });                        
                    }
                    $(this).modal('hide');
                              
                });                
            }else{
                loader(accpos.position, accpos.extras);                
            }            
        }else{
            window.history.back();
        }        
    });
    
    let deferredPrompt;
    const addBtn = document.querySelector('.install-bt');
    $('.install-bt').hide();
    
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      // Update UI to notify the user they can add to home screen
      $('.install-bt').show();

      addBtn.addEventListener('click', (e) => {
        // hide our user interface that shows our A2HS button
        $('.install-bt').hide();
        // Show the prompt
        deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
              //console.log('User accepted the A2HS prompt');
            } else {
              $('.install-bt').show();
            }
            deferredPrompt = null;
          });
      });
    });       
	$(document).on('hidden.bs.modal', '.modal', function () {
        $("video").each(function() {
            $(this).get(0).pause();
        });
        $("audio").each(function() {
            $(this).get(0).pause();
        });
		$('.modal:visible').length && $(document.body).addClass('modal-open');
	});
	$(window).resize(respClass);
	if ($(document).fullScreen() != null) {
		$('.full_screen-bt').show();
		$(document).bind("fullscreenchange", function () {
			if ($(document).fullScreen()) {
				$('.full_screen-bt i.fa-stack-1x').removeClass('icon-fullscreen');
				$('.full_screen-bt i.fa-stack-1x').addClass('icon-fullscreen_exit');
			} else {
				$('.full_screen-bt i.fa-stack-1x').removeClass('icon-fullscreen_exit');
				$('.full_screen-bt i.fa-stack-1x').addClass('icon-fullscreen');
			}
		});
	} else {
		$('.full_screen-bt').hide();
	}
	$('#time_min').circleProgress({
        value: 1.0,
        size: 22,
        startAngle: 1.5*Math.PI,
        thickness: 2,
        fill: "#e9ecef",
        emptyFill: "rgba(255,255,255,0.3)"        
    });
    $('#time_hr').circleProgress({
        value: 0.5,
        size: 14,
        startAngle: 1.5*Math.PI,
        thickness: 4,
        fill: "#ffffff",
        emptyFill: "rgba(255,255,255,0.3)"        
    });    
	$.getJSON( "controllers/weathericons.json").done(function( json ) {weatherIcons = json; displayWeater();});	
	displayTime();
	respClass();	
});

function tooltipsOn(){
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        var place = ($(tooltipTriggerEl).data('fallpla'))?$(tooltipTriggerEl).data('fallpla').split(','):['top', 'right', 'bottom', 'left'];
        return new bootstrap.Tooltip(tooltipTriggerEl, { 
            boundary: 'window',
            fallbackPlacements: place,
            popperConfig: function(defaultBsPopperConfig){
                var newPopperConfig = {strategy: 'fixed'};
                return newPopperConfig;
            }
        });
    }); 
}

function init(){	
	/*if(window.loaded){
		reInit();
	}else{
		$(window).on('load', reInit);
	}*/
    reInit();    
}

function reInit() {
	if (typeof (Storage) !== "undefined") {
		if (localStorage.npa_iden && localStorage.npa_reco) {
			logIn(localStorage.npa_iden, true);
		} else {			
			reloader();
		}
	} else {		
		reloader();
	}	
}

function reloader(){
	closeSubMenu();		
	if($('#loader').is(':hidden')){
		loaderShow();
	}	
	var datos = {
		'id': login,
		'rol': rol
	};
	var positionar = position.split(".");
	if(hubspot){		
		$('#pie_pagina').css('padding-right', '80px');
	}
	$.extend(datos, extras);
	$.ajax({
		url: 'views/' + positionar[positionar.length - 1] + '.php',
		method: 'POST',
		data: datos,
		error: function () {			
			loaderHide();
			$('#loader').one("hide", function() { 
				showOffError(error);
                position = historia[historia.length - 1]['position'];
                extras = historia[historia.length - 1]['extras'];
			});
		},
		success: function (datas, status, jqXHR) {            
			$('#modals_cont').html('');
			if(positionar[0] == 'log'){	
                $('#m_cont').removeClass('mb-auto');
				$('#m_cont').addClass('my-auto');				
			}else{
                setHistoria(position, extras);
				$('#m_cont').removeClass('my-auto');
                $('#m_cont').addClass('mb-auto');
			}			
			$('#m_cont').html($(datas).find('#contenido').addBack('#contenido'));
			$('#m_cont .modal').each(function (index, element) {
                $(element).clone().appendTo('#modals_cont');
				$(element).remove();
			});
			$(window).scrollTop(0);
			if(googlea){
				ga('send', 'event', 'navegacion', positionar[positionar.length - 1], positionar[positionar.length - 1]);
            }
		}
	});
}

function loader(destino, datos, activo) {    
	position = destino;
	extras = datos;
	reloader();
}

function setHistoria(destino, datos){
    var posihis = {'position':destino, 'extras': datos};
    if(typeof historia[historiapos] === 'undefined' || historia[historiapos]['position'] != destino || historia[historiapos]['extras'] != datos){
        historia.push(posihis);
        history.pushState((historia.length - 1), null, null);        
        historiapos = historia.length - 1;        
    }else{
        history.replaceState(historiapos, null, null);
    }
}

function Vallog(){	
	if($('#form-log').parsley().validate()){		
        loaderShow();
        var datos = $('#form-log').serializeArray();
        datos.push({ name: "accion", value: 1 });        
        $.ajax({
            url: dirCont,
            data: $.param(datos),
            method:  'POST',
            dataType: 'json',
            error: function () {
                grecaptcha.reset(widGo);                
                $('#loader').one("hide", function() { 
                    setWid('form-log', true);
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {					
                if(response.resp){
                    var record = ($('#recordar').is(':checked')) ? true : false;                    
                    logIn(response.data.id, record);						
                }else{
                    grecaptcha.reset(widGo);                    
                    $('#loader').one("hide", function() { 
                        setWid('form-log', true);
                        showError(response.data.msg);
                    });
                    loaderHide();
                }					
            }
        });
	}
}

function logIn(iden, record){
	var datos = {
		'accion': 2,
		'id': iden
	};
	$.ajax({
		url: dirCont,
		data: datos,
		method:  'POST',
		dataType: 'json',
		error: function () {
            $('#loader').one("hide", function() { 
                showError(error);
            });
            loaderHide();
		},
		success:  function (response) {
			if(response.resp){
				login = response.data.id;
				rol = response.data.rol;
				if(record){
					LocalSt(login, true);
				}else{
					LocalStrem();
				}                
				showMenu();
				ShowPendientes();
				SetSesion();
				loader('inicio');					
			}else{				
				$('#loader').one("hide", function() { 
					showError(response.data.msg);
				});
                loaderHide();
			}					
		}
	});
}

function logOut(){
	$('.alertas-cont .alertas-no').remove();
	$('.alertas-cont').append('<p class="my-0 text-center alertas-no"><small>No hay alertas.</small></p>');
	$('.alertas-total').html(0);
	login = false;
	rol = false;
	LocalStrem();
	toggleMainMenu();
    $('#left_bar').addClass('log-in');
    $('#left_bar .main_menu .items_menu').html('');
    historia = [];
    historiapos = 0;
	loader('log');
    ShowPendientes();
}

function showMenu(){    
    $('#left_bar .main_menu .items_menu').html('');
    $.getJSON( "controllers/menu.json?v="+Math.floor(Math.random() * 100)).done(function( json ) {
		menuCont = json;
		var items = "";
		var accion = "";
        var activo = "";
		$.each( menuCont, function( key, value ) {
            activo = (key == 'inicio')?'active':'';
			accion = (value.hijos)?'openSecondMenu(\''+key+'\');':'loader(\''+key+'\');';
            if(value.hasOwnProperty('unico') && value.unico){
                if(parseInt(rol) != 0 && $.inArray(parseInt(rol), value.rol) >= 0){
					items += '<a href="#" class="bt-'+key+' d-block my-2 text-decoration-none text-muted '+activo+'" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-trigger="hover" data-fallpla="right,left" title="'+value.texto+'" onClick="'+accion+' return false"><span class="text-responsive"><span class="fa-stack fa-lg align-middle"><i class="fas fa-circle fa-stack-2x"></i><i class="'+value.icono+' fa-stack-1x text-white"></i></span></span></a>';
				}                
            }else{
                if((parseInt(rol) != 0 && parseInt(rol) <= value.rol) || value.rol == 0){
					items += '<a href="#" class="bt-'+key+' d-block my-2 text-decoration-none text-muted '+activo+'" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-trigger="hover" data-fallpla="right,left" title="'+value.texto+'" onClick="'+accion+' return false"><span class="text-responsive"><span class="fa-stack fa-lg align-middle"><i class="fas fa-circle fa-stack-2x"></i><i class="'+value.icono+' fa-stack-1x text-white"></i></span></span></a>';
				}
            }            				 
		});
		$('#left_bar .main_menu .items_menu').html(items);
		$('#left_bar').removeClass('log-in');
        if($('#left_bar .main_menu').hasClass("mmen_hide")){
            $('#left_bar').css('right', 'auto');
            $('#left_bar .main_menu').switchClass( "mmen_hide", "mmen_show", 300, "easeInOutQuad", respClass);
            $('#left_bar .main_menu .icon_main_menu').switchClass( "fa-chevron-circle-right", "fa-chevron-circle-left", 300, "easeInOutQuad");            
        }else{
           respClass(); 
        }
        tooltipsOn();
	});
}

function openSecondMenu(destino, url){
    if($('#left_bar .second_menu').hasClass('men_hide')){
        $('#left_bar .second_menu').switchClass( "men_hide", "men_show", 300, "easeInOutQuad");
        $('#left_bar .over-black').fadeIn(300);
    }    
	$('#left_bar .second_menu .items_menu').html('<div class="w-100 text-center my-2 text-muted"><div class="fa-3x text-center mb-n2"><i class="fas fa-circle-notch fa-spin"></i></div><small class="fw-bold">cargando</small></div>');
	$('#left_bar .second_menu .items-nombre').html('');
    if(url){
		$('#left_bar .second_menu .items-nombre').html('<i class="'+destino[2]+' fa-lg fa-fw"></i> '+destino[1]);
		$.ajax({
			url: 'views/' + destino[0] + '.php',
			method: 'POST',
			data: {"id": login},
			error: function () {			
				$('#left_bar .second_menu .items_menu').html('<div class="w-100 text-center text-muted"><div class="fa-3x text-center mb-1"><i class="fas fa-exclamation-circle"></i></div><small><span class="text-responsive">'+error+'</span></small></div>');
			},
			success: function (datas, status, jqXHR) {
				$('#left_bar .second_menu .items_menu').html($(datas).find('#contenido').addBack('#contenido'));
			}
		});
	}else{
		$('#left_bar .second_menu .items-nombre').html('<i class="'+menuCont[destino].icono+' fa-lg fa-fw"></i> '+menuCont[destino].texto);
		var items = "";
		var accion = "";
		var activo = "";		
		var btn_activo = (activo_bt)?activo_bt.split("."):position.split(".");
		$.each( menuCont[destino].hijos, function( key, value ) {
			activo = (key == btn_activo[1])?'active':'';
			accion = 'loader(\''+destino+'.'+key+'\');';
            if(value.hasOwnProperty('unico') && value.unico){                 
                if(parseInt(rol) != 0 && $.inArray(parseInt(rol), value.rol) >= 0){
                    items += '<a href="#" class="bt-'+key+' d-flex align-items-center w-100 my-2 text-decoration-none text-muted '+activo+'" onClick="'+accion+' return false"><span class="text-responsive"><span class="fa-stack align-middle"><i class="fas fa-circle fa-stack-2x"></i><i class="'+value.icono+' fa-stack-1x text-white"></i></span></span><small class="fw-bold"><span class="text-responsive">'+value.texto+'</span></small></a>';                    
                }                
            }else{                
                if((parseInt(rol) != 0 && parseInt(rol) <= value.rol) || value.rol == 0){
                    items += '<a href="#" class="bt-'+key+' d-flex align-items-center w-100 my-2 text-decoration-none text-muted '+activo+'" onClick="'+accion+' return false"><span class="text-responsive"><span class="fa-stack align-middle"><i class="fas fa-circle fa-stack-2x"></i><i class="'+value.icono+' fa-stack-1x text-white"></i></span></span><small class="fw-bold"><span class="text-responsive">'+value.texto+'</span></small></a>';
                }                
            }			
		});
		$('#left_bar .second_menu .items_menu').html(items);
	}
	
}

function loaderShow(inicio){	
	listo = false;
    if($('#loader').is(":hidden") || inicio){
        $('#loader .water_cont').css('height', '0%');
        $('#loader .ola-left').css('left', '-100%');
        $('#loader .ola-right').css('right', '-100%');
        $('#loader').show();
        $('#loader .water_cont').animate({
            height: "70%"
        }, 1000, 'easeOutBack', function(){
            creteDrop();
        });
        $('#loader .ola-left').animate({
            left: "100%"
        }, 4500, 'easeOutBack');
        $('#loader .ola-right').animate({
            right: "100%"
        }, 4500, 'easeOutBack');        
    }			
}
function creteDrop(){
    if(!$('#loader .drop_anime').hasClass('on')){
        $('#loader .drop_anime').addClass('on inf');
    }
    if(listo){
        $('#loader .drop_anime.on').one('animationend webkitAnimationEnd oAnimationEnd', function(){
            $('#loader .drop_anime').removeClass('on');
			loaderHiden();
        });
        $('#loader .drop_anime').removeClass('inf');
    }else{
        setTimeout(creteDrop, 400);
    } 
}
function loaderHide(){
	listo = true;
}
function loaderHiden(){
    $('#loader .water_cont').css('height', '70%');
	$('#loader .ola-left').css('left', '100%');
	$('#loader .ola-right').css('right', '100%');
    $('#loader .water_cont').animate({
		height: "0%"
	},{
		duration: 1000,
		easing: 'easeInBack',
		queue: false
		
	});
	$('#loader .ola-left').animate({
		left: "-100%"
	}, {
		duration: 1500,
		easing: 'easeInSine',
		queue: false
		
	});
	$('#loader .ola-right').animate({
		right: "-100%"
	}, {
		duration: 1500,
		easing: 'easeInSine',
		queue: false,
		done: function(){
			listo = false;
			$('#loader').hide();
		}
		
	});
}

function toggleMainMenu(){
    if($('#left_bar .main_menu').hasClass("mmen_show")){
        if($('#left_bar .second_menu').hasClass("men_show")){
            closeSubMenu();            
        }
        $('#left_bar .main_menu').switchClass( "mmen_show", "mmen_hide", 300, "easeInOutQuad", function(){$('#left_bar').css('right', '100%');respClass();});
        $('#left_bar .main_menu .icon_main_menu').switchClass( "fa-chevron-circle-left", "fa-chevron-circle-right", 300, "easeInOutQuad");        
    }else{
        $('#left_bar').css('right', 'auto');
        $('#left_bar .main_menu').switchClass( "mmen_hide", "mmen_show", 300, "easeInOutQuad", respClass);
        $('#left_bar .main_menu .icon_main_menu').switchClass( "fa-chevron-circle-right", "fa-chevron-circle-left", 300, "easeInOutQuad");
    }
}

function closeSubMenu(){
    $('#left_bar .second_menu').switchClass( "men_show", "men_hide", 300, "easeInOutQuad");
    $('#left_bar .over-black').fadeOut(300, function(){
        $('#left_bar .second_menu .items-nombre').html('');
        $('#left_bar .second_menu .items_menu').html('');
    });
}

function SetSesion(){
	if(login){
		var datos = {
			'accion': 4,
			'idveruser': login
		};
		$.ajax({
			url: dirCont,
			data: datos,
			method:  'POST',
			dataType: 'json',
			error: function () {
				setTimeout(SetSesion, 60000);
			},
			success:  function (response) {
				setTimeout(SetSesion, 60000);
				if(response.resp){
					sesionT = response.data.fecha;										
				}				
			}
		});
	}	
}

function showSes(iden, nombre){
	$('#mod-ses .modal-title small').html(nombre);	
	$('#mod-ses').one('show.bs.modal', function () {
		if($('#sesiones').length > 0){
			$('#sesiones').DataTable().destroy(true);
		}
		$('#mod-ses .modal-body').prepend('<table id="sesiones" class="table table-striped table-bordered table-sm data-table align-middle w-100"><thead><tr><th class="select-filter no_wrap" data-filtro=\'{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}\'>Creación</th><th>Duración (hh:mm:ss)</th><th class="select-filter no_wrap">IP</th></tr></thead><tbody></tbody></table>');
		tablasD('sesiones',{'user':iden, 'iden': login},'sesiones', true);
		$('#sesiones').on('draw.dt', function (settings) {
			var api = new $.fn.dataTable.Api( '#sesiones' );
			var respuesta = api.ajax.json();						
			var datosp = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'`ses`.`tiempo`', 'tabla':'`ses_admin` AS `ses`', 'user': login};
			$.ajax({
				url: 'controllers/valores.php',
				data: datosp,
				type: 'post',
				dataType: 'json',
				error: function () {
					$('#mod-ses .total_ses').html('0');
				},
				success: function (response) {
					var tiempoT = parseInt(response.valor.replace(/\./g, ""));
					var duration = moment.duration(tiempoT, 'seconds');
					$('#mod-ses .total_ses').html(pad(Math.floor(duration.asHours()), 2)+':'+pad(duration.minutes(), 2)+':'+pad(duration.seconds(), 2));
				}
			});
		});
	});
	$('#mod-ses').modal('show');	
}

function Valrecdata(){	
	if($('#form-recdata').parsley().validate()){		
        loaderShow();
        var datos = $('#form-recdata').serializeArray();
        datos.push({ name: "accion", value: 3 });        
        $.ajax({
            url: dirCont,
            data: $.param(datos),
            method:  'POST',
            dataType: 'json',
            error: function () {
                grecaptcha.reset(widGo2);                
                $('#loader').one("hide", function() { 
                    setWid('form-recdata', true);
                    showError(error);
                });
                loaderHide();
            },
            success:  function (response) {					
                if(response.resp){                    
                    $('#mod-recdata').one('hidden.bs.modal', function () {
                        showGood(response.data.msg);
                    });
                    $('#loader').one("hide", function() { 
                        $('#mod-recdata').modal('hide');
                    });
                    loaderHide();                    						
                }else{
                    grecaptcha.reset(widGo2);                    
                    $('#loader').one("hide", function() { 
                        setWid('form-recdata', true);
                        showError(response.data.msg);
                    });
                    loaderHide();
                }					
            }
        });
	}
}

function inputvars_Base(ele, extras){
    var varCus = ele.data('funcionvar');
    var cont = ele.data('imgsho');
    var fdestino = ele.data('imgcont');
    var indom = ele.parents('.file_up').first().find('input[type="file"]');
    var forigen = indom.attr('name');
    var inputvars = (extras)?$.extend(true, {}, window[varCus], extras):window[varCus];    
    
    var inputvars_especial = {
        maxFileSize: 4000,
        maxFileCount: 1,
        elErrorContainer: '#'+ele.parents('form').first().attr('id')+' .error_upload_file',
        initialPreviewShowDelete: false,        
        showCancel: false,
        showUpload: true,
        showRemove: false,
        showBrowse: false,
        uploadExtraData: function(){
            return {
                'carpeta': fdestino,
                'user': login,
                'origen': forigen
            }
        }
    };      
    indom.fileinput('destroy').fileinput($.extend(true, {}, inputDefaults, inputvars_especial, inputvars))
    .on('filecleared', function(event) {        
        ele.val('');
    })
	.on('fileremoved', function(event, id, index) {
		if(indom.prop('multiple') === true){
			console.log(id);
			console.log(index);
			console.log(event);
		}        
    })
    .on('filepreajax', function(event, previewId, index) {
        loaderShow();
    })
    .on('fileloaded', function(event, file, previewId, index, reader) {       
        ele.val('');
    })
    .on('fileuploaderror', function(event, data, msg) {
        if(data.formdata === null){
            showError(msg);
        }else{
            loaderHide();
            showError('Error en la subida de archivos, inténtalo de nuevo.');						
        }
        $('#error_mod').one('shown.bs.modal', function () {
            indom.fileinput('clear');
        });					
    })
    .on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        ele.parent('.file_up').first().find('.file-input.theme-fas').addClass('file-input-ajax-new');
		if(indom.prop('multiple') === true){
			if(ele.val() != ''){
				var arreglo = ele.val().split(',');
				if(jQuery.inArray(response.extras, arreglo) < 0){
					arreglo.push(response.extras);
				}
				ele.val(arreglo.join());
			}else{
				ele.val(response.extras);
			}			
		}else{
			ele.val(response.extras);
		}        
        loaderHide();					
    });
    ele.parent('.file_up').first().find('.file-input.theme-fas').addClass('file-input-ajax-new');
}

function inputMultiple(ele, extras){
    var opciones = {
        filterTextClear: '<small class="text-muted"><span class="text-responsive">Todos</span></small>',
        filterPlaceHolder: 'Filtro',
        moveAllLabel: 'Seleccionar todos',
        removeAllLabel: 'deseleccionar todos',
        infoText: '<small class="text-muted"><span class="text-responsive">Mostrando ({0})</span></small>',
        infoTextFiltered: '<small class="text-muted"><span class="text-responsive">Filtrado {0} de {1}</span></small>',
        infoTextEmpty: '<small class="text-muted"><span class="text-responsive">Vacio</span></small>'
    };
    ele.bootstrapDualListbox($.extend({},opciones,extras));
}

function SetCodep(elem, filtro){    
    var destino = $(elem.data('destino'));
    var objeto = elem.data('relacion');
    var datos =  $.extend({}, objeto, {'valor': elem.val(), 'user': login});
	var selected = (elem.data('valori'))?elem.data('valori'):((datos.hasOwnProperty('val'))?'"'+datos.val+'"':"");
    if(filtro){
		$.extend(datos, {'filtro': filtro});
	}
    if(elem.val() == ''){
		if(datos.hasOwnProperty('val')){
			destino.html('<option value="'+datos.val+'">Seleccionar</option>');
			destino.val('"'+datos.val+'"').trigger('change');
		}else{
			destino.html('<option value="">Seleccionar</option>');
			destino.val('').trigger('change');
		}
    }else{
        $.ajax({
			url: 'controllers/codep.php',
			data: datos,
			method:  'POST',
			dataType: 'json',
			error: function () {
				if(datos.hasOwnProperty('val')){
					destino.html('<option value="'+datos.val+'">Seleccionar</option>');
					destino.val('"'+datos.val+'"').trigger('change');
				}else{
					destino.html('<option value="">Seleccionar</option>');
					destino.val('').trigger('change');
				}
			},
			success:  function (response) {
				destino.html(response.datos);
				if(selected != "" || (datos.hasOwnProperty('val') && selected != '"'+datos.val+'"')){
					destino.val(selected).trigger('change');
				}else{
					if(datos.hasOwnProperty('val')){						
						destino.val('"'+datos.val+'"').trigger('change');
					}else{
						destino.val('').trigger('change');
					}					
				}
			}
		});
    }
}

function openData(elem, iden, nohide, nomodal){
	$('#form-'+elem+' input:not(.noclear)').val('');
	$('#form-'+elem+' textarea:not(.noclear)').val('');
	$('#form-'+elem+' textarea:not(.noclear)').html('');
	if($('#form-'+elem+' textarea.summer:not(.noclear)').length > 0){
		$('#mod-'+elem).one('shown.bs.modal', function() {
			$('#form-'+elem+' textarea.summer:not(.noclear)').each(function( index ) {			
				$(this).summernote({
					placeholder: $(this).attr('placeholder'),
					dialogsInBody: true,
					tabsize: 2,
					height: 200,
					toolbar: [
						['style', ['style', 'bold', 'italic', 'underline']],												
						['para', ['ul', 'ol', 'paragraph']],
						['table', ['table']],

						['view', ['codeview']]
					],
					lang: 'es-ES',
					codemirror: {
						theme: 'monokai'
					}
				});
				$(this).summernote('code', '');
			});			
			$('#mod-'+elem).one('hidden.bs.modal', function() {
				$('#form-'+elem+' textarea.summer:not(.noclear)').each(function( index ) {			
					$(this).summernote('destroy');
				});
			});
		});
	}	
	$('#form-'+elem+' input:checkbox:not(.noclear)').prop('checked', false);
	$('#form-'+elem+' select:not(.noclear)').val('');
	if($('#form-'+elem+' select:not(.noclear)').hasClass('icono-selector')){
		icono_select.refreshPicker();
		$('.icons-selector .selector-button').addClass('stretched-link');
	}
	$('#form-'+elem+' select:not(.noclear)[multiple]').bootstrapDualListbox('refresh');
    if($('#form-'+elem+' select:not(.noclear)[multiple]').length > 0){
        $('#form-'+elem+' .bootstrap-duallistbox-container select').addClass('form-select');
    }      
	$('#form-'+elem+' select.set_codep:not(.noclear)').data('valori', '').trigger('change');
	if($('#form-'+elem+' .file-input.theme-fas').length > 0){
		$('#form-'+elem+' .file-input.theme-fas input[type="file"]').each(function() {
			$(this).fileinput('clear');
		});		
        $('#form-'+elem+' input[data-funcion]').each(function() {
            var este = this;
            if($(este).data('funcion') == 'inputvars_Base'){
                window[$(este).data('funcion')].apply(null,[                    
                    $(este)
                ]);
            }else{                
                window[$(este).data('funcion')].apply(null); 
            }            
        });        
	}	
	if($('#form-'+elem+' input.date:not(.noclear)').length > 0){
        $('.flatpickr-calendar input').addClass('form-control');
        $('.flatpickr-calendar select').addClass('form-select d-inline-block');
		$('#form-'+elem+' input.date:not(.noclear)').each(function( index ) {
            var este = this;
            try{
                window[$(este).data('vardate')].clear();                
            } catch (err){}
            $(este).removeClass('invisible');
		});        
	}
	if($('#form-'+elem+' .input-group.colorPick').length > 0){
		$('#form-'+elem+' .input-group.colorPick input').val('');
		$('#form-'+elem+' .input-group.colorPick input').each(function( index ) {
			$(this).spectrum('set');		  
		});       						
    }	
	
	$('#form-'+elem).parsley().reset();
	if(iden){		
		loaderShow();
		$.ajax({
			url: dirCont,
			data: {
				id: iden,
				db: $('#form-'+elem+' .db').val(),                
				accion: 6,
                'idveruser': login
			},
			type: 'post',
			dataType: 'json',
			error: function () {				
				$('#loader').one("hide", function() { 
					showError(error);
				});
				loaderHide();
			},
			success: function (response) {
				if (response.resp) {					
					$.each( response.data, function( key, value ) {
						if($('#form-'+elem+' .'+key).prop('multiple')){
							if(value != ''){
								$('#form-'+elem+' .'+key+' option').prop("selected", false);
								$.each(value.split(","), function(i,e){
									$('#form-'+elem+' .'+key+' option[value="'+e+'"]').prop("selected", true);								
								});
							}
							$('#form-'+elem+' .'+key).bootstrapDualListbox('refresh');
						}else{
							$('#form-'+elem+' .'+key).val(value);
							if($('#form-'+elem+' .'+key).hasClass('summer')){
								$('#mod-'+elem).one('shown.bs.modal', function() {
									$('#form-'+elem+' .'+key).summernote('code', value);
								});
							}
                                                       
							if($('#form-'+elem+' .set_codep.'+key).length > 0){
								$('#form-'+elem+' .set_codep.'+key).data('valori', response.data[$('#form-'+elem+' .set_codep.'+key).data('destino').split('.').pop()]);
							}
							$('#form-'+elem+' .set_codep.'+key).trigger('change');
							if($('#form-'+elem+' .'+key).data('imgcont')){
								if(value != ''){
                                    if($('#form-'+elem+' .'+key).data('funcion')){
                                        if($('#form-'+elem+' .'+key).data('funcion') == 'inputvars_Base'){
                                            if($('#form-'+elem+' .'+key).data('prevurl')){
												if(value.indexOf(',') > 0){
													var arreglo = [];													
													$.each(value.split(','), function(ind, vale){
														if(vale.startsWith("http")){
															arreglo.push(vale);
														}else{
															arreglo.push($('#form-'+elem+' .'+key).data('prevurl')+'/'+vale);
														}														
													});
													window[$('#form-'+elem+' .'+key).data('funcion')].apply(null,[                                                
														$('#form-'+elem+' .'+key),                                                
														{                                                
															initialPreview: arreglo,
															initialPreviewAsData: true
														}
													]);
													
												}else{
													window[$('#form-'+elem+' .'+key).data('funcion')].apply(null,[                                                
														$('#form-'+elem+' .'+key),                                                
														{                                                
															initialPreview:(value.startsWith("http"))?[
																value
															]:[
																$('#form-'+elem+' .'+key).data('prevurl')+'/'+value
															],
															initialPreviewAsData: true
														}
													]);
												}                                                                                                
                                            }else{
                                                window[$('#form-'+elem+' .'+key).data('funcion')].apply(null,[                                                
                                                    $('#form-'+elem+' .'+key),                                                
                                                    {                                                
                                                        initialPreview:(value.startsWith("http"))?[
                                                            value
                                                        ]:[
                                                            $('#form-'+elem+' .'+key).data('imgcont')+'/'+value
                                                        ],
                                                        initialPreviewAsData: true
                                                    }
                                                ]);                                                
                                            }
                                        }else{
                                            window[$('#form-'+elem+' .'+key).data('funcion')].apply(null, [
                                                {                                                
                                                    initialPreview:(value.startsWith("http"))?[
                                                        value
                                                    ]:[
                                                        $('#form-'+elem+' .'+key).data('imgcont')+'/'+value
                                                    ],
                                                    initialPreviewAsData: true
                                                }
                                            ]); 
                                        }                                                                                                                      
                                    }
								}		
							}
							
							if($('#form-'+elem+' .input-group.colorPick .'+key).length > 0){
                                $('#form-'+elem+' .'+key).spectrum('set', value);															      						
							}
							
							if($('#form-'+elem+' .'+key+'.date').length > 0){
								window[$('#form-'+elem+' .'+key).data('vardate')].setDate(value);
							}
							if($('#form-'+elem+' .'+key).hasClass('icono-selector')){								
								icono_select.setIcon(value);
							}
						}												
					});
					if(!nomodal){
						if(!nohide){
							$('#loader').one("hide", function() { 
								$('#mod-'+elem).modal('show');
							});
							loaderHide();
						}else{
							$('#mod-'+elem).modal('show');
						}
					}else{
						if(!nohide){							
							loaderHide();
						}
					}										
				} else {					
					$('#loader').one("hide", function() { 
						showError(response.data.msg);
					});
					loaderHide();
				}
			}
		});
	}else{
		$('#form-'+elem+' .id').val(0);
		if(!nomodal){
			$('#mod-'+elem).modal('show');
		}		
	}
}

function consData(bd, iden, datos, callback){
    $.ajax({
        url: dirCont,
        data: {
            'id': iden,
            'db': bd,                
            'accion': 6,
            'idveruser': login
        },
        method:  'POST',
        dataType: 'json',
        error: function () {						
            callback(false);
        },
        success:  function (response) {
            if (response.resp) {
                var respuesta = {};
                $.each( datos, function( index, value ) {
                    respuesta[value] = response.data[value];								
                });
                callback(respuesta);
            }else{
                callback(false);
            }						
        }
    });
}

function Valform(formulario, accion, params, loaderhide, modalin, acciont){
	if($('#'+formulario).parsley().validate()){
		loaderShow();
		if($('#'+formulario+' .summer').length > 0){
			$('#'+formulario+' .summer').each(function( index ) {
				$(this).val('**esSummertexto**'+$(this).summernote('code').replace(/</g, '*{*').replace(/>/g, '*}*'));
			});
		}
        if($('#'+formulario+' .db').length > 0){
            $('#'+formulario).find(':input').prop('disabled', false);
            var datos = $('#'+formulario).serializeArray();	
            datos.push({ name: "accion", value: (acciont)?acciont:5 });
            datos.push({ name: "editor", value: login });
            datos.push({ name: "idveruser", value: login });        
            $.ajax({
                url: dirCont,
                data: $.param(datos),
                method:  'POST',
                dataType: 'json',
                error: function () {					
                    $('#loader').one("hide", function() { 
                        showError(error);
                    });
                    loaderHide();
                },
                success:  function (response) {					
                    if(response.resp){
                        if($('.modal.show').length > 0){
                            var modid = (modalin)?$('#'+modalin):$('.modal.show');
                            modid.one('hidden.bs.modal', function(){
                                if(loaderhide){
                                    if(accion){
                                        var para = [response.data.id, response.data.msg];
                                        var npara = $.merge(params, para);
                                        accion.apply(null, npara);
                                    }								
                                    loaderHide();
                                    $('#loader').one("hide", function() { 
                                        showGood(response.data.msg);
                                    });
                                }else{
                                    if(accion){
                                        var para = [response.data.id, response.data.msg];
                                        var npara = $.merge(params, para);
                                        accion.apply(null, npara);
                                    }                                
                                }
                            });
                            modid.modal('hide');
                        }else{
                            if(loaderhide){
                                if(accion){
                                    var para = [response.data.id, response.data.msg];
                                    var npara = $.merge(params, para);
                                    accion.apply(null, npara);
                                }							
                                $('#loader').one("hide", function() { 
                                    showGood(response.data.msg);
                                });
                                loaderHide();
                            }else{
                                if(accion){
                                    var para = [response.data.id, response.data.msg];
                                    var npara = $.merge(params, para);
                                    accion.apply(null, npara);
                                }							
                            }
                        }					
                    }else{					
                        $('#loader').one("hide", function() { 
                            showError(response.data.msg);
                        });
                        loaderHide();
                    }					
                }
            });            
        }else{
           if($('.modal.show').length > 0){
               var modid = (modalin)?$('#'+modalin):$('.modal.show');
               modid.one('hidden.bs.modal', function(){
                   if(loaderhide){
                       if(accion){
                           var para = [];
                           var npara = $.merge(params, para);
                           accion.apply(null, npara);
                       }
                       loaderHide(); 
                   }else{
                       if(accion){
                           var para = [];
                           var npara = $.merge(params, para);
                           accion.apply(null, npara);
                       }
                   }
               });
               modid.modal('hide');
            }else{
                if(loaderhide){
                    if(accion){
                        var para = [];
                        var npara = $.merge(params, para);
                        accion.apply(null, npara);
                    }
                    loaderHide();
                }else{
                    if(accion){
                        var para = [];
                        var npara = $.merge(params, para);
                        accion.apply(null, npara);
                    }							
                }
            } 
        }        
	}
}

function ValformMas(formulario, accion, params, loaderhide, limit){
	if($('#'+formulario).parsley().validate()){
		if($('#loader').is(':hidden')){
			loaderShow();
		}
		if(!limit){
			if($('.modal.show').length > 0){
				var modid = $('.modal.show');
				modid.modal('hide');			
			}			
			$('#procc_mod').one('shown.bs.modal', function(){
				onValformMas(formulario, accion, params, loaderhide);
			});
            showProcc('Procesando archivo');
		}else{
			$('#procc_mod .procc_msg').html('Registros procesados: <strong>'+limit+'</strong>');
			onValformMas(formulario, accion, params, loaderhide, limit);
		}
		
	}else{
		loaderHide();					
	}
}

function onValformMas(formulario, accion, params, loaderhide, limit){
	var datos = $('#'+formulario).serializeArray();	
    datos.push({ name: "accion", value: 8 });
    datos.push({ name: "editor", value: login });
    datos.push({ name: "limite", value: (limit)?limit:0 });
    datos.push({ name: "idveruser", value: login });    
    $.ajax({
        url: dirCont,
        data: $.param(datos),
        method:  'POST',
        dataType: 'json',
        error: function () {
            $('#procc_mod').modal('hide');                
            $('#loader').one("hide", function() { 
                showError(error);
            });
            loaderHide();
        },
        success:  function (response) {					
            if(response.resp){
                if(response.data.fin){
                    $('#procc_mod').one('hidden.bs.modal', function(){
                        if(loaderhide){
                            accion(params.join(','));                            
                            $('#loader').one("hide", function() { 
                                showGood(response.data.msg);
                            });
                            loaderHide();
                        }else{
                            accion(params.join(','));
                            showGood(response.data.msg);
                        }
                    });
                    $('#procc_mod').modal('hide');
                }else{						
                    ValformMas(formulario, accion, params, loaderhide, response.data.limite);
                }					
            }else{
                $('#procc_mod').one('hidden.bs.modal', function(){                    
                    $('#loader').one("hide", function() { 
                        showError(response.data.msg);
                    });
                    loaderHide();
                });
                $('#procc_mod').modal('hide');
            }					
        }
    });
}

function delData(iden, db, field, accion, params){
    $('#mod-eliminar .eliminar').val(0);
    $('#mod-eliminar').one('hidden.bs.modal', function(){
        if($('#mod-eliminar .eliminar').val() == 1){
            delDatas(iden, db, field, accion, params);
        }
    });
    $('#mod-eliminar').modal('show');
}

function delDatas(iden, db, field, accion, params){
	loaderShow();
	$.ajax({
		url: dirCont,
		data: {'accion': 7, 'id': iden, 'db': db, 'field': field, 'idveruser': login},
		method:  'POST',
		dataType: 'json',
		error: function () {
            $('#loader').one("hide", function() { 
                showError(error);
            });
            loaderHide();
		},
		success:  function (response) {					
			if(response.resp){
                window[accion].apply(null, params);
				$('#loader').one("hide", function() { 
					showGood(response.data.msg);
				});
                loaderHide();
			}else{				
				$('#loader').one("hide", function() { 
					showError(response.data.msg);
				});
                loaderHide();
			}					
		}
	});
}

function closeActModal(accion, params, ismodal){
	if($('.modal.show').length > 0){
		var modid = $('.modal.show');		
		modid.one('hidden.bs.modal', function(){
			if(ismodal){
				$('#'+accion).modal('show');
			}else{
				accion(params.join(','));				
			}
		});
        modid.modal('hide');
	}else{
		if(ismodal){
			$('#'+accion).modal('show');
		}else{
			accion(params.join(','));				
		}
	}
}

function ShowPendientes(){
	$('.alertas-cont .alertas-no').remove();
	$('.alertas-total').html(0);
	var datos = {		
		'id': login,
		'rol': rol		
	};
    if(login){
        $.ajax({
            url: 'controllers/alertas.php',
            data: datos,
            method:  'POST',
            dataType: 'json',
            error: function () {
                $('.alertas-cont').append('<p class="my-0 pb-2 text-center alertas-no"><small>No hay alertas.</small></p>');
                $('.alertas-total').html(0);
				setTimeout(ShowPendientes, 30000);
            },
            success:  function (response) {
                if(response.data != ''){
                    $('.alertas-cont').append(response.data);
                    var cuenta = $('.alertas-cont .alertas-no').length;
                    $('.alertas-total').html(cuenta);
					setTimeout(ShowPendientes, 30000);
                }else{
                    $('.alertas-cont').append('<p class="my-0 pb-2 text-center alertas-no"><small>No hay alertas.</small></p>');
                    $('.alertas-total').html(0);
					setTimeout(ShowPendientes, 30000);
                }						
            }
        });
    }	
}

function openVid(video){
    if(video.indexOf('.mp4') >= 0){
        $('#mod-video .video_p').html('<video class="embed-responsive-item" preload="metadata" autoplay controls><source src="'+video+'#t=0.5" type="video/mp4" /></video>');
    }else{
       $('#mod-video .video_p').html('<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'+video+'?autoplay=1&origin='+window.location.origin+'&rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'); 
    }    
    $('#mod-video').one('hidden.bs.modal', function () {
        $('#mod-video .video_p').html('');
    });
    $('#mod-video').modal('show');
}

function setWid(form, vali){
    $('#'+form+' .g-recaptcha-response').prop('required', true);
    $('#'+form+' .g-recaptcha-response').attr('data-parsley-errors-container', '#'+form+' .recerror');
    if(vali){
        $('#'+form+' .g-recaptcha-response').parsley().reset();
        $('#'+form+' .g-recaptcha-response').parsley().validate();
    }
}

function printDom(elem){
	if($('#graph_cont_'+elem).hasClass('show')){
		$('.graph[data-graph="'+elem+'"]').printThis({
			debug: false,					
			importCSS: true,
			importStyle: true,
			canvas: true,					
			printContainer: false
		});
	}else{
		$('#graph_cont_'+elem).collapse('show');
		$('#graph_cont_'+elem).one('shown.bs.collapse', function () {
			$('.graph[data-graph="'+elem+'"]').printThis({
				debug: false,					
				importCSS: true,
				importStyle: true,
				canvas: true,					
				printContainer: false
			});		 
		});
	}	
}

function drawGraficas(destino, tipo, fecha, extra){	
	//$('.graph[data-graph="'+destino+'"] .datos-contendor').html('');
	var datos = {
		'accion': destino,
        'fecha': fecha,
        'user': login,
		'rol': rol
	};    
	$.extend(datos, extra);
	$.ajax({
		url: 'controllers/graficas.php',
		data: datos,
		type: 'post',
		dataType: 'json',
		error: function () {
			//$('#loader').hide();
			//showError('Se presento un problema de conexión, intentalo de nuevo mas tarde.');
		},
		success: function (response) {
			if(tipo == 'doughnut'){
				var options = {
					aspectRatio: 1,
					title: {
						display: false
					},
					legend: {
						display: false						
					}
				};							
			}
            if(tipo == 'bar'){               
				var options = {
					aspectRatio: 2.4,
					title: {
						display: true
					},
					legend: {
						display: true
					},
					scales: {						
						xAxes: [{
							ticks: {
								beginAtZero: true
							},
							stacked: false
						}],
						yAxes: [{
							ticks: {
								beginAtZero: true
							},
							stacked: false
						}]
					}
				};				
			}
			if(tipo == 'horizontalBar'){
                tipo = 'bar';
				var options = {
					aspectRatio: 2.4,
                    indexAxis: 'y',
					title: {
						display: true
					},
					legend: {
						display: true
					},
					scales: {						
						xAxes: [{
							ticks: {
								beginAtZero: true
							},
							stacked: false
						}],
						yAxes: [{
							ticks: {
								beginAtZero: true
							},
							stacked: false
						}]
					}
				};				
			}
            if(tipo == 'radar'){
				var options = {
					aspectRatio: 1,					
                    elements:{
                        line: {
                            tension: 0,
                            borderWidth: 3
                        }                        
                    },
                    tooltips: {
                        callbacks: {
                            title: function(tooltipItem, data){                                
                                return data.labels[tooltipItem[0].index];
                            }                            
                        }                        
                    }
				};
                
			}
            
            if(window['graph_'+destino]){
                window['graph_'+destino].type = tipo;
                window['graph_'+destino].data = response.data;
                window['graph_'+destino].option = options;
                window['graph_'+destino].update();
            }else{
                var ctx = document.getElementById('graph_'+destino);
                window['graph_'+destino] = new Chart(ctx, {
                    'type': tipo,
                    'data': response.data,
                    'options': options
                });
                
            }			
			
		}
	});
}

function showClock(){
    if($('.cont_clock').hasClass('clock_show')){
        $('.cont_clock').removeClass('clock_show');
    }else{
        $('.cont_clock').addClass('clock_show');
    }
}

function displayTime() {
    var time = moment().format('hh:mm A');
    $('.clock').html(time);
	var hora_act = parseInt(moment().format('k'));	
	if (hora_act > 5 && hora_act < 19) {
		//$('.tiempo_img').attr('src', 'images/day.svg');
		$('.noche').addClass('invisible');
		$('.dia').removeClass('invisible');		
	} else {
		//$('.tiempo_img').attr('src', 'images/night.svg');
		$('.dia').addClass('invisible');	
		$('.noche').removeClass('invisible');
	}
    var hora_circ = parseInt(moment().format('h')) / 12;    
    var min_circ = parseInt(moment().format('m')) / 60;
    $('#time_min').circleProgress({
        value: min_circ,
        animationStartValue: curmin
    });
    $('#time_hr').circleProgress({
        value: hora_circ,
        animationStartValue: curhora
    });
    curhora = hora_circ;
    curmin = min_circ;
    setTimeout(displayTime, 30000);
}

function displayWeater(){
	getLocation(function(llamado){
		if(llamado){
			var req = $.getJSON('https://api.openweathermap.org/data/2.5/weather?lat='+geoLocation.lat+'&lon='+geoLocation.lng+'&APPID=e6a98f281710f4a182c748b1d9b4e4a2');
			req.then(function(resp) {				
				var prefix = 'wi wi-';
				var code = resp.weather[0].id;
				var icon = weatherIcons[code].icon;
				var hora_act = parseInt(moment().format('k'));
				if (hora_act > 5 && hora_act < 19) {
					icon = 'day-' + icon;		
				} else {
					icon = 'night-' + icon;
				}
				icon = prefix + icon;
				$('.tiempo_img').html('<i class="'+icon+' wi-fw"></i>');
				setTimeout(displayWeater, 3600000);
			});
		}
	});
}

function getLocation(callback) {
  if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(
		  function(geoposition){
			  geoLocation = {
				  lat: geoposition.coords.latitude,
				  lng: geoposition.coords.longitude
			  };
			  pos = geoLocation;
			  callback(true);
		  },
		  function(error){
			  geoLocation = {
				  lat: '6.2369837',
				  lng: '-75.5868331'
			  };
			  pos = geoLocation;
			  callback(true);
		  }, { enableHighAccuracy: true });
  }else{
	  geoLocation = {
		  lat: '6.2369837',
		  lng: '-75.5868331'
	  };
	  pos = geoLocation;
	  callback(true);
  }
}

function showProcc(msg) {
	$('#procc_mod .procc_msg').html(msg);
	$('#procc_mod').modal('show');
}

function showError(msg) {
	$('#error_mod .error_msg').html(msg);
	$('#error_mod').modal('show');
}

function showOffError(msg) {
	$('#off-error .error_msg').html(msg);
	$('#off-error').offcanvas('show');
}

function showGood(msg) {
	$('#good_mod .good_msg').html(msg);
	$('#good_mod').modal('show');
}

function LocalSt(iden, record) {
	if (typeof (Storage) !== "undefined") {
		localStorage.setItem("npa_iden", iden);
		localStorage.setItem("npa_reco", true);
	}
}

function LocalStrem() {
	if (typeof (Storage) !== "undefined") {
		localStorage.removeItem("npa_iden");
		localStorage.removeItem("npa_reco");
	}
}

function isTouchDevice(){
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

function reLoadTable(tabla, reset){
	$('#'+tabla).DataTable().ajax.reload(null, reset);	
	var columns = $('#'+tabla).dataTable().api().columns('.select-filter');	
	columns.every(function () {
        var column = this;		
		if($(column.header()).data('filtro') && $(column.header()).data('filtro') != ''){			
			var filtro_obj = $(column.header()).data('filtro');
            $.extend(filtro_obj, {'user': login});
			if(filtro_obj.opt == 'join' || filtro_obj.opt == 'basic'){
				var valoract = $(column.header()).find('select').val();
				var titulo = $(column.header()).find('select option:first').text();
				$(column.header()).find('select').html('<option value="">' + titulo + '</option>');
				$.ajax({
                    url: 'controllers/filter_db.php',
                    data: filtro_obj,
                    type: 'post',
                    dataType: 'json',
                    error: function () {
                        console.log('error de filtro'+filtro_obj.fl);
                    },
                    success: function (response) {									
                        $.each(response.data, function( index, value ) {							
                            $(column.header()).find('select').append('<option value="' + value[0] + '">' + value[1] + '</option>');									  
                        });
						$(column.header()).find('select').val(valoract);
                    }
                });
            }
		}        
    });
}

function newexportaction(e, dt, button, config) {
   var self = this;
   var oldStart = dt.settings()[0]._iDisplayStart;
   dt.one('preXhr', function (e, s, data) {
       // Just this once, load all data from the server...
       data.start = 0;
       data.length = 2147483647;
       dt.one('preDraw', function (e, settings) {
           // Call the original action function
           if (button[0].className.indexOf('buttons-copy') >= 0) {
               $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
           } else if (button[0].className.indexOf('buttons-excel') >= 0) {
               $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                   $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                   $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
           } else if (button[0].className.indexOf('buttons-csv') >= 0) {
               $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                   $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                   $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
           } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
               $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                   $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                   $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
           } else if (button[0].className.indexOf('buttons-print') >= 0) {
               $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
           }
           dt.one('preXhr', function (e, s, data) {
               // DataTables thinks the first item displayed is index 0, but we're not drawing that.
               // Set the property to what it was before exporting.
               settings._iDisplayStart = oldStart;
               data.start = oldStart;
           });
           // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
           setTimeout(dt.ajax.reload, 0);
           // Prevent rendering of the full data to the DOM
           return false;
       });
   });
   // Requery the server with the new one-time export settings
   dt.ajax.reload();
}

var dataTableFecha = [];
function tablasD(tabla, datos, urld, busqueda) {
	var ahora = moment().format('YYYY-MM-DD');
	var opc_tabla = {
		responsive: true,
		processing: true,
		serverSide: true,
		ajax: {
			url: 'controllers/' + urld + '.php',
			type: 'POST',
			data: datos,
			dataType : 'json'
		},
		deferRender: true,
		columnDefs: [{
			responsivePriority: 1,
			targets: 0
		}, {
			responsivePriority: 2,
			targets: 'no_clear'
		}, {
			responsivePriority: 3,
			orderable: false,
			searchable: false,
			targets: 'no_print',
			className: "text-end text-nowrap no-print"
		}, {
			responsivePriority: 4,
			targets: 'no_clear2'
		}, {
			responsivePriority: 10001,
			targets: 'second_prior'
		}, {
			targets: 'number_col',			
			className: 'numbers_col text-nowrap'
		}, {
			targets: 'no_wrap',
			className: 'text-nowrap'
		}, {
			targets: 'no_show',
			visible: false,
			searchable: false
		}, {
			targets: 'no_print2',
			visible: true,
			orderable: false,
			searchable: false,
			className: 'no-print'
		}, {
			targets: 'hdvis',
			visible: false,
		}, {
			targets: 'hideall',
			visible: false,
			orderable: false,
			searchable: true
		}, {
			targets: 'hideall2',
			visible: false,
			orderable: false,
			searchable: true,
			className: 'no-print'
		}, {
			targets: 'no_order',			
			orderable: false,
			searchable: true
		}, {
			targets: 'no_order_full',			
			orderable: false,
			searchable: false
		}, {
			targets: 'number',			
			render: $.fn.dataTable.render.number( '.', ',', 0 )
		}, {
			targets: 'money',			
			render: $.fn.dataTable.render.number( '.', ',', 0, '$' )
		}, {
			targets: 'number_fr',			
			render: $.fn.dataTable.render.number( '.', ',', 2 )
		}, {
			targets: 'money_fr',			
			render: $.fn.dataTable.render.number( '.', ',', 2, '$' )
		}],
		lengthMenu: [
			[10, 25, 50, 100, 500, 1000, -1],
			[10, 25, 50, 100, 500, 1000, "todos"]
		],
		pageLength: 50,
		language: {
			"sProcessing": '<div class="w-100 text-center my-2 text-muted"><div class="fa-3x text-center"><i class="fas fa-circle-notch fa-spin"></i></div><small class="fw-bold">procesando</small></div>',
			"sLengthMenu": "<div class='input-group input-group-sm'><span class='input-group-text'><i class='far fa-list-alt'></i></span>_MENU_</div>",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningún dato disponible en esta tabla",
			"sInfo": "<small class='text-muted text-end text-md-start'><span class='text-responsive'>Mostrando del _START_ al _END_ de un total de _TOTAL_ registros</span></small>",
			"sInfoEmpty": "<small class='text-muted text-end text-md-start'><span class='text-responsive'>Mostrando del 0 al 0 de un total de 0 registros</span></small>",
			"sInfoFiltered": "<small class='text-muted text-end text-md-start'><span class='text-responsive'>(filtrado de un total de _MAX_ registros)</span></small>",
			"sInfoPostFix": "",
			"sSearch": "<div class='input-group input-group-sm'><span class='input-group-text'><i class='fas fa-search'></i></span>_INPUT_</div>",
			"searchPlaceholder": "Buscar",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": '<div class="w-100 text-center my-2 text-muted"><div class="fa-3x text-center"><i class="fas fa-circle-notch fa-spin"></i></div><small class="font-weight-bold">cargando</small></div>',
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "<i class='fas fa-chevron-circle-right'></i>",
				"sPrevious": "<i class='fas fa-chevron-circle-left'></i>"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		dom: "<'row justify-content-between align-items-center w-100 mx-0'<'col-12 px-0'<'d-flex flex-wrap flex-sm-nowrap justify-content-between align-items-center'<'flex-fill pe-1 pe-sm-0 order-1 order-sm-0 text-start data_le'l><'flex-fill text-center px-0 px-sm-1 order-0 order-sm-1 pb-2 pb-sm-0 butonera'B><'flex-fill ps-1 ps-sm-0 order-2 text-end data_fi'f>>>>" + "<'row justify-content-between align-items-start w-100 mx-0'<'col-12 px-0'tr>>" + "<'row justify-content-between align-items-start w-100 mx-0'<'col-12 col-md-5 text-start px-0 pe-md-2 order-1 order-md-0 pt-1 pt-md-0'i><'col-12 col-md-7 px-0 ps-md-2 text-right order-0 order-md-1'p>>",
		buttons: {
            dom: {
               container: {
                   className: 'btn-group btn-group-sm'
               }
            },
            buttons: [{
                extend: 'colvis',                
                className: 'text-white btn-sm',
                text: '<i class="fas icon-hdr_strong fa-fw"></i>',
                columns : ':not(.hideall,.hideall2)',                
                columnText: function ( dt, idx, title ) {
                    return '<small>'+title+'</small>';
                },
                prefixButtons: [{
                    text: '<small class="dropdown-header py-0">ver columnas</small>',
                    className: 'disabled fw-bold'
                }]
            }, {
                extend: 'excel',
                className: 'text-white flex-fill',
                text: '<i class="fas fa-file-excel fa-fw"></i>',
                extension: '.xlsx',
                title: ahora + '-' + tabla + '-' + desc,
                exportOptions: {
                    columns: ':visible :not(.no-print,.hdvis)',
                    orthogonal: 'export',
                    format: {
                        header: function (data, column, row) {
                            if(data.indexOf("flatpickr-calendar") >= 0){
                                var posi = data.indexOf('<span style="display:none">') + 27;
                                var posi2 = data.indexOf('span>') - 2;
                                var textos = data.substring(posi, posi2);
                                return textos.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                            }else{
                                if(data.indexOf("<select") >= 0){
                                    var posi = data.indexOf('value="">') + 9;
                                    var posi2 = data.indexOf('option>') - 2;
                                    var textos = data.substring(posi, posi2);							
                                    return textos.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                                }else{
                                    return data.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                                }
                            }						
                        }
                    }
                }
            }, {
                extend: 'print',
                className: 'text-white flex-fill',
                text: '<i class="fas fa-print fa-fw" style="padding: 2px 0px"></i>',
                title: ahora + '-' + tabla + '-' + desc,
                exportOptions: {
                    columns: ':visible :not(.no-print,.hdvis)',
                    format: {
                        header: function (data, column, row) {
                            if(data.indexOf("flatpickr-calendar") >= 0){
                                var posi = data.indexOf('<span style="display:none">') + 27;
                                var posi2 = data.indexOf('span>') - 2;
                                var textos = data.substring(posi, posi2);
                                return textos.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                            }else{
                                if(data.indexOf("<select") >= 0){
                                    var posi = data.indexOf('value="">') + 9;
                                    var posi2 = data.indexOf('option>') - 2;
                                    var textos = data.substring(posi, posi2);							
                                    return textos.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                                }else{
                                    return data.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                                }
                            }						
                        },					
                        body: function (data, column, row) {						
                            return (Number.isInteger(data))?data:data.replace(/<\/?[^>]*(hidden-print).*>/g, "").replace(/<ul[^>]*>/gi, "{ul}").replace(/<li>/gi, "{li}").replace(/<\/li>/gi, "{\/li}").replace(/<\/ul>/gi, "{\/ul}").replace(/<br>/gi, "{br}").replace(/<\/?[^>]+(>|$)/g, "").replace(/{br}/gi, "<br>").replace(/{ul}/gi, "<ul class=\"my-0 pl-3\">").replace(/{li}/gi, "<li>").replace(/{\/li}/gi, "<\/li>").replace(/{\/ul}/gi, "<\/ul>");

                            //return (Number.isInteger(data))?data:data.replace(/<\/?[^>]*(hidden-print).*>/g, "").replace(/<li>([^<\\/li>]*)<\/li>/gi, "$1\,<br>").replace("<br>", "{br}").replace(/<\/?[^>]+(>|$)/g, "").replace("{br}", "<br>");
                        }
                    }
                }
            },{
                extend: 'excel',
                className: 'text-white flex-fill',
                text: '<i class="fas fa-cloud-download-alt fa-fw" style="padding: 2px 0px"></i>',
                extension: '.xlsx',
                title: ahora + '-' + tabla + '-' + desc,
                exportOptions : {
                    columns: ':visible :not(.no-print,.hdvis)',
                    orthogonal: 'export',
                    format: {
                        header: function (data, column, row) {
                            if(data.indexOf("flatpickr-calendar") >= 0){
                                var posi = data.indexOf('<span style="display:none">') + 27;
                                var posi2 = data.indexOf('span>') - 2;
                                var textos = data.substring(posi, posi2);
                                return textos.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                            }else{
                                if(data.indexOf("<select") >= 0){
                                    var posi = data.indexOf('value="">') + 9;
                                    var posi2 = data.indexOf('option>') - 2;
                                    var textos = data.substring(posi, posi2);							
                                    return textos.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                                }else{
                                    return data.replace(/<br\s*\/?>/gi, "\r\n").replace(/<is\s*\/?>/gi, ",").replace(/<\/?[^>]+(>|$)/g, "");
                                }
                            }						
                        }
                    }
                },
                action: newexportaction
            }] 
        },
		initComplete: function (settings, json) {
			if (busqueda) {				
				//console.log(this.api().columns());
				var columns = this.api().columns('.select-filter');
				//columns[0].shift();
				//console.log(this.api().columns());
				var cuenta = dataTableFecha.length;
				columns.every(function () {
					var column = this;					
					//console.log(this);
					if($(column.header()).data('filtro') && $(column.header()).data('filtro') != ''){
						var filtro_obj = $(column.header()).data('filtro');
                        $.extend(filtro_obj, {'user': login});
						if(filtro_obj.opt == 'date'){							
							var select = $('<span style="display:none">' + $(this.header()).text() + '</span><div class="form-group"><div class="input-group input-group-sm tabla_date_filter_'+cuenta+'"><input type="text" class="form-control" placeholder="' + $(this.header()).text() + '" onClick="stopPropagation(event)" data-input><button class="btn btn-danger text-white" type="button" onClick="stopPropagation(event)" data-clear><i class="fas fa-backspace fa-fw"></i></button></div></div>')
							.appendTo($(column.header()).empty());
							dataTableFecha[cuenta] = flatpickr($("#"+tabla+" .tabla_date_filter_"+cuenta),{
								"locale": "es",
								"mode": "range",
								"enableTime": false,
								"enableTime": false,								
								"dateFormat": "Y-m-d",
								"static": true,
								"wrap": true,
								"onChange": function(selectedDates, dateStr, instance) {
									var val = false;
									if(selectedDates.length > 1){
										val = 'Date:'+dateStr;                                        
									}
                                    column.search(val ? val : '', true, false).draw();
								}
							});
							if($(column.header()).hasClass('hdvis_af')){
								$(column.header()).removeClass('hdvis_af');
								$(column.header()).addClass('hdvis');
								column.visible( ! column.visible() );
							}
							cuenta++;
						}else{
							var select = $('<div class="input-group input-group-sm" style="width:100%"><select class="max-width-100 form-control form-select" style="width:100%" onClick="stopPropagation(event)"><option value="">' + $(this.header()).text() + '</option></select></div>')
							.appendTo($(column.header()).empty());
							select.find('select').on('change', function () {
								var val = $.fn.dataTable.util.escapeRegex(
									$(this).val()
								);

								column
									.search(val ? val : '', true, false)
									.draw();
							});
						}
					}else{
						var select = $('<div class="input-group input-group-sm" style="width:100%"><select class="max-width-100 form-control form-select" style="width:100%" onClick="stopPropagation(event)"><option value="">' + $(this.header()).text() + '</option></select></div>')
						.appendTo($(column.header()).empty());
						select.find('select').on('change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
							column
								.search(val ? val : '', true, false)
								.draw();
						});
					}
					
					if($(column.header()).data('filtro') && $(column.header()).data('filtro') != ''){
						if(filtro_obj.opt == 'join' || filtro_obj.opt == 'basic'){
							$.ajax({
								url: 'controllers/filter_db.php',
								data: filtro_obj,
								type: 'post',
								dataType: 'json',
								error: function () {
									console.log('error de filtro'+filtro_obj.fl);
								},
								success: function (response) {									
									$.each(response.data, function( index, value ) {
										select.find('select').append('<option value="' + value[0] + '">' + value[1] + '</option>');									  
									});
								}
							});
						}else{
							if(filtro_obj.opt != 'date'){
								$.each(filtro_obj.opt, function( index, value ) {
									if(value[0] == '*'){
										select.find('select').append('<option value="NoEmp:' + value[0] + '">' + value[1] + '</option>');
									}else{
										select.find('select').append('<option value="' + value[0] + '">' + value[1] + '</option>');
									}																		  
								});
							}							
						}
					}else{
						column.data().unique().sort().each(function (d, j) {						
							if(~d.indexOf("data-parametro=")){
								var datoparam = d.split('data-parametro="');
								var parametro = datoparam[1].substring(0, datoparam[1].indexOf('"'));
								select.find('select').append('<option value="' + parametro + '">' + String(d.replace( /<.*?>/g, '' )) + '</option>');
							}else{
								select.find('select').append('<option value="' + d.replace( /<.*?>/g, '' ) + '">' + d + '</option>');
							}						
						});
					}
				});
			}			
		},
		drawCallback: function (settings) {	
            $('.dataTables_paginate .pagination').addClass('pagination-sm');
			$( window ).trigger( 'resize' );
		},
		createdRow: function( row, data, dataIndex ) {			
			if($('#' + tabla).data('orden') !== ''){
				var info = this.api().page.info();
				var numero = (info.page * info.length) + (dataIndex + 1);
				$('td', row).eq($('#' + tabla).data('orden')).html(numero);
			}						
		  }
	};
	var table = $('#' + tabla).DataTable(opc_tabla);    
	table.buttons().containers().find('.btn-group').addClass('flex-fill');    
	table.on( 'page.dt', function () {
		table.one( 'draw', function () {            
			$( window ).trigger( 'resize' );
		});
	} );
	table.on('responsive-display', function (e, datatable, row, showHide, update) {		
	});
	table.on('column-visibility.dt', function (e, settings, column, state) {
		var clase = settings.aoHeader[0][column].cell.className;
		if (state) {
			settings.aoHeader[0][column].cell.className = clase.replace('hdvis', '');
		} else {
			settings.aoHeader[0][column].cell.className = clase + '';
		}
	});
}

function respClass() {
	$('#m_cont').css('padding-top', $('#barra_nav').outerHeight()+'px');
	$('#left_bar').css('padding-top', $('#barra_nav').outerHeight()+'px');
    $('#left_bar .second_menu').css('padding-left', $('#left_bar .main_menu').outerWidth()+'px');
    if($('#left_bar').is(":visible")){
       $('#main_c > div').css('padding-left', ($('#left_bar').outerWidth() + 25)+'px'); 
    }else{
       $('#main_c > div').css('padding-left', '0px'); 
    }
	if($('#paralal').length > 0){
		$('#paralal').css('padding-top', $('#barra_nav').outerHeight()+'px');
	}
    if($('.flatpickr-calendar').length > 0){
        $('.flatpickr-calendar input').addClass('form-control');
        $('.flatpickr-calendar select').addClass('form-select d-inline-block');        
    }
     
}

function stopPropagation(evt) {
    if (evt.stopPropagation !== undefined) {
        evt.stopPropagation();
 
    } else {
        evt.cancelBubble = true;
    }
}

function remove_accents(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents =    "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');

    return strAccentsOut.replace(/[^a-z0-9\s]/gi, '').replace(/[-\s]/g, '_').toLowerCase();
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}