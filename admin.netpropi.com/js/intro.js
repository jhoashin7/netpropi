function loadIntro(intro){
	if(intro == 1){
		$('#paralal').html('<div class="hojas position-relative w-100 h-100"><div class="w-100 h-100 position-absolute" style="top: 0px; left: 0px"><canvas id="c"></canvas></div><div id="bubbles" class="w-100 h-100 position-absolute" style="top: 0px; left: 0px"></div></div>');
		var hojas = randNum(2, 5);
		var reverse;			
		var flip;
		var widC;
		var topC;
		var leftC;
		for (var i = 0; i < hojas; i++) {			
			reverse = (randNum(1, 2) == 1)?'':'reverse';			
			flip = (randNum(1, 2) == 1)?'':'flip';
			widC = randNum(4, 10);
			topC = randNum(1, 90);
			leftC = randNum(5, 95);
			$('#paralal .hojas').append('<div class="position-absolute anirota '+reverse+' '+flip+'" style="width: '+widC+'%; top: '+topC+'%; left: '+leftC+'%; min-width: 80px"><img src="images/leaf.svg" class="img-fluid"/></div>');
		}
		$('#bubbles').clickBubble({
			color: '#ced4da',
			size: 80,
			time: 1000,
			borderWidth: 5
		});
		fish();
	}else if(intro == 2){
		$('#paralal').html('<div class="position-relative w-100 h-100"><div id="rampart" class="w-100 h-100 position-absolute rama"></div></div>');
		particlesJS.load('rampart', 'controllers/particlesjs-config.json');
	}
}

function randNum(min, max){
	return Math.floor(Math.random()*(max-min+1)+min);
}

function fish(){
	var c = document.getElementById('c');
	var a = c.getContext('2d');
	var mode;
	var bee
	var bees = [];

	// Green RGBA is to obtain a trail after bees
	var colors = ["#ced4da", "#fff", "#ced4da", "rgba(255,255,255,.3)"];

	// Aliases
	var M = Math;
	var pow = M.pow;
	var random = M.random;
	// Configuration
	var turnRateChangeOccurence = 0.5;
	var tempMaxTurnRate;
	var maxTurnRate = 0.10;
	var maxTurnRateChange = 0.9;
	c.width = window.innerWidth;
	c.height = window.innerHeight;
	var stageWi = c.width;
	var stagehe = c.height;
	var beeWidth = 18;
	var speed = 2; // Distance run on each step
	var beeLength = 18;
	var beeTarget;
	var beeCount = 10;
	var i = beeCount; // First iteration will be on BeeCount
	var isTargetRepulsing = 0;
	var repulseCycles = 100;
	var repulseStrengh = 2;

	// bee  = [positions, direction, speed, turn rate, health]
	// position is an array of [posX, posY]
	// Speed is units per cycle
	// Turn rate is how much the direction shifts at every cycle
	/*
	Bee
		.c = coordinates
		.d = direction
		.t = turn rate
		.h = health
	*/
	
	window.addEventListener('resize', resizeCanvas, false);
	function resizeCanvas() {
		c.width = window.innerWidth;
		c.height = window.innerHeight;
		stageWi = c.width;
		stagehe = c.height;
	}


	onmousemove = onclick = polyFunc;
	function polyFunc(mode, a, b, c, value) {

		// mode = event
		if (mode.x) {
 		   	beeTarget = polyFunc(4, mode.x, mode.y);
			// onclick event
	    	if (mode.which) {
	    		isTargetRepulsing = repulseCycles;
	    	}
		}

		// function distanceToTarget (x1, y1) {
		if (mode == 1) {
			value = M.sqrt( pow(beeTarget.x - a, 2) + pow(beeTarget.y - b, 2) );
		}
		//function forwardPos(angle, length, pos) {
		if (mode == 2) {
			// polyFunc = Coord
			value = polyFunc(4,
				c.x + M.cos(a) * b,
				c.y + M.sin(a) * b
			);
		}
		//function Bee() {
		if (mode == 3) {
			value = {
					// polyFunc = Coord
					c:[polyFunc(4, stageWi * random(), stagehe * random())],
					d:0.5,
					t:0,
					h:0.5 + random()
				}
		}
		//function Coord (x, y);
		if (mode == 4) {
			value = {x:a, y:b} 
		}
		return value;
	};



	// Spawn X ammount of bees
	// polyFunc = Bee
	for (;i--;) bees[i] = polyFunc(3);


	(function loop() {
		// Shorthands for Bee attributes
		var health;
		var turnRate;
		var direction;
		var positions;

		var posIndex;
		var positions;

		// Fill the canvas with green
		a.fillStyle = colors[3];
		a.fillRect(0, 0, c.width, c.height);

		for (i = beeCount; i--;) {
			bee = bees[i];

			turnRate = bee.t;
			direction = bee.d;
			health = bee.h;
			positions = bee.c;

			for (posIndex = 0; posIndex < (beeLength * health) && posIndex < positions.length-1; posIndex++) {
				a.beginPath();
				a.lineWidth = beeWidth * health * (1-(posIndex/beeLength)); // According to health
				a.lineCap = (posIndex == 0) ? "round" : "butt";
				a.quadraticCurveTo(
					positions[posIndex].x,
					positions[posIndex].y,
					positions[posIndex+1].x,
					positions[posIndex+1].y);
				a.strokeStyle = colors[posIndex%3];
				a.stroke();

			}


			/* MOVE ROUTINE */
			var pos2;
			var distA;
			var distB;
			var change
			// Speed is according to health
			// polyFunc = forwardPos
			var pos = polyFunc(2, direction, (isTargetRepulsing ? speed * repulseStrengh : speed) * health, positions[0] );
			positions.unshift(pos);
			direction += turnRate;

			// One in a while, the turn rate changes
			// Force turn change if a target is in sight
			if (random() < turnRateChangeOccurence || isTargetRepulsing || !beeTarget%80) {

				change = (random() - 0.5) * 2 * maxTurnRateChange * (isTargetRepulsing ? repulseStrengh : 1);
				turnRate += change;

				// Test future position with both angles
				if (beeTarget) {
					// polyFunc = forwardPos
					pos2 = polyFunc(2, direction + change, speed * health, pos );
					// polyFunc = distanceToTarget
					distA = polyFunc(1, pos2.x, pos2.y);
					// polyFunc = forwardPos
					pos2 = polyFunc(2, direction - change, speed * health, pos );
					// polyFunc = distanceToTarget
					distB = polyFunc(1, pos2.x, pos2.y);
				}


				// If the directon change takes the bee's angle of attack away from the tarket
				// inverse the adjustment
				if (distA > distB && beeTarget) {
					turnRate += (isTargetRepulsing ? change : -change) * 2;
				}

				tempMaxTurnRate = maxTurnRate * (isTargetRepulsing ? repulseStrengh : 1);
				// Enforce maxTurnRate
				if (turnRate > tempMaxTurnRate) turnRate = tempMaxTurnRate;
				if (turnRate < - tempMaxTurnRate) turnRate = -tempMaxTurnRate;
				// Decrement repulse if not already at 0
				isTargetRepulsing && isTargetRepulsing--;

				// Reeasing shortcut vars to bee object
				bee.t = turnRate;
				bee.d = direction;


			}


		}

		requestAnimationFrame(loop);
	})();
}

(function( $ ) {
	'use strict';

	$.fn.clickBubble = function(opt) {

		var options = $.extend({
			color: '#000',
			size: 20,
			time: 500,
			borderWidth: 2
		}, opt);

		var id = 0;

		$(this).on('mousedown', function(e) {			
			var x = e.pageX;
			var y = e.pageY - $(this).offset().top;
			$('<div>')
				.attr('id', 'clickBubble_' + id++)
				.css({
					'width': 0,
					'height': 0,
					'border': options.borderWidth + 'px solid ' + options.color,
					'position': 'absolute',
					'left': x,
					'top': y,
					'background-color': 'rgba(255,255,255,1)',
					'border-radius': '50%'
				})
				.animate({
					'width': options.size + 'px',
					'height': options.size + 'px',
					'left': x - 0.5 * options.size,
					'top': y - 0.5 * options.size,
					opacity: 0
				}, options.time, function() {
					$(this).remove();
				})
				.appendTo(this);
		});

		return this;

	}

}( jQuery ));