var stripe = Stripe(document.getElementById('customjs').getAttribute('data-str'), {'locale': 'es-419'});
var card = false;
function showSesUS(campo, iden, nombre, onevento){
	$('#mod-sesus .modal-title small').html(nombre);	
	$('#mod-sesus').one('show.bs.modal', function () {
		if($('#sesiones_us').length > 0){
			$('#sesiones_us').DataTable().destroy(true);
		}
		$('#mod-sesus .modal-body').prepend('<table id="sesiones_us" class="table table-striped table-bordered table-sm data-table align-middle w-100"><thead><tr><th class="select-filter no_wrap" data-filtro=\'{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}\'>Creación</th><th>Duración (hh:mm:ss)</th><th>Usuario</th><th class="select-filter" data-filtro=\'{"tb":"ses_user","fl":"id_categoria","opt":"join","tbj":"categorias","flr":"id","fln":"nombre","flnd":"NA"}\'>Categoría</th><th class="select-filter" data-filtro=\'{"tb":"ses_user","fl":"id_evento","opt":"join","tbj":"eventos","flr":"id","fln":"nombre","flnd":"NA"}\'>Evento</th><th class="select-filter" data-filtro=\'{"tb":"","fl":"","opt":[[0,"NO"], [1,"SI"]],"tbj":"","flr":"","fln":"","flnd":""}\'>En vivo</th><th class="select-filter no_wrap">IP</th></tr></thead><tbody></tbody></table>');
		tablasD('sesiones_us',{'user':iden, 'iden': login, 'filtro':(onevento)?'`ses`.`'+campo+'` = '+iden+' AND `ses`.`onevento` = 1':'`ses`.`'+campo+'` = '+iden},'sesiones_us', true);
		$('#sesiones_us').on('draw.dt', function (settings) {
			var api = new $.fn.dataTable.Api( '#sesiones_us' );
			var respuesta = api.ajax.json();						
			var datosp = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'`ses`.`tiempo`', 'tabla':'`ses_user` AS `ses`', 'user': login};
			$.ajax({
				url: 'controllers/valores.php',
				data: datosp,
				type: 'post',
				dataType: 'json',
				error: function () {
					$('#mod-sesus .total_ses').html('0');
				},
				success: function (response) {
					var tiempoT = parseInt(response.valor.replace(/\./g, ""));
					var duration = moment.duration(tiempoT, 'seconds');
					$('#mod-sesus .total_ses').html(pad(Math.floor(duration.asHours()), 2)+':'+pad(duration.minutes(), 2)+':'+pad(duration.seconds(), 2));
				}
			});
		});
	});
	$('#mod-sesus').modal('show');	
}

function showVis(iden, nombre){
	$('#mod-showvis .modal-title small').html(nombre);
	$('#mod-showvis').one('show.bs.modal', function () {
		if($('#showvis_us').length > 0){
			$('#showvis_us').DataTable().destroy(true);
		}
		$('#mod-showvis .modal-body').prepend('<table id="showvis_us" class="table table-striped table-bordered table-sm data-table align-middle w-100"><thead><tr><th class="select-filter no_wrap" data-filtro=\'{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}\'>Fecha</th><th>Duración (hh:mm:ss)</th></tr></thead><tbody></tbody></table>');
		tablasD('showvis_us',{'user':iden, 'iden': login, 'filtro': iden},'showvis', true);
	});
	$('#mod-showvis').modal('show');	
}

function showUS(refe){	
	$('#form-user .data_user').val('');
	$('#mod-user').one('show.bs.modal', function () {
		if($('#usuariostb').length > 0){
			$('#usuariostb').DataTable().destroy(true);
		}
		$('#mod-user .modal-body').append('<table id="usuariostb" class="table table-striped table-bordered table-sm data-table align-middle w-100"><thead><tr><th>Nombre</th><th>Id</th><th>E-mail</th><th class="hdvis">Teléfono</th><th class="select-filter" data-filtro=\'{"tb":"usuarios","fl":"pais","opt":"join","tbj":"countries","flr":"id","fln":"name","flnd":"NA"}\'>País</th><th class="select-filter" data-filtro=\'{"tb":"","fl":"","opt":[[0,"Particular"], [1,"Agente"]],"tbj":"","flr":"","fln":"","flnd":""}\'>Tipo</th><th class="no_wrap">Código</th><th class="select-filter no_wrap" data-filtro=\'{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}\'>Creación</th><th class="no_print text-right">Acción</th></tr></thead><tbody></tbody></table>');
		tablasD('usuariostb',{'user': login, 'rol': rol, 'refe': refe},'usuariostb', true);		
	});
	$('#mod-user').modal('show');	
}

function showUsers(formu){
    $('#'+formu+' .usuario').val('');
    $('#'+formu+' .id_user').val(0);
    $('#'+formu+' .email_user').val('');
    showUS(formu);
}
function setSelUs(data, refe){				
    $('#mod-user').one('hide.bs.modal', function () {
        var datos_us = data.split(',');
        $('#'+refe+' .usuario').val(datos_us[1]).trigger('change');
        $('#'+refe+' .id_user').val(datos_us[0]).trigger('change');
        $('#'+refe+' .email_user').val(datos_us[2]).trigger('change');
    });
    $('#mod-user').modal('hide');
}

function setCambio(formu, valor, campo, campo2){
	var ncampo = (campo)?campo:'.valor';
	var ncampo2 = (campo2)?campo2:'.cambio';
    if(!isNaN($('#'+formu+' '+ncampo).val()) && $('#'+formu+' '+ncampo).val() != ''){
        $('#'+formu+' '+ncampo2).val(parseFloat($('#'+formu+' '+ncampo).val() * valor).toFixed(2));
    }else{
        $('#'+formu+' '+ncampo2).val('');
    }
}