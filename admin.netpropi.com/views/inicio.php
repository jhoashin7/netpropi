<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
if($prol <= 4 && $prol > 0){
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM usuarios";
	if ( $result = $mysqli->query( $consulta ) ) {
		$numuser = mysqli_fetch_object( $result );
		$result->close();
	}
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM plan WHERE estado = 1";
	if ( $result = $mysqli->query( $consulta ) ) {
		$numplan = mysqli_fetch_object( $result );
		$result->close();
	}
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM propiedades WHERE estado = 1";
	if ( $result = $mysqli->query( $consulta ) ) {
		$numprop = mysqli_fetch_object( $result );
		$result->close();
	}
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM contrato WHERE estado > 0";
	if ( $result = $mysqli->query( $consulta ) ) {
		$numcont = mysqli_fetch_object( $result );
		$result->close();
	}
}
if($prol >= 6){
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE estado = 0 AND referente = 0 AND id_responsable = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$numnov = mysqli_fetch_object( $result );
		$result->close();
	}
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total, COALESCE(AVG(valoracion), 0) AS perc FROM novedades WHERE estado > 0 AND referente = 0 AND id_responsable = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$numval = mysqli_fetch_object( $result );
		$result->close();
	}
	if($prol == 7){
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM contratos WHERE id_abogado = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
			$numcona = mysqli_fetch_object( $result );
			$result->close();
		}
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM pagos WHERE estado = 1 AND id_abogado = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
			$numconp = mysqli_fetch_object( $result );
			$result->close();
		}
	}
}
if($prol == 5){
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM propiedades WHERE estado != 3 AND id_user = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
        $numprop = mysqli_fetch_object( $result );
        $result->close();
    }
	$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM pagos WHERE tipo = 2 AND estado = 1 AND id_user = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
        $numconp = mysqli_fetch_object( $result );
        $result->close();
    }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch">
			<?php
			if($prol <= 4 && $prol > 0){
			?>
            <div class="col-12 col-lg-2  rounded border border-dark border-dark-hover p-3 p-md-5 bg-light rounded text-center m-2">
                <a class="text-decoration-none text-dark" href="#"  onclick="loader('usuarios'); return false">
                <!-- <div class="p-3 p-md-5 bg-light rounded text-center border-dark-hover"> -->
                    <!-- <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false"> -->
                        <h4 class="m-0 align-items-center" >
                            <span class="text-responsive">
                                <span class="fa-stack fa-2x">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-users fa-stack-1x"></i>
                                </span>
                            </span>						
                        </h4>
                    <!-- </a> -->
                    <!-- <div class="d-flex pl-1 align-items-center text-end lh-1"> -->
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numuser->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-1 text-uppercase"><span class="text-responsive">Usuarios</span></p>
                    <!-- </div> -->
                    </a>
                <!-- </div> -->
            </div>
            <div class="col-12 col-lg-2  rounded border border-dark border-dark-hover p-3 p-md-5 bg-light rounded text-center m-2">
                <a class="text-decoration-none text-dark" href="#"  onclick="loader('propiedades.plan'); return false">
                <!-- <div class="p-3 p-md-5 bg-light rounded text-center border-dark-hover"> -->
                    <!-- <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false"> -->
                        <h4 class="m-0 align-items-center" >
                            <span class="text-responsive">
                                <span class="fa-stack fa-2x">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-hand-holding-usd fa-stack-1x"></i>
                                </span>
                            </span>						
                        </h4>
                    <!-- </a> -->
                    <!-- <div class="d-flex pl-1 align-items-center text-end lh-1"> -->
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numuser->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-1 text-uppercase"><span class="text-responsive">Planes activos</span></p>
                    <!-- </div> -->
                    </a>
                <!-- </div> -->
            </div>
            <div class="col-12 col-lg-2  rounded border border-dark border-dark-hover p-3 p-md-5 bg-light rounded text-center m-2">
                <a class="text-decoration-none text-dark" href="#"  onclick="loader('propiedades.propiedad'); return false">
                <!-- <div class="p-3 p-md-5 bg-light rounded text-center border-dark-hover"> -->
                    <!-- <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false"> -->
                        <h4 class="m-0 align-items-center" >
                            <span class="text-responsive">
                                <span class="fa-stack fa-2x">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-building fa-stack-1x"></i>
                                </span>
                            </span>						
                        </h4>
                    <!-- </a> -->
                    <!-- <div class="d-flex pl-1 align-items-center text-end lh-1"> -->
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numuser->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-1 text-uppercase"><span class="text-responsive">propiedades publicadas</span></p>
                    <!-- </div> -->
                    </a>
                <!-- </div> -->
            </div>
            <div class="col-12 col-lg-2  rounded border border-dark border-dark-hover p-3 p-md-5 bg-light rounded text-center m-2">
                <a class="text-decoration-none text-dark" href="#"  onclick="loader('contratos.contratos_pag'); return false">
                <!-- <div class="p-3 p-md-5 bg-light rounded text-center border-dark-hover"> -->
                    <!-- <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false"> -->
                        <h4 class="m-0 align-items-center" >
                            <span class="text-responsive">
                                <span class="fa-stack fa-2x">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-hand-holding-usd fa-stack-1x"></i>
                                </span>
                            </span>						
                        </h4>
                    <!-- </a> -->
                    <!-- <div class="d-flex pl-1 align-items-center text-end lh-1"> -->
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numuser->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-1 text-uppercase"><span class="text-responsive">contratos pagados</span></p>
                    <!-- </div> -->
                    </a>
                <!-- </div> -->
            </div>
            <!-- <div class="col-12 col-md-6 mb-3">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-warning text-white rounded-3" onclick="loader('usuarios'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-users fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numuser->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">usuarios</span></p>
                    </div>
                </a>
            </div> -->
            <!-- <div class="col-12 col-md-6 mb-3">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-info text-white rounded-3" onclick="loader('propiedades.plan'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-hand-holding-usd fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numplan->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">planes activos</span></p>
                    </div>
                </a>
            </div> -->
			<!-- <div class="col-12 col-md-6 mb-3 mb-md-0">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-primary text-white rounded-3" onclick="loader('propiedades.propiedad'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-building fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numprop->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">propiedades publicadas</span></p>
                    </div>
                </a>
            </div> -->
			<!-- <div class="col-12 col-md-6">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-success text-white rounded-3" onclick="loader('contratos.contratos_pag'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-hand-holding-usd fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numcont->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">contratos pagados</span></p>
                    </div>
                </a>
            </div> -->
			<?php
			}
			?>
			<?php
			if($prol >= 6){
			?>
            <div class="col-12 col-md-6 mb-3">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-info text-white rounded-3" onclick="loader('misnovedades'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-comments fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numnov->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">novedades activas</span></p>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 mb-3">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-warning text-white rounded-3" onclick="loader('misnovedades'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-star fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numval->perc , 2 , "." , "." ) ?> / <?php echo number_format( $numval->total , 0 , "." , "." ) ?> votos</span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">valoración de usuarios</span></p>
                    </div>
                </a>
            </div>
			<?php
			if($prol == 7){
			?>
			<div class="col-12 col-md-6 mb-3 mb-md-0">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-primary text-white rounded-3" onclick="loader('contratos.contratos_abg'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numcona->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">contratos activos</span></p>
                    </div>
                </a>
            </div>
			<div class="col-12 col-md-6">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-success text-white rounded-3" onclick="loader('contratos.movimientos_abg'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-hand-holding-usd fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numconp->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">contratos pagados</span></p>
                    </div>
                </a>
            </div>
			<?php
			}
			?>
			<?php
			}
			?>
			<?php
			if($prol == 5){
			?>
            <!-- <section class="py-5">
                <div class="container">
                    <div class="row align-items-center card-ini">
                        <div class="col-12 col-lg-4 mb-5 mb-lg-0">
                            <div class="p-3 p-md-5 bg-light rounded text-center">
                            <h4 class="text-muted">Starter</h4>
                            <span class="d-inline-block display-4 fw-bold mb-5">$34,99</span>
                            
                            <a class="btn btn-primary w-100" href="#">Action</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 mb-5 mb-lg-0">
                            <div class="p-3 p-md-5 bg-light rounded text-center">
                            <h4 class="text-muted">Starter</h4>
                            <span class="d-inline-block display-4 fw-bold mb-5">$34,99</span>
                            
                            <a class="btn btn-primary w-100" href="#">Action</a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 mb-5 mb-lg-0 card-ini">
                            <div class="p-3 p-md-5 rounded text-center">
                            <h4 class="text-muted">Pro</h4>
                            <span class="d-inline-block display-4 fw-bold text-white mb-5">$65,99</span>
                            <a class="btn btn-light w-100 text-center" href="#">Action</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->
            <div class="col-12 col-lg-2  rounded border border-dark border-dark-hover p-3 p-md-5 bg-light rounded text-center m-2">
                <a class="text-decoration-none text-dark" href="#"  onclick="loader('propiedades.propiedad'); return false">
                <!-- <div class="p-3 p-md-5 bg-light rounded text-center border-dark-hover"> -->
                    <!-- <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false"> -->
                        <h4 class="m-0 align-items-center" >
                            <span class="text-responsive">
                                <span class="fa-stack fa-2x">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-building fa-stack-1x"></i>
                                </span>
                            </span>						
                        </h4>
                    <!-- </a> -->
                    <!-- <div class="d-flex pl-1 align-items-center text-end lh-1"> -->
                        <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numprop->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-1 text-uppercase"><span class="text-responsive">propiedades activas</span></p>
                    <!-- </div> -->
                    </a>
                <!-- </div> -->
            </div>
            <div class="col-12 col-lg-2  rounded border border-dark border-dark-hover p-3 p-md-5 bg-light rounded text-center m-2">
                <a class="text-decoration-none text-dark" href="#"  onclick="loader('miscontratos'); return false">
                <!-- <div class="p-3 p-md-5 bg-light rounded text-center border-dark-hover"> -->
                    <!-- <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false"> -->
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <!-- </a> -->
                    <!-- <div class="d-flex pl-1 align-items-center text-end lh-1"> -->
                    <h1 class="m-0 fw-bold"><span class="text-responsive"><?php echo number_format( $numconp->total , 0 , "." , "." ) ?></span></h1>
                    <p class="m-0 text-uppercase"><span class="text-responsive">contratos activos</span></p>
                    <!-- </div> -->
                </a>
                <!-- </div> -->
            </div>
            <!-- <div class="col-12 col-md-2 mb-3 mb-md-0 rounded border border-danger m-2 ">
                <a href="#" class="align-items-center justify-content-center w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3 text-danger text-danger-hover" onclick="loader('propiedades.propiedad'); return false">
                    <h4 class="m-0 align-items-center" >
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-building fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="d-flex pl-1 align-items-center text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive">< ?php echo number_format( $numprop->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">propiedades activas</span></p>
                    </div>
                </a> -->
            <!-- </div> -->
			<!-- <div class="col-12 col-md-2 mb-3 mb-md-0 rounded border border-danger m-2" >
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none rounded-3" onclick="loader('miscontratos'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 text-end lh-1 align-items-end">
                        <h1 class="m-0 fw-bold"><span class="text-responsive">< ?php echo number_format( $numconp->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">contratos activos</span></p>
                    </div>
                </a>
            </div> -->
			<!-- <div class="col-12 col-md-6 mb-3 mb-md-0">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none text-white rounded-3" onclick="loader('propiedades.propiedad'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-building fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive">< ?php echo number_format( $numprop->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">propiedades activas</span></p>
                    </div>
                </a>
            </div>
			<div class="col-12 col-md-6 mb-3 mb-md-0">
                <a href="#" class="d-flex align-items-start justify-content-between w-100 h-100 p-2 p-sm-3 text-decoration-none bg-warning text-white rounded-3" onclick="loader('miscontratos'); return false">
                    <h4 class="m-0">
                        <span class="text-responsive">
                            <span class="fa-stack fa-2x">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x"></i>
                            </span>
                        </span>						
                    </h4>
                    <div class="pl-1 align-self-end text-end lh-1">
                        <h1 class="m-0 fw-bold"><span class="text-responsive">< ?php echo number_format( $numconp->total , 0 , "." , "." ) ?></span></h1>
                        <p class="m-0 text-uppercase"><span class="text-responsive">contratos activos</span></p>
                    </div>
                </a>
            </div> -->
			<?php
			}
			?>
            <div class="col-12"><hr></div>
        </div>
		<?php
        if($prol <= 4 && $prol > 0){
        ?>
        <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch">
            <div class="col-4 mb-2 graph filter" data-graph="1" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Pagos * día (15 dias máximo)</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_1" aria-expanded="false" aria-controls="graph_cont_1"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(1); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Pagos * día</h5>					
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_1" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date" data-rango="15">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_1"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
        <!-- <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch"> -->
            <div class="col-4 mb-2 graph filter" data-graph="2" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Pagos * Producto</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_2" aria-expanded="false" aria-controls="graph_cont_2"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(2); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Pagos * Producto</h5>					
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_2" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
        <!-- <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch"> -->
            <div class="col-4 mb-2 graph filter" data-graph="3" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Usuarios nuevos * día (15 dias máximo)</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_3" aria-expanded="false" aria-controls="graph_cont_3"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(3); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Usuarios nuevos * día</h5>
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_3" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date" data-rango="15">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_3"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php
        }
        ?>
		<?php
        if($prol >= 6){
        ?>
        <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch">
            <div class="col-4 mb-2 graph filter" data-graph="4" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Novedades * Estado</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_4" aria-expanded="false" aria-controls="graph_cont_4"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(4); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Novedades * Estado</h5>					
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_4" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_4"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
		<?php
        if($prol == 7){
        ?>
		<!-- <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch"> -->
            <div class="col-4 mb-2 graph filter" data-graph="5" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Contratos pagados * día (15 dias máximo)</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_5" aria-expanded="false" aria-controls="graph_cont_5"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(5); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Contratos pagados * día</h5>					
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_5" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date" data-rango="15">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_5"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
		<?php
        }
        ?>
		<?php
        }
        ?>
		<?php
        if($prol == 5){
        ?>
		<div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch">
            <div class="col-4 mb-2 graph filter" data-graph="6" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Vistas de propiedad * día (15 dias máximo)</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_6" aria-expanded="false" aria-controls="graph_cont_6"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(6); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Vistas de propiedad * día</h5>
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_6" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date" data-rango="15">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_6"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
		<!-- <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch"> -->
            <div class="col-4 mb-2 graph filter" data-graph="7" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Favoritos * Propiedad (TOP 10)</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_7" aria-expanded="false" aria-controls="graph_cont_7"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(7); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Favoritos * Propiedad (TOP 10)</h5>					
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_7" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_7"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
		<!-- <div class="row mx-0 w-100 mt-2 justify-content-center align-items-stretch"> -->
            <div class="col-4 mb-2 graph filter" data-graph="8" data-tipo="bar">
                <div class="w-100 position-relative text-center">
                    <div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
                        <h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack align-top">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="far fa-chart-bar fa-stack-1x"></i>
                                </span>                                
                                <small>Solicitudes * día (15 dias máximo)</small>
                            </span>
                        </h6>
                        <div class="btn-group d-flex" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_8" aria-expanded="false" aria-controls="graph_cont_8"><i class="fas fa-eye fa-fw"></i></button>
                            <button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(8); return false"><i class="fas fa-print fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="d-none d-print-block w-100 mb-4">
                        <h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Solicitudes * día</h5>					
                    </div>
                    <div class="d-none d-print-block text-start w-100 mb-4">
                        <h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
                    </div>
                    <div id="graph_cont_8" class="collapse show">
                        <div class="form-group pb-2 d-print-none">
                            <div class="input-group filtro-date" data-rango="15">
                                <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                <input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
                                <button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="w-100 text-center position-relative">
                            <canvas class="mx-auto fixed-size" id="graph_8"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php
        }
        ?>
        <script>
            var filtroCalendar;			
			var graph_1 = false;
            var graph_2 = false;
            var graph_3 = false;
            var graph_4 = false;
			var graph_5 = false;
			var graph_6 = false;
			var graph_7 = false;
			var graph_8 = false;
            $(function() { 
                filtroCalendar = flatpickr($( ".graph.filter .filtro-date" ), {
					"locale": "es",
					"mode": "range",
					"dateFormat": "Y-m-d",
					"static": true,
					"wrap": true,
                    "disableMobile": false,
					"onChange": function(selectedDates, dateStr, instance) {
						var val = '';
						$(instance.element).parents('.graph.filter').find('.filtro_fecha').html('Ninguno');
						var tipo = $(instance.element).parents('.graph.filter').data('tipo');
						var graph = $(instance.element).parents('.graph.filter').data('graph');
						if(selectedDates.length > 1){							
							if($(instance.element).data('rango')){
								if(Math.abs(moment(selectedDates[0]).diff(selectedDates[1], 'days')) > parseInt($(instance.element).data('rango')) ){
									instance.setDate([moment(selectedDates[1]).subtract(parseInt($(instance.element).data('rango')), 'days').format('Y-MM-DD'), moment(selectedDates[1]).format('Y-MM-DD')], false);
									val = moment(selectedDates[1]).subtract(parseInt($(instance.element).data('rango')), 'days').format('Y-MM-DD')+' a '+moment(selectedDates[1]).format('Y-MM-DD');
								}else{
									val = dateStr;
								}								
							}else{
								val = dateStr;
							}							
							$(instance.element).parents('.graph.filter').find('.filtro_fecha').html(val);
                            if(tipo != 'map'){
                                drawGraficas(graph,tipo,val,{});
                            }
						}
                        if(selectedDates.length == 0){
                            drawGraficas(graph,tipo,val,{});
                        }
					}					
				} );
                if(filtroCalendar.length > 1){
                    for (var i = 0; i < filtroCalendar.length; i++) {
                        filtroCalendar[i].clear();
                    }                    
                }else{
                   filtroCalendar.clear(); 
                }
                
                respClass();							
				loaderHide();
            });
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>