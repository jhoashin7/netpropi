<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
$consulta = "SELECT publico FROM stripe ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $stripe = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-hand-holding-usd fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">contratos contratados</span>
                        </span>						
					</h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('contratos.movimientos_abg', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>						
                    </div>                                        
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="contrato" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 8, &quot;desc&quot; ]]">
					<thead>
                        <tr>
							<th>Referencia</th>
                            <th class="select-filter" data-filtro='{"tb":"pagos","fl":"nombre_pago","opt":"basic","tbj":"","flr":"","fln":"","flnd":"NA", "where":"`main`.`tipo` = 2 AND `main`.`id_abogado` = <?php echo $id_veri[1] ?>"}'>Contrato</th>                            
                            <th class="money_fr">Valor(<?php echo $monob->moneda ?>)</th>
							<th class="money_fr">Valor abogado(<?php echo $monob->moneda ?>)</th>
                            <th>Beneficiario</th>
							<th class="hdvis">ID Beneficiario</th>
							<th class="hideall2">Abogado</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Pendiente"], [1,"Sin llenar"], [2,"Finalizado"]],"tbj":"","flr":"","fln":"","flnd":""}'>Estado</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="hdvis">Editor</th>							
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
			<div class="col-12 text-start">
				<hr>				
				<h5 class="fw-bold m-0"><span class="text-responsive">TOTAL ABOGADO: <span class="text-muted total_tblp"></span></span></h5>
			</div>			
        </div>       				
        <script>						
            $(function() {
                tablasD('contrato',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'contrato', true);
				$('#contrato').on('draw.dt', function (settings) {
					var api = new $.fn.dataTable.Api( '#contrato' );
					var respuesta = api.ajax.json();
					var datosp2 = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'`pag`.`valor_abg`', 'tabla':'`pagos` AS `pag` LEFT JOIN `contrato` AS `con` ON (`con`.`id_pago` = `pag`.`id`)', 'decimales': 2, 'where': '`pag`.`estado` = 1 AND `pag`.`tipo` = 2', 'user': login};
					$.ajax({
						url: 'controllers/valores.php',
						data: datosp2,
						type: 'post',
						dataType: 'json',
						error: function () {
							$('.total_tblp').html('$'+(0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');
						},
						success: function (response) {
							var resT = parseFloat(response.valor);
							<?php if($monob->moneda != 'USD'){ ?>
							resT = resT * <?php echo $monob->valor ?>;
							<?php } ?>
							$('.total_tblp').html('$'+resT.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');							
						}
					});
				});						
                respClass();							
				loaderHide();
            });			
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>