<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
$consulta = "SELECT id, nombre, email FROM admins WHERE id = ".$id_veri[1];
if ( $result = $mysqli->query( $consulta ) ) {
    $user = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-money-check-alt fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Estado de cuenta</span>
                        </span>						
					</h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('estado_cuenta', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>
                    </div>                                        
                </div>
                <hr>
            </div>
			<div class="col-12 px-0">
				<div class="row mx-0 w-100 justify-content-center">
					<div class="col-12 mb-2 col-md-6 graph filter" data-graph="1" data-tipo="horizontalBar">
						<div class="w-100 position-relative text-center">
							<div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
								<h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
									<span class="text-responsive d-flex justify-content-start align-items-center">
										<span class="fa-stack align-top">
											<i class="far fa-circle fa-stack-2x"></i>
											<i class="far fa-chart-bar fa-stack-1x"></i>
										</span>                                
										<small>Pagos * día (15 dias máximo)</small>
									</span>
								</h6>
								<div class="btn-group d-flex" role="group" aria-label="Acciones">
									<button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_1" aria-expanded="false" aria-controls="graph_cont_1"><i class="fas fa-eye fa-fw"></i></button>
									<button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(1); return false"><i class="fas fa-print fa-fw"></i></button>
								</div>
							</div>
							<div class="d-none d-print-block w-100 mb-4">
								<h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Pagos * día</h5>					
							</div>
							<div class="d-none d-print-block text-start w-100 mb-4">
								<h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
							</div>
							<div id="graph_cont_1" class="collapse">
								<div class="form-group pb-2 d-print-none">
									<div class="input-group filtro-date" data-rango="15">
										<span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
										<input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
										<button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
									</div>
								</div>
								<div class="w-100 text-center position-relative">
									<canvas class="mx-auto fixed-size" id="graph_1"></canvas>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 mb-2 col-md-6 graph filter" data-graph="2" data-tipo="horizontalBar">
						<div class="w-100 position-relative text-center">
							<div class="d-flex d-print-none justify-content-between align-items-start gray-200 rounded w-100 mb-2 p-1">
								<h6 class="text-muted text-start text-uppercase fw-bold m-0 align-self-center flex-fill">
									<span class="text-responsive d-flex justify-content-start align-items-center">
										<span class="fa-stack align-top">
											<i class="far fa-circle fa-stack-2x"></i>
											<i class="far fa-chart-bar fa-stack-1x"></i>
										</span>                                
										<small>Pagos * Producto</small>
									</span>
								</h6>
								<div class="btn-group d-flex" role="group" aria-label="Acciones">
									<button type="button" class="btn btn-secondary gray-700 btn-sm text-white" data-bs-toggle="collapse" data-bs-target="#graph_cont_2" aria-expanded="false" aria-controls="graph_cont_2"><i class="fas fa-eye fa-fw"></i></button>
									<button type="button" class="btn btn-secondary gray-700 btn-sm text-white" onClick="printDom(2); return false"><i class="fas fa-print fa-fw"></i></button>
								</div>
							</div>
							<div class="d-none d-print-block w-100 mb-4">
								<h5 class="text-muted text-uppercase fw-bold m-0"><i class="fas fa-chart-bar fa-fw"></i> Pagos * Producto</h5>					
							</div>
							<div class="d-none d-print-block text-start w-100 mb-4">
								<h5 class="text-muted m-0"><i class="fas fa-calendar-check fa-fw"></i> Filtro por Fecha: <span class="fw-bold filtro_fecha"></span></h5>
							</div>
							<div id="graph_cont_2" class="collapse">
								<div class="form-group pb-2 d-print-none">
									<div class="input-group filtro-date">
										<span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
										<input type="text" class="form-control bg-white" placeholder="Filtro por Fecha" value="" data-input>
										<button class="btn btn-danger text-white" type="button" data-clear><i class="fas fa-backspace fa-fw"></i></button>
									</div>
								</div>
								<div class="w-100 text-center position-relative">
									<canvas class="mx-auto fixed-size" id="graph_2"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
            <div class="col-12">
				<hr>
                <table id="pagos" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 9, &quot;desc&quot; ]]">
					<thead>
                        <tr>
                            <th>Referencia</th>
                            <th class="select-filter" data-filtro='{"tb":"pagos","fl":"nombre_pago","opt":"basic","tbj":"","flr":"","fln":"","flnd":"NA", "where":"`main`.`tipo_pago` = 0"}'>Producto</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[1,"Propiedad"], [2,"Contrato"]],"tbj":"","flr":"","fln":"","flnd":""}'>Tipo</th>
                            <th class="money_fr">Valor(<?php echo $monob->moneda ?>)</th>
							<th class="hideall2">Usuario</th>
							<th class="hideall2">E-mail</th>
                            <th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Digital"], [1,"Manual"]],"tbj":"","flr":"","fln":"","flnd":""}'>Origen</th>
							<th>Id transacción</th>
                            <th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Pendiente"], [1,"Pagado"], [2,"Cancelado"]],"tbj":"","flr":"","fln":"","flnd":""}'>Estado</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Fecha</th>
							<th class="hdvis">Editor</th>							
							<th class="no_print text-right">Acción</th>														
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
			<div class="col-12 text-start">
				<hr>
				<h5 class="fw-bold m-0"><span class="text-responsive">TOTAL PAGADOS: <span class="text-muted total_tbl"></span></span></h5>
			</div>
        </div>
        <div class="modal fade" id="mod-pagos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-file-invoice-dollar fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Movimiento</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-pagos">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="pagos" />
							<input type="hidden" class="tipo" name="tipo" value="" />
							<input type="hidden" class="nombre_pago" name="nombre_pago" value="" />
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="email_user" name="email_user" value="" />
							
							<input type="hidden" class="usuario noclear" name="usuario" value="<?php echo utf8_encode($user->nombre) ?>" />
							<input type="hidden" class="id_pasarelan" name="id_pasarelan" value="" />
							<input type="hidden" class="estado" name="estado" value="" />
							<input type="hidden" class="tipo_pago noclear" name="tipo_pago" value="0" />
							
                            <div class="form-group pb-3">
                                <label><small>Referencia</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice fa-fw"></i></span>
                                    <input type="text" class="form-control prefijo" placeholder="Prefijo" aria-label="Prefijo" readonly required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Valor(USD)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" readonly required>
                                </div>
                            </div>
                            <?php
                            if($monob->moneda != 'USD'){
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Valor(<?php echo $monob->moneda ?>)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" class="form-control cambio" placeholder="Valor(<?php echo $monob->moneda ?>)" aria-label="Valor" readonly>
                                </div>
                            </div>
                            <?php
                            }
                            ?>                                                        
                            <div class="form-group pb-3 cont_pago">
                                <label><small>Id de pago</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice-dollar fa-fw"></i></span>
                                    <input type="text" name="id_pasarela" class="form-control id_pasarela" placeholder="Id de pago" aria-label="Id de pago" readonly>
                                </div>
                            </div>							
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="setMetodo('form-pagos'); return false"><span class="text-responsive">pagar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
			var filtroCalendar;
			var graph_1 = false;
			var graph_2 = false;			
            $(function() {				            
				filtroCalendar = flatpickr($( ".graph.filter .filtro-date" ), {
					"locale": "es",
					"mode": "range",
					"dateFormat": "Y-m-d",
					"static": true,
					"wrap": true,
                    "disableMobile": false,
					"onChange": function(selectedDates, dateStr, instance) {
						var val = '';
						$(instance.element).parents('.graph.filter').find('.filtro_fecha').html('Ninguno');
						var tipo = $(instance.element).parents('.graph.filter').data('tipo');
						var graph = $(instance.element).parents('.graph.filter').data('graph');
						if(selectedDates.length > 1){							
							if($(instance.element).data('rango')){
								if(Math.abs(moment(selectedDates[0]).diff(selectedDates[1], 'days')) > parseInt($(instance.element).data('rango')) ){
									instance.setDate([moment(selectedDates[1]).subtract(parseInt($(instance.element).data('rango')), 'days').format('Y-MM-DD'), moment(selectedDates[1]).format('Y-MM-DD')], false);
									val = moment(selectedDates[1]).subtract(parseInt($(instance.element).data('rango')), 'days').format('Y-MM-DD')+' a '+moment(selectedDates[1]).format('Y-MM-DD');
								}else{
									val = dateStr;
								}								
							}else{
								val = dateStr;
							}							
							$(instance.element).parents('.graph.filter').find('.filtro_fecha').html(val);
                            if(tipo != 'map'){
                                drawGraficas(graph,tipo,val,{});
                            }
						}
                        if(selectedDates.length == 0){
                            drawGraficas(graph,tipo,val,{});
                        }
					}					
				} );
                if(filtroCalendar.length > 1){
                    for (var i = 0; i < filtroCalendar.length; i++) {
                        filtroCalendar[i].clear();
                    }                    
                }else{
                   filtroCalendar.clear(); 
                }
				
                tablasD('pagos',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'pagos', true);
				$('#pagos').on('draw.dt', function (settings) {
					var api = new $.fn.dataTable.Api( '#pagos' );
					var respuesta = api.ajax.json();						
					var datosp = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'`pag`.`valor`', 'tabla':'`pagos` AS `pag`', 'decimales': 2, 'where': '`pag`.`estado` = 1', 'user': login};
					$.ajax({
						url: 'controllers/valores.php',
						data: datosp,
						type: 'post',
						dataType: 'json',
						error: function () {
							$('.total_tbl').html('$'+(0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');
						},
						success: function (response) {
							var resT = parseFloat(response.valor);
							<?php if($monob->moneda != 'USD'){ ?>
							resT = resT * <?php echo $monob->valor ?>;
							<?php } ?>
							$('.total_tbl').html('$'+resT.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');							
						}
					});
				});
				$('#form-pagos .valor').on('change', function(){
					if($('#form-pagos .valor').val() != ''){
						$('#form-pagos .cont_pago').show();
					}else{
						$('#form-pagos .cont_pago').hide();
						$('#form-pagos .tipo_pago').val('');
						$('#form-pagos .id_pasarela').val('');
					}
					<?php if($monob->moneda != 'USD'){ ?>
					setCambio('form-pagos', '<?php echo $monob->valor ?>');
					<?php } ?>
				});                
                respClass();							
				loaderHide();
            });
			
			function setPago(formu, iden, nombre, email, refe){
				$('#mod-'+formu).one('show.bs.modal', function () {
					$('#form-'+formu+' .prefijo').val(refe);
					$('#form-'+formu+' .usuario').val(nombre);
					$('#form-'+formu+' .email_user').val(email);
					$('#form-'+formu+' .valor').trigger('change');
				});
				openData(formu, iden);
			}
			
			function setMetodo(formu){				
				if($('#'+formu+' .valor').parsley().validate()){
					$('#'+formu+' .tipo_pago').val('0');
					if($('#'+formu+' .tipo_pago').val() === '0'){
						if($('#mod-pago').length > 0){
							$('#mod-pago').remove();							
						}
						$('<div class="modal fade" id="mod-pago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title"><span class="text-responsive d-flex justify-content-start align-items-center"><span class="fa-stack text-info align-top"><i class="fas fa-circle fa-stack-2x"></i><i class="fab fa-stripe fa-stack-1x text-white"></i></span><span>Pago</span></span></h5><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div><div class="modal-body"><p class="m-0 fw-bold"><span class="text-responsive">Resumen</span></p><small class="m-0"><span class="text-responsive respago"></span></small><hr><form id="form-pago"><input type="hidden" class="id" name="id" value=0 /><input type="hidden" class="email" value="" /><div class="form-group pb-3"><label><small>Valor(USD)</small></label><div class="input-group"><span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span><input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" readonly></div></div><div class="form-group pb-3 cont_valor"><label><small></small></label><div class="input-group"><span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span><input type="text" class="form-control cambio" placeholder="" aria-label="Valor" readonly></div></div><div class="w-100" style="border: 1px solid #ced4da; border-radius: 0.5rem; padding: 0.375rem 0.75rem;"><div id="card-element"></div></div><p role="alert"><span id="card-error" class="text-responsive"></span></p></form></div><div class="modal-footer"><div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button><button type="button" class="btn btn-success btn-enviar text-white"><span class="text-responsive">pagar <i class="fas fa-check-circle fa-fw"></i></span></button></div></div></div></div></div>').clone().appendTo('#modals_cont');
						$('#mod-pago .respago').html('<strong>'+$('#'+formu+' .prefijo').val()+'</strong>');
						$('#mod-pago .btn-enviar').prop('disabled', true);
						card = false;
						if(card){
							card.unmount();
						}
						$('#mod-pago').one('shown.bs.modal', function() {
							$('#mod-pago .valor').val($('#'+formu+' .valor').val());
							$('#mod-pago .cont_valor').hide();
							$('#form-pago .email').val($('#'+formu+' .email_user').val());
							<?php if($monob->moneda != 'USD'){ ?>
							$('#mod-pago .cont_valor label small').html('Valor(<?php echo $monob->moneda ?>)');
							$('#mod-pago .cont_valor').show();
							setCambio('form-pago', <?php echo $monob->valor ?>);
							<?php } ?>
							defPay2(formu, $('#'+formu+' .id').val(), $('#'+formu+' .id_user').val());
						});
						$('#mod-pago').one('hidden.bs.modal', function() {
							$('#mod-pago').remove();
							if($('#'+formu+' .id_pasarela').val() == ''){
								$('#'+formu+' .tipo_pago').val('').trigger('change');
							}
						});
						openData('pago');
						loaderShow();
                    }					
				}				
			}
			function defPay2(formu, plan, iduser){				
				var purchase = {
					'id': plan,					
					'id_user': iduser,
					'tipo': 'pago',
					'email': $('#form-pago .email').val(),
					'idveruser': login
				};
				fetch("controllers/create.php?v="+Math.floor((Math.random() * 1000) + 1), {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify(purchase)
				}).then(function(result) {
					return result.json();
				}).then(function(data) {
					var elements = stripe.elements();
					var style = {
						base: {
							color: "#212529",
							fontFamily: 'Poppins, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
							fontSmoothing: "antialiased",
							fontSize: "12px",
							"::placeholder": {
								color: "#9CA2AA"
							}
						},
						invalid: {
							fontFamily: 'Poppins, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
							color: "#F93154",
							iconColor: "#F93154"
						}
					};					
					if(!card){						
						card = elements.create("card", { style: style });
					}				
					card.mount("#card-element");
					card.on("change", function (event) {						
						if(event.complete){
							$('#mod-pago .btn-enviar').prop('disabled', false);
						}else{
							$('#mod-pago .btn-enviar').prop('disabled', true);
						}
						//document.querySelector("#mod-pago .btn-enviar").disabled = event.empty;
						document.querySelector("#card-error").textContent = event.error ? event.error.message : "";
					});
					var form = document.getElementById("form-pago");
					$('#mod-pago .btn-enviar').click(function(event) {
						event.preventDefault();
						payWithCard(stripe, card, data.clientSecret);
					});
					loaderHide();
				});
				var payWithCard = function(stripe, card, clientSecret) {
					if($('#form-pago').parsley().validate()){
						$('#mod-pago .btn-enviar').prop('disabled', true);
						loaderShow();
						stripe.confirmCardPayment(clientSecret, {
							receipt_email: $('#form-pago .email').val(),
							payment_method: {
								card: card,
								billing_details: {
                                  name: $('#'+formu+' .usuario').val()
                                }
							}
						}).then(function(result) {
							if (result.error) {
								loaderHide();
								card.clear();
								showError(result.error.message);
							}else{
								orderComplete(result.paymentIntent.id);
							}							
						});
					}
				};
				var orderComplete = function(paymentIntentId){
					$('#'+formu+' .id_pasarela').val(paymentIntentId);
					$('#'+formu+' .estado').val(1);
					$('#mod-pago').one('hidden.bs.modal', function() {
						Valform(formu, reLoadTable, ['pagos', false], true, false, 13);
                    });
					$('#mod-pago').modal('hide');					
				};
			}
			function setpagEsta(formu){
				if($('#'+formu+' .estado').val() != '1'){
					$('#'+formu+' .id_pasarela').attr("required", false);
				}else{
					$('#'+formu+' .id_pasarela').attr("required", true);
				}
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>