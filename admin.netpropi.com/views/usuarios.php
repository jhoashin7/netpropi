<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-users fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Usuarios</span>
                        </span>						
					</h6>                    
                </div>
                <hr>
            </div>
            <div class="col-12">                
                <table id="usuarios" class="table table-striped table-bordered table-sm data-table align-middle w-100">
					<thead>
                        <tr>
                            <th>Nombre</th>                            
                            <th>Email</th>
                            <th>Teléfono</th>
                            <th class="select-filter" data-filtro='{"tb":"usuarios","fl":"pais","opt":"join","tbj":"countries","flr":"id","fln":"name","flnd":"NA"}'>País</th>
                            <th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Particular"], [1,"Agente"]],"tbj":"","flr":"","fln":"","flnd":""}'>Tipo</th> 
                            <th class="hdvis">Código</th>
                            <th>Sesiones</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>           
        </div>           
        <div class="modal fade" id="mod-usuarios" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-users fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Usuarios</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-usuarios">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="usuarios" />
                            <input type="hidden" class="id_evento noclear" name="id_evento" value="<?php echo $evento ?>" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-id-badge fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Cédula</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-id-card fa-fw"></i></span>
                                    <input type="text" name="cedula" class="form-control cedula" placeholder="Cédula" aria-label="Cédula" data-parsley-pattern="^[0-9-;]+$" data-parsley-minlength="6" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Email</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-envelope-open-text fa-fw"></i></span>
                                    <input type="email" name="email" class="form-control email" placeholder="Email" aria-label="Email" data-parsley-type="email" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Teléfono</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-mobile-alt fa-fw"></i></span>
                                    <input type="text" name="telefono" class="form-control telefono" placeholder="Teléfono" aria-label="Teléfono">
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Departamento</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked-alt fa-fw"></i></span>
                                    <select name="id_depto" class="form-select set_codep id_depto" aria-label="Departamento" data-relacion='{"tb":"asc_ciudades","fl":"id_departamento", "flv": "id_ciudad", "fln": "ciudad"}' data-destino="#form-usuarios .id_ciudad" data-valori="" onChange="SetCodep($(this))">
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id_departamento, departamento FROM asc_departamentos ORDER BY departamento ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id_departamento'].'>'.utf8_encode($row['departamento']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Ciudad</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marker-alt fa-fw"></i></span>
                                    <select name="id_ciudad" class="form-select id_ciudad" aria-label="Ciudad">
                                        <option value="">Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Dirección</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-directions fa-fw"></i></span>
                                    <input type="text" name="direccion" class="form-control direccion" placeholder="Dirección" aria-label="Dirección">
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Inscripción</small></label>
                                <div class="input-group date-inscrito">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-check fa-fw"></i></span>
                                    <input type="text" name="inscrito" class="form-control bg-white date inscrito" data-vardate="inscrito" placeholder="Inscripción" aria-label="Inscripción" style="visibility: visible !important;" data-input>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-usuarios', reLoadTable, ['usuarios', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
        <script>            
            $(function() { 
                tablasD('usuarios',{'user': login, 'rol': rol},'usuarios', true);
                respClass();							
				loaderHide();
            });
            function setEvt(){
                if($('.selevento').val() == ''){
                    loader('usuarios');
                }else{
                    loader('usuarios', {"evento": $('.selevento').val()});
                }                
            }
            function openDataMail(iden, fecha){
                var eventot = moment(fecha).format("dddd D [de] MMMM [- Hora:] hh:mm a");
                $('#mod-sendmail').one('shown.bs.modal', function(){
                    $('#form-sendmail .fecha').val(eventot);
                });
                openData('sendmail', iden);
            }    
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>