<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
date_default_timezone_set('America/Bogota');
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
$puede = true;
if($prol == 5){	
	$puede = false;
	$consulta = "SELECT id FROM plan WHERE fin >= '".date('Y-m-d')."' AND id_user = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$puede = ($rows > 0)?true:false;
		$result->close();
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-piggy-bank fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">mi balance</span>
                        </span>						
					</h6>
					<div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('balance', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>
                        <button type="button" class="btn btn-warning text-white" onClick="openData('balance'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="balance" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 4, &quot;desc&quot; ]]">
					<thead>
                        <tr>							
                            <th>Nombre</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[1,"Ingreso"], [2,"Gasto"]],"tbj":"","flr":"","fln":"","flnd":""}'>Tipo</th>
							<th class="money_fr">Valor(<?php echo $monob->moneda ?>)</th>
                            <th class="select-filter">Propiedades</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Fecha</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
			<div class="col-12 text-start">
				<hr>
				<h5 class="fw-bold mb-2"><span class="text-responsive">TOTAL INGRESOS: <span class="text-success total_tbl"></span></span></h5>
				<h5 class="fw-bold m-0"><span class="text-responsive">TOTAL GASTOS: <span class="text-danger total_tblp"></span></span></h5>
			</div>
        </div>
        <div class="modal fade" id="mod-balance" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-piggy-bank fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Añadir Item</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-balance">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="balance" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Origen / Destino</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <select name="id_propiedad" class="form-select id_propiedad" aria-label="Propiedad" required>
                                        <option value="">Seleccionar</option>
										<option value="0">General</option>
                                        <?php
										$consulta = "SELECT id, nombre, LPAD(id,6,0) AS codigo FROM propiedades WHERE id_user = ".$id_veri[1]." AND nombre != '' ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).' - '.$row['codigo'].'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Fecha</small></label>
                                <div class="input-group date-fecha">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-day fa-fw"></i></span>
                                    <input type="text" name="fecha" class="form-control bg-white date fecha" data-vardate="fecha" placeholder="Fecha" aria-label="Fecha" style="visibility: visible !important;" data-input required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Tipo</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-hand-holding-usd fa-fw"></i></span>
                                    <select name="tipo" class="form-select tipo" aria-label="Tipo" required>
                                        <option value="">Seleccionar</option>
										<option value="1">Ingreso</option>
                                        <option value="2">Gasto</option>
                                    </select>
                                </div>
                            </div>							
							<div class="form-group pb-3">
                                <label><small>Valor</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" required>
									<select name="moneda" class="form-select moneda" aria-label="Moneda" data-parsley-error-message="Debes seleccionar una moneda." required>
										<option value="">Moneda</option>
										<?php
										$consulta = "SELECT id, moneda FROM cambio GROUP BY moneda ORDER BY moneda ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
										?>
										<option value="<?php echo $row['id']?>"><?php echo $row['moneda']?></option>
										<?php                                    
											}
											$result->close();
										}
										?>                                        
                                    </select>
                                </div>
                            </div>							
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-balance', reLoadTable, ['balance', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
        <script>
			var fecha;
            $(function() {
				fecha = flatpickr($("#form-balance .date-fecha"), {
					"locale": "es",
					"mode": "single",
					"enableTime": false,
					"dateFormat": "Y-m-d",
					"static": false,
					"wrap": true,
                    "disableMobile": true
				});
                tablasD('balance',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'balance', true);
				$('#balance').on('draw.dt', function (settings) {
					var api = new $.fn.dataTable.Api( '#balance' );
					var respuesta = api.ajax.json();						
					var datosp = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'ROUND(((`bal`.`valor` / `cam`.`valor`) * <?php echo $monob->valor ?>), 2)', 'tabla':'`balance` AS `bal` LEFT JOIN `cambio` AS `cam` ON (`cam`.`id` = `bal`.`moneda`)', 'decimales': 2, 'sepdeci':'.', 'where': '`bal`.`tipo` = 1', 'user': login};
					$.ajax({
						url: 'controllers/valores.php',
						data: datosp,
						type: 'post',
						dataType: 'json',
						error: function () {
							$('.total_tbl').html('$'+(0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');
						},
						success: function (response) {
							var resT = response.valor;							
							$('.total_tbl').html('$'+resT+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');							
						}
					});
					var datosp2 = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'ROUND(((`bal`.`valor` / `cam`.`valor`) * <?php echo $monob->valor ?>), 2)', 'tabla':'`balance` AS `bal` LEFT JOIN `cambio` AS `cam` ON (`cam`.`id` = `bal`.`moneda`)', 'decimales': 2, 'sepdeci':'.', 'where': '`bal`.`tipo` = 2', 'user': login};
					$.ajax({
						url: 'controllers/valores.php',
						data: datosp2,
						type: 'post',
						dataType: 'json',
						error: function () {
							$('.total_tblp').html('- $'+(0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');
						},
						success: function (response) {
							var resT = response.valor;							
							$('.total_tblp').html('- $'+resT+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');							
						}
					});
				});
                respClass();
				<?php
				if($puede){
				?>
				loaderHide();
				<?php	
				}else{
				?>
				loader('inicio');
				showError('Debes tener al menos 1 plan de propiedades vigente para acceder a esta sección');
				<?php	
				}
				?>				
            });			
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>