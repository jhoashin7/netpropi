<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-wallet fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Planes</span>
                        </span>						
					</h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('balance.planes', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>
						<?php
						if($prol != 3 && $prol <= 4){						
						?>
                        <button type="button" class="btn btn-warning text-white" onClick="openData('planes'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
						<?php						
						}
						?>
                    </div>                                        
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="planes" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 3, &quot;asc&quot; ]]">
					<thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Prefijo</th>
							<th>Cantidad propiedades</th>
                            <th class="money_fr">Valor * propiedad(<?php echo $monob->moneda ?>)</th>
                            <th class="number">Tiempo(días)</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Público"], [1,"Privado"]],"tbj":"","flr":"","fln":"","flnd":""}'>Tipo</th>
							<th>Beneficiario</th>
                            <th class="number">Pagados</th>							
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<?php
							if($prol != 3 && $prol <= 4){						
							?>
							<th class="no_print text-right">Acción</th>
							<?php						
							}
							?>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-planes" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-wallet fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Plan</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-planes">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="planes" />
							<input type="hidden" class="beneficiario id_user" name="beneficiario" value="" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Prefijo</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice fa-fw"></i></span>
                                    <input type="text" name="prefijo" class="form-control prefijo" placeholder="Prefijo" aria-label="Prefijo" data-parsley-type="alphanum" data-parsley-maxlength="5" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Tipo</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user-tag fa-fw"></i></span>
                                    <select name="tipo" class="form-select tipo" aria-label="Tipo" onChange="showSelUser()" required>
                                        <option value="">Seleccionar</option>
                                        <option value="0">Público</option>
										<option value="1">Privado</option>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3 cont_usuario">
                                <label><small>Beneficiario</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user fa-fw"></i></span>
                                    <input type="text" class="form-control usuario" placeholder="Beneficiario" aria-label="Beneficiario" readonly required>
									<button class="btn btn-warning" type="button" onClick="showUsers('form-planes')"><i class="fas fa-users fa-fw text-white"></i></button>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Valor propiedad(USD)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" required>
                                </div>
                            </div>
                            <?php
                            if($monob->moneda != 'USD'){
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Valor propiedad(<?php echo $monob->moneda ?>)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" class="form-control cambio" placeholder="Valor(<?php echo $monob->moneda ?>)" aria-label="Valor" readonly>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Mínimo propiedades</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" name="minimo" class="form-control minimo" placeholder="Mínimo propiedades" aria-label="Mínimo propiedades" data-parsley-min="1" data-parsley-type="digits" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Máximo propiedades</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" name="maximo" class="form-control minimo" placeholder="Máximo propiedades" aria-label="Máximo propiedades" data-parsley-min="1" data-parsley-gte="#form-planes .minimo" data-parsley-type="digits" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Tiempo(días)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-alt fa-fw"></i></span>
                                    <input type="text" name="tiempo" class="form-control tiempo" placeholder="Tiempo" aria-label="Tiempo" data-parsley-min="1" data-parsley-type="digits" required>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-planes', reLoadTable, ['planes', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                tablasD('planes',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'planes', true);
                <?php if($monob->moneda != 'USD'){ ?>
                $('#form-planes .valor').on('input', function(){					
					setCambio('form-planes', <?php echo $monob->valor ?>);
				});                
                <?php } ?>
				$('#mod-planes').on('show.bs.modal', function(){
					$('#form-planes .usuario').attr('required', false);
					$('#form-planes .cont_usuario').hide();					
					if($('#form-planes .tipo').val() === '1'){
						$('#form-planes .usuario').attr('required', true);						
						consData('admins', $('#form-planes .id_user').val(), ['nombre'], function(result){
							if(result){
								$('#form-planes .usuario').val(result.nombre);
							}							
						});
						$('#form-planes .cont_usuario').show();
					}
					<?php if($monob->moneda != 'USD'){ ?>
					setCambio('form-planes', <?php echo $monob->valor ?>);
					<?php } ?>
				});
                respClass();							
				loaderHide();
            });            
			function showSelUser(){
				$('#form-planes .usuario').attr('required', false);
                $('#form-planes .cont_usuario').hide();
				$('#form-planes .usuario').val('');
				$('#form-planes .id_user').val(0);				
                if($('#form-planes .tipo').val() === '1'){
                    $('#form-planes .usuario').attr('required', true);
                    $('#form-planes .cont_usuario').show();
                }				
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>