<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Contratos</span>
                        </span>						
					</h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('contratos.contratos_abg', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>                        
                    </div>                                        
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="contratos" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 3, &quot;asc&quot; ]]">
					<thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Prefijo</th>
							<th class="hideall2">Abogado</th>
                            <th class="money_fr">Valor(<?php echo $monob->moneda ?>)</th>
							<th class="money_fr">Valor abogado(<?php echo $monob->moneda ?>)</th>
                            <th class="number">Paises</th>							
                            <th class="number">Pagados</th>							
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-contratos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-file-contract fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Contrato</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-contratos">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="contratos" />
							<input type="hidden" class="variables" name="variables" value="" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>                            
							<div class="form-group pb-3">
                                <label><small>Paises donde aplica</small></label>
                                <div class="input-group">
                                    <select class="paises form-select" multiple="multiple" size="10" name="paises[]" required>
                                        <option value="0">Todos</option>
                                        <?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
							<hr>
							<div class="d-flex w-100 justify-content-between align-items-center">
								<h6 class="m-0 text-muted pr-2">
									<span class="text-responsive d-flex justify-content-start align-items-center">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x"></i>
											<i class="fas fa-file-code fa-stack-1x text-white"></i>
										</span>										
										<span class="fw-bold">variables</span>
									</span>						
								</h6>
								<button type="button" class="btn btn-warning btn-sm text-white" onClick="openData('variables'); return false"><i class="fas fa-plus-circle"></i></button>
							</div>							
							<small class="d-block my-2 text-muted">Las variables son los campos dinámicos que el usuario completará, en el texto principal se ubican y seran reemplazadas por los datos que el usuario introduzca.</small>
							<div class="d-block w-100 py-2 text-start vars_cont"></div>
                            <div class="form-group pb-3">
                                <label><small>Texto principal</small></label>
								<textarea name="texto" class="summer texto form-control" placeholder="Texto principal" required></textarea>
                            </div>                         
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-contratos', reLoadTable, ['contratos', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="mod-variables" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-file-code fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Agregar variable</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-variables">
                            <input type="hidden" class="id" name="id" value=0 />
                            <div class="form-group pb-3">
                                <label><small>Enunciado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" class="form-control enunciado" placeholder="Enunciado" aria-label="Enunciado" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Nombre variable <span class="text-muted">(sin espacios ni cractéres especiales)</span></small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" data-parsley-type="alphanum" required>
                                </div>
                            </div>
							<div class="form-group pb-3 cont_pago">
                                <label><small>Tipo</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-paragraph fa-fw"></i></span>
                                    <select name="tipo" class="form-select tipo" aria-label="Tipo" required>
                                        <option value="">Seleccionar</option>
                                        <option value="input">Línea de texto</option>
										<option value="textarea">Bloque de texto</option>
                                    </select>
                                </div>
                            </div>							                     
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="ValVar(); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
				inputMultiple($('#form-contratos .paises'), {});
                tablasD('contratos',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'contratos', true);
				$('#mod-contratos').on('show.bs.modal', setVars);
                respClass();							
				loaderHide();
            });
			function setVars(){
				$('#form-contratos .vars_cont').html('');
				if($('#form-contratos .variables').val() != ''){
					var objeto = JSON.parse($('#form-contratos .variables').val());
					var contval = '';
					$.each(objeto, function (index, value){
						contval += '<span class="badge bg-secondary text-white py-2 text-nowrap d-inline-block mx-1 var var_'+value.id+'"><h6 class="m-0 d-inline-block fw-bold">['+value.nombre+']</h6> <a href="#" class="ms-2 d-inline-block text-decoration-none link-danger" onClick="delVar('+value.id+'); return false"><i class="fas fa-minus-circle fa-lg"></i></a></span>';
					});
					$('#form-contratos .vars_cont').html(contval);
				}
			}
			function ValVar(){
				if($('#form-contratos').parsley().validate()){
					var nobjeto = [];
					var count = 1;
					if($('#form-contratos .variables').val() != ''){
						var objeto = JSON.parse($('#form-contratos .variables').val());
						$.each(objeto, function (index, value){
							nobjeto.push({
								'id': count,
								'enunciado': value.enunciado,
								'nombre': value.nombre,
								'tipo': value.tipo
							});
							count++;							
						});
					}
					nobjeto.push({
                        'id': count,
						'enunciado': $('#form-variables .enunciado').val(),
                        'nombre': $('#form-variables .nombre').val(),
                        'tipo': $('#form-variables .tipo').val()
                    });
					$('#form-contratos .variables').val(JSON.stringify(nobjeto));
					setVars();
					$('#mod-variables').modal('hide');
				}
			}
			function delVar(iden){
				var nobjeto = [];
				var count = 1;
				var objeto = JSON.parse($('#form-contratos .variables').val());
				$.each(objeto, function (index, value){
					if(value.id != iden){
						nobjeto.push({
							'id': count,
							'enunciado': value.enunciado,
							'nombre': value.nombre,
							'tipo': value.tipo
						});
					}                    
                    count++;							
                });
				if(nobjeto.length > 0){
					$('#form-contratos .variables').val(JSON.stringify(nobjeto));
				}else{
					$('#form-contratos .variables').val('');
				}				
				setVars();
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>