<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
$puede = true;
if($prol == 5){	
	$puede = false;
	$consulta = "SELECT id FROM plan WHERE fin >= '".date('Y-m-d')."' AND id_user = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$puede = ($rows > 0)?true:false;
		$result->close();
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-building fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold"><?php echo ($prol != 5)?'Propiedades':'mis propiedades' ?></span>
                        </span>						
					</h6>                    
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="propiedades" class="table table-striped table-bordered data-table align-middle w-100">
					<thead>
                        <tr>
							<th>Código</th>
                            <th>Nombre</th>
                            <th class="select-filter">Plan</th>
							<th class="select-filter hdvis" data-filtro='{"tb":"propiedades","fl":"id_pais","opt":"join","tbj":"countries","flr":"id","fln":"name","flnd":"NA", "where":"`sub`.`activo` = 1"}'>País</th>							
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[1,"Comercial"], [2,"Vivienda"]],"tbj":"","flr":"","fln":"","flnd":""}'>Tipo</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[1,"Venta"], [2,"Alquiler"]],"tbj":"","flr":"","fln":"","flnd":""}'>Fin</th>
							<th class="<?php echo ($prol != 5)?'':'hideall2' ?>">Responsable</th>
							<th class="<?php echo ($prol != 5)?'hdvis':'hideall2' ?>">E-mail Responsable</th>
							<th class="money_fr">Valor</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Pendiente"], [1,"Publicada"], [2,"Ocupada"], [3,"Suspendida"]],"tbj":"","flr":"","fln":"","flnd":""}'>Estado</th>                            
                            <th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Vigencia</th>
                            <th>Visitas</th>
                            <th class="number">Favoritos</th>
                            <th>Solicitudes</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-propiedades" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-building fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Propiedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-propiedades">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Tipo</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-store-alt fa-fw"></i></span>
                                    <select name="tipo" class="form-select tipo" aria-label="Tipo" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un tipo de propiedad." required>
                                        <option value="0">Seleccionar</option>
										<option value="1">Comercial</option>
                                        <option value="2">Vivienda</option>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Fin</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-sign fa-fw"></i></span>
                                    <select name="fin" class="form-select fin" aria-label="Fin" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un fin para la propiedad." required>
                                        <option value="0">Seleccionar</option>
										<option value="1">Venta</option>
                                        <option value="2">Alquiler</option>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Valor</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" required>
									<select name="moneda_valor" class="form-select moneda_valor" aria-label="Moneda" data-parsley-min="1" data-parsley-error-message="Debes seleccionar una moneda.">
										<option value="0">Moneda</option>
										<?php
										$consulta = "SELECT id, moneda FROM cambio GROUP BY moneda ORDER BY moneda ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
										?>
										<option value="<?php echo $row['id']?>"><?php echo $row['moneda']?></option>
										<?php                                    
											}
											$result->close();
										}
										?>                                        
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Valor Administración</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="administracion" class="form-control administracion" placeholder="Administración" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" required>
									<select name="moneda_admon" class="form-select moneda_admon" aria-label="Moneda" data-parsley-min="1" data-parsley-error-message="Debes seleccionar una moneda.">
										<option value="0">Moneda</option>
										<?php
										$consulta = "SELECT id, moneda FROM cambio GROUP BY moneda ORDER BY moneda ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
										?>
										<option value="<?php echo $row['id']?>"><?php echo $row['moneda']?></option>
										<?php                                    
											}
											$result->close();
										}
										?>                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Habitaciones</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-bed fa-fw"></i></span>
                                    <input type="text" name="habitaciones" class="form-control habitaciones" placeholder="Habitaciones" aria-label="Habitaciones" data-parsley-type="digits" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Baños</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-bath fa-fw"></i></span>
                                    <input type="text" name="banos" class="form-control banos" placeholder="Baños" aria-label="Baños" data-parsley-pattern="^[0-9]*(\.?[0-9]{1}$)?" required>
                                </div>
                            </div>							
							<div class="form-group pb-3">
                                <label><small>Área</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-ruler-combined fa-fw"></i></span>
                                    <input type="text" name="area" class="form-control area" placeholder="Área" aria-label="Área" data-parsley-pattern="^[0-9]*(\.?[0-9]{0,2}$)?" required>
									<span class="input-group-text fw-bold">m²</span>
                                </div>
                            </div>							
							<div class="form-group pb-3">
                                <label><small>Estrato</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice fa-fw"></i></span>
                                    <input type="text" name="estrato" class="form-control estrato" placeholder="Estrato" aria-label="Estrato" data-parsley-type="digits" >
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Características</small></label>
                                <div class="input-group">
                                    <select class="caracteristicas form-select" multiple="multiple" size="10" name="caracteristicas[]" required>
                                        <?php
										$consulta = "SELECT id, nombre FROM caracteristicas ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Facilidades</small></label>
                                <div class="input-group">
                                    <select class="facilidades form-select" multiple="multiple" size="10" name="facilidades[]" required>
                                        <?php
										$consulta = "SELECT id, nombre FROM facilidades ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Descripción</small></label>
                                <textarea name="descripcion" class="descripcion form-control" rows="4" placeholder="Descripción" required></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-propiedades', reLoadTable, ['propiedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-camera-retro fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Fotos</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-fotos">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />
							<input type="hidden" class="foto" name="foto" value="" required />
							<input type="hidden" class="location noclear" value="<?php echo $conArr['base_url_sitio'] ?>/propiedades" />
							<!-- <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>                            						 -->
                        </form>
						<p class="text-muted"><span class="text-responsive">Haz click en subir foto para agregar una nueva, organizalas para determinar su orden de aparición o elimina las que ya no quieras mostrar, puedes subir hasta 6 fotos por propiedad.<br><strong>Recuerda guardar para aplicar tus cambios.</strong></span></p>
						<div class="row w-100 mx-0 justify-content-start align-items-center sorteable_cont">
							<div class="col-6 col-md-4 bg-white py-2 text-center no-sorteable">
								<a href="#" class="text-decoration-none position-relative ratio ratio-1x1 d-block link-dark" onClick="openData('fotosU'); return false">
									<div class="d-flex w-100 justify-content-start align-items-stretch">
										<div class="border border-2 border-secondary p-2 flex-fill d-flex justify-content-center align-items-center" style="border-style: dashed !important">
											<div class="text-center">
												<i class="fas fa-camera text-muted fa-4x fa-fw"></i>
												<p class="text-muted mt-2 mb-0"><span class="text-responsive">foto nueva</span>
											</div>
										</div>
									</div>
								</a>
							</div>							
						</div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="valFotos(); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-map-signs fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Ubicación</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-ubicacion">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />                            
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Dirección</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-directions fa-fw"></i></span>
                                    <input type="text" name="direccion" class="form-control direccion" onChange="chkPosition()" placeholder="Dirección" aria-label="Dirección" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>País</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map fa-fw"></i></span>
                                    <select name="id_pais" class="form-select set_codep id_pais" aria-label="País" data-relacion='{"tb":"states","fl":"country_id", "flv": "id", "fln": "name", "val": "0"}' data-destino="#form-ubicacion .id_states" data-valori="" onChange="SetCodep($(this)); chkPosition()" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un País." required>
                                        <option value="0">Seleccionar</option>
										<?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
										?>
										<option value="<?php echo $row['id']?>"><?php echo utf8_encode($row['name'])?></option>
										<?php                                    
											}
											$result->close();
										}
										?> 
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Estado / Departamento</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked fa-fw"></i></span>
                                    <select name="id_states" class="form-select set_codep id_states" aria-label="Estado / Departamento" data-relacion='{"tb":"cities","fl":"state_id", "flv": "id", "fln": "name", "val": "0"}' data-destino="#form-ubicacion .id_city" data-valori="" onChange="SetCodep($(this)); chkPosition()" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un Estado o Departamento." required>
                                        <option value="0">Seleccionar</option>										
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Ciudad</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked-alt fa-fw"></i></span>
                                    <select name="id_city" class="form-select id_city" aria-label="Ciudad" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un Estado o Departamento." onChange="chkPosition()" required>
                                        <option value="0">Seleccionar</option>										
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
								<label><small>Ubicación<br><span class="text-muted">(Una vez tengas definido la dirección, el país, el estado/Departamento y la ciudad, podrás arrastrar el pin en el mapa para ajustar la ubicación)</span></small></label>
								<div class="input-group">
									<input type="hidden" class="ubicacion" name="ubicacion" value="" required />
									<div id="map_cont" class="ratio ratio-16x9">
										<div id="mapa" class="w-100 h-100"></div>
									</div>
								</div>
							</div>							
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-ubicacion', reLoadTable, ['propiedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                </div>
            </div>
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-ubicacion" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-map-signs fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Ubicación</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-ubicacion">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />                            
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Dirección</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-directions fa-fw"></i></span>
                                    <input type="text" name="direccion" class="form-control direccion" onChange="chkPosition()" placeholder="Dirección" aria-label="Dirección" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>País</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map fa-fw"></i></span>
                                    <select name="id_pais" class="form-select set_codep id_pais" aria-label="País" data-relacion='{"tb":"states","fl":"country_id", "flv": "id", "fln": "name", "val": "0"}' data-destino="#form-ubicacion .id_states" data-valori="" onChange="SetCodep($(this)); chkPosition()" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un País." required>
                                        <option value="0">Seleccionar</option>
										<?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
										?>
										<option value="<?php echo $row['id']?>"><?php echo utf8_encode($row['name'])?></option>
										<?php                                    
											}
											$result->close();
										}
										?> 
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Estado / Departamento</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked fa-fw"></i></span>
                                    <select name="id_states" class="form-select set_codep id_states" aria-label="Estado / Departamento" data-relacion='{"tb":"cities","fl":"state_id", "flv": "id", "fln": "name", "val": "0"}' data-destino="#form-ubicacion .id_city" data-valori="" onChange="SetCodep($(this)); chkPosition()" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un Estado o Departamento." required>
                                        <option value="0">Seleccionar</option>										
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Ciudad</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked-alt fa-fw"></i></span>
                                    <select name="id_city" class="form-select id_city" aria-label="Ciudad" data-parsley-min="1" data-parsley-error-message="Debes seleccionar un Estado o Departamento." onChange="chkPosition()" required>
                                        <option value="0">Seleccionar</option>										
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
								<label><small>Ubicación<br><span class="text-muted">(Una vez tengas definido la dirección, el país, el estado/Departamento y la ciudad, podrás arrastrar el pin en el mapa para ajustar la ubicación)</span></small></label>
								<div class="input-group">
									<input type="hidden" class="ubicacion" name="ubicacion" value="" required />
									<div id="map_cont" class="ratio ratio-16x9">
										<div id="mapa" class="w-100 h-100"></div>
									</div>
								</div>
							</div>							
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-ubicacion', reLoadTable, ['propiedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-fotos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-camera-retro fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Fotos</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-fotos">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />
							<input type="hidden" class="foto" name="foto" value="" required />
							<input type="hidden" class="location noclear" value="<?php echo $conArr['base_url_sitio'] ?>/propiedades" />
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>                            						
                        </form>
						<p class="text-muted"><span class="text-responsive">Haz click en subir foto para agregar una nueva, organizalas para determinar su orden de aparición o elimina las que ya no quieras mostrar, puedes subir hasta 6 fotos por propiedad.<br><strong>Recuerda guardar para aplicar tus cambios.</strong></span></p>
						<div class="row w-100 mx-0 justify-content-start align-items-center sorteable_cont">
							<div class="col-6 col-md-4 bg-white py-2 text-center no-sorteable">
								<a href="#" class="text-decoration-none position-relative ratio ratio-1x1 d-block link-dark" onClick="openData('fotosU'); return false">
									<div class="d-flex w-100 justify-content-start align-items-stretch">
										<div class="border border-2 border-secondary p-2 flex-fill d-flex justify-content-center align-items-center" style="border-style: dashed !important">
											<div class="text-center">
												<i class="fas fa-camera text-muted fa-4x fa-fw"></i>
												<p class="text-muted mt-2 mb-0"><span class="text-responsive">foto nueva</span>
											</div>
										</div>
									</div>
								</a>
							</div>							
						</div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="valFotos(); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-fotosU" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-camera-retro fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Subir Foto</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-fotosU">
                            <input type="hidden" class="id" name="id" value=0 />
                            <div class="form-group file_up pb-3">
                                <label><small>Imagenes de propiedad</small></label>
                                <input type="hidden" class="foto" data-funcion="inputvars_Base" data-funcionvar="inputvars_foto" data-imgcont="<?php echo $conArr['updestino'] ?>propiedades" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/propiedades" value="" data-parsley-error-message="Debes subir una foto de tu propiedad." required />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_foto" multiple accept="image/*" />
                                </div>
                            </div>							
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="valFoto(); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-estado" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-building fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Estado Propiedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-estado">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Estado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-store-alt fa-fw"></i></span>
                                    <select name="estado" class="form-select estado" aria-label="Estado" required>
                                        <option value="">Seleccionar</option>
										<option value="0">Pendiente</option>
                                        <option value="1">Publicada</option>
										<option value="2">Ocupada</option>
                                        <option value="3">Suspendida</option>
                                    </select>
                                </div>
                            </div>							
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-estado', reLoadTable, ['propiedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-solicitudeslog" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-info align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-tasks fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Solicitudes</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-solicitudeslog">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="propiedades" />
							<div class="form-group pb-3">
								<label><small>Nombre</small></label>
								<div class="input-group">
									<span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
									<input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
								</div>
							</div>
                        </form>						
						<div class="d-block w-100 cont_logs"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
        <script>            
            var inputvars_foto = {
                maxFileSize: 4000,				
                maxFileCount: 1,
                dropZoneTitle: 'Arrastra y suelta aquí tu foto de la propiedad.',
                initialPreviewFileType: 'image',
				allowedFileTypes: ['image'],
                fileActionSettings: {
                    showZoom: true,
					showDrag: false,
					showRemove: false
                }				
            };			
            $(function() {				
				$('#mod-ubicacion').on('show.bs.modal', function () {
					initMapa();
					if($('#form-ubicacion .ubicacion').val() != ''){
						chkPosition($('#form-ubicacion .ubicacion').val());
					}
				});
				inputMultiple($('#form-propiedades .caracteristicas'), {});
				inputMultiple($('#form-propiedades .facilidades'), {});
                inputvars_Base($('#form-fotosU .foto'));
				$('#mod-fotos .sorteable_cont').sortable({
					items: 'div.col-6:not(.no-sorteable)',
					containment: "parent",
					cursor: "move",					
					handle: '.portlet',
					cancel: ".portlet-btn",
					placeholder: 'portlet-placeholder bg-secondary col-6 col-md-4 align-self-stretch'
				});
				$('#mod-fotos .sorteable_cont').disableSelection();
				$('#mod-fotos').on('show.bs.modal', function(){
                    setFotos();
                });
                tablasD('propiedades',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'propiedades', true);
                respClass();							
				<?php
				if($puede){
				?>
				loaderHide();
				<?php	
				}else{
				?>
				loader('inicio');
				showError('Debes tener al menos 1 plan de propiedades vigente para acceder a esta sección');
				<?php	
				}
				?>
            });
			function showSol(iden, nombre){
				$('#mod-solicitudeslog').one('show.bs.modal', function(){
					var datos = {
						'id': iden,
						'accion': 21,
						'idveruser': login
					};                              
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#mod-solicitudeslog .cont_logs').html('');
						},
						success:  function (response) {					
							if(response.resp){
								var contenido = '';		
								$.each(response.data.logs, function(key, value){
									contenido += '<div class="row mx-0 w-100 justify-content-center align-items-center border border-light rounded-3"><div class="col-12 text-center py-2"><div class="d-flex w-100 justify-content-start align-items-center"><div class="flex-fill text-start"><small class="fw-bold">'+value.nombre+'</small></div><div class="ps-2 text-end text-nowrap cont_visto_'+value.id+'">';
									if(value.vista == 0 && value.editable){
										contenido += '<small class="d-inline-block pe-1">revisado</small><div class="form-group d-inline-block align-middle"><div class="form-check form-switch swt-sm d-flex justify-content-start align-items-center"><input class="form-check-input mt-0 chk-warning visto_'+value.id+'" type="checkbox" onChange="setvistosol('+value.id+');" ></div></div>';
									}else{
										contenido += (value.vista == 0)?'<small class="d-inline-block text-danger">pendiente</small>':'<small class="d-inline-block text-success">revisado</small>';
									}																
									contenido += '</div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-comment-dots"></i> <small class="fw-bold">'+value.responsable+'</small></div><div class="text-end ps-2"><i class="fas fa-calendar"></i> <small class="fw-bold">'+value.creado+'</small></div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-start align-items-center">';									
									contenido += '</div></div><div class="col-12 text-start pb-2"><small>'+value.texto+'</small></div></div><hr>';							
								});
								$('#mod-solicitudeslog .cont_logs').html(contenido);						
							}else{
								$('#mod-solicitudeslog .cont_logs').html('');
							}					
						}
					});
				});
				openData('solicitudeslog', iden);
			}			
			function setvistosol(iden){
				if($('#mod-solicitudeslog .visto_'+iden).is(':checked')){
					loaderShow();
					var datos = {
						'id': iden,						
						'accion': 16,
						'editor': login,
						'idveruser': login
					};                              
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#loader').one("hide", function() { 
								showError(error);
							});
							loaderHide();
						},
						success:  function (response) {					
							if(response.resp){
								$('#mod-solicitudeslog .cont_visto_'+iden).html('<small class="d-inline-block text-success">revisado</small>');
								reLoadTable('propiedades', false);
								loaderHide();				
							}else{
								$('#loader').one("hide", function() { 
									showError(response.data.msg);
								});
								loaderHide();
							}					
						}
					});
				}
			}
			
			
			function setFotos(){
				var fotos = $('#form-fotos .foto').val();
				var cuenta = 0;
				var arreglo = [];
				var ncont = '';
				$('#mod-fotos .sorteable_cont div.col-6:not(.no-sorteable)').remove();
				if(fotos != ''){
					arreglo = fotos.split(',');					
					var count = 1;
					$.each(arreglo, function( index, value ) {
						ncont += '<div class="col-6 col-md-4 bg-white py-2 text-center foto-'+count+'" data-valor="'+value+'"><div class="position-relative ratio ratio-1x1 d-block"><div class="d-flex w-100 justify-content-start align-items-stretch"><div class="border border-2 border-secondary p-2 position-relative flex-fill" style="border-style: dashed !important; background-image: url('+$('#form-fotos .location').val()+'/'+value+'); background-position: center center; background-repeat: no-repeat; background-size: contain"><div class="portlet position-absolute w-100 h-100 top-0 start-0"></div><button class="btn btn-danger btn-sm text-white position-absolute top-0 end-0 portlet-btn" onClick="delFoto('+count+'); return false"><i class="fas fa-trash fa-fw"></i></button></div></div></div></div>';
						count++;						
					});
					cuenta = count - 1;
				}
				if(cuenta >= 6){
					$('#mod-fotos .sorteable_cont .no-sorteable').hide();
				}else{
					$('#mod-fotos .sorteable_cont .no-sorteable').show();
				}
				$('#mod-fotos .sorteable_cont').prepend(ncont).ready(function(){
					$('#mod-fotos .sorteable_cont').sortable( "refresh" );
				});
			}
			function setFoto(){
				var nfoto = $('#form-fotosU .foto').val();
				var arreglo = [];
				if($('#form-fotos .foto').val() != ''){
					arreglo = $('#form-fotos .foto').val().split(',');
				}
				arreglo.push(nfoto);
				$('#form-fotos .foto').val(arreglo.join(','));
				setFotos();
			}
			function delFoto(iden){
				$('#mod-eliminar .eliminar').val(0);
				$('#mod-eliminar').one('hidden.bs.modal', function(){
					if($('#mod-eliminar .eliminar').val() == 1){
						delFotoIn(iden);
					}
				});
				$('#mod-eliminar').modal('show');
			}
			function delFotoIn(numer){
				var fotos = $('#form-fotos .foto').val();
				var narreglo = [];
				var arreglo = fotos.split(',');
				$.each(arreglo, function( index, value ) {
					if(value != $('#mod-fotos .sorteable_cont .foto-'+numer).data('valor')){
                        narreglo.push(value);
                    }
				});
				if(narreglo.length > 0){
					$('#form-fotos .foto').val(narreglo.join(','));
				}else{
					$('#form-fotos .foto').val('');
				}				
				setFotos();				
			}
			function valFoto(){
				if($('#form-fotosU').parsley().validate()){
					$('#mod-fotosU').one('hide.bs.modal', function(){
						setFoto();
					});
					$('#mod-fotosU').modal('hide');
				}				
			}
			function valFotos(){
				if($('#form-fotos .foto').val() != ''){
					var narreglo = [];					
					$('#mod-fotos .sorteable_cont div.col-6:not(.no-sorteable)').each(function(){
						narreglo.push($(this).data('valor'));
					});
					$('#form-fotos .foto').val(narreglo.join(','));
				}
				Valform('form-fotos', reLoadTable, ['propiedades', false], true);
			}
			function chkPosition(coordenadas){
				if(coordenadas){
					var local = coordenadas.split(',');
					var positiont = {
						lat: parseFloat(local[0]),
						lng: parseFloat(local[1])
					};
					if(marcador){
                        marcador.setMap(null);
                        marcador = false;
                    }
                    marcador = new google.maps.Marker({
                        clickable: true,
                        map: map,
                        title: $('#form-ubicacion .nombre').val(),
                        zIndex: 100,								
                        position: positiont,
						draggable: true
                    });
                    map.setCenter(positiont);
					google.maps.event.addListener(marcador, 'dragend',
                      function(marker) {						
						$('#form-ubicacion .ubicacion').val(marker.latLng.lat()+','+marker.latLng.lng());
                      }
                    );
				}else{
					if($('#form-ubicacion .direccion').val() != '' && $('#form-ubicacion .id_pais').val() != '0' && $('#form-ubicacion .id_states').val() != '0' && $('#form-ubicacion .id_city').val() != '0' && $('#mod-ubicacion').hasClass('show')){
						var address = $('#form-ubicacion .direccion').val()+', '+$('#form-ubicacion .id_city > option:selected').text()+', '+$('#form-ubicacion .id_pais > option:selected').text();
						geocoder.geocode({'address': address}, function(results, status) {
							if (status === 'OK') {							
								$('#form-ubicacion .ubicacion').val(results[0].geometry.location.lat()+','+results[0].geometry.location.lng());
								$('#form-ubicacion .ubicacion').parsley().validate();
								var positiont = {
										lat: parseFloat(results[0].geometry.location.lat()),
										lng: parseFloat(results[0].geometry.location.lng())
									};
								if(marcador){
									marcador.setMap(null);
									marcador = false;
								}
								marcador = new google.maps.Marker({
									clickable: true,
									map: map,
									title: $('#form-ubicacion .nombre').val(),
									zIndex: 100,								
									position: positiont,
									draggable: true
								});
								map.setCenter(positiont);
								google.maps.event.addListener(marcador, 'dragend',
								  function(marker) {
									$('#form-ubicacion .ubicacion').val(marker.latLng.lat()+','+marker.latLng.lng());
								  }
								);
							} else {
								showError('Debes verificar tu dirección: '+status);							
							}
						});

					}else{
						if(marcador){
							marcador.setMap(null);
							marcador = false;
						}					
					}
				}
			}
            function saveEvt(){
                $('#form-eventos .urls').val(remove_accents($('#form-eventos .nombre').val()));
                Valform('form-eventos', reLoadTable, ['eventos', false], true);
            }
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>