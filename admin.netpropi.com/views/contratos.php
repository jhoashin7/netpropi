<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Contratos</span>
                        </span>						
					</h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('contratos.contratos', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>
                        <button type="button" class="btn btn-warning text-white" onClick="openData('contratos'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
                    </div>                                        
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="contratos" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 3, &quot;asc&quot; ]]">
					<thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Prefijo</th>
							<th class="select-filter" data-filtro='{"tb":"contratos","fl":"id_abogado","opt":"join","tbj":"admins","flr":"id","fln":"nombre","flnd":"Suspendido", "where":"`sub`.`rol` = 7"}'>Abogado</th>
                            <th class="money_fr">Valor(<?php echo $monob->moneda ?>)</th>
							<th class="money_fr">Valor abogado(<?php echo $monob->moneda ?>)</th>
                            <th class="number">Paises</th>							
                            <th class="number">Pagados</th>							
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-contratos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-file-contract fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Contrato</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-contratos">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="contratos" />							
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Prefijo</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice fa-fw"></i></span>
                                    <input type="text" name="prefijo" class="form-control prefijo" placeholder="Prefijo" aria-label="Prefijo" data-parsley-type="alphanum" data-parsley-maxlength="5" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Abogado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user-tie fa-fw"></i></span>
                                    <select name="id_abogado" class="form-select id_abogado" aria-label="Abogado" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id, nombre FROM admins WHERE rol = 7 ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Paises donde aplica</small></label>
                                <div class="input-group">
                                    <select class="paises form-select" multiple="multiple" size="10" name="paises[]" required>
                                        <option value="0">Todos</option>
                                        <?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>							
                            <div class="form-group pb-3">
                                <label><small>Valor total(USD)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" data-parsley-gt="#form-contratos .valor_abg" required>
                                </div>
                            </div>
                            <?php
                            if($monob->moneda != 'USD'){
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Valor total(<?php echo $monob->moneda ?>)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" class="form-control cambio" placeholder="Valor(<?php echo $monob->moneda ?>)" aria-label="Valor" readonly>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
							<div class="form-group pb-3">
                                <label><small>Valor abogado(USD)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor_abg" class="form-control valor_abg" placeholder="Valor abogado" aria-label="Valor abogado" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" required>
                                </div>
                            </div>
                            <?php
                            if($monob->moneda != 'USD'){
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Valor abogado(<?php echo $monob->moneda ?>)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" class="form-control cambio_abg" placeholder="Valor abogado(<?php echo $monob->moneda ?>)" aria-label="Valor abogado" readonly>
                                </div>
                            </div>
                            <?php
                            }
                            ?>                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-contratos', reLoadTable, ['contratos', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
				inputMultiple($('#form-contratos .paises'), {});
                tablasD('contratos',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'contratos', true);
                <?php if($monob->moneda != 'USD'){ ?>
                $('#form-contratos .valor').on('input', function(){					
					setCambio('form-contratos', <?php echo $monob->valor ?>);
				});
				$('#form-contratos .valor_abg').on('input', function(){					
					setCambio('form-contratos', <?php echo $monob->valor ?>, '.valor_abg', '.cambio_abg');
				});
                <?php } ?>
				$('#mod-contratos').on('show.bs.modal', function(){					
					<?php if($monob->moneda != 'USD'){ ?>
					setCambio('form-contratos', <?php echo $monob->valor ?>);
					setCambio('form-contratos', <?php echo $monob->valor ?>, '.valor_abg', '.cambio_abg');
					<?php } ?>
				});
                respClass();							
				loaderHide();
            });
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>