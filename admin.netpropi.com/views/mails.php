<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-envelope-open-text fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">plantillas de correo</span>
                        </span>						
					</h6>			
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="mails" class="table table-striped table-bordered table-sm data-table align-middle w-100">
					<thead>
                        <tr>
                            <th>Nombre</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>						
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
		<div class="modal fade" id="mod-mails" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-envelope-open-text fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Plantilla</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-mails">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="mails" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Subject</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="subject" class="form-control subject" placeholder="Subject" aria-label="Subject" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Contenido</small></label>
								<textarea name="texto" class="summer texto form-control" placeholder="Contenido" required></textarea>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Texto Plano</small></label>
                                <textarea name="plano" class="plano form-control" rows="4" placeholder="Texto Plano" required></textarea>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Texto SMS <span class="text-muted">(máximo 120 caracteres)</span></small></label>
                                <textarea name="sms" class="sms form-control" rows="4" placeholder="Texto SMS" data-parsley-maxlength="120" ></textarea>
                            </div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-mails', reLoadTable, ['mails', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
        <script>			
            $(function() {							
                tablasD('mails',{'user': login, 'rol': rol},'mails', true);
                respClass();							
				loaderHide();
            });			
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>