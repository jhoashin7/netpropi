<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$filas = 0;
$consulta = "SELECT id FROM sms ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $sms = mysqli_fetch_object( $result );
    $filas = $sms->id;
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-sms fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Configuración SMS</span>
                        </span>						
					</h6>
                    <?php
                    if($filas == 0){
                    ?>
                    <button type="button" class="btn btn-warning btn-sm text-white" onClick="openData('sms'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
                    <?php
                    }
                    ?>                    
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="sms" class="table table-striped table-bordered table-sm data-table align-middle w-100">
					<thead>
                        <tr>
                            <th>Código</th>
							<th>EndPoint</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-sms" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-sms fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar SMS</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-sms">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="sms" />
                            <div class="form-group pb-3">
                                <label><small>API <span class="text-muted">(La cabecera de autorización se construye combinando la cadena <strong>usuario:password</strong> y codificándola en base64.)</span></small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="codigo" class="form-control codigo" placeholder="API" aria-label="API" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>EndPoint</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-code fa-fw"></i></span>
                                    <input type="url" name="endpoint" class="form-control endpoint" placeholder="EndPoint" aria-label="EndPoint" data-parsley-type="url" required>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-sms', reloader, [], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                tablasD('sms',{'user': login, 'rol': rol},'sms', true);
                respClass();							
				loaderHide();
            });            
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>