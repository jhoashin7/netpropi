<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-comments fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Novedades</span>
                        </span>						
					</h6>                                  
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="novedades" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 9, &quot;desc&quot; ]]">
					<thead>
                        <tr>
                            <th>Nombre</th>
							<th class="select-filter" data-filtro='{"tb":"novedades","fl":"id_user","opt":"join","tbj":"admins","flr":"id","fln":"nombre","flnd":"Suspendido", "where":"`main`.`tipo` = 1"}'>Solicitante</th>
							<th class="select-filter" data-filtro='{"tb":"novedades","fl":"id_responsable","opt":"join","tbj":"admins","flr":"id","fln":"nombre","flnd":"Suspendido", "where":"`main`.`tipo` = 1"}'>Responsable</th>						
							<th>Código propiedad</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Abierta"], [1,"Cerrada"]],"tbj":"","flr":"","fln":"","flnd":""}'>Estado</th>
							<th>Comentarios pendientes</th>
							<th>Valoración</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Fin</th>							
							<th class="hdvis">Editor</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-novedades" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comments fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Novedades</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedades">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />							
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>
							<div class="form-group pb-3 cont_pago">
                                <label><small>Estado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-clipboard-check fa-fw"></i></span>
                                    <select name="estado" class="form-select estado" aria-label="Estado" required>
                                        <option value="">Seleccionar</option>
                                        <option value="0">Abierta</option>
										<option value="1">Cerrada</option>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Comentario</small></label>
                                <textarea name="comentario" class="comentario form-control" rows="4" placeholder="Comentario" readonly></textarea>
                            </div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>							
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-novedades', reLoadTable, ['novedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="mod-novedadeslog" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comment-dots fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Registro Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedadeslog">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<input type="hidden" class="nombre" name="nombre" value="" />
							<input type="hidden" class="estado" name="estado" value="" />
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="id_responsable" name="id_responsable" value="" />
							<input type="hidden" class="tipo" name="tipo" value="" />
							<input type="hidden" class="id_propiedad" name="id_propiedad" value="" />
                        </form>
						<div class="d-block w-100 cont_logs"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-primary text-white" onClick="addComment(); return false"><span class="text-responsive">responder <i class="fas fa-comment-dots fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="mod-novedadescom" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comment-dots fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Responder Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedadescom">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="id_responsable" name="id_responsable" value="" />
							<input type="hidden" class="tipo" name="tipo" value="" />
							<input type="hidden" class="id_propiedad" name="id_propiedad" value="" />
							<input type="hidden" class="referente" name="referente" value="" />
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Comentario</small></label>
                                <textarea name="texto" class="texto form-control" rows="4" placeholder="Comentario" required></textarea>
                            </div>
							<div class="form-group file_up pb-3">
                                <label><small>Adjunto</small></label>
                                <input type="hidden" class="adjunto" name="adjunto" data-funcion="inputvars_Base" data-funcionvar="inputvars_adjunto" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/novedades" data-imgcont="<?php echo $conArr['updestino'] ?>novedades" value="" data-parsley-error-message="Debes subir un documento en PDF o imagen." />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_adjunto" accept=".pdf,image/*" />
                                </div>                                
                            </div>                            
                            <div class="error_upload_file" class="d-none"></div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-success text-white" onClick="Valform('form-novedadescom', reLoadTable, ['novedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
			var inputvars_adjunto = {
                maxFileSize: 4000,
                maxFileCount: 1,
                dropZoneTitle: 'Arrastra y suelta aquí tu adjunto (imágenes o PDF).',
                fileActionSettings: {
                    showZoom: true
                }
            };
            $(function() {
				inputvars_Base($('#form-novedadescom .adjunto'));
                tablasD('novedades',{'user': login, 'rol': rol},'novedades_srv', true);
                respClass();							
				loaderHide();
            });
			function showLogs(iden){
				$('#mod-novedadeslog').one('show.bs.modal', function(){
					var datos = {
						'id': iden,
						'tipo': 1,
						'accion': 15,
						'idveruser': login
					};                              
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#mod-novedadeslog .cont_logs').html('');
						},
						success:  function (response) {					
							if(response.resp){
								var contenido = '';	
								contenido += '<div class="row mx-0 w-100 justify-content-center align-items-center border border-secondary rounded-3 bg-light"><div class="col-12 text-center py-2"><div class="d-flex w-100 justify-content-start align-items-center"><div class="flex-fill text-start"><small class="fw-bold">'+response.data.main.nombre+'</small></div><div class="ps-2 text-end text-nowrap cont_visto_'+response.data.main.id+'">';
								if(response.data.main.vista == 0 && response.data.main.editable){
									contenido += '<small class="d-inline-block pe-1">revisado</small><div class="form-group d-inline-block align-middle"><div class="form-check form-switch swt-sm d-flex justify-content-start align-items-center"><input class="form-check-input mt-0 chk-warning visto_'+response.data.main.id+'" type="checkbox" onChange="setvisto('+response.data.main.id+');" ></div></div>';
								}else{
									contenido += (response.data.main.vista == 0)?'<small class="d-inline-block text-danger">pendiente</small>':'<small class="d-inline-block text-success">revisado</small>';
								}
								var icono = (response.data.main.tipo == 1)?'toolbox':'user-tie';								
								contenido += '</div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-user"></i> <small class="fw-bold">'+response.data.main.user+'</small></div><div class="text-end ps-2"><i class="fas fa-calendar"></i> <small class="fw-bold">'+response.data.main.creado+'</small></div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-'+icono+'"></i> <small class="fw-bold">'+response.data.main.responsable+'</small></div>';
								contenido += (parseInt(response.data.main.propiedad) != 0)?'<div class="text-end ps-2"><i class="fas fa-building"></i> <small class="fw-bold">'+response.data.main.propiedad+'</small></div>':'';
								contenido += '</div></div>';
								contenido += (response.data.main.adjunto != '')?'<div class="col-12 text-start pb-2"><a href="'+response.data.main.adjunto+'" class="btn btn-sm btn-secondary text-white" target="blank"><i class="fas fa-paperclip"></i> adjunto</a></div>':'';
								contenido += '<div class="col-12 text-start pb-2"><small>'+response.data.main.texto+'</small></div></div><hr>';								
								$.each(response.data.logs, function(key, value){
									contenido += '<div class="row mx-0 w-100 justify-content-center align-items-center border border-light rounded-3"><div class="col-12 text-center py-2"><div class="d-flex w-100 justify-content-start align-items-center"><div class="flex-fill text-start"><small class="fw-bold">'+value.nombre+'</small></div><div class="ps-2 text-end text-nowrap cont_visto_'+value.id+'">';
									if(value.vista == 0 && value.editable){
										contenido += '<small class="d-inline-block pe-1">revisado</small><div class="form-group d-inline-block align-middle"><div class="form-check form-switch swt-sm d-flex justify-content-start align-items-center"><input class="form-check-input mt-0 chk-warning visto_'+value.id+'" type="checkbox" onChange="setvisto('+value.id+');" ></div></div>';
									}else{
										contenido += (value.vista == 0)?'<small class="d-inline-block text-danger">pendiente</small>':'<small class="d-inline-block text-success">revisado</small>';
									}																
									contenido += '</div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-comment-dots"></i> <small class="fw-bold">'+value.editor+'</small></div><div class="text-end ps-2"><i class="fas fa-calendar"></i> <small class="fw-bold">'+value.creado+'</small></div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-start align-items-center">';
									contenido += (value.adjunto != '')?'<div class="text-start"><a href="'+value.adjunto+'" class="btn btn-sm btn-secondary text-white" target="blank"><i class="fas fa-paperclip"></i> adjunto</a></div>':'';
									contenido += '</div></div><div class="col-12 text-start pb-2"><small>'+value.texto+'</small></div></div><hr>';							
								});
								$('#mod-novedadeslog .cont_logs').html(contenido);						
							}else{
								$('#mod-novedadeslog .cont_logs').html('');
							}					
						}
					});
				});
				openData('novedadeslog', iden);
			}
			function setvisto(iden){
				if($('#mod-novedadeslog .visto_'+iden).is(':checked')){
					loaderShow();
					var datos = {
						'id': iden,						
						'accion': 16,
						'editor': login,
						'idveruser': login
					};                              
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#loader').one("hide", function() { 
								showError(error);
							});
							loaderHide();
						},
						success:  function (response) {					
							if(response.resp){
								$('#mod-novedadeslog .cont_visto_'+iden).html('<small class="d-inline-block text-success">revisado</small>');
								reLoadTable('novedades', false);
								loaderHide();				
							}else{
								$('#loader').one("hide", function() { 
									showError(response.data.msg);
								});
								loaderHide();
							}					
						}
					});
				}
			}
			function addComment(){	
				if($('#form-novedadeslog .estado').val() == 1){
					showError('La novedad ya ha sido cerrada por parte del usuario.');
				}else{
					var iden = $('#form-novedadeslog .id').val();				
					$('#mod-novedadeslog').one('hidden.bs.modal', function(){
						$('#mod-novedadescom').one('show.bs.modal', function(){
							$('#form-novedadescom .nombre').val('RS / '+$('#form-novedadeslog .nombre').val());
							$('#form-novedadescom .id_user').val($('#form-novedadeslog .id_user').val());
							$('#form-novedadescom .id_responsable').val($('#form-novedadeslog .id_responsable').val());
							$('#form-novedadescom .referente').val(iden);
							$('#form-novedadescom .tipo').val($('#form-novedadeslog .tipo').val());
							$('#form-novedadescom .id_propiedad').val($('#form-novedadeslog .id_propiedad').val());
						});
						$('#mod-novedadescom').one('hidden.bs.modal', function(){
							showLogs(iden);
						});
						openData('novedadescom');
					});
					$('#mod-novedadeslog').modal('hide');
				}								
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>