<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-blog fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Blog</span>
                        </span>						
					</h6>                    
                    <button type="button" class="btn btn-warning btn-sm text-white" onClick="openData('blog'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>                                       
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="blog" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 5, &quot;desc&quot; ]]">
					<thead>
                        <tr>
                            <th>Nombre</th>
							<th>Link</th>
							<th>Paises</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-blog" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-blog fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Blog</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-blog">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="blog" />
							<input type="hidden" class="url" name="url" value="" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
							<div class="form-group file_up pb-3">
                                <label><small>Imagen</small></label>
                                <input type="hidden" class="imagen" name="imagen" data-funcion="inputvars_Base" data-funcionvar="inputvars_imagen" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/blog" data-imgcont="<?php echo $conArr['updestino'] ?>blog" value="" data-parsley-error-message="Debes subir una imagen para el blog." required />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_imagen" accept="image/*" />
                                </div>                                
                            </div>
							<div class="form-group pb-3">
                                <label><small>Texto introductorio</small></label>
								<textarea name="texto_corto" class="summer texto_corto form-control" placeholder="Texto introductorio" required></textarea>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Texto principal</small></label>
								<textarea name="texto" class="summer texto form-control" placeholder="Texto principal" required></textarea>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Paises donde aplica</small></label>
                                <div class="input-group">
                                    <select class="paises form-select" multiple="multiple" size="10" name="paises[]" required>
										<option value="0">Todos</option>
                                        <?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>							                            
                            <div class="error_upload_file" class="d-none"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="saveBlog(); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
			var inputvars_imagen = {
                maxFileSize: 4000,				
                maxFileCount: 1,
                dropZoneTitle: 'Arrastra y suelta aquí tu foto para el blog.',
                initialPreviewFileType: 'image',
				allowedFileTypes: ['image'],
                fileActionSettings: {
                    showZoom: true,					
					showRemove: false
                }				
            };
            $(function() {
				inputMultiple($('#form-blog .paises'), {});
				inputvars_Base($('#form-blog .imagen'));
                tablasD('blog',{'user': login, 'rol': rol},'blog', true);
                respClass();							
				loaderHide();
            });
			function saveBlog(){
				$('#form-blog .url').val(remove_accents($('#form-blog .nombre').val()));
				Valform('form-blog', reLoadTable, ['blog', false], true);                
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>