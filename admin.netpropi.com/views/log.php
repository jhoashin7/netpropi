<?php
header('X-Frame-Options: DENY');
require_once( '../controllers/con_set.php' );
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div id="paralal" class="position-fixed paral vw-100 vh-100 top-0 start-0"></div>
        <div class="row mx-0 w-100 position-relative justify-content-center align-items-center">
            <div class="col-11 col-sm-8 col-md-6 col-lg-4 col-xl-4 col-xxl-3 text-center">
                <div class="w-100 rounded-3 shadow border" style="background-color: rgba(255,255,255,0.7)">
                    <div class="text-center w-100 p-2 p-md-3 border-bottom">
                        <h5 class="m-0 text-muted text-uppercase fw-bold"><span class="text-responsive"><i class="fas icon-lock2 fa-lg fa-fw"></i> ingreso</span></h5>
                    </div>
                    <div class="p-3 w-100">
                        <form id="form-log">
                            <div class="form-group pb-4">
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas icon-mail fa-fw"></i></span>
                                    <input type="text" name="usuario" class="form-control usuario" placeholder="Email" aria-label="Email" required>
                                </div>
                            </div>
                            <div class="form-group pb-4">
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-fingerprint fa-fw"></i></span>
                                    <input type="password" autocomplete="new-password" name="contrasena" class="form-control contrasena" placeholder="Contraseña" aria-label="Contraseña" data-parsley-minlength="6" data-parsley-type="alphanum" data-parsley-pattern="^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$" data-parsley-pattern-message="La contraseña debe contener dígitos y letras" required>
                                </div>
                            </div>
                            <div class="form-group pb-4">
                                <div class="form-check form-switch swt-md d-flex justify-content-start align-items-center">
                                    <input class="form-check-input mt-0 chk-warning" type="checkbox" id="recordar">
                                    <label class="form-check-label text-wrap ps-2" for="recordar"><span class="text-responsive">Recordarme en este dispositivo</span></label>
                                </div>
                            </div>
                            <div class="d-block pb-4">
                                <div id="recG" class="recap d-flex justify-content-center align-items-center"></div>
                                <div class="recerror d-block"></div>
                            </div>
                            <div class="d-grid">
                                <button class="btn btn-success text-uppercase fw-bold text-white" onClick="Vallog(); return false;"><span class="text-responsive">ingresar <i class="fas icon-lock_open fa-lg fa-fw"></i></span></button>
                            </div>                            
                        </form>
                    </div>
                </div>
                <button class="btn btn-warning text-white text-uppercase shadow" data-bs-toggle="modal" data-bs-target="#mod-recdata" style="border-top-right-radius: 0rem !important; border-top-left-radius: 0rem !important;"><span class="text-responsive">recuperar acceso <i class="fas fa-lightbulb fa-lg fa-fw"></i></span></button>
            </div>
        </div>
        <div class="modal fade" id="mod-recdata" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title fw-bold"><span class="text-responsive"><i class="fas fa-lightbulb fa-lg fa-fw"></i> Recuperar Acceso</span></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p><span class="text-responsive text-muted">Te enviaremos un E-mail con tu información de acceso.</span></p>
                        <form id="form-recdata">
                            <div class="form-group pb-4">
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-envelope fa-fw"></i></span>
                                    <input type="email" name="mail" class="form-control mail" placeholder="E-mail" aria-label="E-mail" data-parsley-type="email" required>
                                </div>
                            </div>
                            <div class="d-block pb-4">
                                <div id="recGc" class="recap d-flex justify-content-center align-items-center"></div>
                                <div class="recerror d-block text-center"></div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valrecdata(); return false"><span class="text-responsive">recuperar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {                
                widGo = grecaptcha.render('recG', {
                    'sitekey' : '<?php echo $conArr['google_public'] ?>',
                    'callback': function(response){
                        setWid('form-log', true);
                    },
                    'expired-callback': function(response){
                        setWid('form-log', true);
                    },
                    'error-callback': function(response){
                        setWid('form-log', true);
                    }                    
                });                
                setWid('form-log');
                widGo2 = grecaptcha.render('recGc', {
                    'sitekey' : '<?php echo $conArr['google_public'] ?>',
                    'callback': function(response){
                        setWid('form-recdata', true);
                    },
                    'expired-callback': function(response){
                        setWid('form-recdata', true);
                    },
                    'error-callback': function(response){
                        setWid('form-recdata', true);
                    }                    
                });
                $('#mod-recdata').on('show.bs.modal', function(){
                    $('#form-recdata input').val('');
                    $('#form-recdata').parsley().reset();
                    grecaptcha.reset(widGo2);
                    setWid('form-recdata');
                });
                loadIntro(<?php echo mt_rand(1, 2) ?>);
                respClass();							
				loaderHide();
            });
        </script>
    </div>
</body>
</html>