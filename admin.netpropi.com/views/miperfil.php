<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$rela = false;
$consulta = "SELECT relacion FROM admins WHERE id = ".$id_veri[1];
if ( $result = $mysqli->query( $consulta ) ) {
    $res = mysqli_fetch_object( $result );
	$rela = $res->relacion;
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-id-badge fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Mi Perfil Profesional</span>
                        </span>						
					</h6>					
                </div>
                <hr>
            </div>
            <div class="col-12 px-0">				
				<form id="form-aliados">					
					<input type="hidden" class="id" name="id" value=0 />							
					<input type="hidden" class="db noclear" name="db" value="aliados" />
					<input type="hidden" class="redes" name="redes" value="" />
					<div class="error_upload_file" class="d-none"></div>
					<div class="row mx-0 w-100 justify-content-center">
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-id-badge fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Email</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-envelope-open-text fa-fw"></i></span>
                                    <input type="email" name="email" class="form-control email" placeholder="Email" aria-label="Email" data-parsley-type="email" required>
                                </div>
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Teléfono</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-mobile-alt fa-fw"></i></span>
                                    <input type="text" name="celular" class="form-control celular" placeholder="Teléfono" aria-label="Teléfono">
                                </div>
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>País</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked-alt fa-fw"></i></span>
                                    <select name="pais" class="form-select pais" aria-label="País" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
						</div>
						<div class="col-12 text-start pb-2">
							<hr>
							<div class="d-flex w-100 justify-content-between align-items-center">
								<h6 class="m-0 text-muted pr-2">
									<span class="text-responsive d-flex justify-content-start align-items-center">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x"></i>
											<i class="fas fa-<?php echo ($prol == 6)?'toolbox':'book' ?> fa-stack-1x text-white"></i>
										</span>
										<span class="fw-bold">Especialidades</span>
									</span>						
								</h6>					
							</div>
						</div>
						<div class="col-12 text-center">							
							<div class="form-group pb-3">                                
                                <div class="input-group">
                                    <select class="especialidad form-select" name="especialidad[]" multiple="multiple" size="5" style="height: 150px;" required>
                                        <?php
										$consulta = "SELECT id, nombre FROM especialidad_srv ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
							<hr>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group file_up pb-3">
                                <label><small>Perfil Coorporativo</small></label>
                                <input type="hidden" class="logo" name="logo" data-funcion="inputvars_Base" data-funcionvar="inputvars_logo" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/aliados" data-imgcont="<?php echo $conArr['updestino'] ?>aliados" value="" data-parsley-error-message="Debes subir una imagen de perfil." />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_logo" accept="image/*" />
                                </div>                                
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group file_up pb-3">
                                <label><small><?php echo ($prol == 6)?'Brochure':'Hoja de vida' ?></small></label>
                                <input type="hidden" class="adjunto" name="adjunto" data-funcion="inputvars_Base" data-funcionvar="inputvars_adjunto" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/aliados" data-imgcont="<?php echo $conArr['updestino'] ?>aliados" value="" data-parsley-error-message="Debes subir un documento en PDF." />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_adjunto" accept=".pdf" />
                                </div>                                
                            </div>
						</div>
						<div class="col-12 text-start pb-2">
							<hr>
							<div class="d-flex w-100 justify-content-between align-items-center">
								<h6 class="m-0 text-muted pr-2">
									<span class="text-responsive d-flex justify-content-start align-items-center">
										<span class="fa-stack align-top">
											<i class="fas fa-circle fa-stack-2x"></i>
											<i class="fas fa-share-alt fa-stack-1x text-white"></i>
										</span>
										<span class="fw-bold">Redes Sociales</span>
									</span>						
								</h6>					
							</div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Web</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas icon-network fa-fw"></i></span>
                                    <input type="url" class="form-control redes_url web" data-rednombre="web" placeholder="Web" aria-label="Web" data-parsley-type="url">
                                </div>
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Facebook</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fab fa-facebook fa-fw"></i></span>
                                    <input type="url" class="form-control redes_url facebook" data-rednombre="facebook" placeholder="facebook" aria-label="facebook" data-parsley-type="url">
                                </div>
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Instagram</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fab fa-instagram fa-fw"></i></span>
                                    <input type="url" class="form-control redes_url instagram" data-rednombre="instagram" placeholder="Instagram" aria-label="Instagram" data-parsley-type="url">
                                </div>
                            </div>
						</div>
						<div class="col-12 col-sm-6 text-start">
							<div class="form-group pb-3">
                                <label><small>Linkedin</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fab fa-linkedin fa-fw"></i></span>
                                    <input type="url" class="form-control redes_url linkedin" data-rednombre="linkedin" placeholder="Linkedin" aria-label="Linkedin" data-parsley-type="url">
                                </div>
                            </div>
						</div>
						<div class="col-12 pb-4 pt-2 text-center">
							<button type="button" class="btn btn-success btn-lg text-white" onClick="ValSavePerfil(); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
						</div>						
					</div>
				</form>
            </div>           
        </div>
		<div class="modal fade" id="mod-filtro" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-filter fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Filtro Especialidades</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-filtro">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="especialidad_srv" />
                            <div class="form-group pb-3">
                                <label><small>Especialidades</small></label>
                                <div class="input-group">
                                    <select class="especialidades noclear form-select" required>
										<option value="">Seleccionar</option>
										<option value="0" <?php echo (!$espe)?'selected':'' ?>>Ninguna</option>
                                        <?php
										$consulta = "SELECT id, nombre FROM especialidad_srv ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												$selected = ($espe == $row['id'])?'selected':'';
												echo '<option value='.$row['id'].' '.$selected.'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-primary text-white" onClick="ValfiltroSrv(); return false"><span class="text-responsive">filtrar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
       
		<div class="modal fade" id="mod-perfilald" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-user-edit fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Ver Perfil</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-perfilald">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="aliados" />
							<input type="hidden" class="redes" name="redes" value="" />
							<div class="form-group file_up pb-3">
                                <label><small>Perfil Coorporativo</small></label>
                                <input type="hidden" class="logo" name="logo" data-funcion="inputvars_Base" data-funcionvar="inputvars_logo" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/aliados" data-imgcont="<?php echo $conArr['updestino'] ?>aliados" value="" data-parsley-error-message="Debes subir una imagen de perfil." required />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_logo" accept="image/*" />
                                </div>                                
                            </div>
							<div class="form-group pb-3">
                                <label><small>Web</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas icon-network fa-fw"></i></span>
                                    <input type="text" class="form-control web" placeholder="Web" aria-label="Web" readonly>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Facebook</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fab fa-facebook fa-fw"></i></span>
                                    <input type="text" class="form-control facebook" placeholder="facebook" aria-label="facebook" readonly>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Instagram</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fab fa-instagram fa-fw"></i></span>
                                    <input type="text" class="form-control instagram" placeholder="Instagram" aria-label="Instagram" readonly>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Linkedin</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fab fa-linkedin fa-fw"></i></span>
                                    <input type="text" class="form-control linkedin" placeholder="Linkedin" aria-label="Linkedin" readonly>
                                </div>
                            </div>
                            <div class="form-group file_up pb-3">
                                <label><small>Brochure</small></label>
                                <input type="hidden" class="adjunto" name="adjunto" data-funcion="inputvars_Base" data-funcionvar="inputvars_adjunto" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/aliados" data-imgcont="<?php echo $conArr['updestino'] ?>aliados" value="" data-parsley-error-message="Debes subir un documento en PDF." required />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_adjunto" accept=".pdf" />
                                </div>                                
                            </div>                            
                            <div class="error_upload_file" class="d-none"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <script>
			var inputvars_adjunto = {
                maxFileSize: 4000,
                maxFileCount: 1,
                dropZoneTitle: 'Documento de brochure en PDF.',
				initialPreviewFileType: 'text',
                allowedFileExtensions: ['pdf'],				
                fileActionSettings: {
                    showZoom: true
                }
            };
			var inputvars_logo = {
                maxFileSize: 4000,				
                maxFileCount: 1,
                dropZoneTitle: 'Imagen de perfil coorporativo.',
                initialPreviewFileType: 'image',
				allowedFileTypes: ['image'],				
                fileActionSettings: {
                    showZoom: true
                }				
            };
            $(function() { 
				inputvars_Base($('#form-aliados .adjunto'));
				inputvars_Base($('#form-aliados .logo'));
				inputMultiple($('#form-aliados .especialidad'), {});
				$('#loader').one("hide", function() { 
                    if($('#form-aliados .redes').val() != ''){
						$.each( jQuery.parseJSON($('#form-aliados .redes').val()), function( key, value ) {
							$('#form-aliados .'+value.id).val(value.dir);
						});
					}
                });
				openData('aliados',<?php echo $rela ?>,false,false);
                respClass();
            });
			function ValSavePerfil(){
				if($('#form-aliados').parsley().validate()){
					var objetored = [];
					$('#form-aliados .redes_url').each(function(index){
						if($(this).val() != ''){
							objetored.push({
								'id': $(this).data('rednombre'),
								'dir': $(this).val()
							});
						}
					});
					$('#form-aliados .redes').val(JSON.stringify(objetored));
					Valform('form-aliados', reloader, [], true, false);
				}
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>