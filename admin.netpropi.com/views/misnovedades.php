<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$puede = true;
if($prol == 5){	
	$puede = false;
	$consulta = "SELECT id FROM plan WHERE fin >= '".date('Y-m-d')."' AND id_user = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$puede = ($rows > 0)?true:false;
		$result->close();
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-comments fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Novedades</span>
                        </span>						
					</h6>
					<?php
					if($prol == 5){
					?>
					<button type="button" class="btn btn-warning btn-sm text-white" onClick="openData('newnovedad'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
					<?php
					}
					?>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="novedades" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 8, &quot;desc&quot; ]]">
					<thead>
                        <tr>
                            <th>Nombre</th>
							<th class="<?php echo ($prol == 5)?'hideall2':'' ?>">Solicitante</th>
							<th class="<?php echo ($prol != 5)?'hideall2':'' ?>">Responsable</th>						
							<th class="<?php echo ($prol == 7)?'hideall2':'' ?>">Código propiedad</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[0,"Abierta"], [1,"Cerrada"]],"tbj":"","flr":"","fln":"","flnd":""}'>Estado</th>
							<th>Comentarios pendientes</th>
							<th>Valoración</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Fin</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
		<div class="modal fade" id="mod-newnovedad" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comments fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Crear Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-newnovedad">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<small class="text-muted d-block pb-3">Define el país y el tipo de ayuda que necesitas.</small>
                            <div class="form-group pb-3">
                                <label><small>País</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-map-marked-alt fa-fw"></i></span>
                                    <select name="pais" class="form-select pais" aria-label="País" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id, name FROM countries WHERE activo = 1 ORDER BY name ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['name']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-warning text-white" onClick="setNovTipo(1); return false"><span class="text-responsive"><i class="fas fa-toolbox fa-fw"></i> aliado</span></button>
							<button type="button" class="btn btn-primary text-white" onClick="setNovTipo(2); return false"><span class="text-responsive">legal <i class="fas fa-user-tie fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-newnovedadali" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comments fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Crear Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-newnovedadali">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<input type="hidden" class="tipo noclear" name="tipo" value="1" />
							<input type="hidden" class="pais noclear" name="pais" value="" />
							<input type="hidden" class="id_user noclear" name="id_user" value="<?php echo $id_veri[1] ?>" />
							<input type="hidden" class="id_responsable" name="id_responsable" value="" />
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Requerimiento</small></label>
								<div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-toolbox fa-fw"></i></span>
                                    <select class="form-select especialidad" aria-label="Requerimiento" onChange="showAli('form-newnovedadali')" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id, nombre FROM especialidad_srv ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>                                
                            </div>
							<div class="form-group pb-3 cont_usuario">
                                <label><small>Aliado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-toolbox fa-fw"></i></span>
                                    <input type="text" class="form-control aliado" placeholder="Aliado" aria-label="Aliado" readonly required>
									<button class="btn btn-warning" type="button" onClick="showAliados('form-newnovedadali')"><i class="fas fa-users fa-fw text-white"></i></button>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Propiedad</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <select name="id_propiedad" class="form-select id_propiedad" aria-label="Propiedad" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id, IF(nombre = '','Pendiente',nombre) AS nombreprop, LPAD(id,6,0) AS codigo FROM propiedades WHERE id_user = ".$id_veri[1]." ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombreprop']).' - '.$row['codigo'].'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Comentario</small></label>
                                <textarea name="texto" class="texto form-control" rows="4" placeholder="Comentario" required></textarea>
                            </div>
                            <div class="form-group file_up pb-3">
                                <label><small>Adjunto</small></label>
                                <input type="hidden" class="adjunto" name="adjunto" data-funcion="inputvars_Base" data-funcionvar="inputvars_adjunto" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/novedades" data-imgcont="<?php echo $conArr['updestino'] ?>novedades" value="" data-parsley-error-message="Debes subir un documento en PDF o imagen." />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_adjunto" accept=".pdf,image/*" />
                                </div>                                
                            </div>                            
                            <div class="error_upload_file" class="d-none"></div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-newnovedadali', reLoadTable, ['novedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-newnovedadlel" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comments fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Crear Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-newnovedadlel">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<input type="hidden" class="tipo noclear" name="tipo" value="2" />
							<input type="hidden" class="pais noclear" name="pais" value="" />
							<input type="hidden" class="id_propiedad noclear" name="id_propiedad" value="0" />
							<input type="hidden" class="id_user noclear" name="id_user" value="<?php echo $id_veri[1] ?>" />
							<input type="hidden" class="id_responsable" name="id_responsable" value="" />
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Requerimiento</small></label>
								<div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-toolbox fa-fw"></i></span>
                                    <select class="form-select especialidad" aria-label="Requerimiento" onChange="showAli('form-newnovedadlel')" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id, nombre FROM especialidad_abg ORDER BY nombre ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                    </select>
                                </div>                                
                            </div>
							<div class="form-group pb-3 cont_usuario">
                                <label><small>Abogado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user-tie fa-fw"></i></span>
                                    <input type="text" class="form-control aliado" placeholder="Aliado" aria-label="Aliado" readonly required>
									<button class="btn btn-warning" type="button" onClick="showAliados('form-newnovedadlel')"><i class="fas fa-users fa-fw text-white"></i></button>
                                </div>
                            </div>							
							<div class="form-group pb-3">
                                <label><small>Comentario</small></label>
                                <textarea name="texto" class="texto form-control" rows="4" placeholder="Comentario" required></textarea>
                            </div>
                            <div class="form-group file_up pb-3">
                                <label><small>Adjunto</small></label>
                                <input type="hidden" class="adjunto" name="adjunto" data-funcion="inputvars_Base" data-funcionvar="inputvars_adjunto" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/novedades" data-imgcont="<?php echo $conArr['updestino'] ?>novedades" value="" data-parsley-error-message="Debes subir un documento en PDF o imagen." />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_adjunto" accept=".pdf,image/*" />
                                </div>                                
                            </div>                            
                            <div class="error_upload_file" class="d-none"></div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-newnovedadlel', reLoadTable, ['novedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-listali" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-users fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Profesionales</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-listali">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />							
                        </form>
						<div class="d-block w-100 cont_logs"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-listrese" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comment-dots fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Reseña de Profesional</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-listrese">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="admins" />							
                        </form>
						<div class="d-block w-100 cont_logs">
							
							<div class="d-block w-100 p-2 my-2 border-bottom border-secondary bg-light">
								<div class="d-flex w-100 justify-content-between align-items-start pb-2">
									<div class="pe-1 text-start">
										<small><i class="fas fa-user fa-fw"></i>: <span class="fw-bold">usuario</span></small>
									</div>
									<div class="ps-1 text-end">
										<small><i class="fas fa-calendar fa-fw"></i>: <span class="fw-bold">fecha</span></small>
									</div>
								</div>
								<div class="d-block w-100 text-start pb-2">
									<small><i class="fas fa-star fa-fw"></i>: <span class="fw-bold">votacion</span></small>
								</div>
								<div class="d-block w-100 text-start"><p>comentario</p></div>
							</div>
							
						</div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>							
                        </div>
                    </div>
                </div>
            </div>
        </div>		
		
		<div class="modal fade" id="mod-novedadesFin" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-check-double fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Finalizar Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedadesFin">
                            <input type="hidden" class="id" name="id" value=0 />
							<input type="hidden" class="estado" name="estado" value="" />
							<input type="hidden" class="vista" name="vista" value="" />							
							<input type="hidden" class="valoracion" name="valoracion" value="" required />
							<input type="hidden" class="db noclear" name="db" value="novedades" />							
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>
							<small><strong>Valoración</strong> (valora el servicio recibido, 5 estrellas para un servicio excelente, 1 estrella para un servicio deficiente.)</small>
							<div class="d-flex w-100 px-2 py-2 justify-content-around align-items-center">
								<div class="px-1">
									<a href="#" class="text-decoration-none link-warning star star_1" onClick="setVoto(1); return false">
										<i class="far fa-star fa-fw fa-2x"></i>
									</a>
								</div>
								<div class="px-1">
									<a href="#" class="text-decoration-none link-warning star star_2" onClick="setVoto(2); return false">
										<i class="far fa-star fa-fw fa-2x"></i>
									</a>
								</div>
								<div class="px-1">
									<a href="#" class="text-decoration-none link-warning star star_3" onClick="setVoto(3); return false">
										<i class="far fa-star fa-fw fa-2x"></i>
									</a>
								</div>
								<div class="px-1">
									<a href="#" class="text-decoration-none link-warning star star_4" onClick="setVoto(4); return false">
										<i class="far fa-star fa-fw fa-2x"></i>
									</a>
								</div>
								<div class="px-1">
									<a href="#" class="text-decoration-none link-warning star star_5" onClick="setVoto(5); return false">
										<i class="far fa-star fa-fw fa-2x"></i>
									</a>
								</div>
							</div>
							<div class="d-block w-100 error_val"></div>
							<div class="form-group pb-3">
                                <label><small>Reseña</small></label>
                                <textarea name="comentario" class="comentario form-control" rows="4" placeholder="Reseña"></textarea>
                            </div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-novedadesFin', reLoadTable, ['novedades', false], true); return false"><span class="text-responsive">finalizar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="modal fade" id="mod-novedades" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comments fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Ver Comentario</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedades">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />							
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" readonly>
                                </div>
                            </div>							
							<div class="form-group pb-3">
                                <label><small>Reseña</small></label>
                                <textarea name="comentario" class="comentario form-control" rows="4" placeholder="Comentario" readonly></textarea>
                            </div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="mod-novedadeslog" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comment-dots fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Registro Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedadeslog">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<input type="hidden" class="nombre" name="nombre" value="" />
							<input type="hidden" class="estado" name="estado" value="" />
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="id_responsable" name="id_responsable" value="" />
							<input type="hidden" class="tipo" name="tipo" value="" />
							<input type="hidden" class="id_propiedad" name="id_propiedad" value="" />
                        </form>
						<div class="d-block w-100 cont_logs"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-primary text-white" onClick="addComment(); return false"><span class="text-responsive">responder <i class="fas fa-comment-dots fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="modal fade" id="mod-novedadescom" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-primary align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-comment-dots fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Responder Novedad</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-novedadescom">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="novedades" />
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="id_responsable" name="id_responsable" value="" />
							<input type="hidden" class="tipo" name="tipo" value="" />
							<input type="hidden" class="id_propiedad" name="id_propiedad" value="" />
							<input type="hidden" class="referente" name="referente" value="" />
							<div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-info-circle fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Comentario</small></label>
                                <textarea name="texto" class="texto form-control" rows="4" placeholder="Comentario" required></textarea>
                            </div>
							<div class="form-group file_up pb-3">
                                <label><small>Adjunto</small></label>
                                <input type="hidden" class="adjunto" name="adjunto" data-funcion="inputvars_Base" data-funcionvar="inputvars_adjunto" data-prevurl="<?php echo $conArr['base_url_sitio'] ?>/novedades" data-imgcont="<?php echo $conArr['updestino'] ?>novedades" value="" data-parsley-error-message="Debes subir un documento en PDF o imagen." />
                                <div class="input-group">                                    
                                    <input type="file" class="archivo" name="archivo_adjunto" accept=".pdf,image/*" />
                                </div>                                
                            </div>                            
                            <div class="error_upload_file" class="d-none"></div>
                        </form>						
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
							<button type="button" class="btn btn-success text-white" onClick="Valform('form-novedadescom', reLoadTable, ['novedades', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
			var inputvars_adjunto = {
                maxFileSize: 4000,
                maxFileCount: 1,
                dropZoneTitle: 'Arrastra y suelta aquí tu adjunto (imágenes o PDF).',
                fileActionSettings: {
                    showZoom: true
                }
            };
            $(function() {
				inputvars_Base($('#form-novedadescom .adjunto'));
				inputvars_Base($('#form-newnovedadali .adjunto'));
				inputvars_Base($('#form-newnovedadlel .adjunto'));
				$('#mod-novedadesFin').on('show.bs.modal', function(){
					$('#form-novedadesFin .star i').removeClass('fas');
					$('#form-novedadesFin .star i').addClass('far');
					$('#form-novedadesFin .estado').val('1');
					$('#form-novedadesFin .vista').val('1');					
				});				
                tablasD('novedades',{'user': login, 'rol': rol},'misnovedades', true);
                respClass();							
				<?php
				if($puede){
				?>
				loaderHide();
				<?php	
				}else{
				?>
				loader('inicio');
				showError('Debes tener al menos 1 plan de propiedades vigente para acceder a esta sección');
				<?php	
				}
				?>
            });
			function setVoto(valor){
				$('#form-novedadesFin .star i').removeClass('fas');
				$('#form-novedadesFin .star i').addClass('far');
				if(valor >= 1){
					$('#form-novedadesFin .star_1 i').removeClass('far');
					$('#form-novedadesFin .star_1 i').addClass('fas');
				}
				if(valor >= 2){
					$('#form-novedadesFin .star_2 i').removeClass('far');
					$('#form-novedadesFin .star_2 i').addClass('fas');
				}
				if(valor >= 3){
					$('#form-novedadesFin .star_3 i').removeClass('far');
					$('#form-novedadesFin .star_3 i').addClass('fas');
				}
				if(valor >= 4){
					$('#form-novedadesFin .star_4 i').removeClass('far');
					$('#form-novedadesFin .star_4 i').addClass('fas');
				}
				if(valor >= 5){
					$('#form-novedadesFin .star_5 i').removeClass('far');
					$('#form-novedadesFin .star_5 i').addClass('fas');
				}
				$('#form-novedadesFin .valoracion').val(valor);
			}
			function setNovTipo(tipo){
				if($('#form-newnovedad').parsley().validate()){
					if(tipo == 1){
						$('#mod-newnovedad').one('hidden.bs.modal', function(){
							$('#form-newnovedadali .pais').val($('#form-newnovedad .pais').val());
							$('#mod-newnovedadali').one('show.bs.modal', function(){
								showAli('form-newnovedadali');
							});
							openData('newnovedadali');							
						});
					}else{
						$('#mod-newnovedad').one('hidden.bs.modal', function(){
							$('#form-newnovedadlel .pais').val($('#form-newnovedad .pais').val());
							$('#mod-newnovedadlel').one('show.bs.modal', function(){
								showAli('form-newnovedadlel');
							});
							openData('newnovedadlel');
						});
					}					
					$('#mod-newnovedad').modal('hide');
				}
			}
			function showAli(formu){
				$('#'+formu+' .aliado').val('');
				$('#'+formu+' .id_responsable').val('');
				$('#'+formu+' .cont_usuario').hide();
				if($('#'+formu+' .especialidad').val() != ''){
					$('#'+formu+' .cont_usuario').show();
				}
			}
			function setAliad(formu, iden, nombre){
				$('#'+formu+' .aliado').val(nombre);
				$('#'+formu+' .id_responsable').val(iden);
				$('#mod-listali').modal('hide');
			}
			function showAliados(formu){
				$('#'+formu+' .aliado').val('');
				$('#'+formu+' .id_responsable').val('');
				$('#mod-listali .cont_logs').html('');
				$('#mod-listali').one('show.bs.modal', function(){
					var datos = {
						'pais': $('#'+formu+' .pais').val(),
						'especialidad': $('#'+formu+' .especialidad').val(),
						'tipo': $('#'+formu+' .tipo').val(),
						'accion': 19,
						'idveruser': login
					};
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#mod-listali .cont_logs').html('');
						},
						success:  function (response) {					
							if(response.resp){
								var contenido = '';		
								$.each(response.data.aliados, function(key, value){
									var icono = ($('#'+formu+' .tipo').val() == 1)?'toolbox':'user-tie';
									contenido += '<div class="d-block w-100 p-2 my-2 border-bottom border-secondary bg-light"><div class="row mx-0 w-100 justify-content-between align-items-start"><div class="col-4 text-center ps-0 pe-2"><div class="ratio ratio-1x1 rounded-circle overflow-hidden border-secondary">';
									contenido += (value.logo != '')?'<div class="d-flex w-100 h-100 justify-content-center align-items-center" style="background-image: url(<?php echo $conArr['base_url_sitio'] ?>/aliados/'+value.logo+'); background-repeat: no-repeat; background-position: center center; background-size: cover"></div>':'<div class="bg-secondary d-flex w-100 h-100 justify-content-center align-items-center"><i class="fas fa-'+icono+' fa-4x text-white"></i></div>'
									contenido += '</div></div><div class="col-8 text-start px-0"><h5 class="mb-2 fw-bold"><span class="text-responsive">'+value.nombre+'</span></h5><small class="d-block py-1"><i class="fas fa-envelope-open-text fa-fw"></i>: <span class="text-muted fw-bold">'+value.email+'</span></small><small class="d-block py-1"><i class="fas fa-mobile-alt fa-fw"></i>: <span class="text-muted fw-bold">'+value.celular+'</span></small><small class="d-block py-1"><i class="fas fa-star fa-fw"></i>: <span class="text-muted fw-bold">'+value.votacion+'</span></small><small class="d-block py-1"><i class="fas fa-comment-dots fa-fw"></i>: <a href="#" class="btn btn-sm btn-secondary text-decoration-none text-white fw-bold" onClick="showResena('+value.id+'); return false;">reseñas</a></small>';
									contenido += (value.adjunto != '')?'<small class="d-block py-1"><i class="fas fa-file-alt fa-fw"></i>: <a href="<?php echo $conArr['base_url_sitio'] ?>/aliados/'+value.adjunto+'" class="btn btn-sm btn-secondary text-decoration-none text-white fw-bold" target="blank">Hoja de vida</a></small>':'';
									contenido += '<div class="d-flex w-100 justify-content-start align-items-start py-3">';
									if(value.web != ''){
										contenido += '<a href="'+value.web+'" class="text-decoration-none link-secondary me-2" target="blank"><span class="fa-stack align-top"><i class="fas fa-circle fa-stack-2x"></i><i class="fas icon-network fa-stack-1x text-white"></i></span></a>';
									}
									if(value.facebook != ''){
										contenido += '<a href="'+value.facebook+'" class="text-decoration-none link-secondary me-2" target="blank"><span class="fa-stack align-top"><i class="fas fa-circle fa-stack-2x"></i><i class="fab fa-facebook fa-stack-1x text-white"></i></span></a>';
									}
									if(value.instagram != ''){
										contenido += '<a href="'+value.instagram+'" class="text-decoration-none link-secondary me-2" target="blank"><span class="fa-stack align-top"><i class="fas fa-circle fa-stack-2x"></i><i class="fab fa-instagram fa-stack-1x text-white"></i></span></a>';
									}
									if(value.linkedin != ''){
										contenido += '<a href="'+value.linkedin+'" class="text-decoration-none link-secondary me-2" target="blank"><span class="fa-stack align-top"><i class="fas fa-circle fa-stack-2x"></i><i class="fab fa-linkedin fa-stack-1x text-white"></i></span></a>';
									}
									contenido += '</div></div></div><div class="d-block w-100 py-1"><div class="d-grid"><button class="btn btn-primary btn-sm text-white fw-bold" onClick="setAliad(\''+formu+'\', '+value.id+', \''+value.nombre+'\'); return false">seleccionar</button></div></div></div>';							
								});
								$('#mod-listali .cont_logs').html(contenido);						
							}else{
								$('#mod-listali .cont_logs').html('');
							}					
						}
					});
				});
				openData('listali');
			}
			function showResena(iden){
				$('#mod-listrese .cont_logs').html('');
				$('#mod-listrese').one('show.bs.modal', function(){
					var datos = {
						'id': iden,
						'accion': 20,
						'idveruser': login
					};
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#mod-listrese .cont_logs').html('');
						},
						success:  function (response) {					
							if(response.resp){
								var contenido = '';		
								$.each(response.data.resenas, function(key, value){
									contenido += '<div class="d-block w-100 p-2 my-2 border-bottom border-secondary bg-light"><div class="d-flex w-100 justify-content-between align-items-start pb-2"><div class="pe-1 text-start"><small><i class="fas fa-user fa-fw"></i>: <span class="fw-bold">'+value.nombre+'</span></small></div><div class="ps-1 text-end"><small><i class="fas fa-calendar fa-fw"></i>: <span class="fw-bold">'+value.fecha+'</span></small></div></div><div class="d-block w-100 text-start pb-2"><small><i class="fas fa-star fa-fw"></i>: <span class="fw-bold">'+value.votacion+'</span></small></div><div class="d-block w-100 text-start"><p>'+value.comentario+'</p></div></div>';						
								});
								if(contenido == ''){
									contenido = '<p class="text-center"><span class="text-responsive">No hay reseñas.</span></p>';
								}
								$('#mod-listrese .cont_logs').html(contenido);						
							}else{
								$('#mod-listrese .cont_logs').html('');
							}					
						}
					});
				});
				openData('listrese', iden);
				
			}
			function showLogs(iden){
				$('#mod-novedadeslog').one('show.bs.modal', function(){
					var datos = {
						'id': iden,
						'tipo': <?php echo ($prol != 5)?1:2 ?>,
						'accion': 15,
						'idveruser': login
					};                              
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#mod-novedadeslog .cont_logs').html('');
						},
						success:  function (response) {					
							if(response.resp){
								var contenido = '';	
								contenido += '<div class="row mx-0 w-100 justify-content-center align-items-center border border-secondary rounded-3 bg-light"><div class="col-12 text-center py-2"><div class="d-flex w-100 justify-content-start align-items-center"><div class="flex-fill text-start"><small class="fw-bold">'+response.data.main.nombre+'</small></div><div class="ps-2 text-end text-nowrap cont_visto_'+response.data.main.id+'">';
								if(response.data.main.vista == 0 && response.data.main.editable && response.data.main.estado == 0){
									contenido += '<small class="d-inline-block pe-1">revisado</small><div class="form-group d-inline-block align-middle"><div class="form-check form-switch swt-sm d-flex justify-content-start align-items-center"><input class="form-check-input mt-0 chk-warning visto_'+response.data.main.id+'" type="checkbox" onChange="setvisto('+response.data.main.id+');" ></div></div>';
								}else{
									contenido += (response.data.main.vista == 0)?'<small class="d-inline-block text-danger">pendiente</small>':'<small class="d-inline-block text-success">revisado</small>';
								}
								var icono = (response.data.main.tipo == 1)?'toolbox':'user-tie';								
								contenido += '</div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-user"></i> <small class="fw-bold">'+response.data.main.user+'</small></div><div class="text-end ps-2"><i class="fas fa-calendar"></i> <small class="fw-bold">'+response.data.main.creado+'</small></div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-'+icono+'"></i> <small class="fw-bold">'+response.data.main.responsable+'</small></div>';
								contenido += (parseInt(response.data.main.propiedad) != 0)?'<div class="text-end ps-2"><i class="fas fa-building"></i> <small class="fw-bold">'+response.data.main.propiedad+'</small></div>':'';
								contenido += '</div></div>';
								contenido += (response.data.main.adjunto != '')?'<div class="col-12 text-start pb-2"><a href="'+response.data.main.adjunto+'" class="btn btn-sm btn-secondary text-white" target="blank"><i class="fas fa-paperclip"></i> adjunto</a></div>':'';
								contenido += '<div class="col-12 text-start pb-2"><small>'+response.data.main.texto+'</small></div></div><hr>';								
								$.each(response.data.logs, function(key, value){
									contenido += '<div class="row mx-0 w-100 justify-content-center align-items-center border border-light rounded-3"><div class="col-12 text-center py-2"><div class="d-flex w-100 justify-content-start align-items-center"><div class="flex-fill text-start"><small class="fw-bold">'+value.nombre+'</small></div><div class="ps-2 text-end text-nowrap cont_visto_'+value.id+'">';
									if(value.vista == 0 && value.editable && response.data.main.estado == 0){
										contenido += '<small class="d-inline-block pe-1">revisado</small><div class="form-group d-inline-block align-middle"><div class="form-check form-switch swt-sm d-flex justify-content-start align-items-center"><input class="form-check-input mt-0 chk-warning visto_'+value.id+'" type="checkbox" onChange="setvisto('+value.id+');" ></div></div>';
									}else{
										contenido += (value.vista == 0)?'<small class="d-inline-block text-danger">pendiente</small>':'<small class="d-inline-block text-success">revisado</small>';
									}																
									contenido += '</div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-between align-items-center"><div class="text-start"><i class="fas fa-comment-dots"></i> <small class="fw-bold">'+value.editor+'</small></div><div class="text-end ps-2"><i class="fas fa-calendar"></i> <small class="fw-bold">'+value.creado+'</small></div></div></div><div class="col-12 text-start pb-2"><div class="d-flex w-100 justify-content-start align-items-center">';
									contenido += (value.adjunto != '')?'<div class="text-start"><a href="'+value.adjunto+'" class="btn btn-sm btn-secondary text-white" target="blank"><i class="fas fa-paperclip"></i> adjunto</a></div>':'';
									contenido += '</div></div><div class="col-12 text-start pb-2"><small>'+value.texto+'</small></div></div><hr>';							
								});
								$('#mod-novedadeslog .cont_logs').html(contenido);						
							}else{
								$('#mod-novedadeslog .cont_logs').html('');
							}					
						}
					});
				});
				openData('novedadeslog', iden);
			}
			function setvisto(iden){
				if($('#mod-novedadeslog .visto_'+iden).is(':checked')){
					loaderShow();
					var datos = {
						'id': iden,						
						'accion': 16,
						'editor': login,
						'idveruser': login
					};                              
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {
							$('#loader').one("hide", function() { 
								showError(error);
							});
							loaderHide();
						},
						success:  function (response) {					
							if(response.resp){
								$('#mod-novedadeslog .cont_visto_'+iden).html('<small class="d-inline-block text-success">revisado</small>');
								reLoadTable('novedades', false);
								loaderHide();				
							}else{
								$('#loader').one("hide", function() { 
									showError(response.data.msg);
								});
								loaderHide();
							}					
						}
					});
				}
			}
			function addComment(){	
				if($('#form-novedadeslog .estado').val() == 1){
					showError('La novedad ya ha sido cerrada por parte del usuario.');
				}else{
					var iden = $('#form-novedadeslog .id').val();				
					$('#mod-novedadeslog').one('hidden.bs.modal', function(){
						$('#mod-novedadescom').one('show.bs.modal', function(){
							$('#form-novedadescom .nombre').val('RS / '+$('#form-novedadeslog .nombre').val());
							$('#form-novedadescom .id_user').val($('#form-novedadeslog .id_user').val());
							$('#form-novedadescom .id_responsable').val($('#form-novedadeslog .id_responsable').val());
							$('#form-novedadescom .referente').val(iden);
							$('#form-novedadescom .tipo').val($('#form-novedadeslog .tipo').val());
							$('#form-novedadescom .id_propiedad').val($('#form-novedadeslog .id_propiedad').val());
						});
						$('#mod-novedadescom').one('hidden.bs.modal', function(){
							showLogs(iden);
						});
						openData('novedadescom');
					});
					$('#mod-novedadeslog').modal('hide');
				}								
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>