<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-user-tie fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Gestores</span>
                        </span>						
					</h6>
                    <button class="btn btn-warning btn-sm text-white" onClick="openData('gestor'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="gestores" class="table table-striped table-bordered table-sm data-table align-middle w-100">
					<thead>
                        <tr>
                            <th>Nombre</th>							
							<th>E-mail</th>
							<th>Usuario</th>
							<th class="select-filter" data-filtro='{"tb":"admins","fl":"rol","opt":"join","tbj":"perfiles","flr":"id","fln":"nombre","flnd":"Suspendido", "where":"`sub`.`id_perfil` >= <?php echo $prol ?> AND `sub`.`id_perfil` < 5"}'>Rol</th>							
							<th>Sesiones</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Edición</th>
							<th class="hdvis">Editor</th>
							<th class="select-filter hdvis_af no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="no_print text-right">Acción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="mod-gestor" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-user-tie fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Editar Gestor</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-gestor">
                            <input type="hidden" class="id" name="id" value=0 />							
							<input type="hidden" class="db noclear" name="db" value="admins" />
                            <div class="form-group pb-3">
                                <label><small>Nombre</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-address-card fa-fw"></i></span>
                                    <input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>E-mail</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-envelope fa-fw"></i></span>
                                    <input type="email" name="email" class="form-control email" placeholder="E-mail" aria-label="E-mail" data-parsley-type="email" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Usuario</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user-circle fa-fw"></i></span>
                                    <input type="text" name="usuario" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" data-parsley-type="alphanum" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Contraseña</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-fingerprint fa-fw"></i></span>
                                    <input type="text" name="contrasena" class="form-control contrasena" placeholder="Contraseña" aria-label="Contraseña" autocomplete="new-password" data-parsley-minlength="6" required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Rol</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user-tie fa-fw"></i></span>
                                    <select name="rol" class="form-select rol" aria-label="Rol" required>
                                        <option value="">Seleccionar</option>
                                        <?php
										$consulta = "SELECT id_perfil, nombre FROM perfiles WHERE id_perfil >= ".$prol." AND id_perfil <= 4 ORDER BY id_perfil ASC";
										if ( $result = $mysqli->query( $consulta ) ) {
											while($row = $result->fetch_assoc()){
												echo '<option value='.$row['id_perfil'].'>'.utf8_encode($row['nombre']).'</option>';
											}
											$result->close();
										}
										?>
                                        <option value="0">Suspendido</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-gestor', reLoadTable, ['gestores', false], true); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                tablasD('gestores',{'user': login, 'rol': rol},'gestores', true);
                respClass();							
				loaderHide();
            });
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>