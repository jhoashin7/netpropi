<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Contratos</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-file-contract fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">Contratos</span>
                        </span>
                    </h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-warning text-white" onclick="openData('contratos'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="lista_contratos" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Plantilla / Tipo</th>
                        <th>Estado</th>
                        <th>F. Creación</th>
                        <th>F. Edición</th>
                        <th class="no_print text-right">Acciones</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <script>
            respClass()
            loaderHide()

            $('#lista_contratos').DataTable({
                ajax: 'controllers/contratos_lista.php?a=index',
                columns: [
                    {data: 'nombre'},
                    {data: 'plantilla'},
                    {data: 'estado'},
                    {data: 'fecha_creacion'},
                    {data: 'fecha_edicion'},
                    {data: 'acciones'},
                ]
            })
        </script>
    </div>
</body>
</html>