<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $_POST['id'] ) ), 'd', $conArr['enc_string'] ));
if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
    $id = $id_veri[1];
    $consulta = "SELECT `admin`.*, `perfil`.`nombre` AS `perfil` FROM `admins` AS `admin` LEFT JOIN `perfiles` AS `perfil` ON (`perfil`.`id_perfil` = `admin`.`rol`) WHERE `admin`.`id` = " .$id;
    if ( $result = $mysqli->query( $consulta ) ) {
        $usuario = mysqli_fetch_object( $result );
        $result->close();
    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row m-0 justify-content-center">
            <div class="col-12 text-center">
                <div class="position-relative d-inline-block">
					<i class="fas fa-user-circle fa-5x text-muted"></i>
					<button class="btn btn-sm btn-danger position-absolute text-white text-nowrap" style="top: 19px; left: 45px" onClick="logOut(); return false;"><i class="fas fa-times-circle"></i> salir</button>
				</div>
            </div>
            <div class="col-12 text-left p-0">
                <hr>
                <small class="text-muted"><span class="text-responsive"><i class="fas fa-user-tie fa-fw fa-lg"></i> <?php echo ($usuario->rol != 0)?utf8_encode($usuario->perfil):'Suspendido' ?></span></small>
                <div class="d-grid gap-2">
                    <button class="btn btn-info btn-sm btn-block text-uppercase fw-bold mt-2 text-white" onClick="showSes(<?php echo $usuario->id ?>, '<?php echo utf8_encode($usuario->nombre) ?>'); return false"><i class="fas fa-user-clock fa-fw"></i> sesiones</button>
                </div>
				<hr>
            </div>
            <div class="col-12 text-left p-0">
                <form id="form-perfil">
                    <input type="hidden" class="id" name="id" value=<?php echo $usuario->id ?> />
					<input type="hidden" class="rol" name="rol" value=<?php echo $usuario->rol ?> />
					<input type="hidden" class="db" name="db" value="admins" />
                    <div class="form-group pb-3">
						<div class="input-group input-group-sm">
							<span class="input-group-text"><i class="fas fa-address-card fa-fw"></i></span>
							<input type="text" name="nombre" class="form-control nombre" placeholder="Nombre" aria-label="Nombre" value="<?php echo utf8_encode($usuario->nombre) ?>" required>
						</div>
					</div>
                    <div class="form-group pb-3">
						<div class="input-group input-group-sm">
							<span class="input-group-text"><i class="fas fa-envelope fa-fw"></i></span>
							<input type="email" name="email" class="form-control email" placeholder="E-mail" aria-label="E-mail" value="<?php echo $usuario->email ?>" data-parsley-type="email" required>
						</div>
					</div>
                    <div class="form-group pb-3">
						<div class="input-group input-group-sm">
							<span class="input-group-text"><i class="fas fa-user-circle fa-fw"></i></span>
							<input type="text" name="usuario" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" value="<?php echo $usuario->usuario ?>" data-parsley-type="alphanum" required>
						</div>
					</div>
                    <div class="form-group">
						<div class="input-group input-group-sm">
							<span class="input-group-text"><i class="fas fa-fingerprint fa-fw"></i></span>
							<input type="password" autocomplete="new-password" name="contrasena" class="form-control contrasena" placeholder="Contraseña" aria-label="Contraseña" value="" data-parsley-minlength="6" data-parsley-type="alphanum" data-parsley-pattern="^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$" data-parsley-pattern-message="La contraseña debe contener dígitos y letras" required>
						</div>
					</div>
                </form>
                <hr>
            </div>
            <div class="col-12 text-center p-0">
                <div class="d-grid gap-2 pb-3">
                    <button class="btn btn-success btn-sm text-uppercase fw-bold text-white" onClick="Valform('form-perfil', closeSubMenu, [], true); return false"><i class="fas fa-user-cog fa-fw"></i> editar</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php
}else{
?>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
</body>
</html>
<?php    
}
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>