<?php
header('X-Frame-Options: DENY');
include '../controllers/db_connect.php';
$puser = strip_tags( $mysqli->real_escape_string( $_POST[ 'id' ] ) );
$prol = strip_tags( $mysqli->real_escape_string( $_POST[ 'rol' ] ) );
$id_veri = explode('***', simple_crypt( $puser, 'd', $conArr['enc_string'] ));
$moneda = (isset($_POST[ 'moneda' ]))?strip_tags( $mysqli->real_escape_string( $_POST[ 'moneda' ] ) ):1;
$consulta = "SELECT moneda, valor FROM cambio WHERE id = ".$moneda;
if ( $result = $mysqli->query( $consulta ) ) {
    $monob = mysqli_fetch_object( $result );    
    $result->close();
}
$consulta = "SELECT minimo FROM planes WHERE id != 1 ORDER BY minimo ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $plan = mysqli_fetch_object( $result );    
    $result->close();
}
$gratis = false;
$consulta = "SELECT * FROM planes WHERE id = 1 LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $gratis = mysqli_fetch_object( $result );    
    $result->close();
}
$consulta = "SELECT publico FROM stripe ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $stripe = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
    <div id="contenido" class="w-100 position-relative">
        <div class="row mx-0 w-100 mt-2 justify-content-center">
            <div class="col-12">
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <h6 class="m-0 text-muted pr-2">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-hand-holding-usd fa-stack-1x text-white"></i>
                            </span>
                            <span class="fw-bold">planes contratados</span>
                        </span>						
					</h6>
                    <div class="btn-group btn-group-sm" role="group" aria-label="Acciones">
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="cambiomoneda" type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-donate"></i> <?php echo $monob->moneda ?></button>
                            <ul class="dropdown-menu" aria-labelledby="cambiomoneda">
                                <?php
                                $consulta = "SELECT id, moneda FROM cambio WHERE moneda != '".$monob->moneda."' GROUP BY moneda";
                                if ( $result = $mysqli->query( $consulta ) ) {
                                    while($row = $result->fetch_assoc()){
                                ?>
                                <li><a class="dropdown-item" href="#" onClick="loader('balance.plan', {'moneda': <?php echo $row['id']?>}); return false"><small><?php echo $row['moneda']?></small></a></li>
                                <?php                                    
                                    }
                                    $result->close();
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
						if($prol != 3 && $prol <= 4){						
						?>
						<button type="button" class="btn btn-warning text-white" onClick="newPlan('form-preplan'); return false"><span class="d-none d-sm-inline">crear</span> <i class="fas fa-plus-circle"></i></button>
						<?php						
						}
						?>
                    </div>                                        
                </div>
                <hr>
            </div>
            <div class="col-12">
                <table id="planes" class="table table-striped table-bordered table-sm data-table align-middle w-100" data-order="[[ 8, &quot;desc&quot; ]]">
					<thead>
                        <tr>
							<th>Referencia</th>
                            <th class="select-filter" data-filtro='{"tb":"pagos","fl":"nombre_pago","opt":"basic","tbj":"","flr":"","fln":"","flnd":"NA", "where":"`main`.`tipo` = 1"}'>Plan</th>                            
                            <th class="money_fr">Valor(<?php echo $monob->moneda ?>)</th>
                            <th>Beneficiario</th>
							<th class="hdvis">ID Beneficiario</th>
                            <th>Propiedades Activas</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Vigencia</th>
							<th class="select-filter" data-filtro='{"tb":"","fl":"","opt":[[1,"Activo"], [2,"Finalizado"], [3,"Renovado"]],"tbj":"","flr":"","fln":"","flnd":""}'>Estado</th>
							<th class="select-filter no_wrap" data-filtro='{"tb":"","fl":"","opt":"date","tbj":"","flr":"","fln":"","flnd":""}'>Creación</th>
							<th class="hdvis">Editor</th>
							<?php
							if($prol != 3 && $prol <= 4){						
							?>
							<th class="no_print text-right">Acción</th>
							<?php						
							}
							?>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
			<div class="col-12 text-start">
				<hr>
				<h5 class="fw-bold mb-2"><span class="text-responsive">TOTAL PAGADOS: <span class="text-muted total_tbl"></span></span></h5>
			</div>
        </div>
        <div class="modal fade" id="mod-plan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-hand-holding-usd fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Contratar Plan</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-plan">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="pagos" />
							<input type="hidden" class="referencia" name="referencia" value="" />
							<input type="hidden" class="nombre_pago" name="nombre_pago" value="" />
							<input type="hidden" class="id_pago" name="id_pago" value="" />
							<input type="hidden" class="tipo noclear" name="tipo" value="1" />							
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="email_user" name="email_user" value="" />
                            <div class="form-group pb-3">
                                <label><small>Usuario</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user fa-fw"></i></span>
                                    <input type="text" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" readonly required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Propiedades</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" name="propiedades" class="form-control propiedades" placeholder="Propiedades" aria-label="Propiedades" data-parsley-min="<?php echo $plan->minimo ?>" data-parsley-type="digits" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Tiempo(meses)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-week fa-fw"></i></span>
                                    <input type="text" name="meses" class="form-control meses" placeholder="Tiempo(meses)" aria-label="Tiempo(meses)" data-parsley-min="1" data-parsley-type="digits" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Valor(USD)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" readonly required>
                                </div>
                            </div>
                            <?php
                            if($monob->moneda != 'USD'){
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Valor(<?php echo $monob->moneda ?>)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" class="form-control cambio" placeholder="Valor(<?php echo $monob->moneda ?>)" aria-label="Valor" readonly>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
							<div class="form-group pb-3 cont_pago">
                                <label><small>Tipo de Pago</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-money-check-alt fa-fw"></i></span>
                                    <select name="tipo_pagon" class="form-select tipo_pagon" aria-label="Tipo" onChange="setMetodo('form-plan')" required>
                                        <option value="">Seleccionar</option>
                                        <option value="0">Digital</option>
										<option value="1">Manual</option>
                                    </select>
                                </div>
                            </div>                            
                            <div class="form-group pb-3 cont_pago">
                                <label><small>Id de pago</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice-dollar fa-fw"></i></span>
                                    <input type="text" name="id_pasarelan" class="form-control id_pasarelan" placeholder="Id de pago" aria-label="Id de pago">
                                </div>
                            </div>
							<div class="form-group pb-3 cont_pago">
                                <label><small>Estado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-check-double fa-fw"></i></span>
                                    <select name="estado" class="form-select estado" aria-label="Estado" onChange="setpagEsta('form-plan')" required>
                                        <option value="">Seleccionar</option>
                                        <option value="0">Pendiente</option>
										<option value="1">Pagado</option>
										<option value="2">Cancelado</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-plan', reLoadTable, ['planes', false], true, false, 10); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-plann" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-hand-holding-usd fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Contratar Plan</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-plann">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="pagos" />
							<input type="hidden" class="referencia" name="referencia" value="" />
							<input type="hidden" class="nombre_pago" name="nombre_pago" value="" />
							<input type="hidden" class="id_pago" name="id_pago" value="" />
							<input type="hidden" class="tipo noclear" name="tipo" value="1" />							
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="email_user" name="email_user" value="" />
							<input type="hidden" class="propiedades" name="propiedades" data-parsley-min="<?php echo $plan->minimo ?>" data-parsley-error-message="Debes determinar no menos de <?php echo $plan->minimo ?> propiedades entre actuales y nuevas" value="" required />
                            <div class="form-group pb-3">
                                <label><small>Usuario</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user fa-fw"></i></span>
                                    <input type="text" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" readonly required>
                                </div>
                            </div>
                            <div class="form-group pb-3">
                                <label><small>Propiedades actuales</small></label>
                                <div class="input-group">
                                    <select class="actuales form-select" multiple="multiple" size="10" name="actuales[]">
                                    </select>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Propiedades nuevas</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-building fa-fw"></i></span>
                                    <input type="text" name="extras" class="form-control extras" placeholder="Propiedades nuevas" aria-label="Propiedades nuevas" data-parsley-type="digits">
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Tiempo(meses)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-calendar-week fa-fw"></i></span>
                                    <input type="text" name="meses" class="form-control meses" placeholder="Tiempo(meses)" aria-label="Tiempo(meses)" data-parsley-min="1" data-parsley-type="digits" required>
                                </div>
                            </div>
							<div class="form-group pb-3">
                                <label><small>Valor(USD)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" data-parsley-pattern="^[0-9]*(\.?[0-9]{2}$)?" readonly required>
                                </div>
                            </div>
                            <?php
                            if($monob->moneda != 'USD'){
                            ?>
                            <div class="form-group pb-3">
                                <label><small>Valor(<?php echo $monob->moneda ?>)</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span>
                                    <input type="text" class="form-control cambio" placeholder="Valor(<?php echo $monob->moneda ?>)" aria-label="Valor" readonly>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
							<div class="form-group pb-3 cont_pago">
                                <label><small>Tipo de Pago</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-money-check-alt fa-fw"></i></span>
                                    <select name="tipo_pagon" class="form-select tipo_pagon" aria-label="Tipo" onChange="setMetodo('form-plann')" required>
                                        <option value="">Seleccionar</option>
                                        <option value="0">Digital</option>
										<option value="1">Manual</option>
                                    </select>
                                </div>
                            </div>                            
                            <div class="form-group pb-3 cont_pago">
                                <label><small>Id de pago</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-file-invoice-dollar fa-fw"></i></span>
                                    <input type="text" name="id_pasarelan" class="form-control id_pasarelan" placeholder="Id de pago" aria-label="Id de pago">
                                </div>
                            </div>
							<div class="form-group pb-3 cont_pago">
                                <label><small>Estado</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-check-double fa-fw"></i></span>
                                    <select name="estado" class="form-select estado" aria-label="Estado" onChange="setpagEsta('form-plann')" required>
                                        <option value="">Seleccionar</option>
                                        <option value="0">Pendiente</option>
										<option value="1">Pagado</option>
										<option value="2">Cancelado</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                            <button type="button" class="btn btn-success text-white" onClick="Valform('form-plann', reLoadTable, ['planes', false], true, false, 10); return false"><span class="text-responsive">guardar <i class="fas fa-check-circle fa-fw"></i></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="modal fade" id="mod-preplan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
						<h5 class="modal-title">
                            <span class="text-responsive d-flex justify-content-start align-items-center">
                                <span class="fa-stack text-warning align-top">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-wallet fa-stack-1x text-white"></i>
                                </span>                                
                                <span>Definir tipo de plan</span>
                            </span>                            
                        </h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
                    <div class="modal-body">
                        <form id="form-preplan">
                            <input type="hidden" class="id" name="id" value=0 />                            
							<input type="hidden" class="db noclear" name="db" value="pagos" />
							<input type="hidden" class="referencia" name="referencia" value="<?php echo $gratis->prefijo ?>" />
							<input type="hidden" class="nombre_pago" name="nombre_pago" value="<?php echo utf8_encode($gratis->nombre) ?>" />
							<input type="hidden" class="id_pago" name="id_pago" value="<?php echo $gratis->id ?>" />
							<input type="hidden" class="tipo noclear" name="tipo" value="1" />							
							<input type="hidden" class="id_user" name="id_user" value="" />
							<input type="hidden" class="email_user" name="email_user" value="" />
							<input type="hidden" class="propiedades" name="propiedades" value="<?php echo $gratis->maximo ?>" />
							<input type="hidden" class="meses" name="meses" value="<?php echo $gratis->tiempo ?>" />
							<input type="hidden" class="valor" name="valor" value="<?php echo $gratis->valor ?>" />
							<input type="hidden" class="tipo_pagon" name="tipo_pagon" value="0" />
							<input type="hidden" class="id_pasarelan" name="id_pasarelan" value="NA" />
							<input type="hidden" class="estado" name="estado" value="1" />							
                            <div class="form-group pb-3">
                                <label><small>Usuario</small></label>
                                <div class="input-group">
                                    <span class="input-group-text gray-700 text-white"><i class="fas fa-user fa-fw"></i></span>
                                    <input type="text" class="form-control usuario" placeholder="Usuario" aria-label="Usuario" readonly required>
                                </div>
                            </div>                            
                        </form>
						<div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-warning text-white" onClick="Valform('form-preplan', reLoadTable, ['planes', false], true, false, 10); return false"><span class="text-responsive"><i class="fas icon-money_off fa-lg fa-fw"></i> gratuito</span></button>
                            <button type="button" class="btn btn-info text-white" onClick="newPlanP('plan'); return false"><span class="text-responsive">pagado <i class="fas icon-attach_money fa-lg fa-fw"></i></span></button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
				
        <script>
			card = false;			
            $(function() {
                tablasD('planes',{'user': login, 'rol': rol, 'moneda': '<?php echo $monob->valor ?>'},'plan', true);
				$('#planes').on('draw.dt', function (settings) {
					var api = new $.fn.dataTable.Api( '#planes' );
					var respuesta = api.ajax.json();						
					var datosp = {'querys': window.btoa(respuesta.query.replace(/(\r\n|\n|\r)/gm, "")), 'campo':'`pag`.`valor`', 'tabla':'`pagos` AS `pag` LEFT JOIN `plan` AS `plan` ON (`plan`.`id_pago` = `pag`.`id`)', 'decimales': 2, 'where': '`pag`.`estado` = 1 AND `pag`.`tipo` = 1', 'user': login};
					$.ajax({
						url: 'controllers/valores.php',
						data: datosp,
						type: 'post',
						dataType: 'json',
						error: function () {
							$('.total_tbl').html('$'+(0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');
						},
						success: function (response) {
							var resT = parseFloat(response.valor);
							<?php if($monob->moneda != 'USD'){ ?>
							resT = resT * <?php echo $monob->valor ?>;
							<?php } ?>
							$('.total_tbl').html('$'+resT.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')+'<?php if($monob->moneda != 'USD'){ ?> (<?php echo $monob->moneda ?>) <?php } ?>');							
						}
					});					
				});				
				inputMultiple($('#form-plann .actuales'), {});
				$('#form-plan .propiedades, #form-plan .meses').on('input', function(){					
					if(!isNaN($('#form-plan .propiedades').val()) && $('#form-plan .propiedades').val() != '' && !isNaN($('#form-plan .meses').val()) && $('#form-plan .meses').val() != '' && parseInt($('#form-plan .meses').val()) > 0 && $('#form-plan .id_user').val() != '' && $('#form-plan .id_user').val() !== 0){
						setPlanes($('#form-plan .id').val(), $('#form-plan .id_user').val(), $('#form-plan .propiedades').val(), $('#form-plan .meses').val());
					}else{						
						$('#form-plan .valor').val('').trigger('change');
					}
				});
				$('#form-plann .extras, #form-plann .meses').on('input', function(){
					if(!isNaN($('#form-plann .extras').val()) && $('#form-plann .extras').val() != ''){
						$('#form-plann .propiedades').val($('#form-plann .actuales :selected').length + parseInt($('#form-plann .extras').val()));
					}else{
						$('#form-plann .propiedades').val($('#form-plann .actuales :selected').length);
					}					
					if(!isNaN($('#form-plann .propiedades').val()) && $('#form-plann .propiedades').val() != '' && !isNaN($('#form-plann .meses').val()) && $('#form-plann .meses').val() != '' && parseInt($('#form-plann .meses').val()) > 0 && $('#form-plann .id_user').val() != '' && $('#form-plann .id_user').val() !== 0){
						setPlanes($('#form-plann .id').val(), $('#form-plann .id_user').val(), $('#form-plann .propiedades').val(), $('#form-plann .meses').val());
					}else{						
						$('#form-plann .valor').val('').trigger('change');
					}
				});
				$('#form-plann .actuales').on('change', function(){	
					if(!isNaN($('#form-plann .extras').val()) && $('#form-plann .extras').val() != ''){
						$('#form-plann .propiedades').val($('#form-plann .actuales :selected').length + parseInt($('#form-plann .extras').val()));
					}else{
						$('#form-plann .propiedades').val($('#form-plann .actuales :selected').length);
					}
					if(!isNaN($('#form-plann .propiedades').val()) && $('#form-plann .propiedades').val() != '' && !isNaN($('#form-plann .meses').val()) && $('#form-plann .meses').val() != '' && parseInt($('#form-plann .meses').val()) > 0 && $('#form-plann .id_user').val() != '' && $('#form-plann .id_user').val() !== 0){
						setPlanes($('#form-plann .id').val(), $('#form-plann .id_user').val(), $('#form-plann .propiedades').val(), $('#form-plann .meses').val());
					}else{						
						$('#form-plann .valor').val('').trigger('change');
					}
				});
				$('#form-plan .valor').on('change', function(){
					if($('#form-plan .valor').val() != ''){
						$('#form-plan .cont_pago').show();
					}else{
						$('#form-plan .cont_pago').hide();
						$('#form-plan .tipo_pagon').val('');
						$('#form-plan .id_pasarelan').val('');
					}
					<?php if($monob->moneda != 'USD'){ ?>
					setCambio('form-plan', '<?php echo $monob->valor ?>');
					<?php } ?>
				});
				$('#form-plann .valor').on('change', function(){
					if($('#form-plann .valor').val() != ''){
						$('#form-plann .cont_pago').show();
					}else{
						$('#form-plann .cont_pago').hide();
						$('#form-plann .tipo_pagon').val('');
						$('#form-plann .id_pasarelan').val('');
					}
					<?php if($monob->moneda != 'USD'){ ?>
					setCambio('form-plann', '<?php echo $monob->valor ?>');
					<?php } ?>
				});
                respClass();							
				loaderHide();
            });
			function newPlan(des){
				$('#mod-user').one('hidden.bs.modal', function () {					
					if($('#'+des+' .usuario').val() != '' && $('#'+des+' .id_user').val() !== 0 && $('#'+des+' .email_user').val() != ''){
						loaderShow();
						var datos = {
							'accion': 11,							
							'id_user': $('#'+des+' .id_user').val(),
							'idveruser': login
						};
						$.ajax({
							url: dirCont,
							data: datos,
							method:  'POST',
							dataType: 'json',
							error: function () {								
								$('#loader').one("hide", function() { 
									showError(error);
								});
								loaderHide();
							},
							success:  function (response) {						
								if(response.resp){
									if(parseInt(response.data.numero) < 1){
										$('#loader').one("hide", function() { 
											$('#mod-preplan').modal('show');
										});										
									}else{
										$('#loader').one("hide", function() { 
											newPlanP('plan');
										});										
									}									
									loaderHide();									
								}else{
									$('#loader').one("hide", function() { 
										showError('Usuario Inválido');
									});									
									loaderHide();
								}				
							}
						});						
					}
				});
				showUsers(des);
			}
			
			function newPlanP(des){					
				$('#mod-'+des).one('show.bs.modal', function(){					
                    $('#form-'+des+' .usuario').val($('#form-preplan .usuario').val());
					$('#form-'+des+' .id_user').val($('#form-preplan .id_user').val());
					$('#form-'+des+' .email_user').val($('#form-preplan .email_user').val());
					setPlanes($('#form-plan .id').val(), $('#form-plan .id_user').val(), $('#form-plan .propiedades').val(), $('#form-plan .meses').val());
					$('#form-plan').parsley().reset();
                });				
				if($('.modal.show').length > 0){
					$('.modal.show').one('hidden.bs.modal', function(){
						openData(des);
					});
					$('.modal.show').modal('hide');
				}else{
					openData(des);
				}
			}
			
			function renewPlan(plan, user){				
				$('#mod-plann').one('show.bs.modal', function(){					
					if($('#form-plann .id_user').val() != ''){
						$('#form-plann .valor').val('');
						$('#form-plann .propiedades').val('');
						$('#form-plann .meses').val('');
						$('#form-plann .actuales').html('');
						$('#form-plann .actuales').bootstrapDualListbox('refresh');
						$('#form-plann .bootstrap-duallistbox-container select').addClass('form-select');
						consData('admins', $('#form-plann .id_user').val(), ['nombre', 'email'], function(result){
							if(result){
								$('#form-plann .usuario').val(result.nombre);
								$('#form-plann .email_user').val(result.email);
								var datos = {
                                    'accion': 12,											
                                    'id': $('#form-plann .id').val(),
                                    'idveruser': login
                                };
                                $.ajax({
                                    url: dirCont,
                                    data: datos,
                                    method:  'POST',
                                    dataType: 'json',
                                    error: function () {							
                                        $('#loader').one("hide", function() { 
                                            showError(error);
                                        });
                                        loaderHide();
                                    },
                                    success:  function (response) {						
                                        if(response.resp){
											if(response.data.id_plan != 1){											
												$('#form-plann .meses').val(response.data.dias / 30);											
											}
                                            $.each( response.data.actuales, function( key, value ) {
                                                switch (value.estado) {
                                                    case 0:
                                                        estado = '';
                                                        break;
                                                    case 1:
                                                        estado = ' - Públicada';
                                                        break;
                                                    case 2:
                                                        estado = ' - Ocupada';
                                                        break;
                                                    case 3:
                                                        estado = ' - Suspendida';
                                                        break;
                                                    default:
                                                        estado = '';																
                                                }
                                                $('#form-plann .actuales').append('<option value="'+value.id+'">'+pad(value.id, 6)+' | '+value.nombre+estado+'</option>');
                                            });													
                                            $('#form-plann .actuales').bootstrapDualListbox('refresh');
                                            $('#form-plann .bootstrap-duallistbox-container select').addClass('form-select');
                                            $('#form-plann .estado').val('').trigger('change');
                                            $('#form-plann .valor').trigger('change');
                                            setPlanes($('#form-plann .id').val(), $('#form-plann .id_user').val(), $('#form-plann .propiedades').val(), $('#form-plann .meses').val());
                                            loaderHide();
                                        }else{
                                            $('#loader').one("hide", function() { 
                                                showError(response.data.msg);
                                            });
                                            loaderHide();
                                        }				
                                    }
                                });						
							}							
						});						
					}					
				});
				openData('plann', plan, true);				
			}
			
			function setPlanes(id_p, iden, propiedades, meses){
				var formu = (id_p != 0)?'form-plann':'form-plan';
				if($('#'+formu+' .propiedades').parsley().validate() && $('#'+formu+' .meses').parsley().validate()){
					var datos = {
						'accion': 9,
						'id_plan': id_p,
						'id': iden,
						'propiedades': (propiedades)?propiedades:0,
						'meses': (meses)?meses:0,
						'idveruser': login
					};
					$.ajax({
						url: dirCont,
						data: datos,
						method:  'POST',
						dataType: 'json',
						error: function () {							
							$('#'+formu+' .referencia').val('');
                            $('#'+formu+' .nombre_pago').val('');
                            $('#'+formu+' .id_pago').val('');
                            $('#'+formu+' .valor').val('').trigger('change');
						},
						success:  function (response) {						
							if(response.resp){
								$('#'+formu+' .referencia').val(response.data.planes.referencia);
								$('#'+formu+' .nombre_pago').val(response.data.planes.nombre);
								$('#'+formu+' .id_pago').val(response.data.planes.id);
								$('#'+formu+' .valor').val(response.data.planes.valor).trigger('change');
							}else{
								$('#'+formu+' .referencia').val('');
								$('#'+formu+' .nombre_pago').val('');
								$('#'+formu+' .id_pago').val('');
								$('#'+formu+' .valor').val('').trigger('change');
							}				
						}
					});					
				}								
			}			
			function setMetodo(formu){				
				if($('#'+formu+' .valor').parsley().validate()){
					if($('#'+formu+' .tipo_pagon').val() === '0'){
						if($('#mod-pago').length > 0){
							$('#mod-pago').remove();							
						}
						$('<div class="modal fade" id="mod-pago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title"><span class="text-responsive d-flex justify-content-start align-items-center"><span class="fa-stack text-info align-top"><i class="fas fa-circle fa-stack-2x"></i><i class="fab fa-stripe fa-stack-1x text-white"></i></span><span>Pago</span></span></h5><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></div><div class="modal-body"><p class="m-0 fw-bold"><span class="text-responsive">Resumen</span></p><small class="m-0"><span class="text-responsive respago"></span></small><hr><form id="form-pago"><input type="hidden" class="id" name="id" value=0 /><input type="hidden" class="email" value="" /><div class="form-group pb-3"><label><small>Valor(USD)</small></label><div class="input-group"><span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span><input type="text" name="valor" class="form-control valor" placeholder="Valor" aria-label="Valor" readonly></div></div><div class="form-group pb-3 cont_valor"><label><small></small></label><div class="input-group"><span class="input-group-text gray-700 text-white"><i class="fas fa-dollar-sign fa-fw"></i></span><input type="text" class="form-control cambio" placeholder="" aria-label="Valor" readonly></div></div><div class="w-100" style="border: 1px solid #ced4da; border-radius: 0.5rem; padding: 0.375rem 0.75rem;"><div id="card-element"></div></div><p role="alert"><span id="card-error" class="text-responsive"></span></p></form></div><div class="modal-footer"><div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button><button type="button" class="btn btn-success btn-enviar text-white"><span class="text-responsive">pagar <i class="fas fa-check-circle fa-fw"></i></span></button></div></div></div></div></div>').clone().appendTo('#modals_cont');
						$('#mod-pago .respago').html('<strong>'+$('#'+formu+' .nombre_pago').val()+'</strong><br><strong>Número de propiedades: '+$('#'+formu+' .propiedades').val()+'</strong><br><strong>Número de meses: '+$('#'+formu+' .meses').val()+'</strong>');
						$('#mod-pago .btn-enviar').prop('disabled', true);
						card = false;
						if(card){
							card.unmount();
						}
						$('#mod-pago').one('shown.bs.modal', function() {
							$('#mod-pago .valor').val($('#'+formu+' .valor').val());
							$('#mod-pago .cont_valor').hide();
							$('#form-pago .email').val($('#'+formu+' .email_user').val());
							<?php if($monob->moneda != 'USD'){ ?>
							$('#mod-pago .cont_valor label small').html('Valor(<?php echo $monob->moneda ?>)');
							$('#mod-pago .cont_valor').show();
							setCambio('form-pago', <?php echo $monob->valor ?>);
							<?php } ?>
							defPay(formu, $('#'+formu+' .id_pago').val(), $('#'+formu+' .propiedades').val(), $('#'+formu+' .meses').val(), $('#'+formu+' .id_user').val());
						});
						$('#mod-pago').one('hidden.bs.modal', function() {
							$('#mod-pago').remove();
							if($('#'+formu+' .id_pasarelan').val() == ''){
								$('#'+formu+' .tipo_pagon').val('').trigger('change');
							}
						});
						openData('pago');
						loaderShow();
                    }					
				}				
			}
			function defPay(formu, plan, numero, meses, iduser){				
				var purchase = {
					'id': plan,
					'numero': numero,
					'meses': meses,
					'tipo': 'plan',
					'id_user': iduser,
					'email': $('#form-pago .email').val(),
					'idveruser': login
				};
				fetch("controllers/create.php?v="+Math.floor((Math.random() * 1000) + 1), {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify(purchase)
				}).then(function(result) {
					return result.json();
				}).then(function(data) {
					var elements = stripe.elements();
					var style = {
						base: {
							color: "#212529",
							fontFamily: 'Poppins, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
							fontSmoothing: "antialiased",
							fontSize: "12px",
							"::placeholder": {
								color: "#9CA2AA"
							}
						},
						invalid: {
							fontFamily: 'Poppins, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
							color: "#F93154",
							iconColor: "#F93154"
						}
					};					
					if(!card){						
						card = elements.create("card", { style: style });
					}				
					card.mount("#card-element");
					card.on("change", function (event) {						
						if(event.complete){
							$('#mod-pago .btn-enviar').prop('disabled', false);
						}else{
							$('#mod-pago .btn-enviar').prop('disabled', true);
						}
						//document.querySelector("#mod-pago .btn-enviar").disabled = event.empty;
						document.querySelector("#card-error").textContent = event.error ? event.error.message : "";
					});
					var form = document.getElementById("form-pago");
					$('#mod-pago .btn-enviar').click(function(event) {
						event.preventDefault();
						payWithCard(stripe, card, data.clientSecret);
					});
					loaderHide();
				});
				var payWithCard = function(stripe, card, clientSecret) {
					if($('#form-pago').parsley().validate()){
						$('#mod-pago .btn-enviar').prop('disabled', true);
						loaderShow();
						stripe.confirmCardPayment(clientSecret, {
							receipt_email: $('#form-pago .email').val(),
							payment_method: {
								card: card,
								billing_details: {
                                  name: $('#'+formu+' .usuario').val()
                                }
							}
						}).then(function(result) {
							if (result.error) {
								loaderHide();
								card.clear();
								showError(result.error.message);
							}else{
								orderComplete(result.paymentIntent.id);
							}							
						});
					}
				};
				var orderComplete = function(paymentIntentId){
					$('#'+formu+' .id_pasarelan').val(paymentIntentId);
					$('#'+formu+' .estado').val(1);
					$('#mod-pago').one('hidden.bs.modal', function() {
                        Valform(formu, reLoadTable, ['planes', false], true, false, 10);
                    });
					$('#mod-pago').modal('hide');					
				};
			}
			function setpagEsta(formu){
				if($('#'+formu+' .estado').val() != '1'){
					$('#'+formu+' .id_pasarelan').attr("required", false);
				}else{
					$('#'+formu+' .id_pasarelan').attr("required", true);
				}
			}
        </script>
    </div>
</body>
</html>
<?php
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>