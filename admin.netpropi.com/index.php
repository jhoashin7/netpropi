<?php
header('X-Frame-Options: DENY');
include 'controllers/db_connect.php';
$rand = mt_rand();
$consulta = "SELECT publico FROM stripe ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $stripe = mysqli_fetch_object( $result );    
    $result->close();
}
$mapas = false;
$consulta = "SELECT codigo FROM mapas ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $mapas = mysqli_fetch_object( $result );    
    $result->close();
}
$google = false;
$consulta = "SELECT codigo FROM analytics ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $google = mysqli_fetch_object( $result );    
    $result->close();
}
$hubspot = false;
$consulta = "SELECT codigo FROM hubspot ORDER BY id ASC LIMIT 1";
if ( $result = $mysqli->query( $consulta ) ) {
    $hubspot = mysqli_fetch_object( $result );    
    $result->close();
}
?>
<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Netpropi">
    <meta name="theme-color" content="#9ca2aa"/>
    <meta name = "viewport" content = "width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="shortcut icon" href="images/icons/icono.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="180x180" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="76x76" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="152x152" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="apple-touch-icon" sizes="58x58" href="images/icons/ic_192m.png?v=<?php echo $rand ?>"/>
	<link rel="icon" sizes="192x192" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
	<link rel="icon" sizes="128x128" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
	<link rel="icon" sizes="32x32" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
	<link rel="icon" sizes="16x16" href="images/icons/ic_192m.png?v=<?php echo $rand ?>">
    <link href="images/icons/splashscreens/iphone5_splash.png?v=<?php echo $rand ?>" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphone6_splash.png?v=<?php echo $rand ?>" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphoneplus_splash.png?v=<?php echo $rand ?>" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphonex_splash.png?v=<?php echo $rand ?>" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphonexr_splash.png?v=<?php echo $rand ?>" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/iphonexsmax_splash.png?v=<?php echo $rand ?>" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipad_splash.png?v=<?php echo $rand ?>" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipadpro1_splash.png?v=<?php echo $rand ?>" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipadpro3_splash.png?v=<?php echo $rand ?>" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="images/icons/splashscreens/ipadpro2_splash.png?v=<?php echo $rand ?>" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <title>NetPropi - Administrador</title>
    <link rel="manifest" href="manifest.json">    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
    <link href="css/icomoon.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" integrity="sha256-B3d+vgSqnO/vLowOZhhkLiYqTSLFv8+P9WBVr3IP80o=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap4-duallistbox@4.0.2/dist/bootstrap-duallistbox.min.css" integrity="sha256-buHEe+156Hk0w29lJZctyXXfZl4mb8OFe1M6QfDanMs=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.css" integrity="sha256-RXPAyxHVyMLxb0TYCM2OW5R4GWkcDe02jdYgyZp41OU=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/plugins/confirmDate/confirmDate.css" integrity="sha256-WcAI0bCUFw5J3lXNkoQRlLI4WoUQhAhP5uMnmFZo2fE=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/codemirror@5.61.0/lib/codemirror.css" integrity="sha256-0wqkEinay6WmMf1F6gVEv9sHx4mSggtnkAsQm1cJX7I=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/codemirror@5.61.0/theme/monokai.css" integrity="sha256-JGPcb9kgGaDHyiqqAdAxFrKA+nxq4BvyHffBB9m2g+g=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" integrity="sha256-n3YISYddrZmGqrUgvpa3xzwZwcvvyaZco0PdOyUKA18=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/css/fileinput.min.css" integrity="sha256-szDlNFIcJxlNdyPGmATPgF6uKIF1H2NBkuJWYcEmveY=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/themes/explorer-fas/theme.min.css" integrity="sha256-+tjjsfGUJBjof9SD+0L4ljplK5Cp4QvhOX4QyTNYtgI=" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/fh-3.1.8/r-2.2.7/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2@2.0.8/src/spectrum.css" integrity="sha256-iOD8YCPdWyUFZM7U7VwvzX41SLL5BrGBRG3Ituwbkyg=" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css?v=<?php echo $rand ?>">
</head>
<?php
    if($google && strlen($google->codigo) > 7){
?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo $google->codigo ?>', 'auto');
ga('send', 'pageview');
</script>
<?php    
    }
?>
<?php
    if($hubspot){
?>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/<?php echo $hubspot->codigo ?>.js"></script>
<!-- End of HubSpot Embed Code -->  
<?php    
    }
?>
<body>
    <div id="loader" class="position-fixed w-100 h-100 flex-column justify-content-center">
        <div class="drop_cont mt-auto text-center position-relative" style="height: 30%">
            <div class="position-relative d-inline-block drop_anime h-100 mx-auto"><img src="images/base_drop.png" style="height: 100%; width: auto" /></div>
			<div class="position-absolute w-100" style="overflow: hidden; bottom: 0px; left: 0px; opacity: 0.6">
				<img src="images/ola.png" class="img-fluid" style="opacity: 0" />
				<div class="ola-left position-absolute w-100" style="bottom: 0px; left: -100%">
					<img src="images/ola.png" class="img-fluid" />
				</div>
				<div class="ola-right position-absolute w-100" style="bottom: 0px; right: -100%; opacity: 0.6">
					<img src="images/ola.png" class="img-fluid" />
				</div>
			</div>			
		</div>
		<div class="water_cont position-relative" style="height: 0%">
			<div class="w-100 h-100 d-flex justify-content-center" style="background-color: rgba(0,0,0,0.6)">
				<div class="pt-4">					
					<div class="fa-3x text-center mb-n2"><i class="fas fa-circle-notch fa-spin" style="color: rgba(255,255,255,0.5)"></i></div>
					<small class="fw-bold" style="color: rgba(255,255,255,0.5)">cargando</small>
				</div>				
			</div>
		</div>
    </div>
    <header id="barra_nav" class="fixed-top w-100 bg-white border-bottom border-light d-flex justify-content-between align-items-center fitCont py-2">
        <div class="text-start ps-3">
            <img src="images/logo.png" alt="beAR.io" class="img-fluid" style="max-width: 110px"/>
        </div>
        <div class="d-inline-flex justify-content-end align-items-center ps-1 pe-3">
            <a href="#" class="install-bt mr-1 text-decoration-none" title="Instalar" onclick="return false">
                <span class="text-responsive">
                    <span class="fa-stack fa-lg align-middle">
                        <i class="fas fa-circle fa-stack-2x text-muted"></i>
                        <i class="fas icon-add_to_queue fa-lg fa-stack-1x text-white"></i>
                    </span> 
                </span>                               
            </a>
            <a href="#" class="full_screen-bt mr-1 text-decoration-none" title="FullScreen" onclick="$(document).toggleFullScreen(); return false">
                <span class="text-responsive">
                    <span class="fa-stack fa-lg align-middle">
                        <i class="fas fa-circle fa-stack-2x text-muted"></i>
                        <i class="fas icon-fullscreen fa-lg fa-stack-1x text-white"></i>
                    </span>
                </span>                
            </a>
            <a href="#" class="mr-1 text-decoration-none" title="Reload" onclick="reloader(); return false">
                <span class="text-responsive">
                    <span class="fa-stack fa-lg align-middle">
                        <i class="fas fa-circle fa-stack-2x text-muted"></i>
                        <i class="fas icon-sync fa-lg fa-stack-1x text-white"></i>
                    </span>
                </span>                
            </a>
            <div class="dropdown">
                <a class="dropdown-toggle dropdown-noarrow d-inline-block position-relative text-decoration-none" title="Alertas" href="#" id="dropdownNav" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-bs-reference="parent">
                    <span class="text-responsive">
                        <span class="fa-stack fa-lg align-middle">
                            <i class="fas fa-circle fa-stack-2x text-muted"></i>
                            <i class="fas fa-bell fa-stack-1x text-white"></i>
                        </span>
                    </span>                   
                    <span class="alertas-total badge rounded-pill bg-danger position-absolute top-0 end-0">0</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end alertas-cont" aria-labelledby="dropdownNav">
                    <h6 class="dropdown-header text-center">ALERTAS</h6>
                    <div class="dropdown-divider"></div>
                    <p class="my-0 pb-2 text-center alertas-no"><small>No hay alertas.</small></p>
                </div>
            </div>            
        </div>
    </header>
    <div id="left_bar" class="log-in position-fixed h-100 top-0 start-0" style="z-index: 100">
        <div class="position-relative h-100">
            <div class="position-fixed h-100 over-black vw-100 top-0 start-0" onClick="closeSubMenu(); return false" style="background-color: rgba(0,0,0,0.6)"></div>
            <div class="position-absolute h-100 second_menu men_hide gray-200 top-0">
                <div class="position-absolute d-inline-flex justify-content-end align-items-center top-0 end-0 p-1 ps-3 me-n4 gray-200 rounded-pill">
                    <a href="#" class="text-decoration-none text-muted" title="Cerrar" onClick="closeSubMenu(); return false">
                        <i class="fas fa-times-circle fa-lg"></i>
                    </a>
                </div>
                <div class="position-relative d-flex flex-column h-100 w-100 overflow-auto px-2">
					<div class="d-flex justify-content-start align-items-center text-muted w-100 ps-4 py-1" style="height: 29px"><small class="fw-bold items-nombre"><span class="text-responsive"></span></small></div>
					<div class="d-flex flex-column w-100 items_menu"></div>
				</div>
            </div>
            <div class="position-relative h-100 gray-300 border-right border-light main_menu mmen_show">
                <div class="d-flex justify-content-center align-items-end flex-column h-100 overflow-auto">
                    <div class="w-100 justify-content-center align-items-start flex-column px-2">
						<div class="d-flex justify-content-center align-items-center text-muted py-1" style="height: 29px">
                            <small class="fw-bold"><span class="text-responsive">menú</span></small></div>
						<div class="d-flex flex-column w-100 items_menu">
						</div>
					</div>
                    <div class="mt-auto w-100 border-top border-light text-center px-2">
                        <a href="#" class="d-block my-2 text-decoration-none text-muted" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-trigger="hover" data-fallpla="right,left" title="Mi Perfil" onClick="openSecondMenu(['perfil', 'Mi Perfil', 'fas fa-user-cog'], true); return false">
                            <span class="text-responsive">
                                <span class="fa-stack fa-lg align-middle">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-user-cog fa-stack-1x text-white"></i>
                                </span>
                            </span>							
						</a>
                    </div>
                </div>
                <div class="position-absolute d-inline-flex justify-content-end align-items-center top-0 end-0 p-1 ps-0 me-n4 gray-300 border-top border-bottom border-right border-light" style="border-radius: 0rem 50rem 50rem 0rem; margin-top: -1px; padding-left: 3px !important;">
                    <a href="#" class="text-decoration-none text-muted" title="Cerrar" onClick="toggleMainMenu(); return false">
                        <i class="fas fa-chevron-circle-left icon_main_menu fa-lg"></i>
                    </a>
                </div>                
            </div>
        </div>
    </div>
    <main id="main_c" class="d-flex w-100 flex-column justify-content-center align-items-center fitCont" style="min-height: 100vh">
		<div id="m_cont" class="w-100 mb-auto" style="position: relative; z-index: 20">
			<div></div>
		</div>
        <!-- <div id="pie_pagina" class="d-flex justify-content-end align-items-center w-100 py-2 position-relative" style="z-index: 30">
            <div class="pe-2">
                <small class="text-muted text-nowrap"><span class="text-responsive">powered by <i class="fas icon-mariposa fa-lg fa-fw"></i><span class="fw-bolder">flyData</span> engine</span></small>
            </div>
            <div class="pe-3">
                <a href="#" class="d-flex position-relative justify-content-center align-items-center text-decoration-none gray-700 rounded-pill cont_clock" title="Mostrar" style="height: 30px" onClick="showClock(); return false">
                    <div class="ps-2 pe-1 clock_hide">
                        <div class="tiempo_img text-white"></div>
                    </div>
                    <div class="pe-1 clock_hide">
                        <small class="text-uppercase text-nowrap text-white clock" style="font-size: 70%">00:00 --</small>
                    </div>
                    <div class="h-100 p-1 position-relative">
                        <div id="time_min" class="h-100" style="width: 22px">
                            <div id="time_hr" class="position-absolute" style="width: 14px; height: 14px; top: 8px; left: 8px"></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>		 -->
	</main>
    
    <div id="modals_cont"></div>
    
    <div class="offcanvas offcanvas-top bg-danger overflow-hidden" tabindex="-1" id="off-error" aria-labelledby="Error">
        <div class="offcanvas-header justify-content-between">
            <h4 class="position-absolute m-0" style="top: -1.5rem; right: -3rem"><i class="fas icon-budita fa-fw fa-10x" style="color: rgba(255,255,255,0.2)"></i></h4>
            <h5 class="offcanvas-title text-uppercase fw-bolder text-white position-relative m-0"><span class="text-responsive">Error</span></h5>
            <button type="button" class="btn btn-close btn-link text-decoration-none text-white position-relative" data-bs-dismiss="offcanvas" aria-label="Close"><h2 class="m-0"><span class="text-responsive"><i class="fas fa-times-circle"></i></span></h2></button>
        </div>
        <div class="offcanvas-body">
            <div class="position-relative text-white"><span class="error_msg text-responsive"></span></div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-showvis" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-warning align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-mail-bulk fa-stack-1x text-white"></i>
                            </span>                                
                            <span class="lh-1">Visitas <small class="text-muted d-block"></small></span>
                        </span>                            
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">                    
                </div>
                <div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mod-ses" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-warning align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-mail-bulk fa-stack-1x text-white"></i>
                            </span>                                
                            <span class="lh-1">Sesiones<small class="text-muted d-block"></small></span>
                        </span>                            
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <hr>
					Total tiempo (hh:mm:ss): <strong class="total_ses"></strong>
                </div>
                <div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="mod-user" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-warning align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-users fa-stack-1x text-white"></i>
                            </span>                                
                            <span class="lh-1">Seleccionar usuario</span>
                        </span>                            
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="form-user">
						<input type="hidden" class="data_user" value=""/>
					</form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mod-sesus" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-warning align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-mail-bulk fa-stack-1x text-white"></i>
                            </span>                                
                            <span class="lh-1">Sesiones<small class="text-muted d-block"></small></span>
                        </span>                            
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <hr>
					Total tiempo (hh:mm:ss): <strong class="total_ses"></strong>
                </div>
                <div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mod-eliminar" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-danger align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-trash-alt fa-stack-1x text-white"></i>
                            </span>                                
                            <span>Eliminar Registro</span>
                        </span>                            
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="eliminar" value=0 />
                    <p class="m-0"><span class="text-responsive">¿Estas seguro de eliminar este registro y sus dependencias?</span></p>
                </div>
                <div class="modal-footer">
                    <div class="btn-group d-flex w-100" role="group" aria-label="Acciones">
                        <button type="button" class="btn btn-secondary text-white" data-bs-dismiss="modal"><span class="text-responsive"><i class="fas fa-times-circle fa-fw"></i> cerrar</span></button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal" onClick="$('#mod-eliminar .eliminar').val(1); return false"><span class="text-responsive">eliminar <i class="fas fa-trash-alt fa-fw"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mod-video" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <span class="text-responsive d-flex justify-content-start align-items-center">
                            <span class="fa-stack text-muted align-top">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-play-circle fa-stack-1x text-white"></i>
                            </span>                                
                            <span class="lh-1 titulo fw-bold">Video</span>
                        </span>                            
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="d-block w-100 video_p text-center ratio ratio-16x9">
                    </div>
                </div>                
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="error_mod" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-danger overflow-hidden">
				<div class="modal-header border-0 py-3">
					<i class="fas icon-budita fa-fw fa-9x position-absolute" style="color: rgba(255,255,255,0.2); top: -0.5rem; left: -3rem"></i>
                    <a href="#" class="text-decoration-none position-absolute text-white" data-bs-dismiss="modal" onClick="return false" style="right: 0.5rem; top: 0.5rem">
                        <i class="fas fa-times-circle"></i>
                    </a>					
				</div>
				<div class="modal-body pl-5">					
					<div class="position-relative text-white"><span class="error_msg text-responsive"></span></div>
				</div>				
			</div>
		</div>
	</div>
    
    <div class="modal fade" id="good_mod" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-success overflow-hidden">
				<div class="modal-header border-0 py-3">
					<i class="fas icon-buda fa-fw fa-9x position-absolute" style="color: rgba(255,255,255,0.2); top: -0.5rem; left: -3rem"></i>
                    <a href="#" class="text-decoration-none position-absolute text-white" data-bs-dismiss="modal" onClick="return false" style="right: 0.5rem; top: 0.5rem">
                        <i class="fas fa-times-circle"></i>
                    </a>					
				</div>
				<div class="modal-body pl-5">					
					<div class="position-relative text-white"><span class="good_msg text-responsive"></span></div>
				</div>				
			</div>
		</div>
	</div>
    
    <div class="modal fade" id="procc_mod" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-warning overflow-hidden">
				<div class="modal-header border-0 py-3">
					<i class="fas fa-circle-notch fa-spin fa-fw fa-9x position-absolute" style="color: rgba(255,255,255,0.2); top: -0.5rem; left: -3rem"></i>
				</div>
				<div class="modal-body pl-5">					
					<div class="position-relative text-white"><span class="procc_msg text-responsive"></span></div>
				</div>				
			</div>
		</div>
	</div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-ui-touch-punch@0.2.3/jquery.ui.touch-punch.min.js" integrity="sha256-AAhU14J4Gv8bFupUUcHaPQfvrdNauRHMt+S4UVcaJb0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-fullscreen-plugin@1.1.5/jquery.fullscreen.js" integrity="sha256-rkIE81gwpMsNyKpuGlgG8T9fp/MvHa6ucB655PuOBik=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/print-this@1.15.1/printThis.js" integrity="sha256-YP43QBno6k4w5t514jReQtD0TAqMjGAiec4dp58LaZY=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/parsley.min.js" integrity="sha256-pEdn/pJ2tyT37axbEIPkyUUfuG1yXR0+YV+h+jphem4=" crossorigin="anonymous"></script>
    <script src="js/extra/comparison.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/dateiso.js" integrity="sha256-udsaGJFsLl5CBWh1W9XZhVJoRlh40ib7xvib9Du68Ps=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/luhn.js" integrity="sha256-mwT7OcVKnfgVEkJR4i5+TOqDkV/4r4ybb8KOUDFoclI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/notequalto.js" integrity="sha256-OlUSGfTtCU1bsBAyrxUei5kCg82cb81ykk1v8+e/jkE=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/src/extra/validator/words.js" integrity="sha256-2utXQtk3tGxBIzkmNPJi8vJURbO1btmdY0I+nlek1aQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/i18n/es.js" integrity="sha256-dcJkxln8t7jxoFFA4jP3/ru8rFOlKpt478JM/wsMsgU=" crossorigin="anonymous"></script>
    <script src="js/i18n/es.extra.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous"></script>    
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.js" integrity="sha256-AkQap91tDcS4YyQaZY2VV34UhSCxu2bDEIgXXXuf5Hg=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/l10n/es.js" integrity="sha256-G5b/9Xk32jhqv0GG6ZcNalPQ+lh/ANEGLHYV6BLksIw=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/plugins/confirmDate/confirmDate.js" integrity="sha256-PdR2doyoDlR7G8IzjCJaBJEOqhyEOedRlokldFdxPuY=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/codemirror@5.61.0/lib/codemirror.js" integrity="sha256-uRc1Mmq6VPUBuVMZV1qLPrDCvPQvfO45owxzsZ4qHFU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/codemirror@5.61.0/mode/xml/xml.js" integrity="sha256-0Yg9o24xI4hYyfU8U7GzhNFz9xmI/LTyK9H7a7GVWHY=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js" integrity="sha256-oOIhv6MPxuIfln8IN7mwct6nrUhs7G1zvImKQxwkL08=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/lang/summernote-es-ES.min.js" integrity="sha256-tt0C0r6dzHN/y0TJ0f7fKRK2c2BXXcrNFO7EGOdxuQI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/js/plugins/piexif.min.js" integrity="sha256-WYoKe0uREimiMKk7Z5ocKDhOubCqP3qHxmC4gXcMutk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/js/fileinput.min.js" integrity="sha256-RBRWMVrgmfIx8hdOUye+1cQuUkb/lmea4qlkqnnEsWM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/themes/explorer-fas/theme.min.js" integrity="sha256-N+j9ZK7htAf9TM7qTUMEnOlyefABPdmDMRiz65N/zi8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/themes/fas/theme.min.js" integrity="sha256-QBBS6qp56s19FjROArAI5cC/C9zKP7Wyznmc+owuiGk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-fileinput@5.2.1/js/locales/es.js" integrity="sha256-7rIYI1hXx7Cu0o94OMKMrb87RH/1zWX93tm2BnBO7FU=" crossorigin="anonymous"></script>    
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.1.1/dist/chart.min.js" integrity="sha256-lISRn4x2bHaafBiAb0H5C7mqJli7N0SH+vrapxjIz3k=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap4-duallistbox@4.0.2/dist/jquery.bootstrap-duallistbox.min.js" integrity="sha256-buHtmjGInogczQRRz4JQC5/SyMzUAsl28puSCaOgvMU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.js" integrity="sha256-icjghcPaibMf1jv4gQIGi5MeWNHem2SispcorCiCfSg=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/fh-3.1.8/r-2.2.7/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.2.2/circle-progress.min.js" integrity="sha512-6kvhZ/39gRVLmoM/6JxbbJVTYzL/gnbDVsHACLx/31IREU4l3sI7yeO0d4gw8xU5Mpmm/17LMaDHOCf+TvuC2Q==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/spectrum-colorpicker2@2.0.8/dist/spectrum.min.js" integrity="sha256-mkd7mvxhNxvMs5bpRKBH37g0T9gOxiQKhiS3ql4LEHA=" crossorigin="anonymous"></script>
    <script src="js/intro.js"></script>
	<script src="https://js.stripe.com/v3/"></script>
    <script src="js/custom.js?v=<?php echo mt_rand() ?>" id="customjs" data-str="<?php echo $stripe->publico ?>"></script>
    <script src="js/main.js?v=<?php echo mt_rand() ?>"></script>	
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=es-419" async defer></script>
    <script src="https://sdk.mercadopago.com/js/v2"></script>
	<?php
	if($mapas){
	?>
	<script async src="https://maps.googleapis.com/maps/api/js?key=<?php echo $mapas->codigo ?>&callback=initMap"></script>
	<?php	
	}
	?>
	<script>
		<?php
        if($google && strlen($google->codigo) > 7){
        ?>
        googlea = true;
        <?php
        }
        ?>
		<?php
        if($hubspot){
        ?>
        hubspot = true;
        <?php
        }
        ?>
	</script>
</body>
</html>
<?php
$mysqli->close();
?>