<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';
require_once('stripe-php-master/init.php');

$json_str = file_get_contents('php://input');
$json_obj = json_decode($json_str);
$valor = false;
$id = $json_obj->id;

$id_veris = explode('***', simple_crypt( $json_obj->idveruser, 'd', $conArr['enc_string'] ));
if($id_veris[0] == $conArr['enc_string'] && is_numeric($id_veris[1])){
	$consulta = "SELECT secreto FROM stripe WHERE id = 1 ORDER BY id ASC LIMIT 1";
	if ( $result = $mysqli->query( $consulta ) ) {
		$stripes = mysqli_fetch_object( $result );    
		$result->close();
	}
	if($json_obj->tipo == 'plan'){
		$consulta = "SELECT valor FROM planes WHERE valor > 0 AND (beneficiario = 0 OR beneficiario = " . $json_obj->id_user . ") AND minimo <= ".$json_obj->numero." ORDER BY valor ASC LIMIT 1";
		if ( $result = $mysqli->query( $consulta ) ) {
			$valores = mysqli_fetch_object( $result );
			$valor = ($valores->valor)?(($valores->valor * $json_obj->numero) * $json_obj->meses) * 100:false;
			$result->close();
		}		
	}
	if($json_obj->tipo == 'contrato'){
		$consulta = "SELECT valor FROM contratos WHERE id = ".$json_obj->id;
		if ( $result = $mysqli->query( $consulta ) ) {
			$valores = mysqli_fetch_object( $result );
			$valor = ($valores->valor)?($valores->valor) * 100:false;
			$result->close();
		}		
	}
	if($json_obj->tipo == 'pago'){
		$consulta = "SELECT valor FROM pagos WHERE id = ".$json_obj->id." AND id_user = " . $json_obj->id_user . " LIMIT 1";
		if ( $result = $mysqli->query( $consulta ) ) {
			$valores = mysqli_fetch_object( $result );
			$valor = ($valores->valor)?$valores->valor * 100:false;
			$result->close();
		}
		
	}
	$stripe = new \Stripe\StripeClient($stripes->secreto);
	//\Stripe\Stripe::setApiKey($stripes->secreto);
	
	header('Content-Type: application/json');
	try{
		$paymentIntent = $stripe->paymentIntents->create([
			'amount' => $valor,
			'currency' => 'usd',
			'receipt_email' => $json_obj->email
		]);
		$output = [
			'clientSecret' => $paymentIntent->client_secret,
		];
		echo json_encode($output);
	}catch (Error $e){
		http_response_code(500);
		echo json_encode(['error' => $e->getMessage()]);
	}
}else{
	header('Content-Type: application/json');
	http_response_code(500);
	echo json_encode(['error' => 'Intento inseguro']);
}
$mysqli->close();
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}