<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            // $moneda = $_POST['moneda'];
			$fecha = date('Y-m-d');
            $filtro = '`pag`.`id` != 0';
			if($_POST['rol'] == 5){
				$filtro .= ' AND `plan`.`id_user` = '.$id_veri[1];
			}

            // DB table to use
            $table = 'plan';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
				array(
                    'db' => 'CONCAT(UPPER(`pag`.`referencia`),"-",LPAD(`pag`.`id`,6,0))',
                    'dt' => 0,
                    'field' => 'refer',
					'as' => 'refer'
                ),
                array(
                    'db' => '`pag`.`nombre_pago`',
                    'dt' => 1,
                    'field' => 'nombre_pago'
                ),                
                array(
                    'db' => 'CONCAT((`pag`.`moneda` )," - $",(`pag`.`valor`))',//.'ROUND((`pag`.`valor`), 2)',
                    'dt' => 2,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
                array(
                    'db' => '`user`.`nombre`',
                    'dt' => 3,
                    'field' => 'nombre'
                ),
				array(
                    'db' => '`user`.`iden`',
                    'dt' => 4,
                    'field' => 'iden'
                ),
				array(
                    'db' => 'CONCAT(COALESCE(`prop`.`total`, 0),"/",`plan`.`propiedades`)',
                    'dt' => 5,
                    'field' => 'propiedad',
					'as' => 'propiedad'
                ),
				array(
                    'db' => '`plan`.`fin`',
                    'dt' => 6,
                    'field' => 'fin',
                    'formatter' => function ( $d, $row ) use ($fecha) {
						$limite = date('Y-m-d', strtotime('-10 days', strtotime($d)));						
						return ($row[7] == 3)?'<span class="text-info">'.$d.'</span>':(($limite <= $fecha || $row[7] == 2 || $row[7] == 3)?'<span class="text-danger">'.$d.'</span>':$d);
					}
                ),
				array(
                    'db' => '`plan`.`estado`',
                    'dt' => 7,
                    'field' => 'estado',
                    'formatter' => function ( $d, $row ) {
						return ($d == 1)?'<span class="text-success fw-bold">Activo</span>':(($d == 2)?'<span class="text-danger fw-bold">Finalizado</span>':(($d == 3)?'<span class="text-info fw-bold">Renovado</span>':'<span class="text-primary fw-bold">Pendiente</span>'));
					}
                ),				
                array(
                    'db' => '`plan`.`creado`',
                    'dt' => 8,
                    'field' => 'creado'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 9,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),                	
                array(
                    'db' => '`plan`.`id`',
                    'dt' => 10,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) use ($fecha) {
						if($row[7] != 3 && $row[7] != 0){
							if($row[13] != 0){
								return '<span class="text-primary fw-bold">Renovación en trámite</span>';
							}else{
								return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="renewPlan(' . $row[12] . ', '.$row[11].'); return false"><span class="d-none d-sm-inline">renovar</span> <i class="fas fa-retweet fa-fw"></i></button></div>';
							}
						}else{
							return '';
						}
                    }
                ),
                array(
                    'db' => '`plan`.`id_user`',
                    'dt' => 11,
                    'field' => 'id_user'
                ),
                array(
                    'db' => '`plan`.`id_pago`',
                    'dt' => 12,
                    'field' => 'id_pago'
                ),
                array(
                    'db' => 'COALESCE(`planre`.`renew`, 0)',
                    'dt' => 13,
                    'field' => 'renew',
					'as' => 'renew'
                ),
                array(
                    'db' => '`pag`.`moneda`',
                    'dt' => 14,
                    'field' => 'moneda'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `plan` AS `plan`
			LEFT JOIN `pagos` AS `pag` ON (`pag`.`id` = `plan`.`id_pago`) 
            LEFT JOIN (SELECT plan_id, COUNT(id) AS total FROM propiedades WHERE estado = 1 OR estado = 2 GROUP BY plan_id) AS `prop` ON (`prop`.`plan_id` = `plan`.`id`) 
			LEFT JOIN (SELECT referido AS renew FROM plan WHERE estado = 0 GROUP BY referido) AS `planre` ON (`planre`.`renew` = `plan`.`id`) 
			LEFT JOIN `admins` AS `usad` ON (`usad`.`id` = `plan`.`id_user`)
			LEFT JOIN `usuarios` AS `user` ON (`user`.`id` = `usad`.`relacion`)
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `plan`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>