<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $filtro = '`user`.`rol` = 5';
			$referido = $_POST['refe'];

            // DB table to use
            $table = 'admins';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`user`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`usdata`.`iden`',
                    'dt' => 1,
                    'field' => 'iden'
                ),
                array(
                    'db' => '`user`.`email`',
                    'dt' => 2,
                    'field' => 'email'
                ),
                array(
                    'db' => '`usdata`.`celular`',
                    'dt' => 3,
                    'field' => 'celular'
                ),
                array(
                    'db' => '`usdata`.`pais`',
                    'dt' => 4,
                    'field' => 'pais',
                    'formatter' => function ( $d, $row ){
                        return ($d != 0)?$row[9]:'NA';
                    }
                ),
                array(
                    'db' => '`usdata`.`tipo`',
                    'dt' => 5,
                    'field' => 'tipo',
                    'formatter' => function ( $d, $row ){
                        return ($d != 0)?'Agente':'Particular';
                    }
                ),
                array(
                    'db' => '`usdata`.`codigo`',
                    'dt' => 6,
                    'field' => 'codigo'
                ),                	
                array(
                    'db' => '`usdata`.`creado`',
                    'dt' => 7,
                    'field' => 'creado'
                ),                              	
                array(
                    'db' => '`user`.`id`',
                    'dt' => 8,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) use ($referido) {                        
                        return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-primary btn-sm text-white" onClick="setSelUs(\'' . $d . ','.$row[0].','.$row[2].'\', \''.$referido.'\'); return false"><span class="d-none d-sm-inline">seleccionar</span> <i class="fas fa-check fa-fw"></i></button></div>';                        
                    }
                ),	
                array(
                    'db' => '`coun`.`name`',
                    'dt' => 9,
                    'field' => 'nombre_pais',
                    'as' => 'nombre_pais'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `admins` AS `user` 
			LEFT JOIN `usuarios` AS `usdata` ON (`usdata`.`id` = `user`.`relacion`)             
            LEFT JOIN `countries` AS `coun` ON (`coun`.`id` = `usdata`.`pais`)";
			

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>