<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $filtro = '';            

            // DB table to use
            $table = 'blog';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`blo`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
				array(
                    'db' => '`blo`.`url`',
                    'dt' => 1,
                    'field' => 'url',
                    'formatter' => function ( $d, $row ) use ($conArr) {
                        return '<div class="d-grid gap-2"><a href="'.$conArr['base_url_sitio'].'/'.$d.'" type="button" class="btn btn-primary btn-sm text-white" target="blank"><i class="fas fa-external-link-alt fa-fw"></i> <span class="font-weight-bold">Link Directo</span></a></div>';
                    }
                ),
				array(
                    'db' => 'IF(FIND_IN_SET("0" ,`blo`.`paises`) ,"TODOS", GROUP_CONCAT(`pais`.`lista` ORDER BY `pais`.`lista` ASC SEPARATOR ", "))',
                    'dt' => 2,
                    'field' => 'paises_lista',
					'as' => 'paises_lista'
                ),
                array(
                    'db' => '`blo`.`fecha`',
                    'dt' => 3,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 4,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`blo`.`creado`',
                    'dt' => 5,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`blo`.`id`',
                    'dt' => 6,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) {                        
                        return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-danger btn-sm text-white" onClick="delData(' . $d . ', \'blog\', \'id\', \'reLoadTable\', [\'blog\', false]); return false"><i class="fas fa-trash fa-fw"></i> <span class="d-none d-sm-inline">eliminar</span></button><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'blog\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>';                        
                    }
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `blog` AS `blo` 
			LEFT JOIN (SELECT id, name AS lista FROM countries GROUP BY id) AS `pais` ON (FIND_IN_SET(`pais`.`id`, `blo`.`paises`)) 
			LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `blo`.`editor`)";

            $extraWhere = $filtro;
			$groupBy = '`blo`.`id`';
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>