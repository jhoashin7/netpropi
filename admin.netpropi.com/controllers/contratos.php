<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
		$user = $_POST['user'];
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $moneda = $_POST['moneda'];
			$rol = $_POST['rol'];
            $filtro = ($rol == 7)?'`con`.`id_abogado` = '.$id_veri[1]:'';          

            // DB table to use
            $table = 'contratos';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`con`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`con`.`prefijo`',
                    'dt' => 1,
                    'field' => 'prefijo',
                    'formatter' => function ( $d, $row ) {
                        return '<span class="text-uppercase">'.$d.'</span>';
                    }
                ),
				array(
                    'db' => '`con`.`id_abogado`',
                    'dt' => 2,
                    'field' => 'id_abogado',
                    'formatter' => function ( $d, $row ) {
						return ($d != 0)?$row[11]:'NA';
					}
                ),
                array(
                    'db' => 'ROUND((`con`.`valor` * '.$moneda.'), 2)',
                    'dt' => 3,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
                array(
                    'db' => 'ROUND((`con`.`valor_abg` * '.$moneda.'), 2)',
                    'dt' => 4,
                    'field' => 'valor_abg',
                    'as' => 'valor_abg'
                ),
				array(
                    'db' => 'IF(FIND_IN_SET("0" ,`con`.`paises`) ,"TODOS", GROUP_CONCAT(`pais`.`lista` ORDER BY `pais`.`lista` ASC SEPARATOR ", "))',
                    'dt' => 5,
                    'field' => 'paises_lista',
					'as' => 'paises_lista'
                ),				
                array(
                    'db' => 'COALESCE(`pag`.`total`, 0)',
                    'dt' => 6,
                    'field' => 'total_pag',
                    'as' => 'total_pag'
                ),
                array(
                    'db' => '`con`.`fecha`',
                    'dt' => 7,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 8,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`con`.`creado`',
                    'dt' => 9,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`con`.`id`',
                    'dt' => 10,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) use ($conArr, $user, $rol) { 
						$preview = ($row[12] != '')?'<a href="'.$conArr['base_url'].'/test_contrato.php?veruser='.$user.'&idcont='.$d.'&ver='.random_int(100, 999).'" class="btn btn-primary btn-sm text-white" target="blank"><i class="fas fa-file-contract fa-fw"></i> <span class="d-none d-sm-inline">previsualizar</span></a>':'';
						if($rol == 7){
							return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones">'.$preview.'<button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'contratos\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>';
						}else{
							return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones">'.$preview.'<button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'contratos\', ' . $d . '); return false"><i class="fas fa-edit fa-fw"></i> <span class="d-none d-sm-inline">editar</span></button><button type="button" class="btn btn-danger btn-sm text-white" onClick="delData(' . $d . ', \'contratos\', \'id\', \'reloader\', []); return false"><span class="d-none d-sm-inline">eliminar</span> <i class="fas fa-trash fa-fw"></i></button></div>';
						}                                                
                    }
                ),
				array(
                    'db' => '`abg`.`nombre`',
                    'dt' => 11,
                    'field' => 'nombre_abg',
                    'as' => 'nombre_abg'
                ),
				array(
                    'db' => '`con`.`texto`',
                    'dt' => 12,
                    'field' => 'texto',
                    'as' => 'texto'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `contratos` AS `con` 
            LEFT JOIN (SELECT id_pago, COUNT(id) AS total FROM pagos WHERE estado = 1 AND tipo = 2 GROUP BY id_pago) AS `pag` ON (`pag`.`id_pago` = `con`.`id`) 
			LEFT JOIN (SELECT id, name AS lista FROM countries GROUP BY id) AS `pais` ON (FIND_IN_SET(`pais`.`id`, `con`.`paises`)) 
			LEFT JOIN `admins` AS `abg` ON (`abg`.`id` = `con`.`id_abogado`)
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `con`.`editor`)";

            $extraWhere = $filtro;
			$groupBy = '`con`.`id`';
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>