<?php
include 'db_connect.php';

$resp = false;
$data = array( 'msg' => 'No se enviaron correctamente tus datos. Intenta de nuevo.' );

$getvars = sanitize( $_POST );
unset( $_POST );
$querys = base64_decode(strip_tags( $mysqli->real_escape_string( $getvars[ 'querys' ] ) ));
$campo = strip_tags( $mysqli->real_escape_string( $getvars[ 'campo' ] ) );
$tabla = strip_tags( $mysqli->real_escape_string( $getvars[ 'tabla' ] ) );
$exwhere = strip_tags( $mysqli->real_escape_string( $getvars[ 'where' ] ) );
$decimales = strip_tags( $mysqli->real_escape_string( $getvars[ 'decimales' ] ) );
$sepdeci = strip_tags( $mysqli->real_escape_string( $getvars[ 'sepdeci' ] ) );
$valor = 0;
$wheref = '';
if (strpos($querys, 'WHERE') !== false) {
	$where_a = explode('WHERE ', $querys );
	$where_b = explode('ORDER BY', end($where_a));
	$where =  str_replace("\'", "'", $where_b[0]);
	$where =  str_replace("\\", "", $where);
	if($exwhere && $exwhere != ''){		
		$where = ($where != '')?$where.' AND '.$exwhere:$where.' '.$exwhere;
	}
	$wheref = ($where != '')?' WHERE '.specialChras($where):'';
}else{
	if($exwhere && $exwhere != ''){
		$wheref = ' WHERE '.specialChras($exwhere);
	}
}

$consulta = "SELECT SUM(".$campo.") AS total FROM ".$tabla.$wheref;
if ( $result = $mysqli->query( $consulta ) ) {
	$data = mysqli_fetch_object( $result );
	$valor = number_format($data->total, ($decimales && $decimales != '')?$decimales:0, ($sepdeci && $sepdeci != '')?$sepdeci:'.', '.');
	$result->close();
}


$respuesta = array( 'valor' => $valor );
$mysqli->close();
echo json_encode( $respuesta );

function cleanTexto($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars.
}

function specialChras( $input ){
	$valores = array('á','Á','é','É','í','Í','ó','Ó','ú','Ú','ñ','Ñ');
	$reemplazo = array('a','A','e','E','i','I','o','O','u','U','n','N');
	return str_replace($valores, $reemplazo, $input);
}

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}

?>