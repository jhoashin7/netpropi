<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
		$user = $_POST['user'];
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $moneda = $_POST['moneda'];
			$rol = $_POST['rol'];
            $filtro = '`bal`.`editor` = '.$id_veri[1];          

            // DB table to use
            $table = 'balance';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`bal`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`bal`.`tipo`',
                    'dt' => 1,
                    'field' => 'tipo',
                    'formatter' => function ( $d, $row ) {
                        return ($d == 1)?'<span class="fw-bold text-success">Ingreso</span>':'<span class="fw-bold text-danger">Gasto</span>';
                    }
                ),				
                array(
                    'db' => 'ROUND(((`bal`.`valor` / `cam`.`valor`) * '.$moneda.'), 2)',
                    'dt' => 2,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
                array(
                    'db' => 'IF(`bal`.`id_propiedad` = 0, "General", CONCAT(`prop`.`nombre`," - ", LPAD(`bal`.`id_propiedad`,6,0)))',
                    'dt' => 3,
                    'field' => 'origen',
                    'as' => 'origen'
                ),				
                array(
                    'db' => '`bal`.`fecha`',
                    'dt' => 4,
                    'field' => 'fecha'
                ),                	
                array(
                    'db' => '`bal`.`id`',
                    'dt' => 5,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) { 
						return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'balance\', ' . $d . '); return false"><i class="fas fa-edit fa-fw"></i> <span class="d-none d-sm-inline">editar</span></button><button type="button" class="btn btn-danger btn-sm text-white" onClick="delData(' . $d . ', \'balance\', \'id\', \'reLoadTable\', [\'balance\', false]); return false"><span class="d-none d-sm-inline">eliminar</span> <i class="fas fa-trash fa-fw"></i></button></div>';                                                
                    }
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `balance` AS `bal` 
			LEFT JOIN `cambio` AS `cam` ON (`cam`.`id` = `bal`.`moneda`) 
			LEFT JOIN `propiedades` AS `prop` ON (`prop`.`id` = `bal`.`id_propiedad`)";

            $extraWhere = $filtro;			
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>