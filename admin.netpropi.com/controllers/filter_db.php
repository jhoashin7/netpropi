<?php
include 'db_connect.php';

$resp = false;
$data = array( 'msg' => 'No se enviaron correctamente tus datos. Intenta de nuevo.' );

$getvars = sanitize( $_POST );
unset( $_POST );

$tb = strip_tags( $mysqli->real_escape_string( $getvars[ 'tb' ] ) );
$fl = strip_tags( $mysqli->real_escape_string( $getvars[ 'fl' ] ) );
$opt = strip_tags( $mysqli->real_escape_string( $getvars[ 'opt' ] ) );
$tbj = strip_tags( $mysqli->real_escape_string( $getvars[ 'tbj' ] ) );
$flr = strip_tags( $mysqli->real_escape_string( $getvars[ 'flr' ] ) );
$fln = strip_tags( $mysqli->real_escape_string( $getvars[ 'fln' ] ) );
$flnd = strip_tags( $mysqli->real_escape_string( $getvars[ 'flnd' ] ) );
$where = (array_key_exists('where', $getvars))?strip_tags( $mysqli->real_escape_string( $getvars[ 'where' ] ) ):'';
$resultados = array();
$nodef = false;
$condicion = ($where != '')?'WHERE '.$where:'';
if($opt == 'join'){	
	$consulta = "SELECT `main`.`".$fl."`, `sub`.`".$fln."` FROM `".$tb."` AS `main` 
	LEFT JOIN `".$tbj."` AS `sub` ON (`sub`.`".$flr."` = `main`.`".$fl."`) ".$condicion." GROUP BY `main`.`".$fl."` ORDER BY `sub`.`".$fln."` ASC";
	if ( $result = $mysqli->query( $consulta ) ) {
		while ($fila = $result->fetch_assoc()) {
			if($fila[$fln] != '' && !is_null($fila[$fln])){
				array_push($resultados, array(utf8_encode($fila[$fl]), utf8_encode($fila[$fln])));
			}else{
                if(!$nodef){
                    array_push($resultados, array(0, $flnd));
                    $nodef = true;
                }				
			}			
		}		
		$result->close();
	}	
}else{
	$consulta = "SELECT `main`.`".$fl."` FROM `".$tb."` AS `main` ".$condicion." GROUP BY `main`.`".$fl."` ORDER BY `main`.`".$fl."` ASC";		
	if ( $result = $mysqli->query( $consulta ) ) {
		while ($fila = $result->fetch_assoc()) {			
			if($fila[$fl] != '' && !is_null($fila[$fl])){				
				array_push($resultados, array(utf8_encode($fila[$fl]), utf8_encode($fila[$fl])));
			}else{				
                if(!$nodef){
                    array_push($resultados, array(0, $flnd));
                    $nodef = true;
                }
			}			
		}		
		$result->close();
	}
}


$respuesta = array( 'data' => $resultados );
$mysqli->close();
echo json_encode( $respuesta );

function cleanTexto($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars.
}

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}

?>