<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';

$getvars = sanitize( $_POST );
unset( $_POST );
$alertas ='';
date_default_timezone_set('America/Bogota');
$fecha_10 = date("Y-m-d", strtotime('-10 days'));
$fecha_hoy = date('Y-m-d H:i:s');
$fecha_1 = date('Y-m-d H:i:s', strtotime("+1 days"));
$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ), 'd', $conArr['enc_string'] ));
if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
	if($getvars[ 'rol' ] <= 4 && $getvars[ 'rol' ] > 0){
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM pagos WHERE estado = 0";
		if ( $result = $mysqli->query( $consulta ) ) {
			$res = mysqli_fetch_object( $result );
        	$result->close();
			if($res->total > 0){
				$alertas .= '<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 alertas-no" onclick="loader(\'movimientos\'); return false">pagos pendientes <span class="badge bg-danger">'.$res->total.'</span></button>';
			}
		}
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM contacto WHERE estado = 0";
		if ( $result = $mysqli->query( $consulta ) ) {
			$res = mysqli_fetch_object( $result );
        	$result->close();
			if($res->total > 0){
				$alertas .= '<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 alertas-no" onclick="loader(\'contactos\'); return false">mensajes pendientes <span class="badge bg-danger">'.$res->total.'</span></button>';
			}
		}
	}
	if($getvars[ 'rol' ] >= 6){
		$total_nov = 0;
		$consulta = "SELECT id FROM novedades WHERE estado = 0 AND referente = 0 AND id_responsable = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
            while($row = $result->fetch_assoc()){
				$consulta2 = "SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE vista = 0 AND id_user = editor AND (referente = ".$row['id']." OR id = ".$row['id'].")";
				if ( $result2 = $mysqli->query( $consulta2 ) ) {
					$res = mysqli_fetch_object( $result2 );
        			$result2->close();
					if($res->total > 0){
						$total_nov = $total_nov + $res->total;
					}
				}				
            }
            $result->close();
        }
		$alertas .= ($total_nov > 0)?'<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 alertas-no" onclick="loader(\'misnovedades\'); return false">novedades <span class="badge bg-danger">'.$total_nov.'</span></button>':'';
	}
	if($getvars[ 'rol' ] == 5){
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM pagos WHERE estado = 0 AND id_user = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
			$res = mysqli_fetch_object( $result );
        	$result->close();
			if($res->total > 0){
				$alertas .= '<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 alertas-no" onclick="loader(\'estado_cuenta\'); return false">pagos pendientes <span class="badge bg-danger">'.$res->total.'</span></button>';
			}
		}
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM plan WHERE fin < '".$fecha_10."' AND estado != 3 AND estado != 0 AND id_user = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
			$res = mysqli_fetch_object( $result );
        	$result->close();
			if($res->total > 0){
				$alertas .= '<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 text-nowrap alertas-no" onclick="loader(\'propiedades.misplanes\'); return false">plan limite vencimiento <span class="badge bg-danger">'.$res->total.'</span></button>';
			}
		}
		$consulta = "SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE vista = 0 AND tipo = 3 AND id_user = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
			$res = mysqli_fetch_object( $result );
        	$result->close();
			if($res->total > 0){
				$alertas .= '<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 text-nowrap alertas-no" onclick="loader(\'propiedades.propiedad\'); return false">nuevos interesados <span class="badge bg-danger">'.$res->total.'</span></button>';
			}
		}
		$total_nov = 0;
		$consulta = "SELECT id FROM novedades WHERE estado = 0 AND referente = 0 AND tipo < 3 AND id_user = ".$id_veri[1];
		if ( $result = $mysqli->query( $consulta ) ) {
            while($row = $result->fetch_assoc()){
				$consulta2 = "SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE vista = 0 AND id_user != editor AND (referente = ".$row['id']." OR id = ".$row['id'].")";
				if ( $result2 = $mysqli->query( $consulta2 ) ) {
					$res = mysqli_fetch_object( $result2 );
        			$result2->close();
					if($res->total > 0){
						$total_nov = $total_nov + $res->total;
					}
				}				
            }
            $result->close();
        }
		$alertas .= ($total_nov > 0)?'<button type="button" class="btn btn-light btn-sm w-100 d-block mb-2 alertas-no" onclick="loader(\'misnovedades\'); return false">novedades <span class="badge bg-danger">'.$total_nov.'</span></button>':'';
		
	}
    $consulta = "SELECT * FROM admins WHERE id = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
        $user = mysqli_fetch_object( $result );
        $result->close();
    }
    
}

$respuesta = array( 'data' => $alertas );
$mysqli->close();
echo json_encode( $respuesta );

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}
function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>