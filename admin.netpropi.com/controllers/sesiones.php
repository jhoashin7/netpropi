<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['iden'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $filtro = '';
            if(isset($_POST['user']) && $_POST['user'] != ''){
                $filtro = '`ses`.`id_user` = '.$_POST['user'];	
            }

            // DB table to use
            $table = 'ses_admin';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`ses`.`creado`',
                    'dt' => 0,
                    'field' => 'creado'
                ),
                array(
                    'db' => '`ses`.`tiempo`',
                    'dt' => 1,
                    'field' => 'tiempo',
                    'formatter' => function ( $d, $row ){			
                        return sprintf('%02d:%02d:%02d', ($d/3600),($d/60%60), $d%60);
                    }
                ),
                array(
                    'db' => '`ses`.`ip`',
                    'dt' => 2,
                    'field' => 'ip'
                ),
                array(
                    'db' => '`ses`.`fecha`',
                    'dt' => 3,
                    'field' => 'fecha'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `ses_admin` AS `ses`";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>