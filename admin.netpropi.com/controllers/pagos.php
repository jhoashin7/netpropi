<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $moneda = $_POST['moneda'];
            $filtro = '';
			$rol = $_POST['rol'];
			if($rol == 5){
				$filtro = '`pag`.`id_user` = '.$id_veri[1];
			}

            // DB table to use
            $table = 'pagos';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => 'CONCAT(UPPER(`pag`.`referencia`),"-",LPAD(`pag`.`id`,6,0))',
                    'dt' => 0,
                    'field' => 'refer',
					'as' => 'refer'
                ),
                array(
                    'db' => '`pag`.`nombre_pago`',
                    'dt' => 1,
                    'field' => 'nombre_pago'
                ),
				array(
                    'db' => '`pag`.`tipo`',
                    'dt' => 2,
                    'field' => 'tipo',
                    'formatter' => function ( $d, $row ) {
						return ($d == 1)?'Propiedad':'Contrato';
					}
                ),
                array(
                    'db' => 'ROUND((`pag`.`valor` * '.$moneda.'), 2)',
                    'dt' => 3,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
                array(
                    'db' => '`usr`.`nombre`',
                    'dt' => 4,
                    'field' => 'nombre'
                ),
				array(
                    'db' => '`usr`.`email`',
                    'dt' => 5,
                    'field' => 'email'
                ),
				array(
                    'db' => '`pag`.`tipo_pago`',
                    'dt' => 6,
                    'field' => 'tipo_pago',
                    'formatter' => function ( $d, $row ) {
						return ($d == 0)?'Digital':'Manual';
					}
                ),
				array(
                    'db' => '`pag`.`id_pasarela`',
                    'dt' => 7,
                    'field' => 'id_pasarela'
                ),
                array(
                    'db' => '`pag`.`estado`',
                    'dt' => 8,
                    'field' => 'estado',
                    'formatter' => function ( $d, $row ) {
						return ($d == 0)?'<span class="fw-bold text-warning">Pendiente</span>':(($d == 1)?'<span class="fw-bold text-success">Pagado</span>':'<span class="fw-bold text-danger">Cancelado</span>');
					}
                ),
                array(
                    'db' => '`pag`.`creado`',
                    'dt' => 9,
                    'field' => 'creado'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 10,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),                
                array(
                    'db' => '`pag`.`id`',
                    'dt' => 11,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) use ($rol) {  
						$texto = ($rol == 5)?'pagar':'editar';
                        return ($row[8] == 0)?'<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="setPago(\'pagos\', ' . $d . ', \''.$row[4].'\', \''.$row[5].'\', \''.$row[0].'\'); return false"><span class="d-none d-sm-inline">'.$texto.'</span> <i class="fas fa-edit fa-fw"></i></button></div>':'';                        
                    }
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `pagos` AS `pag`             
			LEFT JOIN `admins` AS `usr` ON (`usr`.`id` = `pag`.`id_user`)
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `pag`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>