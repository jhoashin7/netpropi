<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $filtro = '';
            
            // DB table to use
            $table = 'contacto';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`con`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`con`.`email`',
                    'dt' => 1,
                    'field' => 'email'
                ),
                array(
                    'db' => '`con`.`telefono`',
                    'dt' => 2,
                    'field' => 'telefono'
                ),
                array(
                    'db' => '`con`.`pais_nombre`',
                    'dt' => 3,
                    'field' => 'pais_nombre'
                ),			
                array(
                    'db' => '`con`.`estado`',
                    'dt' => 4,
                    'field' => 'estado',
                    'formatter' => function ( $d, $row ) {
						return ($d == 0)?'<span class="text-warning fw-bold">Pendiente</span>':'<span class="text-success fw-bold">Revisada</span>';
					}
                ),	
                array(
                    'db' => '`con`.`fecha`',
                    'dt' => 5,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 6,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt',
                    'formatter' => function ( $d, $row ) {
						return ($row[4] == 0)?'':$d;
					}
                ),
                array(
                    'db' => '`con`.`creado`',
                    'dt' => 7,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`con`.`id`',
                    'dt' => 8,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) {
                        return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'contacto\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>';                        
                    }
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `contacto` AS `con` 
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `con`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>