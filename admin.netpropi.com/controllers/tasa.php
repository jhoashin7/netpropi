<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';
$resp = false;
$data = array( 'msg' => 'No se enviaron correctamente tus datos. Intenta de nuevo.' );

$monedas = array();
$monedas_full = array();
$monedas_fin = array();
$consulta = "SELECT id, currency FROM countries WHERE activo = 1";
if ( $result = $mysqli->query( $consulta ) ) {
    while($row = $result->fetch_assoc()){
        array_push($monedas_full, array($row['id'], $row['currency']));
        array_push($monedas, $row['currency']);
    }
    $result->close();
}

$endpoint = 'live';
$access_key = 'a6405ab2acff7471ecfc6193dcc1d4c9';
if(count($monedas) > 0){
    $ch = curl_init('http://api.currencylayer.com/'.$endpoint.'?access_key='.$access_key.'&source=USD&currencies='.implode(',',$monedas).'');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $json = curl_exec($ch);
    curl_close($ch);
    $exchangeRates = json_decode($json, true);
    if($exchangeRates['success']){
        foreach ($monedas_full as $valor){
            array_push($monedas_fin, "(".$valor[0].",'".$valor[1]."',".$exchangeRates['quotes']['USD'.$valor[1]].")");
        }
        $consulta2 = "INSERT INTO cambio (id_pais, moneda, valor) VALUES " . implode(',', $monedas_fin). "ON DUPLICATE KEY UPDATE id_pais = VALUES(id_pais), moneda = VALUES(moneda), valor = VALUES(valor)";
        if ( $result = $mysqli->query( $consulta2 ) ) {
            $data = array( 'consulta' => $exchangeRates['quotes'] );
            $resp = true;				
        }
    }
     
}

$respuesta = array( 'resp' => $resp, 'data' => $data );
$mysqli->close();
echo json_encode( $respuesta );
?>