<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';

require_once('dompdf/autoload.inc.php');

use Dompdf\Dompdf;

use Dompdf\Options;

$getvars = sanitize( $_POST );
unset( $_POST );

$id_veris = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'veruser' ] ) ), 'd', $conArr['enc_string'] ));
if($id_veris[0] == $conArr['enc_string'] && is_numeric($id_veris[1])){
	$orvar = json_decode( $getvars[ 'variables' ], true );
	$variables = array();
	foreach ( $orvar as $key => $val ) {
		$variables[$val['nombre']] = nl2br($val['valor']);
    }
    $consulta = "SELECT imagen, marca FROM cabezera ORDER BY id ASC LIMIT 1";
	if ( $result = $mysqli->query( $consulta ) ) {
		$cabe = mysqli_fetch_object( $result );		
		$result->close();
		$consulta = "SELECT id, nombre, texto FROM contratos WHERE id = ".$getvars[ 'iden' ];
		if ( $result = $mysqli->query( $consulta ) ) {
			$cont = mysqli_fetch_object( $result );
			$result->close();
			$mensaje = utf8_encode( $cont->texto );
			foreach ( $variables as $key => $val ) {
                $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
            }
			$html = '<html>
			<head>
			<meta charset="utf-8">			
			<style>
			footer { position: fixed; right: 0px; bottom: 10px; text-align: right; color:#393939; font-size: 10px;}
        	footer .page:after { content: counter(page, decimal); }
 			@page { margin: 30px 30px 40px 50px; }
			#watermark {position: fixed; left: 0px; top:50%; width: 100%; height: 600px; margin-top:-300px; text-align: center; opacity: 0.15; z-index: 99999;}
			</style>	
			</head>
			<body>
			<div id="watermark">
			<img src="data:image/png;base64,'.base64_encode(file_get_contents('../'.$conArr['updestino'].'contratos/'.$cabe->marca)).'" style="width:600px"/>
			</div>
			<footer>
			<small class="page"></small>
			</footer>
			<main>
			<div style="display: block; width: 100%"><img src="data:image/png;base64,'.base64_encode(file_get_contents('../'.$conArr['updestino'].'contratos/'.$cabe->imagen)).'" style="width:100%"/></div>
			<div style="display: block;">
			'.$mensaje.'
			</div>
			</main> 
			</body>
			</html>';
			$options = new Options(); 
			$options->set('isPhpEnabled', 'true'); 
			$options->set('enable_remote',true);
			$options->set('isFontSubsettingEnabled',true);
			$options->set('IsHtml5ParserEnabled',true);			
			$dompdf = new Dompdf($options);			
			$dompdf->loadHtml($html);
			$dompdf->render();
			//$dompdf->stream(cleanTexto(utf8_encode( $cont->nombre )).'.pdf', array("Attachment" => 0));
			$output = $dompdf->output();
			echo base64_encode($output);
    		//file_put_contents('../contratos/'.cleanTexto(utf8_encode( $cont->nombre )).'.pdf', $output);
		}
	}       
}else{
   echo 'No se enviaron correctamente tus datos. Intenta de nuevo.';
}

$mysqli->close();

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function cleanTexto($string) {
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

   return strtolower(preg_replace('/[^A-Za-z0-9\_]/', '', $string)); // Removes special chars.
}

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}

function convertMethod($text){
    //Problem is that utf8_decode convert HTML chars for &bdquo; and other to ? or &nbsp; to \xA0. And you can not replace, that they are in some char bytes and you broke cyrillic (or other alphabet) chars.
    $problem_enc=array(
        'euro',
        'sbquo',
        'bdquo',
        'hellip',
        'dagger',
        'Dagger',
        'permil',
        'lsaquo',
        'lsquo',
        'rsquo',
        'ldquo',
        'rdquo',
        'bull',
        'ndash',
        'mdash',
        'trade',
        'rsquo',
        'brvbar',
        'copy',
        'laquo',
        'reg',
        'plusmn',
        'micro',
        'para',
        'middot',
        'raquo',
        'nbsp'
    );
    $text=mb_convert_encoding($text,'HTML-ENTITIES','UTF-8');
    $text=preg_replace('#(?<!\&ETH;)\&('.implode('|',$problem_enc).');#s','--amp{$1}',$text);
    $text=mb_convert_encoding($text,'UTF-8','HTML-ENTITIES');
    $text=utf8_decode($text);
    $text=mb_convert_encoding($text,'HTML-ENTITIES','UTF-8');
    $text=preg_replace('#\-\-amp\{([^\}]+)\}#su','&$1;',$text);
    $text=mb_convert_encoding($text,'UTF-8','HTML-ENTITIES');
    return $text;
}

function utf8_encode_deep(&$input) {
	if (is_string($input)) {
		$input = mb_convert_encoding($input,'UTF-8','HTML-ENTITIES');
	} else if (is_array($input)) {
		foreach ($input as &$value) {
			utf8_encode_deep($value);
		}
		
		unset($value);
	} else if (is_object($input)) {
		$vars = array_keys(get_object_vars($input));
		
		foreach ($vars as $var) {
			utf8_encode_deep($input->$var);
		}
	}
}

function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>