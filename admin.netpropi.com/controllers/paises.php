<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $filtro = '';

            // DB table to use
            $table = 'countries';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`cou`.`name`',
                    'dt' => 0,
                    'field' => 'name'
                ),
                array(
                    'db' => '`cou`.`iso3`',
                    'dt' => 1,
                    'field' => 'iso3'
                ),
				array(
                    'db' => '`cou`.`currency`',
                    'dt' => 2,
                    'field' => 'currency'
                ),
                array(
                    'db' => 'COALESCE(`sta`.`total`, 0)',
                    'dt' => 3,
                    'field' => 'total_sta',
                    'as' => 'total_sta'
                ),
                array(
                    'db' => 'COALESCE(`cit`.`total`, 0)',
                    'dt' => 4,
                    'field' => 'total_cit',
                    'as' => 'total_cit'
                ),                		
                array(
                    'db' => '`cou`.`activo`',
                    'dt' => 5,
                    'field' => 'activo',
                    'as' => 'activo',
                    'formatter' => function ( $d, $row ){                        
                        return ($d == 0)?'<strong class="text-danger">NO</strong>':'<strong class="text-success">SI</strong>';
                    }
                ),	
                array(
                    'db' => '`cou`.`updated_at`',
                    'dt' => 6,
                    'field' => 'updated_at'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 7,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`cou`.`created_at`',
                    'dt' => 8,
                    'field' => 'created_at'
                ),	
                array(
                    'db' => '`cou`.`id`',
                    'dt' => 9,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) {
                        return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'paises\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>';                        
                    }
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );

            $joinQuery = "FROM `countries` AS `cou` 
            LEFT JOIN (SELECT country_id, COUNT(id) AS total FROM states GROUP BY country_id) AS `sta` ON (`sta`.`country_id` = `cou`.`id`) 
            LEFT JOIN (SELECT country_id, COUNT(id) AS total FROM cities GROUP BY country_id) AS `cit` ON (`cit`.`country_id` = `cou`.`id`)             	
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `cou`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>