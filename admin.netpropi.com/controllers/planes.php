<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $moneda = $_POST['moneda'];
            $filtro = '';            

            // DB table to use
            $table = 'planes';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`plan`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`plan`.`prefijo`',
                    'dt' => 1,
                    'field' => 'prefijo',
                    'formatter' => function ( $d, $row ) {
                        return '<span class="text-uppercase">'.$d.'</span>';
                    }
                ),
				array(
                    'db' => 'CONCAT(`plan`.`minimo`, " - ", `plan`.`maximo`)',
                    'dt' => 2,
                    'field' => 'num_propi',
                    'as' => 'num_propi'
                ),
                array(
                    'db' => 'ROUND((`plan`.`valor` * '.$moneda.'), 2)',
                    'dt' => 3,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
                array(
                    'db' => '`plan`.`tiempo`',
                    'dt' => 4,
                    'field' => 'tiempo'
                ),
				array(
                    'db' => '`plan`.`tipo`',
                    'dt' => 5,
                    'field' => 'tipo',
                    'formatter' => function ( $d, $row ) {
						return ($d == 0)?'Público':'Privado';
					}
                ),
				array(
                    'db' => '`ben`.`nombre`',
                    'dt' => 6,
                    'field' => 'nombre_ben',
					'as' => 'nombre_ben'
                ),
                array(
                    'db' => 'COALESCE(`pag`.`total`, 0)',
                    'dt' => 7,
                    'field' => 'total_pag',
                    'as' => 'total_pag'
                ),
                array(
                    'db' => '`plan`.`fecha`',
                    'dt' => 8,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 9,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`plan`.`creado`',
                    'dt' => 10,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`plan`.`id`',
                    'dt' => 11,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) { 						
                        return ($d != 1)?'<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'planes\', ' . $d . '); return false"><i class="fas fa-edit fa-fw"></i> <span class="d-none d-sm-inline">editar</span></button><button type="button" class="btn btn-danger btn-sm text-white" onClick="delData(' . $d . ', \'planes\', \'id\', \'reloader\', []); return false"><span class="d-none d-sm-inline">eliminar</span> <i class="fas fa-trash fa-fw"></i></button></div>':'<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'planes\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>';                        
                    }
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `planes` AS `plan` 
            LEFT JOIN (SELECT id_pago, COUNT(id) AS total FROM pagos WHERE estado = 1 AND tipo = 1 GROUP BY id_pago) AS `pag` ON (`pag`.`id_pago` = `plan`.`id`) 
			LEFT JOIN `admins` AS `ben` ON (`ben`.`id` = `plan`.`beneficiario`)
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `plan`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>