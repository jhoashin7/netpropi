<?php

include __DIR__ . '/../../vendor/autoload.php';

use GuzzleHttp\Exception\GuzzleException;

require_once __DIR__ . '/pandadoc/endpoints/Documents.php';

class ContratosController {
    /**
     * @return void
     */
    public static function init() {
        $instance = new static;
        $method = $_GET['a'] ?? strtolower($_SERVER['REQUEST_METHOD']);

        if (method_exists($instance, $method)) {
            $instance->{$method}();
        } else {
            header('HTTP/1.1 404');
            exit;
        }
    }

    /**
     * @param array $data
     * @param int $status
     * @return void
     */
    private function responseJson(array $data, int $status = 200) {
        ob_clean();
        header("HTTP/1.1 $status");
        header("Content-Type: application/json");
        echo json_encode($data);
        exit;
    }

    private function responseDataTable(array $data) {
        $this->responseJson([
            'draw' => count($data),
            'recordsTotal' => count($data),
            'recordsFiltered' => 0,
            'data' => $data,
        ]);
    }

    /**
     * Listado de contratos
     * @return void
     * @throws GuzzleException
     */
    public function index() {
        dd(Documents::list());
        $this->responseDataTable([
            [
                'name' => 'Documento Prueba',
                'plantilla' => 'Básica',
                'estado' => 'Creado',
                'fecha_creacion' => date('d/m/Y'),
                'fecha_edicion' => date('d/m/Y'),
                'acciones' => []
            ]
        ]);
    }
}

ContratosController::init();
