<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';

$getvars = sanitize( $_POST );
unset( $_POST );
$val = (isset($getvars[ 'val' ]))?$getvars[ 'val' ]:false;
$datos = ($val !== false)?'<option value="'.$val.'">Seleccionar</option>':'<option value="">Seleccionar</option>';
$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'user' ] ) ), 'd', $conArr['enc_string'] ));
if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
    $fl = strip_tags( $mysqli->real_escape_string( $getvars[ 'fl' ] ) );
    $tb = strip_tags( $mysqli->real_escape_string( $getvars[ 'tb' ] ) );
    $flv = strip_tags( $mysqli->real_escape_string( $getvars[ 'flv' ] ) );
    $fln = strip_tags( $mysqli->real_escape_string( $getvars[ 'fln' ] ) );
    $valor = strip_tags( $mysqli->real_escape_string( $getvars[ 'valor' ] ) );
    $filtro = (isset($getvars[ 'filtro' ]))?strip_tags( $mysqli->real_escape_string( $getvars[ 'filtro' ] ) ):false;
    $consulta = ($filtro)?"SELECT ".$flv.", ".$fln." FROM ".$tb." WHERE ".$fl." = ".$valor." AND ".str_replace("\\", "", $filtro)." GROUP BY ".$flv." ORDER BY ".$fln." ASC":"SELECT ".$flv.", ".$fln." FROM ".$tb." WHERE ".$fl." = ".$valor." ORDER BY ".$fln." ASC";    
    if ( $result = $mysqli->query( $consulta ) ) {
        while($row = $result->fetch_assoc()){
            $datos .= '<option value='.$row[$flv].'>'.utf8_encode($row[$fln]).'</option>';
        }
        $result->close();
    }
}

$respuesta = array( 'datos' => $datos );
$mysqli->close();
echo json_encode( $respuesta );

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}
function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>