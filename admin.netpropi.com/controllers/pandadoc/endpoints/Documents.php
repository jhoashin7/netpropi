<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Documents {
    protected Client $client;

    public function __construct() {
        $this->client = new Client([
            'base_uri' => 'https://api.pandadoc.com/public/v1/',
            'headers' => [
                'Authorization' => 'API-Key a0f189223fea14adb360d362f7ed0a91ea2c47c9'
            ]
        ]);
    }

    /**
     * @throws GuzzleException
     */
    private function getAll(): array
    {
        $response = (string) $this->client->request('GET', 'documents')->getBody();
        return json_decode($response, true);
    }

    /**
     * @return array
     * @throws GuzzleException
     */
    public static function list(): array
    {
        $instance = new static();
        return $instance->getAll()['results'];
    }
}