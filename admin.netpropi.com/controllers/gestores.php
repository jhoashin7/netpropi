<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $filtro = '';
            if(isset($_POST['user']) && $_POST['user'] != ''){
                $filtro = '`admin`.`id` != '.$id_veri[1].' AND (`admin`.`rol` >= '.$_POST['rol'].' OR `admin`.`rol` = 0) AND `admin`.`rol` < 5';
            }

            // DB table to use
            $table = 'admins';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`admin`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`admin`.`email`',
                    'dt' => 1,
                    'field' => 'email'
                ),
                array(
                    'db' => '`admin`.`usuario`',
                    'dt' => 2,
                    'field' => 'usuario'
                ),
                array(
                    'db' => '`admin`.`rol`',
                    'dt' => 3,
                    'field' => 'rol',		
                    'formatter' => function ( $d, $row ){
                        return ($d != 0)?$row[9]:'Suspendido';
                    }
                ),			
                array(
                    'db' => '`sesion`.`totalt`',
                    'dt' => 4,
                    'field' => 'totalt',
                    'formatter' => function ( $d, $row ){
                        $numero = (is_null($d))?0:$d;
                        return '<div class="d-grid gap-2"><button type="button" class="btn btn-info btn-sm text-white" onClick="showSes('.$row[8].', \''.$row[0].'\'); return false"><i class="fas fa-user-clock fa-fw"></i> <span class="font-weight-bold">'.$numero.'</span></button></div>';
                    }
                ),	
                array(
                    'db' => '`admin`.`fecha`',
                    'dt' => 5,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 6,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`admin`.`creado`',
                    'dt' => 7,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`admin`.`id`',
                    'dt' => 8,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) {
                        return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'gestor\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>';                        
                    }
                ),	
                array(
                    'db' => '`perfil`.`nombre`',
                    'dt' => 9,
                    'field' => 'nombre_perfil',
                    'as' => 'nombre_perfil'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `admins` AS `admin` 
            LEFT JOIN `perfiles` AS `perfil` ON (`perfil`.`id` = `admin`.`rol`) 
            LEFT JOIN (SELECT id, id_user, COUNT(id) AS totalt FROM ses_admin GROUP BY id_user) AS `sesion` ON (`sesion`.`id_user` = `admin`.`id`) 		
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `admin`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>