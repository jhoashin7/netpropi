<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

require_once 'mandrill-api-php/src/Mandrill.php';

require_once('stripe-php-master/init.php');

require_once('dompdf/autoload.inc.php');

use Dompdf\Dompdf;

use Dompdf\Options;

$resp = false;
$data = array( 'msg' => 'No se enviaron correctamente tus datos. Intenta de nuevo.' );

$ip = getUserIP();
$goresp = (isset($_POST['g-recaptcha-response']) && ($_POST['accion'] == 1 || $_POST['accion'] == 3))?$_POST['g-recaptcha-response']:false;
$getvars = sanitize( $_POST );
unset( $_POST );
$accion = $getvars[ 'accion' ];
if($accion >= 4){
    $id_veris = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'idveruser' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veris[0] == $conArr['enc_string'] && is_numeric($id_veris[1])){
        $accion = $accion;        
    }else{
       $accion = 0; 
    }
}
if ( $accion == 1 ) {
    if($goresp){
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$conArr['google_sec']."&response=" . $goresp . "&remoteip=" . $_SERVER['REMOTE_ADDR']));       
        if($captcha->success == false){            
            $resp = false;
        }else{
            $contrasena = strip_tags( $mysqli->real_escape_string( $getvars[ 'contrasena' ] ) );
            $contrasena = simple_crypt( $conArr['enc_string'].'***'.utf8_decode($contrasena), 'e', $conArr['enc_string'] );            
            $usuario = strip_tags( $mysqli->real_escape_string( $getvars[ 'usuario' ] ) );	
            $consulta = "SELECT * FROM admins WHERE usuario = '" . $usuario . "' AND contrasena = '" . $contrasena . "' LIMIT 1";
            if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
                if ( $rows > 0 ) {
                    $user = mysqli_fetch_object( $result );
                    if ( $user->rol > 0 ) {				
                        $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$user->id, 'e', $conArr['enc_string'] ), 'rol' => $user->rol, 'nombre' => utf8_encode( $user->nombre ) );
                        $resp = true;
                    } else {
                        $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$user->id, 'e', $conArr['enc_string'] ), 'rol' => $user->rol, 'nombre' => utf8_encode( $user->nombre ), 'msg' => utf8_encode( $user->nombre ) . ', tu usuario se encuentra suspendido, debes ponerte en contacto con el administrador.' );
                        $resp = false;
                    }

                } else {
                    $data = array( 'msg' => 'Tus datos no concuerdan, verifica la información.' );
                }
                $result->close();
            } 
        }
    }
}
if ( $accion == 2 ) {
    $id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
        $us = $id_veri[1];        	
        $consulta = "SELECT * FROM admins WHERE id = " . $us;
        if ( $result = $mysqli->query( $consulta ) ) {
            $rows = $result->num_rows;
            if ( $rows > 0 ) {
                $user = mysqli_fetch_object( $result );
                if ( $user->rol > 0 ) {				
                    $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$user->id, 'e', $conArr['enc_string'] ), 'rol' => $user->rol, 'nombre' => utf8_encode( $user->nombre ), 'mail' => $user->email, 'pass' => utf8_encode( $user->contrasena ), 'user' => $user->usuario );
                    $resp = true;
                } else {
                    $data = array( 'id' => simple_crypt( $conArr['enc_string'].'***'.$user->id, 'e', $conArr['enc_string'] ), 'rol' => $user->rol, 'nombre' => utf8_encode( $user->nombre ), 'msg' => utf8_encode( $user->nombre ) . ', tu usuario se encuentra suspendido, debes ponerte en contacto con el administrador.' );
                    $resp = false;
                }
            } else {
                $data = array( 'msg' => 'Tus datos no concuerdan, verifica la información.' );
            }
            $result->close();
        }
    }
}
if ( $accion == 3 ) {
    if($goresp){
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$conArr['google_sec']."&response=" . $goresp . "&remoteip=" . $_SERVER['REMOTE_ADDR']));       
        if($captcha->success == false){            
            $resp = false;
        }else{
            $mail = strip_tags( $mysqli->real_escape_string( $getvars[ 'mail' ] ) );            
            $consulta = "SELECT nombre, usuario, contrasena, email FROM admins WHERE email = '" . $mail . "' LIMIT 1";
            if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
                if ( $rows > 0 ) {
                    $user = mysqli_fetch_object( $result );
                    $contrasena = explode('***', simple_crypt( $user->contrasena, 'd', $conArr['enc_string'] ));
                    $extra_data = array('nombre' => utf8_encode( $user->nombre ), 'usuario' => $user->usuario, 'pass' => utf8_encode( $contrasena[1] ), 'urlsitio' => $conArr['base_url_sitio'], 'urladmin' => $conArr['base_url'] );
					$consulta2 = "SELECT subject, texto, plano FROM mails WHERE id = 1";
					if ( $result2 = $mysqli->query( $consulta2 ) ) {
						$mailin = mysqli_fetch_object( $result2 );
						$result2->close();
					}					
					email( $user->email, utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );					
                    $data = array( 'msg' => '<strong>'.utf8_encode( $user->nombre ).'</strong>, te hemos enviado un correo a la dirección <strong>'.$mail.'</strong>, con tus datos de acceso.');
                    $resp = true;
                } else {
                    $data = array( 'msg' => 'No encontramos esta dirección de E-mail (<strong>'.$mail.'</strong>) en nuestros registros.' );
                }
                $result->close();
            }            
        }
    }	
}
if ( $accion == 4 ) {
	date_default_timezone_set('America/Bogota');    
    $id = $id_veris[1];
    $fecha = date("Y-m-d H:i:s", strtotime('-2 minutes'));
    $consulta = "SELECT id, creado FROM ses_admin WHERE id_user = " . $id . " AND fecha >= '".$fecha."' AND ip = '".$ip."' LIMIT 1";
    if ( $result = $mysqli->query( $consulta ) ) {
        $rows = $result->num_rows;
        if ( $rows > 0 ) {
            $datos = mysqli_fetch_object( $result );
            $tiempo = strtotime("now") - strtotime($datos->creado);
            $consulta2 = "UPDATE ses_admin SET tiempo = ".$tiempo.", fecha = '".date("Y-m-d H:i:s")."' WHERE id = " . $datos->id;
            if ( $result2 = $mysqli->query( $consulta2 ) ) {
                $data = array( 'fecha' => $datos->creado );
                $resp = true;
            }
        }else{
            $consulta2 = "INSERT INTO ses_admin (id_user, ip, tiempo, fecha, creado) VALUES (".$id.", '".$ip."', 0, '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')";
            if ( $result2 = $mysqli->query( $consulta2 ) ) {
                $data = array( 'fecha' => date("Y-m-d H:i:s") );
                $resp = true;
            }
        }
        $result->close();
    }   
}

if ( $accion == 5 ) {
    $id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'editor' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
        $editor = $id_veri[1];
        $getvars[ 'editor' ] = $editor;
        $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
        $db = strip_tags( $mysqli->real_escape_string( $getvars[ 'db' ] ) );
        $columns = array();
        $puede = true;
        $consulta = "SHOW COLUMNS FROM ".$db;
        if ( $result = $mysqli->query( $consulta ) ) {
            while($row = $result->fetch_assoc()){
                array_push($columns, $row['Field'].','.$row['Type']);			
            }
            $result->close();
        }
        if($db == 'admins'){
            
            $contrasena = simple_crypt( $conArr['enc_string'].'***'.utf8_decode(strip_tags( $mysqli->real_escape_string( $getvars[ 'contrasena' ] ) )), 'e', $conArr['enc_string'] );
            
            $consulta = ($id != 0)?"SELECT id FROM admins WHERE usuario = '" . $getvars['usuario'] . "' AND contrasena = '" . $contrasena . "' AND id != ".$id." LIMIT 1":"SELECT id FROM admins WHERE usuario = '" . $getvars['usuario'] . "' AND contrasena = '" . $contrasena . "' LIMIT 1";
            if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
                if ( $rows > 0 ) {
                    $puede = false;
                    $data = array( 'msg' => 'Los datos de acceso (usuario y/o contraseña), ya están siendo utilizados por otro usuario.' );
                    $resp = false;
                }
                $result->close();
            }
        }	
        if($puede){
            if($id != 0){
                $setval = array();
                foreach ($columns as &$valor) {
                    $valor = explode(',', $valor);
                    if($valor[0] != "id"){
                        if(array_key_exists($valor[0], $getvars)){
                            $inval = $valor[0]." = "; 
                            $invalor = (is_array($getvars[$valor[0]]))?implode(',', $getvars[$valor[0]]):strip_tags( $mysqli->real_escape_string( str_replace("'", "´", $getvars[$valor[0]]) )); 
                            $invalor = (preg_match('!!u', $invalor))?mb_convert_encoding($invalor,'HTML-ENTITIES','utf-8'):$invalor;
                            if(substr( $invalor, 0, 17 ) === '**esSummertexto**'){
                                $invalor = str_replace(array('**esSummertexto**', '*{*', '*}*'), array('','<','>'), $invalor);
                            }
                            if($db == 'admins' && $valor[0] == 'contrasena'){
                                $invalor = simple_crypt( $conArr['enc_string'].'***'.$invalor, 'e', $conArr['enc_string'] );
                            }
                            
                            $inval .= (strpos($valor[1], 'int'))?$invalor:"'".$invalor."'";
                            array_push($setval, $inval);
                        }
                    }			
                }
                unset($valor);
                $consulta = "UPDATE ".$db." SET ".implode(',', $setval)." WHERE id = " . $id;
                if ( $result = $mysqli->query( $consulta ) ) {
                    $data = array( 'id' => $id, 'msg' => 'Los datos han sido actualizados.' );
                    $resp = true;
                }else{
                    $data = array( 'msg' => $mysqli->error );
                }
            }else{
                $setval = array();
                $setcol = array();
                $creado = false;
                foreach ($columns as &$valor) {
                    $valor = explode(',', $valor);
                    if($valor[0] != "id"){
                        if(array_key_exists($valor[0], $getvars)){
                            array_push($setcol, $valor[0]);									
                            $invalor = (is_array($getvars[$valor[0]]))?implode(',', $getvars[$valor[0]]):strip_tags( $mysqli->real_escape_string( str_replace("'", "´", $getvars[$valor[0]]) )); 
                            $invalor = (preg_match('!!u', $invalor))?mb_convert_encoding($invalor,'HTML-ENTITIES','utf-8'):$invalor;
                            if(substr( $invalor, 0, 17 ) === '**esSummertexto**'){
                                $invalor = str_replace(array('**esSummertexto**', '*{*', '*}*'), array('','<','>'), $invalor);
                            }
                            if($db == 'admins' && $valor[0] == 'contrasena'){
                                $invalor = simple_crypt( $conArr['enc_string'].'***'.$invalor, 'e', $conArr['enc_string'] );
                            }
                            $invalor = (strpos($valor[1], 'int'))?$invalor:"'".$invalor."'";
                            array_push($setval, $invalor);
                        }
                    }
                    if($valor[0] == "creado"){
                        $creado = true;
                    }
                }
                unset($valor);
                $creadofi = ($creado)?array(",creado", ",NOW()"):array("","");
                $consulta = "INSERT INTO ".$db." (".implode(',', $setcol).$creadofi[0].") VALUES (".implode(',', $setval).$creadofi[1].")";			
                if ( $result = $mysqli->query( $consulta ) ) {
                    $iden = $mysqli->insert_id;
                    $data = array( 'id' => $iden, 'msg' => 'Los datos han sido guardados.' );
                    $resp = true;
                }else{
                    $data = array( 'msg' => $mysqli->error );
                }
            }
        }
    }
}

if ( $accion == 6 ) {    
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
    $db = strip_tags( $mysqli->real_escape_string( $getvars[ 'db' ] ) );
    $consulta = "SELECT * FROM ".$db." WHERE id = " . $id;
    if ( $result = $mysqli->query( $consulta ) ) {		
        $rows = $result->num_rows;
        if ( $rows > 0 ) {
            $datos = mysqli_fetch_object( $result );
            if($db == 'admins'){
                $contrasena = explode('***', simple_crypt( $datos->contrasena, 'd', $conArr['enc_string'] ));
                $datos->contrasena = $contrasena[1];
            }
            utf8_encode_deep($datos);
            $data = $datos;
            $resp = true;
        } else {
            $data = array( 'msg' => 'No encontramos estos datos en nuestros registros.' );
        }
        $result->close();
    }    	
}

if ( $accion == 7 ) {
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
    $db = strip_tags( $mysqli->real_escape_string( $getvars[ 'db' ] ) );
    $field = strip_tags( $mysqli->real_escape_string( $getvars[ 'field' ] ) );
    $consulta = "DELETE FROM ".$db." WHERE ".$field." = ".$id;			
    if ( $result = $mysqli->query( $consulta ) ) {		
        $data = array( 'id' => $id, 'msg' => 'El registro ha sido eliminado del sistema.' );
        $resp = true;
    }
    	
}

if ( $accion == 8 ) {
    $id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'editor' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
        $editor = $id_veri[1];
        $getvars[ 'editor' ] = $id_veri[1];
        $limite = strip_tags( $mysqli->real_escape_string( $getvars[ 'limite' ] ) );
        $folder = strip_tags( $mysqli->real_escape_string( $getvars[ 'folder' ] ) );
        $archivo = strip_tags( $mysqli->real_escape_string( $getvars[ 'archivo' ] ) );
        $db = strip_tags( $mysqli->real_escape_string( $getvars[ 'db' ] ) );	
        $lim = 50;
        $max_limit = $limite + $lim;
        $columns = array();
        $fijos = array();		
        $consulta = "SHOW COLUMNS FROM ".$db;
        if ( $result = $mysqli->query( $consulta ) ) {
            while($row = $result->fetch_assoc()){
                array_push($columns, $row['Field'].','.$row['Type']);			
            }
            $result->close();
        }
        $creado = false;
        foreach ($columns as &$valor) {
            $valor = explode(',', $valor);
            if(array_key_exists($valor[0], $getvars)){
                $invalor = (is_array($getvars[$valor[0]]))?implode(',', $getvars[$valor[0]]):$getvars[$valor[0]]; 
                $invalor = (preg_match('!!u', $invalor))?mb_convert_encoding($invalor,'HTML-ENTITIES','utf-8'):$invalor;
                $inval = (strpos($valor[1], 'int'))?$invalor:"'".$invalor."'";
                array_push($fijos, array($valor[0], $inval));
            }
            if($valor[0] == "creado"){
                $creado = true;
            }
        }
        unset($valor);
        $creadofi = ($creado)?array(",creado", ',NOW()'):array("","");

        $fila = 0;	
        $flag = true;
        $valores = array();
        $valores_col = array();	
        if ( ( $gestor = fopen( "../" . $folder . "/" . $archivo, "r" ) ) !== FALSE ) {
            while ( ( $datos = fgetcsv( $gestor, 0, ';' ) ) !== FALSE ) {
                if ( $flag ) {
                    $flag = false;
                    $valores_col = $datos;
                    foreach ($fijos as &$valor) {
                        array_push($valores_col, $valor[0]);
                    }
                    unset($valor);
                    continue;				
                }
                $fila++;			
                if(empty($datos)){				
                    continue;
                }
                if($fila > $limite){				
                    $datos = array_map("utf8_encode", $datos);
                    $datos_in = array_fill(0, count($valores_col), 0);
                    foreach ($columns as &$valor) {
                        //$valor = explode(',', $valor);
                        $key = array_search($valor[0], $valores_col);
                        if($key !== false){
                            //$invalor = $datos[$key]; 
                            
                            $invalor = (is_array($datos[$key]))?implode(',', $datos[$key]):$datos[$key];
                            $invalor = (preg_match('!!u', $invalor))?utf8_decode($invalor):$invalor;
                            if($db == 'admins' && $valor[0] == 'contrasena'){
                                $invalor = simple_crypt( $conArr['enc_string'].'***'.$inval, 'e', $conArr['enc_string'] );
                            }
                            $inval = (strpos($valor[1], 'int'))?$invalor:"'".$invalor."'";
                            $datos_in[$key] = $inval;
                        }			
                    }
                    unset($valor);
                    foreach ($fijos as &$valor) {
                        $key = array_search($valor[0], $valores_col);
                        if($key !== false){
                            $datos_in[$key] = $valor[1];
                        }
                    }
                    unset($valor);
                    array_push($valores, $datos_in);				
                }
                if($fila >= $max_limit){
                    break;
                }
                unset( $datos );
            }
            fclose( $gestor );
            $final_valores = array();
            foreach ($valores as &$val) {		
                array_push($final_valores, '('.implode(',',$val).$creadofi[1].')');
            }
            unset($val);
            $fin = (count($valores) > 0)?false:true;
            
            $valoresupd = array();
            foreach ($valores_col as $val) {		
                array_push($valoresupd, $val."= VALUES(".$val.")");
            }

            if(count($valores) > 0){
                $consulta2 = "INSERT INTO ".$db." (".implode(',', $valores_col).$creadofi[0].") VALUES " . implode(',', $final_valores). "ON DUPLICATE KEY UPDATE ".implode(',', $valoresupd);                
                if ( $result = $mysqli->query( $consulta2 ) ) {
                    $data = array( 'limite' => $fila, 'fin' => $fin, 'msg' => '<strong>'.$fila.'</strong> datos han sido procesados.' );
                    $resp = true;				
                }
            }else{
                $data = array( 'limite' => $fila, 'fin' => $fin, 'msg' => '<strong>'.$fila.'</strong> datos han sido procesados.' );
                $resp = true;
            }

        }
    }
}

if ( $accion == 9 ) {    
    $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$id_plan = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_plan' ] ) );
	$propiedades = strip_tags( $mysqli->real_escape_string( $getvars[ 'propiedades' ] ) );
	$meses = strip_tags( $mysqli->real_escape_string( $getvars[ 'meses' ] ) );
	$pais_id = strip_tags( $mysqli->real_escape_string( $getvars[ 'pais_id' ] ) );
	$planes = array();
    $consulta = "SELECT id, nombre, prefijo, valor FROM planes WHERE pais_id = $pais_id AND (beneficiario = 0 OR beneficiario = " . $id . ") AND ".$propiedades." BETWEEN minimo AND maximo AND id != 1 ORDER BY valor ASC LIMIT 1";
    // var_dump( $consulta );
    if ( $result = $mysqli->query( $consulta ) ) {
        while($row = $result->fetch_assoc()){
			$planes = array(
				'id' => $row['id'],
				'nombre' => utf8_encode($row['nombre']),
				'referencia' => $row['prefijo'],
				'valor' => number_format(($row['valor'] ) * $meses, 2)
			);
		}
        $result->close();
    }    
    $data = array( 'planes' => $planes );
    $resp = true;
}

if ( $accion == 10 ) {
	date_default_timezone_set('America/Bogota');
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$referencia = strip_tags( $mysqli->real_escape_string( $getvars[ 'referencia' ] ) );
	$nombre_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'nombre_pago' ] ) );
	$id_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_pago' ] ) );
	$tipo = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo' ] ) );
	$estado = strip_tags( $mysqli->real_escape_string( $getvars[ 'estado' ] ) );
	$id_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_user' ] ) );
	$email_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'email_user' ] ) );
	$propiedades = strip_tags( $mysqli->real_escape_string( $getvars[ 'propiedades' ] ) );
	$actuales = $getvars[ 'actuales' ];
	$extras = strip_tags( $mysqli->real_escape_string( $getvars[ 'extras' ] ) );
	$meses = strip_tags( $mysqli->real_escape_string( $getvars[ 'meses' ] ) );
	$valor = strip_tags( $mysqli->real_escape_string( $getvars[ 'valor' ] ) );
	$tipo_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo_pagon' ] ) );
	$id_pasarela = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_pasarelan' ] ) );
	$consulta = "INSERT INTO pagos (id_pago, id_pasarela, tipo_pago, referencia, nombre_pago, tipo, valor, id_user, estado, editor, creado) VALUES (".$id_pago.", '".$id_pasarela."', ".$tipo_pago.", '".$referencia."', '".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', ".$tipo.", '".$valor."', ".$id_user.", ".$estado.", ".$id_veris[1].", NOW())";
	if ( $result = $mysqli->query( $consulta ) ) {
		$iden = $mysqli->insert_id;
		$dias = ($id_pago != 1)?30 * $meses:$meses;		
		if($estado <= 1){
			if($id != 0){
				$totalact = ($actuales && $actuales != '' && count($actuales) > 0)?count($actuales):0;
				$totalprop = ($extras && $extras != '')?$totalact + intval($extras):$totalact;
				$hoy = date("Y-m-d");
				$consulta = "SELECT * FROM plan WHERE id_pago = ".$id;
				if ( $result = $mysqli->query( $consulta ) ) {
					$plan = mysqli_fetch_object( $result );    
					$result->close();
					$fin = (strtotime($hoy) > strtotime($plan->fin))?date("Y-m-d", strtotime("+".$dias." days")):date("Y-m-d", strtotime($plan->fin." +".$dias." days"));
					
					$consulta = "INSERT INTO plan (id_user, id_plan, id_pago, propiedades, dias, fin, referido, renovadas, estado, editor, creado) VALUES (".$id_user.", ".$id_pago.", ".$iden.", ".$propiedades.", ".$dias.", '".$fin."', ".$plan->id.", '".implode(',', $actuales)."', ".$estado.", ".$id_veris[1].", NOW())";
					if ( $result = $mysqli->query( $consulta ) ) {
						$iden = $mysqli->insert_id;
						if($estado == 1){							
							$consulta = "UPDATE plan SET fin = '".$hoy."', estado = 3, editor = ".$id_veris[1]." WHERE id = " . $plan->id;
							if ( $result = $mysqli->query( $consulta ) ) {
								if($extras && $extras != ''){
									$valores = array();
									for($i = 0; $i < intval($extras); $i++){
										array_push($valores, "(".$iden.", '".$fin."', ".$id_user.", ".$id_veris[1].", NOW())");
									}
									$consulta = "INSERT INTO propiedades (plan_id, final, id_user, editor, creado) VALUES ". implode(',', $valores);
									$result = $mysqli->query( $consulta );								
								}
								if($totalact > 0){
									$consulta = "UPDATE propiedades SET final = '".$fin."', plan_id = ".$iden.", editor = ".$id_veris[1]." WHERE id IN (".implode(',', $actuales).")";
									$result = $mysqli->query( $consulta );
								}
								
								$consulta = "INSERT INTO balance (nombre, valor, moneda, tipo, id_propiedad, editor, fecha, creado) VALUES ('".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', '".$valor."', 1, 2, 0, ".$id_user.", '".date('Y-m-d')."', NOW())";
								if ( $result = $mysqli->query( $consulta ) ) {
									$data = array( 'id' => $iden, 'msg' => 'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido activado exitosamente.' );
									$resp = true;
								}
							}
						}else{
							$data = array( 'id' => $iden, 'msg' => 'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido creado y a la espera de activación, cuando esto suceda tus propiedades apareceran disponibles en la sección de <strong>propiedades</strong>.' );
							$resp = true;
						}
					}					
				}
			}else{
				$fin = ($estado == 1)?date("Y-m-d", strtotime("+".$dias." days")):'';
				$consulta = "INSERT INTO plan (id_user, id_plan, id_pago, propiedades, dias, fin, renovadas, estado, editor, creado) VALUES (".$id_user.", ".$id_pago.", ".$iden.", ".$propiedades.", ".$dias.", '".$fin."', '', ".$estado.", ".$id_veris[1].", NOW())";		
				if ( $result = $mysqli->query( $consulta ) ) {
					$iden = $mysqli->insert_id;
					if($estado == 1){
						$valores = array();
						for($i = 0; $i < $propiedades; $i++){
							array_push($valores, "(".$iden.", '".$fin."', ".$id_user.", ".$id_veris[1].", NOW())");
						}
						$consulta = "INSERT INTO propiedades (plan_id, final, id_user, editor, creado) VALUES ". implode(',', $valores);
						if ( $result = $mysqli->query( $consulta ) ) {							
							$consulta = "INSERT INTO balance (nombre, valor, moneda, tipo, id_propiedad, editor, fecha, creado) VALUES ('".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', '".$valor."', 1, 2, 0, ".$id_user.", '".date('Y-m-d')."', NOW())";
                            if ( $result = $mysqli->query( $consulta ) ) {
                                $data = array( 'id' => $iden, 'msg' => 'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido activado exitosamente.' );
								$resp = true;
                            }
						}
					}else{
						$data = array( 'id' => $iden, 'msg' => 'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido creado y a la espera de activación, cuando esto suceda tus propiedades apareceran disponibles en la sección de <strong>propiedades</strong>.' );
						$resp = true;
					}					
				}
			}						
		}else{
			$data = array( 'id' => $iden, 'msg' => ($estado == 2)?'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido cancelado.':'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido creado y esta pendiente de pago.' );
			$resp = true;
		}
		
	}
}

if ( $accion == 11 ) {
	$id_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_user' ] ) );
    $id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'idveruser' ] ) ), 'd', $conArr['enc_string'] ));
	$consulta = "SELECT id FROM plan WHERE id_user = ".$id_veri[1];
	if ( $result = $mysqli->query( $consulta ) ) {
		$rows = $result->num_rows;
		$result->close();
		$data = array( 'numero' => $rows );
    	$resp = true;
	}
}

if ( $accion == 12 ){
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$actuales = array();
	$consulta = "SELECT id, id_plan, dias FROM plan WHERE id_pago = ".$id;
    if ( $result = $mysqli->query( $consulta ) ) {
        $plan = mysqli_fetch_object( $result );    
        $result->close();
        $consulta = "SELECT id, nombre, estado FROM propiedades WHERE plan_id = ".$plan->id." ORDER BY nombre ASC";
        if ( $result = $mysqli->query( $consulta ) ) {
            while($row = $result->fetch_assoc()){
                array_push($actuales, array(
                    'id' => $row['id'],
                    'nombre' => ($row['nombre'] != '')?utf8_encode($row['nombre']):'Pendiente',
                    'estado' => $row['estado']
                ));
            }
            $result->close();			
        }
    }
	$data = array( 'id_plan' => $plan->id_plan, 'dias' => $plan->dias, 'actuales' => $actuales );
    $resp = true;
}

if ( $accion == 13 ){
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$id_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_user' ] ) );
	$tipo = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo' ] ) );
	$tipo_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo_pago' ] ) );
	$nombre_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'nombre_pago' ] ) );
	$estado = strip_tags( $mysqli->real_escape_string( $getvars[ 'estado' ] ) );
	$id_pasarela = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_pasarela' ] ) );
	$hoy = date("Y-m-d");
	$consulta = "UPDATE pagos SET id_pasarela = '".$id_pasarela."', tipo_pago = ".$tipo_pago.", estado = ".$estado.", editor = ".$id_veris[1]." WHERE id = " . $id;
	if ( $result = $mysqli->query( $consulta ) ) {
		if($estado == 1){
			$consulta = "SELECT valor FROM pagos WHERE id = ". $id;
			if ( $result = $mysqli->query( $consulta ) ) {
				$pagov = mysqli_fetch_object( $result );
				$result->close();
			}
			
			if($tipo == 1){
				$consulta = "SELECT pla.*, plr.fin AS reffin FROM plan AS pla LEFT JOIN plan AS plr ON (plr.id = pla.referido) WHERE pla.id_pago = ".$id;
				if ( $result = $mysqli->query( $consulta ) ) {
					$plan = mysqli_fetch_object( $result );    
					$result->close();
					$dias = $plan->dias;
					$plan_fin = ($plan->referido != 0)?date("Y-m-d", strtotime($plan->reffin)):date("Y-m-d", strtotime($plan->creado));
					$fin = (strtotime($hoy) > strtotime($plan_fin))?date("Y-m-d", strtotime("+".$dias." days")):date("Y-m-d", strtotime($plan_fin." +".$dias." days"));					
					$totalact = ($plan->renovadas != '' && count(explode(',',$plan->renovadas)) > 0)?count(explode(',',$plan->renovadas)):0;
					$extras = $plan->propiedades - $totalact;
					$consulta = "UPDATE plan SET fin = '".$fin."', estado = ".$estado.", editor = ".$id_veris[1]." WHERE id = " . $plan->id;
					if ( $result = $mysqli->query( $consulta ) ) {
						if($plan->referido != 0){
							$consulta = "UPDATE plan SET fin = '".$hoy."', estado = 3, editor = ".$id_veris[1]." WHERE id = " . $plan->referido;
							$result = $mysqli->query( $consulta );
						}
						if($extras > 0){
                            $valores = array();
                            for($i = 0; $i < intval($extras); $i++){
                                array_push($valores, "(".$plan->id.", '".$fin."', ".$id_user.", ".$id_veris[1].", NOW())");
                            }
                            $consulta = "INSERT INTO propiedades (plan_id, final, id_user, editor, creado) VALUES ". implode(',', $valores);
                            $result = $mysqli->query( $consulta );								
                        }
						if($totalact > 0){
                            $consulta = "UPDATE propiedades SET final = '".$fin."', plan_id = ".$plan->id.", editor = ".$id_veris[1]." WHERE id IN (".$plan->renovadas.")";
                            $result = $mysqli->query( $consulta );
                        }
						
						$consulta = "INSERT INTO balance (nombre, valor, moneda, tipo, id_propiedad, editor, fecha, creado) VALUES ('".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', '".$pagov->valor."', 1, 2, 0, ".$id_user.", '".date('Y-m-d')."', NOW())";
						if ( $result = $mysqli->query( $consulta ) ) {
							$data = array( 'id' => $id, 'msg' => 'Tu plan <strong>'.$nombre_pago.'</strong>, con <strong>'.$plan->propiedades.'</strong> propiedades por <strong>'.$dias.'</strong> días, ha sido activado exitosamente.' );
							$resp = true;
						}
					}					
				}
			}else{
				$consulta = "SELECT id FROM contrato WHERE id_pago = ".$id;
				if ( $result = $mysqli->query( $consulta ) ) {
					$con = mysqli_fetch_object( $result );    
					$result->close();
					$consulta = "UPDATE contrato SET estado = ".$estado.", editor = ".$id_veris[1]." WHERE id = " . $con->id;
					if ( $result = $mysqli->query( $consulta ) ) {
						$consulta = "INSERT INTO balance (nombre, valor, moneda, tipo, id_propiedad, editor, fecha, creado) VALUES ('".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', '".$pagov->valor."', 1, 2, 0, ".$id_user.", '".date('Y-m-d')."', NOW())";
						if ( $result = $mysqli->query( $consulta ) ) {
							$data = array( 'id' => $id, 'msg' => 'Tu contrato <strong>'.$nombre_pago.'</strong>, ha sido activado exitosamente y esta disponible para ser completado.' );
							$resp = true;
						}
					}
				}
			}
		}else{
			if(($tipo == 1 && $estado == 2) || ($tipo == 2 && $estado == 2)){
				if($tipo == 1){
					$consulta = "DELETE FROM plan WHERE id_pago = ".$id;				
					if ( $result = $mysqli->query( $consulta ) ) {
						$data = array( 'id' => $id, 'msg' => 'El plan ha sido cancelado.' );
						$resp = true;					
					}
				}else{
					$consulta = "DELETE FROM contrato WHERE id_pago = ".$id;				
					if ( $result = $mysqli->query( $consulta ) ) {
						$data = array( 'id' => $id, 'msg' => 'El contrato ha sido cancelado.' );
						$resp = true;					
					}
				}				
			}else{
				$data = array( 'id' => $id, 'msg' => 'El movimiento ha sido actualizado.' );
				$resp = true;
			}			
		}
	}
}

if ( $accion == 14 ) {
	$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'editor' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
        $editor = $id_veri[1];
        $getvars[ 'editor' ] = $editor;
        $id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
		$relacion = strip_tags( $mysqli->real_escape_string( $getvars[ 'relacion' ] ) );
		$rol = strip_tags( $mysqli->real_escape_string( $getvars[ 'rol' ] ) );
		$nombre = strip_tags( $mysqli->real_escape_string( $getvars[ 'nombre' ] ) );
		$email = strip_tags( $mysqli->real_escape_string( $getvars[ 'email' ] ) );
		$celular = ($getvars[ 'celular' ] != '')?strip_tags( $mysqli->real_escape_string( $getvars[ 'celular' ] ) ):0;
		$pais = strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) );
		$especialidad = (is_array($getvars['especialidad']))?implode(',', $getvars['especialidad']):strip_tags( $mysqli->real_escape_string( str_replace("'", "´", $getvars['especialidad']) )); 		
        //$invalor = (preg_match('!!u', $invalor))?mb_convert_encoding($invalor,'HTML-ENTITIES','utf-8'):$invalor;
		if($id != 0){
			$consulta = "SELECT id FROM admins WHERE email = '" . $email . "' AND id != " . $id . " LIMIT 1";
			if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
				$result->close();
				if($rows > 0){
					$data = array( 'id' => $id, 'msg' => 'El correo electrónico <strong>' . $email . '</strong> ya está siendo usado, debes usar otra dirección única para este usuario.' );
					$resp = false;
				}else{
					$consulta2 = "UPDATE aliados SET nombre = '".mb_convert_encoding($nombre,'HTML-ENTITIES','utf-8')."', email = '".$email."', pais = ".$pais.", celular = ".$celular.", especialidad = '".$especialidad."', editor = ".$editor." WHERE id = " . $relacion;
					if ( $result2 = $mysqli->query( $consulta2 ) ) {
						$data = array( 'id' => $id, 'msg' => 'Los datos han sido actualizados.' );
                    	$resp = true;
					}
				}
			}
		}else{
			$consulta = "SELECT id FROM admins WHERE email = '" . $email . "' LIMIT 1";
			if ( $result = $mysqli->query( $consulta ) ) {
                $rows = $result->num_rows;
				$result->close();
				if($rows > 0){
					$data = array( 'id' => $id, 'msg' => 'El correo electrónico <strong>' . $email . '</strong> ya está siendo usado, debes usar otra dirección única para este usuario.' );
					$resp = false;
				}else{				
					$consulta2 = "INSERT INTO aliados (nombre, email, pais, celular, tipo, especialidad, editor, creado) VALUES('".mb_convert_encoding($nombre,'HTML-ENTITIES','utf-8')."', '".$email."', ".$pais.", ".$celular.", ".$rol.", '".$especialidad."', ".$editor.", NOW())";
					if ( $result2 = $mysqli->query( $consulta2 ) ) {
						$iden = $mysqli->insert_id;
						$new_user = cleanTexto($nombre);
						$contrasenain = generateRandomString(6);
						$contrasena = simple_crypt( $conArr['enc_string'].'***'.$contrasenain, 'e', $conArr['enc_string'] );
						$consulta2 = "UPDATE admins SET usuario = '".$new_user."', contrasena = '".$contrasena."' WHERE rol = ".$rol." AND relacion = " . $iden;
						if ( $result2 = $mysqli->query( $consulta2 ) ) {
							$data = array( 'id' => $iden, 'msg' => 'Los datos han sido guardados y un correo con los datos de acceso ha sido enviado al usuario.' );
                    		$resp = true;							
							$extra_data = array('nombre' => $nombre, 'usuario' => $new_user, 'pass' => $contrasenain, 'urlsitio' => $conArr['base_url_sitio'], 'urladmin' => $conArr['base_url'] );
							$consulta2 = "SELECT subject, texto, plano FROM mails WHERE id = 1";
							if ( $result2 = $mysqli->query( $consulta2 ) ) {
								$mailin = mysqli_fetch_object( $result2 );
								$result2->close();
							}					
							email( $email, utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );							
						}						
					}
				}
			}
		}
        
	}
}

if ( $accion == 15 ) {
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$tipo = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo' ] ) );
	$logs = array();
	$main = array();
	$consulta = "SELECT nov.id, nov.nombre, nov.id_user, nov.id_responsable, LPAD(nov.id_propiedad,6,0) AS propiedad, nov.texto, nov.adjunto, nov.vista, nov.tipo, nov.estado, nov.fin, nov.comentario, nov.creado, nov.editor, user.nombre AS us_nombre, res.nombre AS res_nombre, edt.nombre AS edit_nombre FROM novedades AS nov LEFT JOIN admins AS user ON (user.id = nov.id_user) LEFT JOIN admins AS res ON (res.id = nov.id_responsable) LEFT JOIN admins AS edt ON (edt.id = nov.editor) WHERE nov.id = ". $id;
	if ( $result = $mysqli->query( $consulta ) ) {
		$datos = mysqli_fetch_object( $result );		
		$main = array(
			'id' => $datos->id,
			'nombre' => utf8_encode( $datos->nombre ),
			'user' => utf8_encode( $datos->us_nombre ),
			'responsable' => utf8_encode( $datos->res_nombre ),
			'editor' => utf8_encode( $datos->edit_nombre ),
			'propiedad' => $datos->propiedad,
			'texto' => utf8_encode( nl2br($datos->texto) ),
			'adjunto' => ($datos->adjunto != '')?$conArr['base_url_sitio'].'/novedades/'.$datos->adjunto:'',
			'vista' => $datos->vista,
			'tipo' => $datos->tipo,
			'fin' => $datos->fin,
			'estado' => $datos->estado,
			'comentario' => utf8_encode( nl2br($datos->comentario) ),
			'creado' => $datos->creado,
			'editable' => ($tipo == 1 && $datos->id_user == $datos->editor)?true:false
		);
		$result->close();
		$consulta2 = "SELECT nov.id, nov.nombre, nov.id_user, nov.id_responsable, nov.texto, nov.adjunto, nov.vista, nov.tipo, nov.creado, nov.editor, user.nombre AS us_nombre, res.nombre AS res_nombre, edit.nombre AS edit_nombre FROM novedades AS nov LEFT JOIN admins AS user ON (user.id = nov.id_user) LEFT JOIN admins AS res ON (res.id = nov.id_responsable) LEFT JOIN admins AS edit ON (edit.id = nov.editor) WHERE nov.referente = ". $id . " ORDER BY id ASC";
        if ( $result2 = $mysqli->query( $consulta2 ) ) {
            while($row = $result2->fetch_assoc()){
				array_push($logs, array(
					'id' => $row['id'],
					'nombre' => utf8_encode( $row['nombre'] ),
					'user' => utf8_encode( $row['us_nombre'] ),
					'responsable' => utf8_encode( $row['res_nombre'] ),
					'editor' => utf8_encode( $row['edit_nombre'] ),					
					'texto' => utf8_encode( nl2br($row['texto']) ),
					'adjunto' => ($row['adjunto'] != '')?$conArr['base_url_sitio'].'/novedades/'.$row['adjunto']:'',
					'vista' => $row['vista'],
					'tipo' => $row['tipo'],
					'creado' => $row['creado'],
					'editable' => ($tipo == 1 && $row['id_user'] == $row['editor'])?true:(($tipo == 2 && $row['id_user'] != $row['editor'])?true:false)
				));				
            }
            $result2->close();
        }
		$data = array( 'main' => $main, 'logs' => $logs );
        $resp = true;
	}
}

if ( $accion == 16 ) {
	$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'editor' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
		$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
		$editor = $id_veri[1];
		$consulta = "UPDATE novedades SET vista = 1, editor = ".$editor." WHERE id = " . $id;
        if ( $result = $mysqli->query( $consulta ) ) {
            $data = array( 'id' => $id, 'msg' => 'Los datos han sido actualizados.' );
            $resp = true;
        }
	}	
}

if ( $accion == 17 ) {
	date_default_timezone_set('America/Bogota');
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$referencia = strip_tags( $mysqli->real_escape_string( $getvars[ 'referencia' ] ) );
	$nombre_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'nombre_pago' ] ) );
	$id_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_pago' ] ) );
	$tipo = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo' ] ) );
	$estado = strip_tags( $mysqli->real_escape_string( $getvars[ 'estado' ] ) );
	$id_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_user' ] ) );
	$id_abogado = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_abogado' ] ) );
	$email_user = strip_tags( $mysqli->real_escape_string( $getvars[ 'email_user' ] ) );
	$valor = strip_tags( $mysqli->real_escape_string( $getvars[ 'valor' ] ) );
	$valor_abg = strip_tags( $mysqli->real_escape_string( $getvars[ 'valor_abg' ] ) );
	$tipo_pago = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo_pagon' ] ) );
	$id_pasarela = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_pasarelan' ] ) );
	$consulta = "INSERT INTO pagos (id_pago, id_pasarela, tipo_pago, referencia, nombre_pago, tipo, valor, valor_abg, id_abogado, id_user, estado, editor, creado) VALUES (".$id_pago.", '".$id_pasarela."', ".$tipo_pago.", '".$referencia."', '".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', ".$tipo.", '".$valor."', '".$valor_abg."', ".$id_abogado.", ".$id_user.", ".$estado.", ".$id_veris[1].", NOW())";
	if ( $result = $mysqli->query( $consulta ) ) {
		$iden = $mysqli->insert_id;			
		if($estado <= 1){
			$consulta = "INSERT INTO contrato (id_user, id_contrato, id_pago, estado, editor, creado) VALUES (".$id_user.", ".$id_pago.", ".$iden.", ".$estado.", ".$id_veris[1].", NOW())";		
            if ( $result = $mysqli->query( $consulta ) ) {
                $iden = $mysqli->insert_id;
                if($estado == 1){
					$consulta = "INSERT INTO balance (nombre, valor, moneda, tipo, id_propiedad, editor, fecha, creado) VALUES ('".mb_convert_encoding($nombre_pago,'HTML-ENTITIES','utf-8')."', '".$valor."', 1, 2, 0, ".$id_user.", '".date('Y-m-d')."', NOW())";
					if ( $result = $mysqli->query( $consulta ) ) {
						$data = array( 'id' => $iden, 'msg' => 'Tu contrato <strong>'.$nombre_pago.'</strong>, ha sido activado exitosamente y esta disponible para ser completado.' );
                    	$resp = true;
					}
                }else{
                    $data = array( 'id' => $iden, 'msg' => 'Tu contrato <strong>'.$nombre_pago.'</strong>, ha sido creado y a la espera de activación, cuando esto suceda podras completarlo y descargarlo.' );
                    $resp = true;
                }					
            }						
		}else{
			$data = array( 'id' => $iden, 'msg' => ($estado == 2)?'Tu contrato <strong>'.$nombre_pago.'</strong>, ha sido cancelado.':'Tu contrato <strong>'.$nombre_pago.'</strong>, ha sido creado y esta pendiente de pago.' );
			$resp = true;
		}
		
	}
}

if ( $accion == 18 ) {
	$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $getvars[ 'editor' ] ) ), 'd', $conArr['enc_string'] ));
    if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
		$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
		$editor = $id_veri[1];
		$id_contrato = strip_tags( $mysqli->real_escape_string( $getvars[ 'id_contrato' ] ) );
		$variable = strip_tags( $mysqli->real_escape_string( $getvars[ 'variables' ] ) );
		$consulta = "SELECT imagen FROM cabezera ORDER BY id ASC LIMIT 1";
		if ( $result = $mysqli->query( $consulta ) ) {
			$cabe = mysqli_fetch_object( $result );		
			$result->close();
			$consulta = "SELECT id, nombre, texto FROM contratos WHERE id = ".$id_contrato;
			if ( $result = $mysqli->query( $consulta ) ) {
				$cont = mysqli_fetch_object( $result );
				$result->close();
				$mensaje = utf8_encode( $cont->texto );
				$orvar = json_decode( $getvars[ 'variables' ], true );
				$variables = array();
				foreach ( $orvar as $key => $val ) {
					$variables[$val['nombre']] = nl2br($val['valor']);
				}				
				foreach ( $variables as $key => $val ) {
					$mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
				}				
				$html = '<html>
				<head>
				<meta charset="utf-8">			
				<style>
				footer { position: fixed; right: 0px; bottom: 10px; text-align: right; color:#393939; font-size: 10px;}
				footer .page:after { content: counter(page, decimal); }
				@page { margin: 30px 30px 40px 50px; }				
				</style>	
				</head>
				<body>				
				<footer>
				<small class="page"></small>
				</footer>
				<main>
				<div style="display: block; width: 100%"><img src="data:image/png;base64,'.base64_encode(file_get_contents('../'.$conArr['updestino'].'contratos/'.$cabe->imagen)).'" style="width:100%"/></div>
				<div style="display: block;">
				'.$mensaje.'
				</div>
				</main> 
				</body>
				</html>';
				$nombrecont = $id.'_'.cleanTexto(utf8_encode( $cont->nombre )).'.pdf';
				$options = new Options(); 
				$options->set('isPhpEnabled', 'true'); 
				$options->set('enable_remote',true);
				$options->set('isFontSubsettingEnabled',true);
				$options->set('IsHtml5ParserEnabled',true);			
				$dompdf = new Dompdf($options);			
				$dompdf->loadHtml($html);
				$dompdf->render();
				$output = $dompdf->output();
				file_put_contents('../'.$conArr['updestino'].'contratos/'.$nombrecont, $output);
				$consulta = "UPDATE contrato SET texto = '".$cont->texto."', variables = '".$variable."', archivo = '".$nombrecont."', estado = 2, editor = ".$editor." WHERE id = " . $id;
				if ( $result = $mysqli->query( $consulta ) ) {
					$data = array( 'id' => $id, 'msg' => 'El contrato ha sido generado.' );
					$resp = true;
				}				
			}
		}		
	}	
}

if ( $accion == 19 ) {	
	$tipo = strip_tags( $mysqli->real_escape_string( $getvars[ 'tipo' ] ) );
	$pais = strip_tags( $mysqli->real_escape_string( $getvars[ 'pais' ] ) );
	$especialidad = strip_tags( $mysqli->real_escape_string( $getvars[ 'especialidad' ] ) );
	$atipo = ($tipo == 1)?6:7;
	$aliados = array();
	$consulta = "SELECT alia.*, admi.id AS admin_id, CONCAT(COALESCE(nov.valor, 0), ' / ', COALESCE(nov.total, 0), ' votos') AS votacion FROM aliados AS alia LEFT JOIN admins AS admi ON (admi.relacion = alia.id AND admi.rol = ".$atipo.") LEFT JOIN (SELECT id_responsable, COUNT(id) AS total, ROUND(AVG(valoracion), 2) AS valor FROM novedades WHERE referente = 0 AND estado > 0 GROUP BY id_responsable) AS nov ON (nov.id_responsable = admi.id) WHERE alia.pais = ". $pais . " AND FIND_IN_SET(".$especialidad.", alia.especialidad) AND alia.tipo = ".$atipo." AND admi.id IS NOT NULL ORDER BY alia.nombre ASC";
	if ( $result = $mysqli->query( $consulta ) ) {
		while($row = $result->fetch_assoc()){			
			$web = '';
			$facebook = '';
			$instagram = '';
			$linkedin = '';
			if($row['redes'] != ''){
				$redes = json_decode( $row['redes'], true );				
				foreach ( $redes as $key => $val ) {					
					if($val['id'] == 'web'){
						$web = $val['dir'];
					}
					if($val['id'] == 'facebook'){
						$facebook = $val['dir'];
					}
					if($val['id'] == 'instagram'){
						$instagram = $val['dir'];
					}
					if($val['id'] == 'linkedin'){
						$linkedin = $val['dir'];
					}
				}
			}			
			array_push($aliados, array(
                'id' => $row['admin_id'],
                'nombre' => utf8_encode( $row['nombre'] ),
                'email' => $row['email'],
				'celular' => $row['celular'],
				'adjunto' => $row['adjunto'],
				'logo' => $row['logo'],
				'votacion' => $row['votacion'],
                'web' => $web,
				'facebook' => $facebook,
				'instagram' => $instagram,
				'linkedin' => $linkedin
            ));
		}
		$result->close();
	}
	$data = array( 'aliados' => $aliados );
    $resp = true;
}

if ( $accion == 20 ) {	
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );
	$resenas = array();
	$consulta = "SELECT nov.comentario, nov.valoracion, nov.fecha, user.nombre  FROM novedades AS nov LEFT JOIN admins AS admi ON (admi.id = nov.id_user) LEFT JOIN usuarios AS user ON (user.id = admi.relacion) WHERE nov.id_responsable = ". $id . " AND nov.estado > 0 AND nov.referente = 0 ORDER BY nov.fecha ASC";
	if ( $result = $mysqli->query( $consulta ) ) {
		while($row = $result->fetch_assoc()){
			array_push($resenas, array(                
                'nombre' => utf8_encode( $row['nombre'] ),
                'fecha' => $row['fecha'],				
				'votacion' => $row['valoracion'],
                'comentario' => utf8_encode( nl2br($row['comentario']) )
            ));
		}
		$result->close();
	}
	$data = array( 'resenas' => $resenas );
    $resp = true;
}

if ( $accion == 21 ) {
	$id = strip_tags( $mysqli->real_escape_string( $getvars[ 'id' ] ) );	
	$logs = array();
	$consulta2 = "SELECT nov.id, nov.nombre, nov.id_user, nov.id_responsable, nov.texto, nov.vista, nov.tipo, nov.creado, nov.editor, res.nombre AS res_nombre FROM novedades AS nov LEFT JOIN admins AS res ON (res.id = nov.id_responsable) WHERE nov.tipo = 3 AND nov.id_propiedad = ". $id . " ORDER BY id ASC";
    if ( $result2 = $mysqli->query( $consulta2 ) ) {
        while($row = $result2->fetch_assoc()){
            array_push($logs, array(
                'id' => $row['id'],
                'nombre' => utf8_encode( $row['nombre'] ),                
                'responsable' => utf8_encode( $row['res_nombre'] ),                				
                'texto' => utf8_encode( nl2br($row['texto']) ),                
                'vista' => $row['vista'],
                'tipo' => $row['tipo'],
                'creado' => $row['creado'],				
                'editable' => true
            ));				
        }
        $result2->close();
    }
	$data = array( 'logs' => $logs );
    $resp = true;	
}

$respuesta = array( 'resp' => $resp, 'data' => $data );
$mysqli->close();
echo json_encode( $respuesta );

function getUserIP() {
	if ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $_SERVER ) && !empty( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] ) ) {
		if ( strpos( $_SERVER[ 'HTTP_X_FORWARDED_FOR' ], ',' ) > 0 ) {
			$addr = explode( ",", $_SERVER[ 'HTTP_X_FORWARDED_FOR' ] );
			return trim( $addr[ 0 ] );
		} else {
			return $_SERVER[ 'HTTP_X_FORWARDED_FOR' ];
		}
	} else {
		return $_SERVER[ 'REMOTE_ADDR' ];
	}
}

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function cleanTexto($string) {
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

   return strtolower(preg_replace('/[^A-Za-z0-9\_]/', '', $string)); // Removes special chars.
}

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}

function convertMethod($text){
    //Problem is that utf8_decode convert HTML chars for &bdquo; and other to ? or &nbsp; to \xA0. And you can not replace, that they are in some char bytes and you broke cyrillic (or other alphabet) chars.
    $problem_enc=array(
        'euro',
        'sbquo',
        'bdquo',
        'hellip',
        'dagger',
        'Dagger',
        'permil',
        'lsaquo',
        'lsquo',
        'rsquo',
        'ldquo',
        'rdquo',
        'bull',
        'ndash',
        'mdash',
        'trade',
        'rsquo',
        'brvbar',
        'copy',
        'laquo',
        'reg',
        'plusmn',
        'micro',
        'para',
        'middot',
        'raquo',
        'nbsp'
    );
    $text=mb_convert_encoding($text,'HTML-ENTITIES','UTF-8');
    $text=preg_replace('#(?<!\&ETH;)\&('.implode('|',$problem_enc).');#s','--amp{$1}',$text);
    $text=mb_convert_encoding($text,'UTF-8','HTML-ENTITIES');
    $text=utf8_decode($text);
    $text=mb_convert_encoding($text,'HTML-ENTITIES','UTF-8');
    $text=preg_replace('#\-\-amp\{([^\}]+)\}#su','&$1;',$text);
    $text=mb_convert_encoding($text,'UTF-8','HTML-ENTITIES');
    return $text;
}

function utf8_encode_deep(&$input) {
	if (is_string($input)) {
		$input = mb_convert_encoding($input,'UTF-8','HTML-ENTITIES');
	} else if (is_array($input)) {
		foreach ($input as &$value) {
			utf8_encode_deep($value);
		}
		
		unset($value);
	} else if (is_object($input)) {
		$vars = array_keys(get_object_vars($input));
		
		foreach ($vars as $var) {
			utf8_encode_deep($input->$var);
		}
	}
}

function deleteAll($str) {
    //It it's a file.
    if (is_file($str)) {
        //Attempt to delete it.
        return unlink($str);
    }
    //If it's a directory.
    elseif (is_dir($str)) {
        //Get a list of the files in this directory.
        $scan = glob(rtrim($str,'/').'/*');
        //Loop through the list of files.
        foreach($scan as $index=>$path) {
            //Call our recursive function.
            deleteAll($path);
        }
        //Remove the directory itself.
        return @rmdir($str);
    }
}

function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}

function email( $para, $titulo, $mensaje, $mensaje_txt, $extra_data, $mysqli, $copia = false ) {
	$to = $para;
	$subject = $titulo;
    
    $correo = false;
    $consulta = "SELECT tipo, nombre, email, servidor, puerto, contrasena FROM correo ORDER BY id ASC LIMIT 1";
    if ( $result = $mysqli->query( $consulta ) ) {
        $correo = mysqli_fetch_object( $result );
        $result->close();
    }
    if($correo){
        if($correo->tipo == 1){
            try {
                $mail = new PHPMailer(true);
                $mail->SMTPDebug = 0;
                $mail->isSMTP();
                $mail->Host = $correo->servidor;
                $mail->SMTPAuth = true;
                $mail->Username = $correo->email;
                $mail->Password = $correo->contrasena;
                $mail->SMTPSecure = 'tls';
                $mail->Port = $correo->puerto;

                $mail->setFrom($correo->email, utf8_encode( $correo->nombre ));
                $mail->addAddress($to, $extra_data['nombre']);
                $mail->addReplyTo($correo->email, utf8_encode( $correo->nombre ));
                if($copia){
                    $mail->addBCC($copia);
                }            


                foreach ( $extra_data as $key => $val ) {
                    $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
                }

                foreach ( $extra_data as $key => $val ) {
                    $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
                }

                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = utf8_decode($mensaje);
                $mail->AltBody = utf8_decode($mensaje_txt);

                $mail->send();
                return true;
            } catch (Exception $e) {
                return false;
            }
        }
        if($correo->tipo == 2){
            $to = $para;

            $boundary = uniqid( 'np' );

            $subject = $titulo;

            $headers = "From: ".utf8_encode( $correo->nombre )." <" . strip_tags( $correo->email ) . ">\r\n";
            if ( $copia ) {
                $headers .= "CC: " . strip_tags( $copia ) . "\r\n";
            }
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "To: " . $to . "\r\n";
            $headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";

            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Return-Path: ".utf8_encode( $correo->nombre )." <" . strip_tags( $correo->email ) . ">\r\n";
            $headers .= "Organization: ".utf8_encode( $correo->nombre )."\r\n";
            //$headers .= "MIME-Version: 1.0\r\n";
            //$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

            //here is the content body
            $message = "";
            $message .= "\r\n\r\n--" . $boundary . "\r\n";
            $message .= "Content-type: text/plain;charset=UTF-8\r\n\r\n";
            //Plain text body
            foreach ( $extra_data as $key => $val ) {
                $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
            }

            $message .= $mensaje_txt;
            $message .= "\r\n\r\n--" . $boundary . "\r\n";

            $message .= "Content-type: text/html;charset=UTF-8\r\n\r\n";

            foreach ( $extra_data as $key => $val ) {
                $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
            }

            $message .= $mensaje;
            $message .= "\r\n\r\n--" . $boundary . "--";
            return mail( $to, $subject, $message, $headers, '-f '.$correo->email.' -F "'.utf8_encode( $correo->nombre ).'"' );
        }
        if($correo->tipo == 3){
            try {
                $mandrill = new Mandrill($correo->servidor);
                foreach ( $extra_data as $key => $val ) {
                    $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
                }

                foreach ( $extra_data as $key => $val ) {
                    $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
                }
                $message = array(
                    'html' => $mensaje,
                    'text' => $mensaje_txt,
                    'subject' => $subject,
                    'from_email' => $correo->email,
                    'from_name' => utf8_encode( $correo->nombre ),
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $extra_data['nombre'],
                            'type' => 'to'
                        )
                    ),
                    'headers' => array('Reply-To' => $correo->email),
                    'important' => false,
                    'track_opens' => null,
                    'track_clicks' => null,
                    'auto_text' => null,
                    'auto_html' => null,
                    'inline_css' => null,
                    'url_strip_qs' => null,
                    'preserve_recipients' => null,
                    'view_content_link' => null,
                    'bcc_address' => ($copia)?$copia:'',
                    'tracking_domain' => null,
                    'signing_domain' => null,
                    'return_path_domain' => null,
                    'merge' => true,
                    'merge_language' => 'mailchimp',                
                    'tags' => array()
                );
                $async = false;
                $result = $mandrill->messages->send($message, $async);
                return true;
            }catch(Mandrill_Error $e){
                return false;
            }            
            
        }
    }else{
        return false;        
    }
}
?>