<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

require_once 'mandrill-api-php/src/Mandrill.php';

date_default_timezone_set('America/Bogota');
$fecha = date('Y-m-d');
$fecha_10 = date('Y-m-d', strtotime( '+10 days' ));
$fecha_1 = date('Y-m-d');
$fecha_10m = date('Y-m-d', strtotime( '-10 days' ));

echo $fecha_1;

$sms1 = array();
$consulta = "SELECT subject, texto, plano, sms FROM mails WHERE id = 4";
if ( $result = $mysqli->query( $consulta ) ) {
    $mailin = mysqli_fetch_object( $result );
    $result->close();
}
$consulta = "SELECT pla.id, user.celular, user.email, user.nombre FROM plan AS pla LEFT JOIN admins AS admi ON (admi.id = pla.id_user) LEFT JOIN usuarios AS user ON (user.id = admi.relacion) WHERE pla.fin = '".$fecha_1."' AND pla.estado != 3 AND pla.estado != 2 AND pla.estado != 0";
if ( $result = $mysqli->query( $consulta ) ) {
	while($row = $result->fetch_assoc()){
		$consulta2 = "UPDATE plan SET estado = 2 WHERE id = " . $row['id'];
		if ( $result2 = $mysqli->query( $consulta2 ) ) {
			$consulta2 = "UPDATE propiedades SET estado = 3 WHERE plan_id = " . $row['id'];
			$result2 = $mysqli->query( $consulta2 );
		}
		if($row['celular'] != '' && $row['celular'] != 0){
			array_push($sms1, $row['celular']);
		}
		if($row['email'] != ''){
			$extra_data = array('nombre' => utf8_encode( $row['nombre'] ), 'urladmin' => $conArr['base_url'] );
            email( $row['email'], utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );
		}        				
    }
    $result->close();
	if(count($sms1) > 0){
		$extra_data = array('urladmin' => $conArr['base_url'] );
		sendSMS( $sms1, utf8_encode( $mailin->sms ), $extra_data, $mysqli );
	}
}

$sms2 = array();
$consulta = "SELECT pla.id, user.celular, user.email, user.nombre FROM plan AS pla LEFT JOIN admins AS admi ON (admi.id = pla.id_user) LEFT JOIN usuarios AS user ON (user.id = admi.relacion) WHERE pla.fin = '".$fecha_10m."' AND pla.estado != 3 AND pla.estado != 0";
if ( $result = $mysqli->query( $consulta ) ) {
	while($row = $result->fetch_assoc()){
		$consulta2 = "UPDATE plan SET estado = 2 WHERE id = " . $row['id'];
		if ( $result2 = $mysqli->query( $consulta2 ) ) {
			$consulta2 = "UPDATE propiedades SET estado = 3 WHERE plan_id = " . $row['id'];
			$result2 = $mysqli->query( $consulta2 );
		}
		if($row['celular'] != '' && $row['celular'] != 0){
			array_push($sms2, $row['celular']);
		}
		if($row['email'] != ''){
			$extra_data = array('nombre' => utf8_encode( $row['nombre'] ), 'urladmin' => $conArr['base_url'] );
            email( $row['email'], utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );
		}        				
    }
    $result->close();
	if(count($sms2) > 0){
		$extra_data = array('urladmin' => $conArr['base_url'] );
		sendSMS( $sms2, utf8_encode( $mailin->sms ), $extra_data, $mysqli );
	}
}

$consulta = "SELECT subject, texto, plano, sms FROM mails WHERE id = 3";
if ( $result = $mysqli->query( $consulta ) ) {
    $mailin = mysqli_fetch_object( $result );
    $result->close();
}
$sms3 = array();
$consulta = "SELECT pla.id, user.celular, user.email, user.nombre FROM plan AS pla LEFT JOIN admins AS admi ON (admi.id = pla.id_user) LEFT JOIN usuarios AS user ON (user.id = admi.relacion) WHERE pla.fin = '".$fecha_10."' AND pla.estado != 3 AND pla.estado != 2 AND pla.estado != 0";
if ( $result = $mysqli->query( $consulta ) ) {
	while($row = $result->fetch_assoc()){		
		if($row['celular'] != '' && $row['celular'] != 0){
			array_push($sms3, $row['celular']);
		}
		if($row['email'] != ''){
			$extra_data = array('nombre' => utf8_encode( $row['nombre'] ), 'urladmin' => $conArr['base_url'] );
            email( $row['email'], utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );
		}        				
    }
    $result->close();
	if(count($sms3) > 0){
		$extra_data = array('urladmin' => $conArr['base_url'] );
		sendSMS( $sms3, utf8_encode( $mailin->sms ), $extra_data, $mysqli );
	}
}

$consulta = "SELECT subject, texto, plano FROM mails WHERE id = 7";
if ( $result = $mysqli->query( $consulta ) ) {
    $mailin = mysqli_fetch_object( $result );
    $result->close();
}
$consulta = "SELECT nov.id, user.celular, user.email, user.nombre, prop.fin FROM novedades AS nov LEFT JOIN admins AS admi ON (admi.id = nov.id_user) LEFT JOIN usuarios AS user ON (user.id = admi.relacion) LEFT JOIN propiedades AS prop ON (prop.id = nov.id_propiedad) WHERE nov.vista = 0 AND nov.tipo = 3";
if ( $result = $mysqli->query( $consulta ) ) {
	while($row = $result->fetch_assoc()){
		if($row['email'] != ''){
			$fin = ($row['fin'] == 1)?'Vendida':'Alquilada';
			$extra_data = array('nombre' => utf8_encode( $row['nombre'] ), 'fin' => $fin, 'urladmin' => $conArr['base_url'] );
            email( $row['email'], utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );
		}
	}    
    $result->close();    
}

$consulta = "SELECT subject, texto, plano FROM mails WHERE id = 5";
if ( $result = $mysqli->query( $consulta ) ) {
    $mailin = mysqli_fetch_object( $result );
    $result->close();
}
$consulta = "SELECT nov.id, user.celular, user.email, user.nombre FROM novedades AS nov LEFT JOIN admins AS admi ON (admi.id = nov.id_user) LEFT JOIN usuarios AS user ON (user.id = admi.relacion) WHERE nov.estado = 0 AND nov.referente = 0 AND nov.tipo < 3";
if ( $result = $mysqli->query( $consulta ) ) {
    while($row = $result->fetch_assoc()){
        $consulta2 = "SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE vista = 0 AND id_user != editor AND (referente = ".$row['id']." OR id = ".$row['id'].")";
        if ( $result2 = $mysqli->query( $consulta2 ) ) {
            $res = mysqli_fetch_object( $result2 );
            $result2->close();
            if($res->total > 0){
                $extra_data = array('nombre' => utf8_encode( $row['nombre'] ), 'urladmin' => $conArr['base_url'] );
            	email( $row['email'], utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );
            }
        }				
    }
    $result->close();
}

$consulta = "SELECT subject, texto, plano FROM mails WHERE id = 6";
if ( $result = $mysqli->query( $consulta ) ) {
    $mailin = mysqli_fetch_object( $result );
    $result->close();
}
$consulta = "SELECT nov.id, user.celular, user.email, user.nombre FROM novedades AS nov LEFT JOIN admins AS admi ON (admi.id = nov.id_responsable) LEFT JOIN aliados AS user ON (user.id = admi.relacion) WHERE nov.estado = 0 AND nov.referente = 0 AND nov.tipo < 3";
if ( $result = $mysqli->query( $consulta ) ) {
    while($row = $result->fetch_assoc()){
        $consulta2 = "SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE vista = 0 AND id_user = editor AND (referente = ".$row['id']." OR id = ".$row['id'].")";
        if ( $result2 = $mysqli->query( $consulta2 ) ) {
            $res = mysqli_fetch_object( $result2 );
            $result2->close();
            if($res->total > 0){
                $extra_data = array('nombre' => utf8_encode( $row['nombre'] ), 'urladmin' => $conArr['base_url'] );
            	email( $row['email'], utf8_decode(html_entity_decode($mailin->subject, ENT_QUOTES, "UTF-8")), utf8_encode( $mailin->texto ), utf8_encode( $mailin->plano ), $extra_data, $mysqli );
            }
        }				
    }
    $result->close();
}


$mysqli->close();

function cleanTexto($string) {
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

   return strtolower(preg_replace('/[^A-Za-z0-9\_]/', '', $string)); // Removes special chars.
}

function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}

function email( $para, $titulo, $mensaje, $mensaje_txt, $extra_data, $mysqli, $copia = false ) {
	$to = $para;
	$subject = $titulo;
    
    $correo = false;
    $consulta = "SELECT tipo, nombre, email, servidor, puerto, contrasena FROM correo ORDER BY id ASC LIMIT 1";
    if ( $result = $mysqli->query( $consulta ) ) {
        $correo = mysqli_fetch_object( $result );
        $result->close();
    }
    if($correo){
        if($correo->tipo == 1){
            try {
                $mail = new PHPMailer(true);
                $mail->SMTPDebug = 0;
                $mail->isSMTP();
                $mail->Host = $correo->servidor;
                $mail->SMTPAuth = true;
                $mail->Username = $correo->email;
                $mail->Password = $correo->contrasena;
                $mail->SMTPSecure = 'tls';
                $mail->Port = $correo->puerto;

                $mail->setFrom($correo->email, utf8_encode( $correo->nombre ));
                $mail->addAddress($to, $extra_data['nombre']);
                $mail->addReplyTo($correo->email, utf8_encode( $correo->nombre ));
                if($copia){
                    $mail->addBCC($copia);
                }            


                foreach ( $extra_data as $key => $val ) {
                    $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
                }

                foreach ( $extra_data as $key => $val ) {
                    $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
                }

                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = utf8_decode($mensaje);
                $mail->AltBody = utf8_decode($mensaje_txt);

                $mail->send();
                return true;
            } catch (Exception $e) {
                return false;
            }
        }
        if($correo->tipo == 2){
            $to = $para;

            $boundary = uniqid( 'np' );

            $subject = $titulo;

            $headers = "From: ".utf8_encode( $correo->nombre )." <" . strip_tags( $correo->email ) . ">\r\n";
            if ( $copia ) {
                $headers .= "CC: " . strip_tags( $copia ) . "\r\n";
            }
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "To: " . $to . "\r\n";
            $headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";

            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Return-Path: ".utf8_encode( $correo->nombre )." <" . strip_tags( $correo->email ) . ">\r\n";
            $headers .= "Organization: ".utf8_encode( $correo->nombre )."\r\n";
            //$headers .= "MIME-Version: 1.0\r\n";
            //$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

            //here is the content body
            $message = "";
            $message .= "\r\n\r\n--" . $boundary . "\r\n";
            $message .= "Content-type: text/plain;charset=UTF-8\r\n\r\n";
            //Plain text body
            foreach ( $extra_data as $key => $val ) {
                $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
            }

            $message .= $mensaje_txt;
            $message .= "\r\n\r\n--" . $boundary . "\r\n";

            $message .= "Content-type: text/html;charset=UTF-8\r\n\r\n";

            foreach ( $extra_data as $key => $val ) {
                $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
            }

            $message .= $mensaje;
            $message .= "\r\n\r\n--" . $boundary . "--";
            return mail( $to, $subject, $message, $headers, '-f '.$correo->email.' -F "'.utf8_encode( $correo->nombre ).'"' );
        }
        if($correo->tipo == 3){
            try {
                $mandrill = new Mandrill($correo->servidor);
                foreach ( $extra_data as $key => $val ) {
                    $mensaje_txt = str_replace( '[' . $key . ']', $val, $mensaje_txt );
                }

                foreach ( $extra_data as $key => $val ) {
                    $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
                }
                $message = array(
                    'html' => $mensaje,
                    'text' => $mensaje_txt,
                    'subject' => $subject,
                    'from_email' => $correo->email,
                    'from_name' => utf8_encode( $correo->nombre ),
                    'to' => array(
                        array(
                            'email' => $to,
                            'name' => $extra_data['nombre'],
                            'type' => 'to'
                        )
                    ),
                    'headers' => array('Reply-To' => $correo->email),
                    'important' => false,
                    'track_opens' => null,
                    'track_clicks' => null,
                    'auto_text' => null,
                    'auto_html' => null,
                    'inline_css' => null,
                    'url_strip_qs' => null,
                    'preserve_recipients' => null,
                    'view_content_link' => null,
                    'bcc_address' => ($copia)?$copia:'',
                    'tracking_domain' => null,
                    'signing_domain' => null,
                    'return_path_domain' => null,
                    'merge' => true,
                    'merge_language' => 'mailchimp',                
                    'tags' => array()
                );
                $async = false;
                $result = $mandrill->messages->send($message, $async);
                return true;
            }catch(Mandrill_Error $e){
                return false;
            }            
            
        }
    }else{
        return false;        
    }
}

function sendSMS( $para, $mensaje, $extra_data, $mysqli ) {
	$sms = false;
    $consulta = "SELECT codigo, endpoint FROM sms ORDER BY id ASC LIMIT 1";
    if ( $result = $mysqli->query( $consulta ) ) {
        $sms = mysqli_fetch_object( $result );
        $result->close();
    }
	if($sms){
		foreach ( $extra_data as $key => $val ) {
            $mensaje = str_replace( '[' . $key . ']', $val, $mensaje );
        }
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $sms->endpoint,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => '{
			"to": ['.implode(',',$para).'],
			"from": "NetPropi",
			"message": "'.$mensaje.'"
			}',
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json',
				'Authorization: Basic '.$sms->codigo
			)
		));
		
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;		
	}else{
		return false;
	}
}
?>