<?php
header('X-Frame-Options: DENY');
header('Content-Type: application/json', false);
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $getvars = sanitize( $_POST );
            unset( $_POST );
            $outData = upload($getvars);
            echo json_encode($outData); // return json data
            exit();            
        }else{
            echo 'intento inseguro';
            exit();
        }        
    }
}else{
	echo 'intento inseguro';
    exit();
}

function upload($getvars) {
	$origen = $getvars[ 'origen' ];	
	$target_dir = '../' . $getvars[ 'carpeta' ] . "/";
	if (!file_exists($target_dir)) {
        @mkdir($target_dir, 0755, true);
    }	
	if (isset($_FILES[ $origen ])) {
		$val = true;
		$resparr = array();
		$archivo = $_FILES[ $origen ];
		$filenames = $archivo['name'];
		if(is_array($filenames)){
			$total = count($filenames);
			$count = 0;
			foreach($_FILES[$origen]['tmp_name'] as $key => $tmp_name){
				$file_name = $key.$_FILES[$origen]['name'][$key];
				$file_size =$_FILES[$origen]['size'][$key];
				$file_tmp =$_FILES[$origen]['tmp_name'][$key];
				$file_type=$_FILES[$origen]['type'][$key];
				$temp = explode( ".", $file_name );
				$newfilename = round( microtime( true ) ). random_int(1, 60) . $count . '.' . array_pop( $temp );
				$target_file = $target_dir . $newfilename;
				$count++;
				if ( move_uploaded_file( $file_tmp, $target_file ) ) {
					array_push($resparr, $newfilename);				
				}else{
					$val = false;
					break;
				}
			}
			
			
			/*for( $i=0 ; $i < $total ; $i++ ) {
				$temp = explode( ".", $archivo[ "name" ][$i] );
				$newfilename = round( microtime( true ) ). random_int(1, 60) . $i . '.' . array_pop( $temp );
				$target_file = $target_dir . $newfilename;
				if ( move_uploaded_file( $archivo[ "tmp_name" ][$i], $target_file ) ) {
					array_push($resparr, $newfilename);				
				}else{
					$val = false;
					break;
				}			
			}*/
			if($val){
				return [
					'extras' => $resparr
				];
			}else{
				return [
					'error' => 'Error en la subida de archivos, inténtalo de nuevo.'
				];
			}
		}else{
			$temp = explode( ".", $archivo[ "name" ] );
			$newfilename = round( microtime( true ) ) . random_int(1, 60) . '.' . array_pop( $temp );
			$target_file = $target_dir . $newfilename;
			if ( move_uploaded_file( $archivo[ "tmp_name" ], $target_file ) ) {
				return [
					'extras' => $newfilename
				];
			}else{
				return [
					'error' => 'Error en la subida de archivos, inténtalo de nuevo.'
				];
			}			
		}
	}
	return [
        'error' => 'Error en la subida de archivos, inténtalo de nuevo.'
    ];
}

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}

function sanitize( $input ) {	
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>