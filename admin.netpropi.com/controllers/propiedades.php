<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
			$moneda = $_POST['moneda'];
			$fecha = date('Y-m-d');
            $filtro = '';
			if($_POST['rol'] == 5){
				$filtro = '`prop`.`id_user` = '.$id_veri[1];
			}

            // DB table to use
            $table = 'propiedades';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => 'LPAD(`prop`.`id`,6,0)',
                    'dt' => 0,
                    'field' => 'codigo',
					'as' => 'codigo'
                ),
                array(
                    'db' => '`prop`.`nombre`',
                    'dt' => 1,
                    'field' => 'nombrep',
					'as' => 'nombrep'
                ),
				array(
                    'db' => 'CONCAT(UPPER(`pag`.`referencia`),"-",LPAD(`pag`.`id`,6,0))',
                    'dt' => 2,
                    'field' => 'plan_ref',
					'as' => 'plan_ref'
                ),
				array(
                    'db' => '`prop`.`id_pais`',
                    'dt' => 3,
                    'field' => 'id_pais',		
                    'formatter' => function ( $d, $row ){
                        return ($d != 0)?$row[18]:'NA';
                    }
                ),
				array(
                    'db' => '`prop`.`tipo`',
                    'dt' => 4,
                    'field' => 'tipo',
                    'formatter' => function ( $d, $row ) {
						return ($d == 1)?'<span class="text-primary fw-bold">Comercial</span>':(($d == 2)?'<span class="text-info fw-bold">Vivienda</span>':'<span class="text-dark fw-bold">NA</span>');
					}
                ),
				array(
                    'db' => '`prop`.`fin`',
                    'dt' => 5,
                    'field' => 'fin',
                    'formatter' => function ( $d, $row ) {
						return ($d == 1)?'<span class="text-primary fw-bold">Venta</span>':(($d == 2)?'<span class="text-info fw-bold">Alquiler</span>':'<span class="text-dark fw-bold">NA</span>');
					}
                ),
				array(
                    'db' => '`usr`.`nombre`',
                    'dt' => 6,
                    'field' => 'nombre'
                ),
				array(
                    'db' => '`usr`.`email`',
                    'dt' => 7,
                    'field' => 'email'
                ),
				array(
                    'db' => 'ROUND((`prop`.`valor`), 2)',
                    'dt' => 8,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
				array(
                    'db' => '`prop`.`estado`',
                    'dt' => 9,
                    'field' => 'estado',
                    'formatter' => function ( $d, $row ) use ($fecha) {
						$limite = date('Y-m-d', strtotime($row[10]));
						if($limite < $fecha){
							return '<span class="text-danger fw-bold">Suspendida</span>';
						}else{
							if($row[1] != '' && $row[19] != '' && $row[20] != ''){
								return ($d == 1)?'<div class="d-grid gap-2"><button type="button" class="btn btn-success btn-sm fw-bold text-white" onClick="openData(\'estado\','.$row[17].'); return false">Publicada</button></div>':(($d == 2)?'<div class="d-grid gap-2"><button type="button" class="btn btn-warning btn-sm fw-bold text-white" onClick="openData(\'estado\','.$row[17].'); return false">Ocupada</button></div>':(($d == 3)?'<div class="d-grid gap-2"><button type="button" class="btn btn-danger btn-sm fw-bold text-white" onClick="openData(\'estado\','.$row[17].'); return false">Suspendida</button></div>':'<div class="d-grid gap-2"><button type="button" class="btn btn-primary btn-sm fw-bold text-white" onClick="openData(\'estado\','.$row[17].'); return false">Pendiente</button></div>'));
							}else{
								return '<span class="text-primary fw-bold">Pendiente</span>';
							}
						}
					}
                ),
				array(
                    'db' => '`prop`.`final`',
                    'dt' => 10,
                    'field' => 'final',
                    'formatter' => function ( $d, $row ) use ($fecha) {
						$limite = date('Y-m-d', strtotime('-10 days', strtotime($d)));						
						return ($limite < $fecha && $d > $fecha)?'<span class="text-info">'.$d.'</span>':(($d <= $fecha)?'<span class="text-danger">'.$d.'</span>':$d);
					}
                ),
				array(
                    'db' => '`sesionv`.`total`',
                    'dt' => 11,
                    'field' => 'totalv',
					'as' => 'totalv',
                    'formatter' => function ( $d, $row ){
                        $numero = (is_null($d))?0:$d;
                        return '<div class="d-grid gap-2"><button type="button" class="btn btn-info btn-sm text-white" onClick="showVis('.$row[17].', \''.$row[1].'\'); return false"><i class="fas fa-eye fa-fw"></i> <span class="font-weight-bold">'.$numero.'</span></button></div>';
                    }
                ),
				array(
                    'db' => '`sesionf`.`total`',
                    'dt' => 12,
                    'field' => 'totalf',
					'as' => 'totalf',
                    'formatter' => function ( $d, $row ){
                        $numero = (is_null($d))?0:$d;
                        return $numero;
                    }
                ),
				array(
                    'db' => '`sesions`.`total`',
                    'dt' => 13,
                    'field' => 'totals',
					'as' => 'totals',
                    'formatter' => function ( $d, $row ){
                        $numero = (is_null($d))?0:$d;
                        return '<div class="d-grid gap-2"><button type="button" class="btn btn-info btn-sm text-white" onClick="showSol('.$row[17].', \''.$row[1].'\'); return false"><i class="fas fa-tasks fa-fw"></i> <span class="font-weight-bold">'.$numero.'</span></button></div>';
                    }
                ),                	
                array(
                    'db' => '`prop`.`fecha`',
                    'dt' => 14,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 15,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`prop`.`creado`',
                    'dt' => 16,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`prop`.`id`',
                    'dt' => 17,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) {
						return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'propiedades\', ' . $d . '); return false"><span class="d-none d-sm-inline">editar</span> <i class="fas fa-edit fa-fw"></i></button></div>'; // <button type="button" class="btn btn-primary btn-sm text-white" onClick="openData(\'ubicacion\', ' . $d . '); return false"><i class="fas fa-map-signs fa-fw"></i> <span class="d-none d-sm-inline">ubicación</span></button><button type="button" class="btn btn-success btn-sm text-white" onClick="openData(\'fotos\', ' . $d . '); return false"><span class="d-none d-sm-inline">Fotos</span> <i class="fas fa-camera-retro fa-fw"></i></button>
                    }
                ),	
                array(
                    'db' => '`pais`.`name`',
                    'dt' => 18,
                    'field' => 'name_p',
                    'as' => 'name_p'
                ),	
                array(
                    'db' => '`prop`.`foto`',
                    'dt' => 19,
                    'field' => 'foto'
                ),	
                array(
                    'db' => '`prop`.`ubicacion`',
                    'dt' => 20,
                    'field' => 'ubicacion'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `propiedades` AS `prop` 
            LEFT JOIN `countries` AS `pais` ON (`pais`.`id` = `prop`.`id_pais`) 
            LEFT JOIN (SELECT id_propiedad, COUNT(id) AS total FROM ses_user GROUP BY id_propiedad) AS `sesionv` ON (`sesionv`.`id_propiedad` = `prop`.`id`) 
			LEFT JOIN (SELECT id_propiedad, COUNT(id) AS total FROM fav_prop GROUP BY id_propiedad) AS `sesionf` ON (`sesionf`.`id_propiedad` = `prop`.`id`) 
			LEFT JOIN (SELECT id_propiedad, COUNT(id) AS total FROM novedades WHERE tipo = 3 GROUP BY id_propiedad) AS `sesions` ON (`sesions`.`id_propiedad` = `prop`.`id`) 
			LEFT JOIN `plan` AS `plan` ON (`plan`.`id` = `prop`.`plan_id`) 
			LEFT JOIN `pagos` AS `pag` ON (`pag`.`id` = `plan`.`id_pago`)  
			LEFT JOIN `admins` AS `usr` ON (`usr`.`id` = `prop`.`id_user`) 
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `prop`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>