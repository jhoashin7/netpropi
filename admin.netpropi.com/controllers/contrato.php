<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
		$user = $_POST['user'];
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
            $moneda = $_POST['moneda'];
			$fecha = date('Y-m-d');
			$rol = $_POST['rol'];
            $filtro = ($rol == 7)?'`pag`.`id_abogado` = '.$id_veri[1]:(($rol == 5)?'`pag`.`id_user` = '.$id_veri[1]:'');           

            // DB table to use
            $table = 'contrato';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
				array(
                    'db' => 'CONCAT(UPPER(`pag`.`referencia`),"-",LPAD(`pag`.`id`,6,0))',
                    'dt' => 0,
                    'field' => 'refer',
					'as' => 'refer'
                ),
                array(
                    'db' => '`pag`.`nombre_pago`',
                    'dt' => 1,
                    'field' => 'nombre_pago'
                ),                
                array(
                    'db' => 'ROUND((`pag`.`valor` * '.$moneda.'), 2)',
                    'dt' => 2,
                    'field' => 'valor_r',
                    'as' => 'valor_r'
                ),
				array(
                    'db' => 'ROUND((`pag`.`valor_abg` * '.$moneda.'), 2)',
                    'dt' => 3,
                    'field' => 'valor_ab',
                    'as' => 'valor_ab'
                ),
                array(
                    'db' => '`user`.`nombre`',
                    'dt' => 4,
                    'field' => 'nombre'
                ),
				array(
                    'db' => '`user`.`iden`',
                    'dt' => 5,
                    'field' => 'iden'
                ),
				array(
                    'db' => '`pag`.`id_abogado`',
                    'dt' => 6,
                    'field' => 'id_abogado',
                    'formatter' => function ( $d, $row ) {
						return ($d != 0)?$row[14]:'NA';
					}
                ),
				array(
                    'db' => '`con`.`estado`',
                    'dt' => 7,
                    'field' => 'estado',
                    'formatter' => function ( $d, $row ) {
						return ($d == 1)?'<span class="text-warning fw-bold">Sin llenar</span>':(($d == 2)?'<span class="text-success fw-bold">Finalizado</span>':'<span class="text-primary fw-bold">Pendiente</span>');
					}
                ),				
                array(
                    'db' => '`pag`.`creado`',
                    'dt' => 8,
                    'field' => 'creado'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 9,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),                	
                array(
                    'db' => '`con`.`id`',
                    'dt' => 10,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) use ($fecha, $conArr, $user, $rol) {
						if($row[7] == 2){
							return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><a href="'.$conArr['base_url_sitio'].'/contratos/'.$row[13].'" class="btn btn-primary btn-sm text-white" target="blank"><i class="fas fa-file-contract fa-fw"></i> <span class="d-none d-sm-inline">visualizar</span></a></div>';
						}else{
							if($rol == 5){
								if($row[7] > 0){
									return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><a href="'.$conArr['base_url'].'/test_contrato.php?veruser='.$user.'&idcont='.$row[15].'&ver='.random_int(100, 999).'" class="btn btn-primary btn-sm text-white" target="blank"><i class="fas fa-file-contract fa-fw"></i> <span class="d-none d-sm-inline">previsualizar</span></a><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'fillcontrato\', ' . $d . '); return false"><span class="d-none d-sm-inline">completar</span> <i class="fas fa-edit fa-fw"></i></button></div>';
								}
							}else{
								return '';
							}							
						}
                    }
                ),
                array(
                    'db' => '`con`.`id_user`',
                    'dt' => 11,
                    'field' => 'id_user'
                ),
                array(
                    'db' => '`con`.`id_pago`',
                    'dt' => 12,
                    'field' => 'id_pago'
                ),
                array(
                    'db' => '`con`.`archivo`',
                    'dt' => 13,
                    'field' => 'archivo'
                ),
				array(
                    'db' => '`abg`.`nombre`',
                    'dt' => 14,
                    'field' => 'nombre_abg',
                    'as' => 'nombre_abg'
                ),
				array(
                    'db' => '`cons`.`id`',
                    'dt' => 15,
                    'field' => 'id_cons',
                    'as' => 'id_cons'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `contrato` AS `con`
			LEFT JOIN `pagos` AS `pag` ON (`pag`.`id` = `con`.`id_pago`) 
			LEFT JOIN `contratos` AS `cons` ON (`cons`.`id` = `con`.`id_contrato`) 
			LEFT JOIN `admins` AS `usad` ON (`usad`.`id` = `con`.`id_user`)
			LEFT JOIN `usuarios` AS `user` ON (`user`.`id` = `usad`.`relacion`) 
			LEFT JOIN `admins` AS `abg` ON (`abg`.`id` = `pag`.`id_abogado`) 
			LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `con`.`editor`)";

            $extraWhere = $filtro;
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>