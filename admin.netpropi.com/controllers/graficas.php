<?php
header('X-Frame-Options: DENY');
include 'db_connect.php';
$data = array();
$extras = '';
$accion = 0;

/*
rgba(13,202,240,0.4) info
rgba(0,183,74,0.4) success
rgba(255,169,0,0.4) warning
rgba(141,60,253,0.4) primary
rgba(249,49,84,0.4) danger
rgba(156,162,170,0.4) secondary
*/

$id_veri = explode('***', simple_crypt( strip_tags( $mysqli->real_escape_string( $_POST[ 'user' ] ) ), 'd', $conArr['enc_string'] ));
if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
	$rol = strip_tags($mysqli->real_escape_string($_POST['rol']));
    $accion = strip_tags($mysqli->real_escape_string($_POST['accion']));
    $fecha = strip_tags($mysqli->real_escape_string($_POST['fecha']));
    $filter = ( $fecha != '' ) ? explode( ' a ', $fecha ) : false;
    if($filter && count($filter) < 2){
        $filter[1] = $filter[0];
    }    
}

if($accion == 1){	
	$extrasand = ($rol == 5)?'AND id_user = '.$id_veri[1]:'';
	$fecha_hoy = date('Y-m-d');
	$fecha_30 = date('Y-m-d', strtotime( '-14 day' ));
	$labels = array();
	$est0 = array();	
	$cons = "(SELECT adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) AS fecha FROM
 (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t0,
 (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
 (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
 (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
 (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t4) AS dia";
	
	$consulta = ($filter)?"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons."  
 LEFT JOIN (SELECT COALESCE(SUM(valor), 0) AS total, fecha FROM pagos WHERE estado = 1 ".$extrasand." GROUP BY id) AS est0 ON (est0.fecha BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) 
 WHERE dia.fecha BETWEEN '".$filter[ 0 ]."' AND '".$filter[ 1 ]."' GROUP BY dia.fecha":"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons." 
 LEFT JOIN (SELECT COALESCE(SUM(valor), 0) AS total, fecha FROM pagos WHERE estado = 1 ".$extrasand." GROUP BY id) AS est0 ON (est0.fecha BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) WHERE dia.fecha BETWEEN '".$fecha_30."' AND '".$fecha_hoy."' GROUP BY dia.fecha";	
	if($result = $mysqli->query($consulta)){
		while($row = mysqli_fetch_assoc($result)){
			array_push($labels, $row['fecha']);
			array_push($est0, $row['rest0']);						
		}	
		$result->close();
	}
		
	$backgroundColor0 = array();
    $borderColor0 = array();
	for($i = 1; $i <= count($est0); $i++){
		array_push($backgroundColor0, 'rgba(0,183,74,0.4)');
        array_push($borderColor0, 'rgba(0,183,74,1)');
	}	
	
	$datas0 = array(
		'label' => 'Total Pagado',
		'data'=> $est0,
		'backgroundColor' => $backgroundColor0,
        'borderColor' => $borderColor0,
        'borderWidth' => 1
	);	
	
	$datasets = array();
	array_push($datasets, $datas0);	
	
	$data = array('labels' => $labels, 'datasets' => $datasets);
}

if($accion == 2){
	$extrasand = ($rol == 5)?'AND id_user = '.$id_veri[1]:'';
    $fecha = date('Y-m-d');
    $labels = array('Plan Propiedades', 'Contratos');
	$labelsid = array(1, 2);
    $datos = array(array(), array(), array());    
    $label = array('Pendiente', 'Pagado', 'Cancelado');
	foreach ($labelsid as $valor) {
		$consulta2 = ($filter)?"SELECT COALESCE(SUM(valor), 0) AS total, COALESCE(SUM(IF(estado = 0, valor, 0)), 0) AS totalpen, COALESCE(SUM(IF(estado = 1, valor, 0)), 0) AS totalpag, COALESCE(SUM(IF(estado = 2, valor, 0)), 0) AS totalcan FROM pagos WHERE tipo = ".$valor." ".$extrasand." AND fecha BETWEEN '".$filter[ 0 ]." 00:00:00' AND '".$filter[ 1 ]." 23:59:59'":"SELECT COALESCE(SUM(valor), 0) AS total, COALESCE(SUM(IF(estado = 0, valor, 0)), 0) AS totalpen, COALESCE(SUM(IF(estado = 1, valor, 0)), 0) AS totalpag, COALESCE(SUM(IF(estado = 2, valor, 0)), 0) AS totalcan FROM pagos WHERE tipo = ".$valor." ".$extrasand;
        if ( $result2 = $mysqli->query( $consulta2 ) ) {
            while($row2 = $result2->fetch_assoc()){
                array_push($datos[0], $row2['totalpen']);
                array_push($datos[1], $row2['totalpag']);
                array_push($datos[2], $row2['totalcan']);
            }
            $result2->close();                
        }
	}	
    $datasets = array(
        array(
            'label' => $label[0],
            'data' => $datos[0],
            'fill' => true,
            "backgroundColor" => "rgba(255,169,0,0.4)",
            "borderColor" => "rgba(255,169,0,1)",
            "pointBackgroundColor" => "rgba(255,169,0,1)",
            "pointBorderColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => "rgba(255,169,0,1)",
            "borderWidth" => 1
        ),
        array(
            'label' => $label[1],
            'data' => $datos[1],
            'fill' => true,
            "backgroundColor" => "rgba(0,183,74,0.4)",
            "borderColor" => "rgba(0,183,74,1)",
            "pointBackgroundColor" => "rgba(0,183,74,1)",
            "pointBorderColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => "rgba(0,183,74,1)",
            "borderWidth" => 1            
        ),
        array(
            'label' => $label[2],
            'data' => $datos[2],
            'fill' => true,
            "backgroundColor" => "rgba(249,49,84,0.4)",
            "borderColor" => "rgba(249,49,84,1)",
            "pointBackgroundColor" => "rgba(249,49,84,1)",
            "pointBorderColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => "rgba(249,49,84,1)",
            "borderWidth" => 1            
        )
    );
    $data = array('labels' => $labels, 'datasets' => $datasets);
    
}

if($accion == 3){	
	$fecha_hoy = date('Y-m-d');
	$fecha_30 = date('Y-m-d', strtotime( '-14 day' ));
	$labels = array();
	$est0 = array();	
	$cons = "(SELECT adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) AS fecha FROM
 (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t0,
 (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
 (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
 (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
 (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t4) AS dia";
	
	$consulta = ($filter)?"SELECT dia.fecha, COALESCE(COUNT(est0.total), 0) AS rest0 FROM ".$cons."  
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, creado FROM usuarios GROUP BY id) AS est0 ON (est0.creado BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) 
 WHERE dia.fecha BETWEEN '".$filter[ 0 ]."' AND '".$filter[ 1 ]."' GROUP BY dia.fecha":"SELECT dia.fecha, COALESCE(COUNT(est0.total), 0) AS rest0 FROM ".$cons." 
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, creado FROM usuarios GROUP BY id) AS est0 ON (est0.creado BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) WHERE dia.fecha BETWEEN '".$fecha_30."' AND '".$fecha_hoy."' GROUP BY dia.fecha";	
	if($result = $mysqli->query($consulta)){
		while($row = mysqli_fetch_assoc($result)){
			array_push($labels, $row['fecha']);
			array_push($est0, $row['rest0']);						
		}	
		$result->close();
	}
		
	$backgroundColor0 = array();
    $borderColor0 = array();
	for($i = 1; $i <= count($est0); $i++){
		array_push($backgroundColor0, 'rgba(255,169,0,0.4)');
        array_push($borderColor0, 'rgba(255,169,0,1)');
	}	
	
	$datas0 = array(
		'label' => 'Usuarios nuevos',
		'data'=> $est0,
		'backgroundColor' => $backgroundColor0,
        'borderColor' => $borderColor0,
        'borderWidth' => 1
	);	
	
	$datasets = array();
	array_push($datasets, $datas0);	
	
	$data = array('labels' => $labels, 'datasets' => $datasets);
}

if($accion == 4){
    $fecha = date('Y-m-d');
    $labels = array('Activas', 'Cerradas');
    $datos = array(0,0); 
	$consulta = ($filter)?"SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE estado = 0 AND referente = 0 AND id_responsable = ".$id_veri[1]." AND creado BETWEEN '".$filter[ 0 ]." 00:00:00' AND '".$filter[ 1 ]." 23:59:59'":"SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE estado = 0 AND referente = 0 AND id_responsable = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
		$numn = mysqli_fetch_object( $result );        
        $result->close();
		$datos[0] = $numn->total;
    }
	$consulta = ($filter)?"SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE estado > 0 AND referente = 0 AND id_responsable = ".$id_veri[1]." AND creado BETWEEN '".$filter[ 0 ]." 00:00:00' AND '".$filter[ 1 ]." 23:59:59'":"SELECT COALESCE(COUNT(id), 0) AS total FROM novedades WHERE estado > 0 AND referente = 0 AND id_responsable = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
		$numn = mysqli_fetch_object( $result );        
        $result->close();
		$datos[1] = $numn->total;
    }
    $datasets = array(        
        array(
            'label' => 'Novedades',
            'data' => $datos,
            'fill' => true,
            "backgroundColor" => array("rgba(13,202,240,0.4)", "rgba(0,183,74,0.4)"),
            "borderColor" => array("rgba(13,202,240,1)", "rgba(0,183,74,1)"),
            "pointBackgroundColor" => array("rgba(13,202,240,1)", "rgba(0,183,74,1)"),
            "pointBorderColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => array("rgba(13,202,240,1)", "rgba(0,183,74,1)"),
            "borderWidth" => 1            
        )
    );
    $data = array('labels' => $labels, 'datasets' => $datasets);
    
}

if($accion == 5){	
	$fecha_hoy = date('Y-m-d');
	$fecha_30 = date('Y-m-d', strtotime( '-14 day' ));
	$labels = array();
	$est0 = array();	
	$cons = "(SELECT adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) AS fecha FROM
 (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t0,
 (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
 (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
 (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
 (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t4) AS dia";
	
	$consulta = ($filter)?"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons."  
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, fecha FROM pagos WHERE estado = 1 AND id_abogado = ".$id_veri[1]." GROUP BY id_abogado) AS est0 ON (est0.fecha BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) 
 WHERE dia.fecha BETWEEN '".$filter[ 0 ]."' AND '".$filter[ 1 ]."' GROUP BY dia.fecha":"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons." 
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, fecha FROM pagos WHERE estado = 1 AND id_abogado = ".$id_veri[1]." GROUP BY id_abogado) AS est0 ON (est0.fecha BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) WHERE dia.fecha BETWEEN '".$fecha_30."' AND '".$fecha_hoy."' GROUP BY dia.fecha";	
	if($result = $mysqli->query($consulta)){
		while($row = mysqli_fetch_assoc($result)){
			array_push($labels, $row['fecha']);
			array_push($est0, $row['rest0']);						
		}	
		$result->close();
	}
		
	$backgroundColor0 = array();
    $borderColor0 = array();
	for($i = 1; $i <= count($est0); $i++){
		array_push($backgroundColor0, 'rgba(0,183,74,0.4)');
        array_push($borderColor0, 'rgba(0,183,74,1)');
	}	
	
	$datas0 = array(
		'label' => 'Total',
		'data'=> $est0,
		'backgroundColor' => $backgroundColor0,
        'borderColor' => $borderColor0,
        'borderWidth' => 1
	);	
	
	$datasets = array();
	array_push($datasets, $datas0);	
	
	$data = array('labels' => $labels, 'datasets' => $datasets);
}

if($accion == 6){	
	$fecha_hoy = date('Y-m-d');
	$fecha_30 = date('Y-m-d', strtotime( '-14 day' ));
	$arrayProp = array();
	$consulta = "SELECT id FROM propiedades WHERE id_user = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
        while($row = $result->fetch_assoc()){
            array_push($arrayProp, $row['id']);            
        }
        $result->close();                
    }
	$labels = array();
	$est0 = array();	
	$cons = "(SELECT adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) AS fecha FROM
 (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t0,
 (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
 (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
 (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
 (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t4) AS dia";
	
	$consulta = ($filter)?"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons."  
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, fecha FROM ses_user WHERE id_propiedad IN (".implode(',',$arrayProp).") GROUP BY id_propiedad) AS est0 ON (est0.fecha BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) 
 WHERE dia.fecha BETWEEN '".$filter[ 0 ]."' AND '".$filter[ 1 ]."' GROUP BY dia.fecha":"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons." 
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, fecha FROM ses_user WHERE id_propiedad IN (".implode(',',$arrayProp).") GROUP BY id_propiedad) AS est0 ON (est0.fecha BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) WHERE dia.fecha BETWEEN '".$fecha_30."' AND '".$fecha_hoy."' GROUP BY dia.fecha";	
	if($result = $mysqli->query($consulta)){
		while($row = mysqli_fetch_assoc($result)){
			array_push($labels, $row['fecha']);
			array_push($est0, $row['rest0']);						
		}	
		$result->close();
	}
		
	$backgroundColor0 = array();
    $borderColor0 = array();
	for($i = 1; $i <= count($est0); $i++){
		array_push($backgroundColor0, 'rgba(0,183,74,0.4)');
        array_push($borderColor0, 'rgba(0,183,74,1)');
	}	
	
	$datas0 = array(
		'label' => 'Total Vistas',
		'data'=> $est0,
		'backgroundColor' => $backgroundColor0,
        'borderColor' => $borderColor0,
        'borderWidth' => 1
	);	
	
	$datasets = array();
	array_push($datasets, $datas0);	
	
	$data = array('labels' => $labels, 'datasets' => $datasets);
}

if($accion == 7){
    $fecha = date('Y-m-d');
    $labels = array();
    $datos = array(); 
	$arrayProp = array();
	$consulta = "SELECT id FROM propiedades WHERE nombre != '' AND id_user = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
        while($row = $result->fetch_assoc()){
            array_push($arrayProp, $row['id']); 
			//array_push($arrayProp[1], html_entity_decode($row['nombre'], ENT_QUOTES, "UTF-8"));
        }
        $result->close();                
    }
	$consulta = ($filter)?"SELECT COALESCE(COUNT(fav.id), 0) AS total, prop.nombre FROM fav_prop AS fav LEFT JOIN propiedades AS prop ON (prop.id = fav.id_propiedad) WHERE fav.id_propiedad IN (".implode(',',$arrayProp).") AND fav.creado BETWEEN '".$filter[ 0 ]." 00:00:00' AND '".$filter[ 1 ]." 23:59:59' GROUP BY fav.id_propiedad ORDER BY total DESC LIMIT 10":"SELECT COALESCE(COUNT(fav.id), 0) AS total, prop.nombre FROM fav_prop AS fav LEFT JOIN propiedades AS prop ON (prop.id = fav.id_propiedad) WHERE fav.id_propiedad IN (".implode(',',$arrayProp).") GROUP BY fav.id_propiedad ORDER BY total DESC LIMIT 10";
    if ( $result = $mysqli->query( $consulta ) ) {
		while($row = $result->fetch_assoc()){
			array_push($datos, $row['total']);
			array_push($labels, html_entity_decode($row['nombre'], ENT_QUOTES, "UTF-8"));
		}    
        $result->close();
		
    }	
    $datasets = array(        
        array(
            'label' => 'Favoritos',
            'data' => $datos,
            'fill' => true,
            "backgroundColor" => "rgba(255,169,0,0.4)",
            "borderColor" => "rgba(255,169,0,1)",
            "pointBackgroundColor" => "rgba(255,169,0,1)",
            "pointBorderColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => "rgba(255,169,0,1)",
            "borderWidth" => 1            
        )
    );
    $data = array('labels' => $labels, 'datasets' => $datasets);    
}

if($accion == 8){	
	$fecha_hoy = date('Y-m-d');
	$fecha_30 = date('Y-m-d', strtotime( '-14 day' ));
	$arrayProp = array();
	$consulta = "SELECT id FROM propiedades WHERE id_user = ".$id_veri[1];
    if ( $result = $mysqli->query( $consulta ) ) {
        while($row = $result->fetch_assoc()){
            array_push($arrayProp, $row['id']);            
        }
        $result->close();                
    }
	$labels = array();
	$est0 = array();	
	$cons = "(SELECT adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) AS fecha FROM
 (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t0,
 (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
 (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
 (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
 (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t4) AS dia";
	
	$consulta = ($filter)?"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons."  
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, creado FROM novedades WHERE id_propiedad IN (".implode(',',$arrayProp).") AND tipo = 3 GROUP BY id_propiedad) AS est0 ON (est0.creado BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) 
 WHERE dia.fecha BETWEEN '".$filter[ 0 ]."' AND '".$filter[ 1 ]."' GROUP BY dia.fecha":"SELECT dia.fecha, COALESCE(SUM(est0.total), 0) AS rest0 FROM ".$cons." 
 LEFT JOIN (SELECT COALESCE(COUNT(id), 0) AS total, creado FROM novedades WHERE id_propiedad IN (".implode(',',$arrayProp).") AND tipo = 3 GROUP BY id_propiedad) AS est0 ON (est0.creado BETWEEN CONCAT(dia.fecha, ' ', '00:00:00') AND CONCAT(dia.fecha, ' ', '23:59:59')) WHERE dia.fecha BETWEEN '".$fecha_30."' AND '".$fecha_hoy."' GROUP BY dia.fecha";	
	if($result = $mysqli->query($consulta)){
		while($row = mysqli_fetch_assoc($result)){
			array_push($labels, $row['fecha']);
			array_push($est0, $row['rest0']);						
		}	
		$result->close();
	}
		
	$backgroundColor0 = array();
    $borderColor0 = array();
	for($i = 1; $i <= count($est0); $i++){
		array_push($backgroundColor0, 'rgba(141,60,253,0.4)');
        array_push($borderColor0, 'rgba(141,60,253,1)');
	}	
	
	$datas0 = array(
		'label' => 'Total Solicitudes',
		'data'=> $est0,
		'backgroundColor' => $backgroundColor0,
        'borderColor' => $borderColor0,
        'borderWidth' => 1
	);	
	
	$datasets = array();
	array_push($datasets, $datas0);	
	
	$data = array('labels' => $labels, 'datasets' => $datasets);
}

$respuesta = array('data' => $data, 'extras' => $extras);
$mysqli->close();
echo json_encode($respuesta);

function cleanInput( $input ) {
	$search = array(
		'@<script[^>]*?>.*?</script>@si', // Elimina Javascript
		'@<[\/\!]*?[^<>]*?>@si', // Elimina etiquetas HTML
		'@<style[^>]*?>.*?</style>@siU', // Elimina propiedades del style
		'@<![\s\S]*?--[ \t\n\r]*>@' // Elimina comentarios multilínea
	);
	$output = preg_replace( $search, '', $input );
	return $output;
}
function sanitize( $input ) {
	if ( is_array( $input ) ) {
		foreach ( $input as $var => $val ) {
			$output[ $var ] = sanitize( $val );
		}
	} else {
		if ( get_magic_quotes_gpc() ) {
			$input = stripslashes( $input );
		}
		$input = cleanInput( $input );
		$output = $input;
	}
	return $output;
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>