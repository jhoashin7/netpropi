<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
			$espe = ($_POST['tipo'] == 6)?'srv':'abg';
            $filtro = '`admin`.`rol` = '.$_POST['tipo'];
            if(isset($_POST['espe']) && $_POST['espe'] != 0){
                $filtro .= ' AND FIND_IN_SET('.$_POST['espe'].', `usdata`.`especialidad`)';
            }

            // DB table to use
            $table = 'admins';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`admin`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
                array(
                    'db' => '`admin`.`email`',
                    'dt' => 1,
                    'field' => 'email'
                ),
                array(
                    'db' => '`usdata`.`celular`',
                    'dt' => 2,
                    'field' => 'celular'
                ),
                array(
                    'db' => '`usdata`.`pais`',
                    'dt' => 3,
                    'field' => 'pais',
                    'formatter' => function ( $d, $row ){
                        return ($d != 0)?$row[11]:'NA';
                    }
                ),
				array(
                    'db' => 'GROUP_CONCAT(`esp`.`lista` ORDER BY `esp`.`lista` ASC SEPARATOR ", ")',
                    'dt' => 4,
                    'field' => 'esp_lista',
					'as' => 'esp_lista'
                ),
				array(
                    'db' => 'CONCAT(COALESCE(`val`.`valor`, 0), " / ", COALESCE(`val`.`total`, 0), " votos")',
                    'dt' => 5,
                    'field' => 'valoracion_us',
					'as' => 'valoracion_us'
                ),
                array(
                    'db' => '`sesion`.`totalt`',
                    'dt' => 6,
                    'field' => 'totalt',
                    'formatter' => function ( $d, $row ){
                        $numero = (is_null($d))?0:$d;
                        return '<div class="d-grid gap-2"><button type="button" class="btn btn-info btn-sm text-white" onClick="showSes('.$row[10].', \''.$row[0].'\'); return false"><i class="fas fa-user-clock fa-fw"></i> <span class="font-weight-bold">'.$numero.'</span></button></div>';
                    }
                ),	
                array(
                    'db' => '`admin`.`fecha`',
                    'dt' => 7,
                    'field' => 'fecha'
                ),
                array(
                    'db' => '`edt`.`nombre`',
                    'dt' => 8,
                    'field' => 'nombre_edt',
                    'as' => 'nombre_edt'
                ),
                array(
                    'db' => '`admin`.`creado`',
                    'dt' => 9,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`admin`.`id`',
                    'dt' => 10,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) {
                        return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-primary btn-sm text-white" onClick="openData(\'perfilald\', ' . $row[12] . '); return false"><i class="fas fa-user-edit fa-fw"></i> <span class="d-none d-sm-inline">perfil</span></button><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'aliados\', ' . $d . '); return false"><i class="fas fa-edit fa-fw"></i> <span class="d-none d-sm-inline">editar</span></button><button type="button" class="btn btn-danger btn-sm text-white" onClick="delData(' . $d . ', \'admins\', \'id\', \'reLoadTable\', [\'aliados\',false]); return false"><span class="d-none d-sm-inline">eliminar</span> <i class="fas fa-trash fa-fw"></i></button></div>';                        
                    }
                ),	
                array(
                    'db' => '`coun`.`name`',
                    'dt' => 11,
                    'field' => 'nombre_pais',
                    'as' => 'nombre_pais'
                ),	
                array(
                    'db' => '`usdata`.`id`',
                    'dt' => 12,
                    'field' => 'id_usdata',
                    'as' => 'id_usdata'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = "FROM `admins` AS `admin` 
			LEFT JOIN `aliados` AS `usdata` ON (`usdata`.`id` = `admin`.`relacion`) 
            LEFT JOIN (SELECT id, id_user, COUNT(id) AS totalt FROM ses_admin GROUP BY id_user) AS `sesion` ON (`sesion`.`id_user` = `admin`.`id`) 
			LEFT JOIN (SELECT id, nombre AS lista FROM especialidad_".$espe." GROUP BY id) AS `esp` ON (FIND_IN_SET(`esp`.`id`, `usdata`.`especialidad`)) 
			LEFT JOIN (SELECT id_responsable, COUNT(id) AS total, ROUND(AVG(valoracion), 2) AS valor FROM novedades WHERE referente = 0 AND estado > 0 GROUP BY id_responsable) AS `val` ON (`val`.`id_responsable` = `admin`.`id`) 
			LEFT JOIN `countries` AS `coun` ON (`coun`.`id` = `usdata`.`pais`) 
            LEFT JOIN `admins` AS `edt` ON (`edt`.`id` = `admin`.`editor`)";

            $extraWhere = $filtro;
			$groupBy = '`admin`.`id`';
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>