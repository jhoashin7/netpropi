<?php
header('X-Frame-Options: Deny');
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if( !isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'])['host'] != $_SERVER['HTTP_HOST'] ){
		exit("Not allowed - Unknown host request! ");
	}else{
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */
        include_once 'con_set.php';
        $id_veri = explode('***', simple_crypt( $_POST['user'], 'd', $conArr['enc_string'] ));
        if($id_veri[0] == $conArr['enc_string'] && is_numeric($id_veri[1])){
			$rol = $_POST['rol'];
            $filtro = ($rol != 5)?'`nov`.`tipo` <= 2 AND `nov`.`id_responsable` = '.$id_veri[1].' AND `nov`.`referente` = 0':'`nov`.`tipo` <= 2 AND `nov`.`id_user` = '.$id_veri[1].' AND `nov`.`referente` = 0';            

            // DB table to use
            $table = 'novedades';

            // Table's primary key
            $primaryKey = 'id';

            $columns = array(
                array(
                    'db' => '`nov`.`nombre`',
                    'dt' => 0,
                    'field' => 'nombre'
                ),
				array(
                    'db' => '`nov`.`id_user`',
                    'dt' => 1,
                    'field' => 'id_user',
                    'formatter' => function ( $d, $row ) {
						return ($d != 0)?$row[10]:'NA';
					}
                ),
				array(
                    'db' => '`nov`.`id_responsable`',
                    'dt' => 2,
                    'field' => 'id_responsable',
                    'formatter' => function ( $d, $row ) {
						return ($d != 0)?$row[11]:'NA';
					}
                ),
				array(
                    'db' => 'LPAD(`nov`.`id_propiedad`,6,0)',
                    'dt' => 3,
                    'field' => 'codigo',
					'as' => 'codigo',
                    'formatter' => function ( $d, $row ) {
						return ($row[12] == 1)?$d:'NA';
					}
                ),
				array(
                    'db' => '`nov`.`estado`',
                    'dt' => 4,
                    'field' => 'estado',
                    'formatter' => function ( $d, $row ) {
						return ($d == 0)?'<span class="text-warning fw-bold">Abierta</span>':'<span class="text-success fw-bold">Cerrada</span>';
					}
                ),
				array(
                    'db' => 'IF(`nov`.`estado` = 1, 0, IF(`nov`.`vista` = 0, COALESCE(`res`.`total`, 0) + 1, COALESCE(`res`.`total`, 0)))',
                    'dt' => 5,
                    'field' => 'res_total',
					'as' => 'res_total',
                    'formatter' => function ( $d, $row ) {
						return ($d == 0)?'<span class="text-success fw-bold">'.$d.'</span>':'<span class="text-danger fw-bold">'.$d.'</span>';
					}
                ),
				array(
                    'db' => '`nov`.`valoracion`',
                    'dt' => 6,
                    'field' => 'valoracion',
                    'formatter' => function ( $d, $row ) {
						return ($row[4] != 0 && $d != 0)?'<span class="fw-bold">'.$d.'</span>':'';
					}
                ),
                array(
                    'db' => 'IF(`nov`.`estado` = 1, `nov`.`fin`, "")',
                    'dt' => 7,
                    'field' => 'final',
					'as' => 'final'
                ),                
                array(
                    'db' => '`nov`.`creado`',
                    'dt' => 8,
                    'field' => 'creado'
                ),	
                array(
                    'db' => '`nov`.`id`',
                    'dt' => 9,
                    'field' => 'id',
                    'formatter' => function ( $d, $row ) use ($rol) { 
						if($rol == 5){
							if($row[4] > 0){
								return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-primary btn-sm text-white" onClick="showLogs(' . $d . '); return false"><span class="d-none d-sm-inline">registros</span> <i class="fas fa-comment-dots fa-fw"></i></button></div>';
							}else{
								return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-primary btn-sm text-white" onClick="showLogs(' . $d . '); return false"><i class="fas fa-comment-dots fa-fw"></i> <span class="d-none d-sm-inline">registros</span></button><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'novedadesFin\', ' . $d . '); return false"><i class="fas fa-check-double fa-fw"></i> <span class="d-none d-sm-inline">finalizar</span></button><button type="button" class="btn btn-danger btn-sm text-white" onClick="delData(' . $d . ', \'novedades\', \'id\', \'reLoadTable\', [\'novedades\', false]); return false"><span class="d-none d-sm-inline">eliminar</span> <i class="fas fa-trash fa-fw"></i></button></div>';
							}							
						}else{
							if($row[4] > 0){
								return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-primary btn-sm text-white" onClick="showLogs(' . $d . '); return false"><i class="fas fa-comment-dots fa-fw"></i> <span class="d-none d-sm-inline">registros</span></button><button type="button" class="btn btn-warning btn-sm text-white" onClick="openData(\'novedades\', ' . $d . '); return false"><span class="d-none d-sm-inline">comentario final</span> <i class="fas fa-user-check fa-fw"></i></button></div>';
							}else{
								return '<div class="btn-group d-flex w-100" role="group" aria-label="Acciones"><button type="button" class="btn btn-primary btn-sm text-white" onClick="showLogs(' . $d . '); return false"><span class="d-none d-sm-inline">registros</span> <i class="fas fa-comment-dots fa-fw"></i></button></div>';
							}							
						}                   
                    }
                ),
				array(
                    'db' => '`user`.`nombre`',
                    'dt' => 10,
                    'field' => 'nombre_user',
                    'as' => 'nombre_user'
                ),
				array(
                    'db' => '`resp`.`nombre`',
                    'dt' => 11,
                    'field' => 'nombre_resp',
                    'as' => 'nombre_resp'
                ),                
                array(
                    'db' => '`nov`.`tipo`',
                    'dt' => 12,
                    'field' => 'tipo'
                )
            );

            // SQL server connection information

            $sql_details = array(
                'user' => $conArr['conus'],
                'pass' => $conArr['conpass'],
                'db' => $conArr['condb'],
                'host' => $conArr['conser'],
                'port' => $conArr['conport']
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            require( 'ssp.customized.class.php' );


            $joinQuery = ($rol != 5)?"FROM `novedades` AS `nov` 
			LEFT JOIN (SELECT referente, COUNT(id) AS total FROM novedades WHERE vista = 0 AND id_user = editor GROUP BY referente) AS `res` ON (`res`.`referente` = `nov`.`id`) 
			LEFT JOIN `admins` AS `user` ON (`user`.`id` = `nov`.`id_user`) 
			LEFT JOIN `admins` AS `resp` ON (`resp`.`id` = `nov`.`id_responsable`)":"FROM `novedades` AS `nov` 
			LEFT JOIN (SELECT referente, COUNT(id) AS total FROM novedades WHERE vista = 0 AND id_user != editor GROUP BY referente) AS `res` ON (`res`.`referente` = `nov`.`id`) 
			LEFT JOIN `admins` AS `user` ON (`user`.`id` = `nov`.`id_user`) 
			LEFT JOIN `admins` AS `resp` ON (`resp`.`id` = `nov`.`id_responsable`)";

            $extraWhere = $filtro;			
            echo json_encode(
                SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
            );
            
        }else{
           echo 'intento inseguro'; 
        }
    }	
}else{
	echo 'intento inseguro';
}
function simple_crypt( $string, $action = 'e', $llave ) {    
    $secret_key = $llave;
    $secret_iv = $llave;
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}
?>