<?php 
    require 'vendor/autoload.php';
    // MercadoPago\SDK::setAccessToken('TEST-933423679631568-020719-162042be6d6d2af73be4d18dd8886c39-775311326');
    MercadoPago\SDK::setAccessToken('APP_USR-933423679631568-020719-34e8359e73fe8246ed42b7e421fe7278-775311326');

    $preference = new MercadoPago\preference();
    $item = new MercadoPago\Item();
    $payer= new MercadoPago\Payer();

    $item->id = '0001';
    $item->title =  "Point Mini";
    // $item->description =  "Producto Point para cobros con tarjetas mediante bluetooth";
    $item->category_id =  "electronics";
    $item->quantity =  1;
    $item->unit_price =  150000;
    $item->currency_id = "COP";
    
    $payer->name = "Netpropi";
    $payer->surname = "user-surname";
   	$payer->email = "user@email.com";
   	$payer->date_created = "2015-06-02T12:58:41.425-04:00";

    $preference->items = array($item);
    $preference->payer = $payer;
    $preference->save();
?>

<!DOCTYPE html>
<html>
<head>
    <script src="https://sdk.mercadopago.com/js/v2"></script>
</head>
<body>
<!-- <img src="dinosaur.jpg"> -->
    <div class="checkout-btn"></div>
    <script>
        // const mp = new MercadoPago('TEST-c27357d7-5c7f-4576-9da7-9fb0810b976b', {
        const mp = new MercadoPago('APP_USR-dd431324-5514-438b-b8bb-14726c7f5347', {
            locale: 'es-CO'
        })

        const checkout = mp.checkout({
                preference: {
                    id: '<?php echo $preference->id?>'
                },
                render: {
                    container: '.checkout-btn',
                    type: 'wallet',
                    label: 'Mercado pago'
                }
            });
    </script>
</body>
</html>